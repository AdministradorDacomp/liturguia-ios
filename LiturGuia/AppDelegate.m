//
//  AppDelegate.m
//  LiturGuia
//
//  Created by Krloz on 08/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import <Parse/PFFacebookUtils.h>
#import "BibleEntity.h"
#import "SQLiteDBManager.h"
#import "PrefaceEntity.h"
#import "RosaryEntity.h"
#import "IAPSubscriptions.h"
#import "Appirater.h"
#import "CalendarEntity.h"
#import "NSDate+Utils.h"
#import "CalendarGenerator.h"
#import <Parse/PFTwitterUtils.h>
#import "DailyImageViewController.h"
#include "TargetConditionals.h"
#import "DailyMeditationViewController.h"
#import <IQKeyboardManagerSwift/IQKeyboardManagerSwift-Swift.h>
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseDynamicLinks/FirebaseDynamicLinks.h>



@interface AppDelegate (){
    BOOL mayorUpdateAlertShowed;
}

@end

@implementation AppDelegate


@synthesize sectionsViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
    [[UINavigationBar appearance] setBarTintColor:kNavigationBarColor];
    if (@available(iOS 13, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithOpaqueBackground];
        appearance.backgroundColor = kNavigationBarColor;
        appearance.titleTextAttributes =  [NSDictionary dictionaryWithObjectsAndKeys:
                                                               kNavigationBarTextColor, NSForegroundColorAttributeName,
                                           [UIFont fontWithName:@"HelveticaNeue" size:18.0], NSFontAttributeName, nil];
        
        
        UINavigationBar.appearance.standardAppearance = appearance;
        UINavigationBar.appearance.scrollEdgeAppearance = appearance;
        UINavigationBar.appearance.compactAppearance = appearance;
    } else {
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                               kNavigationBarTextColor, NSForegroundColorAttributeName,
                                                               [UIFont fontWithName:@"HelveticaNeue" size:18.0], NSFontAttributeName, nil]];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults boolForKey:@"PreloadedDBCopied"]){
        [self copyPreloadedDB];
    }
    if (![userDefaults stringForKey:@"language"]){
        [userDefaults setObject:@"es" forKey:@"language"];
    }
    [userDefaults setBool:false forKey:@"inicioApple"];
    
    [GIDSignIn sharedInstance].clientID = @"383783661561-c2fjn70n7q2f39i9umf7nm7i5f0t2rmm.apps.googleusercontent.com";
    
    // Initialize Parse.
    // [Parse setApplicationId:@"rfHtNnFQ58Ssgixn81ryrx6COAEjQyia4fMK4Zc8"
    //               clientKey:@"7oNJ8lpW5odCnOTymGZVr5I5gCayse9VUYVXxiTX"];
    
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
        configuration.applicationId = @"rfHtNnFQ58Ssgixn81ryrx6COAEjQyia4fMK4Zc8";
        configuration.clientKey = @"X";
        configuration.server = @"http://104.131.189.22:1337/parse";
    }]];
    
    [PFTwitterUtils initializeWithConsumerKey:@"PKSeoufZYaaOV7myfbMuSRWcb" consumerSecret:@"ISggY5EumsixMZbDPFNdtJwH4wZ4KxaGSN17xArAbjsxfx6ykV"];
    
    
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    //[PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions];
    
    
    
#if !(TARGET_OS_SIMULATOR)
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes  categories:nil];
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:settings];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        //        [application registerForRemoteNotificationTypes:
        //         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
#endif
    
    
    
    [IAPSubscriptions sharedInstance];
    
    if ([LiturGuia internetConnected]) {
        [self fetchConfig];
    }
    
    application.applicationIconBadgeNumber = 0;
    
    [Appirater setAppId:@"996234205"];
    [Appirater setDaysUntilPrompt:5];
    [Appirater setUsesUntilPrompt:5];
    [Appirater setSignificantEventsUntilPrompt:5];
    [Appirater setTimeBeforeReminding:7];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
    
    // Extract the notification data
    NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    
    [self handleNotificationWithUserInfo:notificationPayload];
    
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    //    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    //    GAI *gai = [GAI sharedInstance];
    //    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    
    //    if (development) {
    //        gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    //    }
    
    [self configureProgressView];
    
    [[IQKeyboardManager shared] setEnable:YES];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}


-(void)configureProgressView{
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    
    configuration.statusColor = kNavigationBarColor;
    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
    configuration.circleStrokeForegroundColor = kNavigationBarColor;
    configuration.circleStrokeBackgroundColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
    configuration.circleFillBackgroundColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
    configuration.backgroundFillColor = [UIColor colorWithWhite:1.0f alpha:0.8f];
    configuration.backgroundTintColor = [UIColor colorWithWhite:1.0f alpha:0.8f];
    configuration.successColor = kNavigationBarColor;
    configuration.errorColor = [UIColor redColor];
    configuration.stopColor = [UIColor whiteColor];
    //    configuration.circleSize = 110.0f;
    configuration.lineWidth = 2.0f;
    configuration.fullScreen = NO;
    configuration.minimumDisplayTime = 1.0;
    configuration.minimumSuccessDisplayTime = 1.0;
    configuration.minimumErrorDisplayTime = 3.0;
    //    configuration.showStop = YES;
    //    configuration.stopRelativeHeight = 0.4f;
    
    //    configuration.tapBlock = ^(KVNProgress *progressView) {
    //        // Do something you want to do when the user tap on the HUD
    //        // Does nothing by default
    //    };
    
    // You can allow user interaction for behind views but you will losse the tapBlock functionnality just above
    // Does not work with fullscreen mode
    // Default is NO
    configuration.allowUserInteraction = NO;
    
    [KVNProgress setConfiguration:configuration];
}

//void globalExceptionHandler(NSException *exception)
//{
//    NSDictionary *data = @{@"debugDescription":exception.debugDescription, @"description":exception.description, @"trace":exception.callStackSymbols};
//
//    [Bugsnag notify:[NSException exceptionWithName:exception.name reason:exception.reason userInfo:nil] withData:data atSeverity:@"error"];
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[ConscienceManager shared] checkInConscienceSection];
    
    //    UIImageView *imageView = [[UIImageView alloc]initWithFrame:[self.window frame]];
    //    [imageView setImage:[UIImage imageNamed:@"LaunchImage"]];
    //    [self.window addSubview:imageView];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Appirater appEnteredForeground:YES];
}
//
//- (void)applicationDidBecomeActive:(UIApplication *)application {
//    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current Installation and save it to Parse
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    
    NSLog(@"Failed to get token, error: %@", error);
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.dacompsc.LiturGuia" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LiturGuia" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LiturGuia.sqlite"];
    
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}




- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark -  Preloaded database

- (void)copyPreloadedDB{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Preloaded"];
    [LiturGuia addSkipBackupAttributeToItemAtPath:documentsDirectory];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"DB"];
    
    NSError *error;
    
    if([[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:folderPath error:&error]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PreloadedDBCopied"];
        [[NSUserDefaults standardUserDefaults] setObject:@"HelveticaNeue" forKey:@"fontName"];
        [[NSUserDefaults standardUserDefaults] setObject:@"Helvetica Neue" forKey:@"fontDisplayName"];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"HelveticaNeue" forKey:@"fontDisplayNameConscience"];
        [[NSUserDefaults standardUserDefaults] setFloat:12. forKey:@"fontSizeConscience"];
        
        
        [[NSUserDefaults standardUserDefaults] setFloat:12. forKey:@"fontSize"];
        [[NSUserDefaults standardUserDefaults] setObject:@"white" forKey:@"TextStyle"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        NSEntityDescription *entity =
        [NSEntityDescription entityForName:@"BibleEntity"
                    inManagedObjectContext:[self managedObjectContext]];
        
        BibleEntity *bible = [[BibleEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
        
        bible.bibleDescription = @"Biblia español";
        bible.selected = @YES;
        bible.language = @"es";
        bible.fileName = @"biblia_esp.db";
        
        [self saveContext];
        
        
        NSString *query = @"SELECT l.nombre AS libro, c.orden AS orden, id_libro, id_grupo, id_capitulo, v.id AS id FROM capitulo c, libro l, versiculo v WHERE v.id_capitulo = c.id AND c.id_libro = l.id LIMIT 1";
        
        NSMutableArray *result = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:nil];
        
        if (result.count > 0) {
            NSDictionary *dic = result[0];
            
            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"id_capitulo"] intValue] forKey:@"chapterId"];
            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"id"] intValue] forKey:@"verseId"];
            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"id_libro"] integerValue] forKey:@"bookId"];
            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"id_grupo"] integerValue] forKey:@"groupId"];
            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"orden"] integerValue] forKey:@"chapterOrder"];
            [[NSUserDefaults standardUserDefaults] setObject:dic[@"libro"] forKey:@"bookName"];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:kDefaultTrialTime forKey:@"trialTime"];
        //        [[NSUserDefaults standardUserDefaults] setObject:kDefaultFreeCredits forKey:@"freeCredits"];
        //        [[NSUserDefaults standardUserDefaults] setObject:kDefaultWeekMissalPrice forKey:@"weekMissalPrice"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"Preloaded databases were copied successfully");
        
        
        NSString *cssStyleFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Missal HTML/css"];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
        if([[NSFileManager defaultManager] copyItemAtPath:cssStyleFile toPath:folderPath error:&error]){
            if (!error) {
                NSLog(@"Preloaded styles were copied successfully");
            }else {
                NSLog(@"Error description-%@ \n", [error localizedDescription]);
                NSLog(@"Error reason-%@", [error localizedFailureReason]);
            }
        }
        //        [LiturGuia addSkipBackupAttributeToItemAtPath:folderPath];
        
        
        //---------------     PREFACIOS     ---------------------
        NSString *mainFile = [[NSBundle mainBundle] pathForResource:@"/Preloaded/prefacios" ofType:@"json"];
        NSString *jsonString = [NSString stringWithContentsOfFile:mainFile encoding:NSUTF8StringEncoding error:nil];
        
        NSData* data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        for (NSDictionary *dic in array) {
            
            entity =
            [NSEntityDescription entityForName:@"PrefaceEntity"
                        inManagedObjectContext:[self managedObjectContext]];
            
            PrefaceEntity *preface = [[PrefaceEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
            
            preface.title = dic[@"titulo"];
            preface.subtitle = dic[@"subtitulo"];
            preface.text = dic[@"texto"];
            preface.type = dic[@"tipo"];
            preface.language = dic[@"idioma"];
            preface.shortText = dic[@"resumen"];
            
            [self saveContext];
        }
        
        //----------------     MISTERIOS        ---------------
        mainFile = [[NSBundle mainBundle] pathForResource:@"/Preloaded/misterios" ofType:@"json"];
        jsonString = [NSString stringWithContentsOfFile:mainFile encoding:NSUTF8StringEncoding error:nil];
        
        data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        for (NSDictionary *dic in array) {
            
            entity =
            [NSEntityDescription entityForName:@"RosaryEntity"
                        inManagedObjectContext:[self managedObjectContext]];
            
            RosaryEntity *rosary = [[RosaryEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
            
            rosary.title = dic[@"titulo"];
            rosary.subtitle = dic[@"subtitulo"];
            rosary.number = dic[@"numero"];
            rosary.type = dic[@"tipo"];
            rosary.language = dic[@"idioma"];
            
            [self saveContext];
        }
        
    } else {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
    }
}

-(void)generateLiturgyCalendar{
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //    [formatter setDateFormat:@"dd/MM/yyyy"];
    //
    //    NSDate *initialDate = [formatter dateFromString:@"01/01/1950"];
    //    NSDate *endDate = [formatter dateFromString:@"01/01/2050"];
    //
    //    while ([initialDate isEarlierThan:endDate]) {
    //        CalendarGenerator *object = [CalendarGenerator sharedInstance];
    //        NSDictionary *info = [object liturgyInfoForDate:initialDate];
    //
    //        NSEntityDescription *entity =
    //        [NSEntityDescription entityForName:@"CalendarEntity"
    //                    inManagedObjectContext:[self managedObjectContext]];
    //
    //        CalendarEntity *calendar= [[CalendarEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[self managedObjectContext]];
    //
    //        calendar.date = info[@"fecha"];
    //        calendar.week = info[@"semana"];
    //        calendar.time = info[@"tiempo"];
    //        calendar.day = @([calendar.date dayOfTheWeek]);
    //        calendar.cicle = @"C";
    //
    //        if (info[@"especial"]) {
    //            calendar.specialDate = info[@"especial"];
    //        }
    //
    //        initialDate = [initialDate dateByAddingTimeInterval:(24*60*60)];
    //    }
    //
    //    [LiturGuia saveContext];
    //
    //
    //    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"LiturgyCalendarGenerated"];
    //
    //    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Facebook
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]
    || [[GIDSignIn sharedInstance] handleURL:url
                           sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                  annotation:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
    ] ||
    [[GIDSignIn sharedInstance] handleURL:url
                        sourceApplication:sourceApplication
                               annotation:annotation];
}

- (BOOL)application:(UIApplication *)application
continueUserActivity:(nonnull NSUserActivity *)userActivity
 restorationHandler:
#if defined(__IPHONE_12_0) && (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_12_0)
(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> *_Nullable))restorationHandler {
#else
    (nonnull void (^)(NSArray *_Nullable))restorationHandler {
#endif  // __IPHONE_12_0
        NSLog(@"%@", userActivity.webpageURL);
        BOOL handled = [[FIRDynamicLinks dynamicLinks] handleUniversalLink:userActivity.webpageURL
                                                                completion:^(FIRDynamicLink * _Nullable dynamicLink,
                                                                             NSError * _Nullable error) {
            [DynamicLinksManager handleWithDynamicLink:dynamicLink];
        }];
        return handled;
    }
    //- (BOOL)application:(UIApplication *)application
    //            openURL:(NSURL *)url
    //  sourceApplication:(NSString *)sourceApplication
    //         annotation:(id)annotation {
    //    if ([[url absoluteString] containsString:@"google"]) {
    ////        NSDictionary *options = @{UIApplicationOpenURLOptionsSourceApplicationKey: sourceApplication,
    ////                                  UIApplicationOpenURLOptionsAnnotationKey: annotation};
    //        return [self application:application
    //                         openURL:url
    //                         options:nil];
    //    }else{
    //    return [[FBSDKApplicationDelegate sharedInstance] application:application
    //                                                          openURL:url
    //                                                sourceApplication:sourceApplication
    //                                                       annotation:annotation];
    //    }
    //}
    //
    //- (BOOL)application:(UIApplication *)app
    //            openURL:(NSURL *)url
    //            options:(NSDictionary *)options {
    //    return [[GIDSignIn sharedInstance] handleURL:url
    //                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
    //                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    //}
    
    - (void)applicationDidBecomeActive:(UIApplication *)application {
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.00 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [FBSDKAppEvents activateApp];
            
            PFInstallation *currentInstallation = [PFInstallation currentInstallation];
            if (currentInstallation.badge != 0) {
                currentInstallation.badge = 0;
                [currentInstallation saveEventually];
            }
            
            
            PFConfig *config = [PFConfig currentConfig];
            
            long long lastMayorVersion = [config[@"ultima_version_mayor_ios"] longLongValue];
            long long localVersion  = [[self version] longLongValue];
            if (localVersion < lastMayorVersion && !development) {
                [self showAlertMayorUpdate];
            }
            
            
        });
        
        
        
    }
    
    
    
    -(void)fetchConfig{
        NSLog(@"Getting the lastest config...");
        //    PFConfig *previusConfig = [PFConfig currentConfig];
        //    long previousLastMayorVersion = [previusConfig[@"ultima_version_mayor_ios"] longLongValue];
        [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
            if (error){
                config = [PFConfig currentConfig];
            }
            
            long long lastVersion = [config[@"ultima_version_ios"] longLongValue];
            long long lastMayorVersion = [config[@"ultima_version_mayor_ios"] longLongValue];
            long long localVersion  = [[self version] longLongValue];
            if (localVersion < lastMayorVersion && !self->mayorUpdateAlertShowed && !development) {
                [self showAlertMayorUpdate];
            }else if(localVersion < lastVersion && !self->mayorUpdateAlertShowed && !development){
                NSString *messageErr = @"¡Hay una nueva actualización de LiturGuía!\n ¿Desea descargar esta versión?";
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:messageErr
                                                            delegate:self
                                                   cancelButtonTitle:@"Ahora no"
                                                   otherButtonTitles:@"Descargar", nil];
                [av show];
            }
            if (config[@"tiempo_de_prueba"] && config[@"tiempo_de_prueba"] != [NSNull null]) {
                [[NSUserDefaults standardUserDefaults] setObject:config[@"tiempo_de_prueba"] forKey:@"trialTime"];
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:kDefaultTrialTime forKey:@"trialTime"];
            }
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
        }];
        
    }
    
    -(void)showAlertMayorUpdate{
        mayorUpdateAlertShowed = YES;
        NSString *messageErr = @"¡Hay una nueva actualización obligatoria de LiturGuía!\n Para seguir disfrutando de la aplicación es necesario actualizarla.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:self
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"Actualizar", nil];
        [av show];
    }
    
    -(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (alertView.cancelButtonIndex != buttonIndex) {
            NSString *iTunesLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id996234205?mt=8";
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
    }
    
    - (NSString*)version {
        //    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        return [NSString stringWithFormat:@"%@", build];
    }
    
    - (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
        [PFPush handlePush:userInfo];
        
        [self handleNotificationWithUserInfo:userInfo];
    }
    
    -(void)handleNotificationWithUserInfo:(NSDictionary*)userInfo{
        
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            NSString *type = userInfo[@"type"];
            if ([type isEqualToString:@"imagen_diaria"]) {
                NSString *objectId = userInfo[@"objectId"];
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Left menu"
                                                             bundle:[NSBundle mainBundle]];
                DailyImageViewController *vc = [sb instantiateViewControllerWithIdentifier:@"DailyImageViewController"];
                vc.objectId = objectId;
                
                UIViewController *presentationVC = [UIApplication sharedApplication].keyWindow.rootViewController;
                presentationVC.definesPresentationContext = YES;
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                [presentationVC presentViewController:vc animated:YES completion:nil];
            }
            if ([type isEqualToString:@"meditacion_del_dia"]) {
                //                NSString *objectId = userInfo[@"objectId"];
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Left menu"
                                                             bundle:[NSBundle mainBundle]];
                
                DailyMeditationViewController *nc = [sb instantiateViewControllerWithIdentifier:@"DailyMeditationNavigationController"];
                
                UIViewController *presentationVC = [UIApplication sharedApplication].keyWindow.rootViewController;
                presentationVC.definesPresentationContext = YES;
                nc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                [presentationVC presentViewController:nc animated:YES completion:nil];
            }
            
        });
    }
    
    @end
