//
//  CloudFunctionManager.swift
//  LiturGuia
//
//  Created by Daniel Marrufo rivera on 30/11/23.
//  Copyright © 2023 DAComp SC. All rights reserved.
//

import UIKit
import Alamofire
class CloudFunctionManager: NSObject {
    
    static func request(_ path: String, body:[String:Any]?, methodPOST:Bool = true, completion:((_ result:[String:Any]?, _ error:Error?) -> Void)?){
        let urlString = cloudFunctionsBaseURL + path
        
                Alamofire.request(
                    urlString,
                    method: methodPOST ? HTTPMethod.post :  HTTPMethod.get ,
                    parameters: body,
                    encoding: JSONEncoding.default,
                    headers: nil).responseJSON{ (response) in
                        if let error = response.result.error{
                            completion?(nil, error)
                        }else if let data = response.result.value as? [String:Any]{
                            debugPrint(data)
                            if let status = data["estatus"] as? String,
                                status == "error",
                                let mensaje = data["mensaje"] as? String{
                                let userInfo = [
                                    NSLocalizedDescriptionKey: mensaje,
                                    NSLocalizedFailureReasonErrorKey: mensaje
                                ]
                                var code = 0
                                if let stringCode = data["codigo"] as? String{
                                    code = Int(stringCode) ?? 0
                                }
                                let error = NSError(domain: "com.dacompsc.IDue", code: code, userInfo: userInfo)
                                
                                completion?(data, error)
                                
                            }else{
                                completion?(data, nil)
                            }
                        }else{
                            completion?(nil, NSError() as Error)
                        }
                }
    }
}
