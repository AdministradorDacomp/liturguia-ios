//
//  LiturguiaFacebookAuthDelegate.swift
//  LiturGuia
//
//  Created by Daniel Marrufo Rivera on 05/07/21.
//  Copyright © 2021 DAComp SC. All rights reserved.
//

import Foundation
import UIKit
import Parse
import AuthenticationServices
import FBSDKLoginKit

@objc class LiturGuiaFacebookAuthDelegate: NSObject {
    @objc static var shared: LiturGuiaFacebookAuthDelegate = {
        let instance = LiturGuiaFacebookAuthDelegate()
        return instance
    }()
    @objc var delegate: LiturguiaAuthProtocol?
    
    // Unhashed nonce.
    fileprivate var currentNonce: String?
    
    @objc func startSignInFlow() {
        var viewController:UIViewController!
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["email", "public_profile"], from: viewController) { (result, error) -> Void in
            if((AccessToken.current) != nil){
                GraphRequest(
                    graphPath: "me",
                    parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, birthday, gender"
                    ]).start(completionHandler: { (connection, result, error) -> Void in
                        
                        if let error = error{
                            self.delegate?.authenticationFlowComplete(error, isNewUser: false)
                        }else if let resultDic = result as? NSDictionary,
                           let name = resultDic["name"] as? String,
                           let email = resultDic["email"] as? String,
                           let user = resultDic["id"] as? String,
                           let tokenString = AccessToken.current?.tokenString{
                            
                            let first_name = resultDic["first_name"] as? String ?? "Usuario"
                            let last_name = resultDic["last_name"] as? String ?? "Anónimo"
                            print("AUTHORIZED: \(name)")
                            print("AUTHORIZED: \(AccessToken.current?.userID)")
                            
                            //let tokenString = String(data: token, encoding: .utf8)!
                            PFUser.register(self, forAuthType: "Facebook")
                            print(user)
                            PFUser.logInWithAuthType(inBackground: "facebook", authData: ["token":tokenString, "id": user]).continueWith { task -> Any? in
                                if let userObject = task.result {
                                    if userObject.isNew {
                                        userObject["email"] = email
                                        userObject["nombre"] = first_name
                                        userObject["apellido"] = last_name
                                        userObject["facebookId"] = user
                                    }
                                    userObject.saveInBackground { (success, error) in
                                        if let error = error {
                                            self.delegate?.authenticationFlowComplete(error, isNewUser: false)
                                        } else {
                                            self.delegate?.authenticationFlowComplete(nil, isNewUser: userObject.isNew)
                                        }
                                    }
                                } else {
                                   self.delegate?.authenticationFlowComplete(task.error, isNewUser: false)
                                }
                                return nil
                            }
                            
                        }
                 })
            }
        }
    }
}

extension LiturGuiaFacebookAuthDelegate: PFUserAuthenticationDelegate {
    func restoreAuthentication(withAuthData authData: [String : String]?) -> Bool {
        return true
    }
}
