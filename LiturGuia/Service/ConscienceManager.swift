//
//  ConscienceManager.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/21/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import Parse

class ConscienceManager: NSObject {
    @objc static let shared : ConscienceManager = {
        let instance = ConscienceManager()
        return instance
    }()
    
    var conscienceExams = [[String:Any]]()
    var conscienceNVC:UIViewController? = nil
    
    func getConscienceExams(completion:((_ conscienceExams:[[String:Any]]?, _ error:Error?) -> Void)?){
        if conscienceExams.count > 0{
            completion?(conscienceExams, nil)
        }else{
            let query = PFQuery(className: "PreguntaEC")
            query.includeKey("grupoEC")
            query.includeKey("grupoEC.examenConciencia")
                query.limit = 1000
            query.findObjectsInBackground {[weak self] (objects, error) in
                guard let strongSelf = self else { return }
                if let error = error{
                    completion?(nil, error)
                }else if let objects = objects{
                    var exams = [String:[String:Any]]()



                    for parseQuestion in objects{

                        if let grupoEC = parseQuestion["grupoEC"] as? PFObject,
                            let examenConciencia = grupoEC["examenConciencia"] as? PFObject,
                            let titleCE = examenConciencia["titulo"] as? String,
                            let titleGroup = grupoEC["titulo"] as? String,
                            let examId = examenConciencia.objectId,
                            let groupId = grupoEC.objectId,
                            let questionId = parseQuestion.objectId,
                            let groupOrder = grupoEC["orden"] as? Int,
                            let ceOrder = examenConciencia["orden"] as? Int,
                            let text = parseQuestion["texto"] as? String,
                            let questionOrder = parseQuestion["orden"] as? Int{
                            let subtitleGroup = grupoEC["subtitulo"] as? String ?? ""

                            var exam:[String:Any] = [:]
                            if exams[examId] == nil{
                                exam["title"] = titleCE
                                exam["order"] = ceOrder
                                exam["groups"] = [String:Any]()
                                exam["id"] = examId
                            }else{
                                exam = exams[examId]!
                            }
                            
                            var groups = exam["groups"] as! [String:Any]
                            var group:[String:Any] = [:]
                            if groups[groupId] == nil{
                                group["title"] = titleGroup
                                group["subtitle"] = subtitleGroup
                                group["order"] = groupOrder
                                group["id"] = groupId
                                group["questions"] = [String:Any]()
                            }else{
                                group = groups[groupId] as! [String : Any]
                            }
                            
                            var questions = group["questions"] as! [String:Any]
                            var question:[String:Any] = [:]
                            if questions[questionId] == nil{
                                question["text"] = text
                                question["order"] = questionOrder
                                question["id"] = questionId
                            }else{
                                question = questions[questionId] as! [String : Any]
                            }
                            
                            questions[questionId] = question
                            group["questions"] = questions
                            groups[groupId] = group
                            exam["groups"] = groups
                            exams[examId] = exam
                        }

                    }
                    
                    for (examKey, var exam) in exams{
                        if var groups = exam["groups"] as? [String:[String:Any]]{
                            for (groupKey, var group) in groups{
                                if let questions = group["questions"] as? [String:Any]{
                                    group["questions"] = questions.toSortedArrayByKey("order")
                                    groups[groupKey] = group
                                }
                            }
                            exam["groups"] = groups.toSortedArrayByKey("order")
                            exams[examKey] = exam
                        }
                    }
                    
                    strongSelf.conscienceExams = exams.toSortedArrayByKey("order")
                    print(String(data: try! JSONSerialization.data(withJSONObject: strongSelf.conscienceExams, options: .prettyPrinted), encoding: .utf8 )!)
                    
                    completion?(strongSelf.conscienceExams, nil)
                }else{
                    completion?(nil, nil)
                }
            }
            
        }
    }
    
    @objc func checkInConscienceSection() {
        if let conscienceNVC = conscienceNVC,
            let nvc = conscienceNVC.navigationController{
            var contritionView = false
            var conscienceView = false
            for viewController in nvc.viewControllers{

                if viewController is ConscienceViewController{
                    conscienceView = true
                }
                if viewController is ContritionContainerViewController{
                    contritionView = true
                }
                if let vc  = viewController as? QuestionaryViewController{
                    vc.dropDown.hide()
                    
                }
            }
            if conscienceView && !contritionView{
//                nvc.popToRootViewController(animated: false)
                if let vc = nvc.storyboard?.instantiateViewController(withIdentifier: "ConsciencePasswordViewController") as? ConsciencePasswordViewController{
                    vc.isSecurityAlert = true
                    vc.modalPresentationStyle = .fullScreen
                    
                    nvc.present(vc, animated: true, completion: nil)
                    
                }

            }
            
        }
    }
    
    func getQuestionsFromArray(_ array:[String], completion:((_ exams:[String:Any]) -> Void)?){
        if conscienceExams.count > 0{
            completion?(getExamsFromArray(array))
        }else{
            getConscienceExams { (consienceExams, error) in
                completion?(self.getExamsFromArray(array))
            }
        }
        
    }
    
    private func getExamsFromArray(_ array:[String]) -> [String:Any]{
        var exams = [String:Any]()
        
        for exam in conscienceExams {
            for group in exam["groups"] as? [[String:Any]] ?? []{
                let groupId = group["id"] as? String ?? ""
                
                for question in group["questions"] as? [[String:Any]] ?? []{
                    if let questionId = question["id"] as? String,
                        array.contains(questionId){
                        var groupToSave = exams[groupId] as? [String:Any]
                        if groupToSave == nil{
                            groupToSave = [
                                "id":groupId,
                                "title":group["title"] as? String ?? "",
                                "subtitle":group["subtitle"] as? String ?? "",
                                "order": group["order"] as? Int ?? 0,
                                "examOrder": exam["order"] as? Int ?? 0,
                                "questions":[question]
                            ]
                        }else{
                            if var questions = groupToSave!["questions"] as? [[String:Any]]{
                                questions.append(question)
                                groupToSave!["questions"] = questions
                            }
                        }
                        exams[groupId] = groupToSave
                    }
                }
            }
        }
        return exams
    }
}
