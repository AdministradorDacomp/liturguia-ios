//
//  DeleteManager.swift
//  LiturGuia
//
//  Created by Daniel Marrufo rivera on 13/11/23.
//  Copyright © 2023 DAComp SC. All rights reserved.
//

import UIKit
import Parse

class DeleteManager: NSObject {
    static let shared : DeleteManager = {
        let instance = DeleteManager()
        return instance
    }()
    
    func deleteAllData(motivo:String,completion:((_ success:Bool?, _ error:Error?) -> Void)?){
        dump(PFUser.current()?.email)
        dump(PFUser.current()?.objectId)
        if let objectID = PFUser.current()?.objectId{
            let path = "eliminarById"
            let body:[String:Any] = ["id" : objectID, "motivo" : motivo,"origen":"iOS"]
            CloudFunctionManager.request(path, body: body) { (result, error) in
                if let error = error as NSError? {
                    completion?(false, error)
                }else if result != nil{
                    NotificationCenter.default.post(name: NSNotification.Name("UserInfoUpdated"), object: nil)
                    completion?(true, nil)
                }else{
                    completion?(false, nil)
                }
            }
        }
    }
    
    
    ///Se deja sin usar para si se requiere posteriormente el uso nativamente
    func deleteComments(user:PFUser,completion:((_ success:Bool?, _ error:Error?) -> Void)?){
        let queryComentarios = PFQuery(className: "Comentarios")
        queryComentarios.whereKey("user", equalTo: user)
        queryComentarios.findObjectsInBackground {(objects, error) in
            if let tempObjects = objects{
                for object in tempObjects {
                    object.deleteEventually()
                }
            }
            self.deleteUserIntents(user: user, completion: completion)
        }
    }
    
    
    func deleteUserIntents(user:PFUser,completion:((_ success:Bool?, _ error:Error?) -> Void)?){
        let queryIntencionesDeUsuarios = PFQuery(className: "IntencionesDeUsuarios")
        queryIntencionesDeUsuarios.whereKey("usuario", equalTo: user)
        queryIntencionesDeUsuarios.findObjectsInBackground {(objects, error) in
            if let tempObjects = objects{
                for object in tempObjects {
                    object.deleteEventually()
                }
            }
            self.deletePayments(user: user, completion: completion)
        }
    }
    
    func deletePayments(user:PFUser,completion:((_ success:Bool?, _ error:Error?) -> Void)?){
        let queryPago = PFQuery(className: "Pago")
        queryPago.whereKey("usuario", equalTo: user)
        queryPago.findObjectsInBackground {(objects, error) in
            if let tempObjects = objects{
                for object in tempObjects {
                    object.deleteEventually()
                }
            }
            self.deleteUserData(user: user, completion: completion)
        }
    }
    
    func deleteUserData(user:PFUser,completion:((_ success:Bool?, _ error:Error?) -> Void)?){
        NotificationCenter.default.post(name: NSNotification.Name("UserInfoUpdated"), object: nil)
        let installation = PFInstallation.current()
        installation?.remove(forKey: "user")
        installation?.saveInBackground()
        let allMissals = NSFetchRequest<NSFetchRequestResult>()
        allMissals.entity = NSEntityDescription.entity(forEntityName: "MissalEntity", in: LiturGuia.moc())
        allMissals.includesPropertyValues = false //only fetch the managedObjectID
        
        var error: Error?
        var missals: [Any]?
        do {
            missals = try LiturGuia.moc().fetch(allMissals)
        } catch let e {
            error = e
        }
        //error handling goes here
        for missal in missals ?? [] {
            guard let missal = missal as? NSManagedObject else {
                continue
            }
            LiturGuia.moc().delete(missal)
        }
        
        let allLectioDivine = NSFetchRequest<NSFetchRequestResult>()
        allLectioDivine.entity = NSEntityDescription.entity(forEntityName: "LectioDivineEntity", in: LiturGuia.moc())
        allLectioDivine.includesPropertyValues = false //only fetch the managedObjectID
        
        
        var lectioDivines: [Any]?
        do {
            lectioDivines = try LiturGuia.moc().fetch(allLectioDivine)
        } catch let e {
            error = e
        }
        //error handling goes here
        for lectioDivine in lectioDivines ?? [] {
            guard let lectioDivine = lectioDivine as? NSManagedObject else {
                continue
            }
            LiturGuia.moc().delete(lectioDivine)
        }
        LiturGuia.saveContext()
        
        if let e = error{
            completion!(false, e)
        }else{
            user.deleteInBackground(block: { (deleteSuccessful, error) -> Void in
                if deleteSuccessful{
                    completion?(true,nil)
              
                }else{
                    completion?(false, error)
                }
            })
            /*PFUser.logOutInBackground(block: { (logOut) -> Void in
                user.deleteInBackground(block: { (deleteSuccessful, error) -> Void in
                    if deleteSuccessful{
                        self.deleteComments(user: user, completion: completion)
                    }else{
                        completion?(false, error)
                    }
                })
            })
            */
        }
    }
}

