//
//  DynamicLinksManager.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 10/26/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

private let prefix = "https://www.liturguia.com"
private let dynamicLinksDomainURIPrefix = "https://liturguia.page.link"
private let imageURL = URL(string: "http://www.liturguia.com/landingpage/assets/img/features/LogoLiturGuia150.png")!
private let iOSBundleID = "com.dacompsc.LiturGuia"
private let androidPackageName = "com.dacompsc.liturguiaandroid"
private let minimumAppVersioniOS = "1.3.2"
private let minimumAppVersionAndroid = 123
let kIdAppStore = "996234205"

@objc class DynamicLinksManager: NSObject {

   static func shareMeditation(withURL url: URL, title:String?, completion:((_ link:URL?, _ error:Error?) -> Void)?) {
        guard let link = URL(string: "\(prefix)?url=\(url.absoluteString)&type=reflexion") else { return }
     
        let title = "Reflexión compartida en LiturGuía"
        let description = "Ve la reflexión '\(title)' en LiturGuía"
        if let linkBuilder = getDynamicLinkComponents(withLink: link, title: title, description: description){
            linkBuilder.shorten() { url, warnings, error in
                if let warnings = warnings{
                    print(warnings)
                }
                if let error = error{
                    completion?(nil, error)
                }else if let url = url{
                    completion?(url, nil)
                }else{
                    completion?(nil, nil)
                }
            }
        }
    }
    
    private static func getDynamicLinkComponents(withLink link:URL, title:String, description:String) -> DynamicLinkComponents?{
        if let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix){
            
            linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: iOSBundleID)
            linkBuilder.iOSParameters!.appStoreID = kIdAppStore
            linkBuilder.iOSParameters!.minimumAppVersion = minimumAppVersioniOS
            
            linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: androidPackageName )
            linkBuilder.androidParameters!.minimumVersion = minimumAppVersionAndroid
            
            
            linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            linkBuilder.socialMetaTagParameters!.title = title
            linkBuilder.socialMetaTagParameters!.descriptionText = description
            linkBuilder.socialMetaTagParameters!.imageURL = imageURL
            
            return linkBuilder
        }
        
        return nil
    }
    
     @objc static func handle(dynamicLink:DynamicLink){
        if let url = dynamicLink.url{
            if let queryParameters = url.queryParameters,
                let type = queryParameters["type"]{
                switch  type{
                case "reflexion":
                    if let meditationUrlString = queryParameters["url"],
                        let meditationURL = URL(string: meditationUrlString){
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                            let topVC = UIApplication.topViewController()
                            if let vc = topVC?.storyboard?.instantiateViewController(withIdentifier: "MeditationLinkViewController") as? MeditationLinkViewController{
                                vc.url = meditationURL
                                
                                let nvc = UINavigationController(rootViewController: vc)
                                
                                let button = UIBarButtonItem(title: "Cerrar", style: .done, target: nil, action: nil)
                                button.actionClosure = {
                                    nvc.dismiss(animated: true, completion: nil)
                                }
                                
                                button.tintColor = .white
                                
                                vc.navigationItem.setRightBarButton(button, animated: true)
                                nvc.modalPresentationStyle = .fullScreen
                                topVC?.present(nvc, animated: true, completion: nil)
                            }
                            
                        }
                    }
                    
                    
                default: break
                }
                
            }
            
            
        }
        
    }
}
