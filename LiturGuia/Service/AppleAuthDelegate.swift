//
//  AuthDelegate.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 17/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

import UIKit
import Parse
import AuthenticationServices

@objc protocol LiturguiaAuthProtocol {
    @objc func authenticationFlowComplete(_ error: Error?, isNewUser: Bool)
}

@objc class AppleAuthDelegate: NSObject {
    @objc static var shared: AppleAuthDelegate = {
        let instance = AppleAuthDelegate()
        return instance
    }()
    @objc var delegate: LiturguiaAuthProtocol?
    
    // Unhashed nonce.
    fileprivate var currentNonce: String?

    @objc func startSignInFlow() {
        if #available(iOS 13.0, *) {
            let nonce = String.randomNonceString()
//            currentNonce = nonce
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            request.nonce = nonce.sha256(nonce)
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else { }

    }
}

extension AppleAuthDelegate: PFUserAuthenticationDelegate {
    func restoreAuthentication(withAuthData authData: [String : String]?) -> Bool {
        return true
    }
}

@available(iOS 13.0, *)
extension AppleAuthDelegate: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {

    class func unregisterAuthenticationDelegate(forAuthType authType: String){
       // PFUser.unregisterAuthenticationDelegate(forAuthType: "apple")
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let credentials as ASAuthorizationAppleIDCredential:
            print("AUTHORIZED: \(credentials.fullName?.givenName ?? "")")
            let token = credentials.identityToken!
            let tokenString = String(data: token, encoding: .utf8)!
            let user = credentials.user
            let inicioApple = UserDefaults.standard.bool(forKey: "inicioApple")
            if !inicioApple{
                PFUser.register(self, forAuthType: "apple")
            }
    
            PFUser.logInWithAuthType(inBackground: "apple", authData: ["token":tokenString, "id": user]).continueWith { task -> Any? in
                if let userObject = task.result {
                    if userObject.isNew {
                        userObject["email"] = credentials.email ?? "\(credentials.user)@apple.com"
                        userObject["nombre"] = credentials.fullName?.givenName ?? "Usuario"
                        userObject["apellido"] = credentials.fullName?.familyName ?? "Anónimo"
                        userObject["appleId"] = credentials.user
                    }
                    userObject.saveInBackground { (success, error) in
                        if let error = error {
                            self.delegate?.authenticationFlowComplete(error, isNewUser: false)
                        } else {
                            UserDefaults.standard.set(true, forKey: "inicioApple")
                            self.delegate?.authenticationFlowComplete(nil, isNewUser: userObject.isNew)
                        }
                    }
                } else {
                //    self.delegate?.authenticationFlowComplete(task.error, isNewUser: false)
                }
                return nil
            }
            
            break
        default:
            break
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        self.delegate?.authenticationFlowComplete(error, isNewUser: false)
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return (delegate as! UIViewController).view.window!
    }
}
