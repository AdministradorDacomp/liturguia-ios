//
//  MeditationsManager.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 10/19/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import Parse

class MeditationsManager: NSObject {
    static let shared : MeditationsManager = {
        let instance = MeditationsManager()
        return instance
    }()
    
    var meditationsArray = [[String:Any]]()
    
    func getMeditaions(forceUpdate: Bool = false, completion:((_ meditations:[[String:Any]]?, _ error:Error?) -> Void)?){
        if meditationsArray.count > 0 && !forceUpdate{
            completion?(meditationsArray, nil)
        }else{
            let query = PFQuery(className: "FuentesReflexiones")
            query.limit = 1000
            query.findObjectsInBackground {[weak self] (objects, error) in
                guard let strongSelf = self else { return }
                if let error = error{
                    completion?(nil, error)
                }else if let objects = objects{
                    var sources = [String:Any]()
                    for parseObject in objects{
                        if let titulo = parseObject["titulo"] as? String,
                            let tipoFuente = parseObject["tipoFuente"] as? String,
                            let urlString = parseObject["url"] as? String,
                            let sourceURL = URL(string:urlString),
                            let urlImagenString = parseObject["urlImagen"] as? String,
                            let imageURL = URL(string:urlImagenString),
                            let tipoContenido = parseObject["tipoContenido"] as? String,
                            let sourceId = parseObject.objectId,
                            let orden = parseObject["orden"] as? Int,
                            let vistas = parseObject["vistas"] as? Int{
                            
                            var source:[String:Any] = [:]
                            source["sourceType"] = tipoFuente
                            source["sourceURL"] = sourceURL
                            source["imageURL"] = imageURL
                            source["contentType"] = tipoContenido
                            source["order"] = orden
                            source["title"] = titulo
                            source["views"] = vistas
                            
                            source["parseObject"] = parseObject
                            
                            sources[sourceId] = source
                        }
                    }
                    
                    strongSelf.meditationsArray = sources.toSortedArrayByKey("order")
//                    print(String(data: try! JSONSerialization.data(withJSONObject: strongSelf.meditationsArray, options: .prettyPrinted), encoding: .utf8 )!)
                    
                    completion?(strongSelf.meditationsArray, nil)
                }else{
                    completion?(nil, nil)
                }
            }
            
        }
    }
}

