//
//  LiturGuia.h
//  LiturGuia
//
//  Created by Krloz on 13/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface LiturGuia : NSObject

//#define kDefaultFreeCredits @20
//#define kDefaultWeekMissalPrice @2
#define kDefaultTrialTime @2
#define kUserInstallationsLimit @4
#define kIdAppStore @"996234205"

#define development NO


#define DegreesToRadians(x) ((x) * M_PI / 180.0)

#define appDelegate ((AppDelegate*)[UIApplication sharedApplication].delegate)

//==========================     URL's      ================================
#pragma mark - URL's

//#define kReceiptURLProduction @"https://buy.itunes.apple.com/verifyReceipt"
//
//#define kReceiptURLDevelopment @"https://sandbox.itunes.apple.com/verifyReceipt"


//==========================     Colors      ================================
#pragma mark - Colors



#define kGlobalColor [UIColor colorWithRed:75/255.0f green:166/255.0f blue:218/255.0f alpha:1.0f]

#define kNavigationBarColor [UIColor colorWithRed:0.247 green:0.318 blue:0.710 alpha:1]
#define kNavigationBarTextColor [UIColor whiteColor]

#define SEPIA_TEXT_COLOR [UIColor colorWithRed:112.0/255 green:66.0/255 blue:20.0/255 alpha:1.0]

#define SEPIA_BACKGROUND_COLOR [UIColor colorWithRed:253/255.0 green:243/255.0 blue:219/255.0 alpha: 1.0]


//==========================     Purchase id's     ================================

#define kMonthSuscription @"com.dacompsc.LiturGuia.s_mensual"
#define k6MonthSuscription @"com.dacompsc.LiturGuia.s_semestral"
#define k12MonthSuscription @"com.dacompsc.LiturGuia.s_anual"

//==========================     Global methods      ================================
#pragma mark - Global methods

#define screenWidth [UIScreen mainScreen].bounds.size.width
#define screenHeight [UIScreen mainScreen].bounds.size.height

//+ (NSString*) createUUID;
+ (NSString *) append:(NSString*) first, ...;
+ (NSString*) defaultDateFormat;
+ (BOOL)internetConnected;
+ (NSDictionary*)currentUser;
+ (BOOL)isEqualColor:(UIColor *)color to: (UIColor*)otherColor;
+ (NSURL*)profilePhotoURL;
+ (NSManagedObjectContext*)moc;
+ (void)saveContext;
+(NSDate *)dateWithoutTime:(NSDate *)date;
+(NSDate*)dateByAddingMonths:(int)months toDate:(NSDate*)date;
+(BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString;

//#define NSLocalizedString(key, comment) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

#define contentLanguage [[NSUserDefaults standardUserDefaults] objectForKey:@"language"]

#define NSLog(STRING, ...) printf("%s\n", [[NSString stringWithFormat:STRING, ##__VA_ARGS__] UTF8String]);




@end
