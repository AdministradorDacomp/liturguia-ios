//
//  PasswordResetViewController.m
//  LiturGuia
//
//  Created by Krloz on 29/10/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "PasswordResetViewController.h"

@interface PasswordResetViewController ()

@end

@implementation PasswordResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_emailTextField becomeFirstResponder];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation bar

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)resetButtonTapped:(id)sender {
    if ([_emailTextField.text isEqualToString:@""]) {
        NSString *messageErr = @"Por favor, ingrese un correo electrónico para restablecer la contraseña";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
        [_emailTextField becomeFirstResponder];
        return;
    }
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.view.userInteractionEnabled = NO;
    [KVNProgress showWithStatus:@"Buscando usuario..."];
    [PFUser requestPasswordResetForEmailInBackground:[_emailTextField.text lowercaseString] block:^(BOOL succeeded, NSError *error) {
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        self.view.userInteractionEnabled = YES;
        [KVNProgress dismiss];
        if (!error) {
            
            NSString *messageErr = @"Se ha enviado un correo electrónico a la dirección ingresada para restablecer su contraseña. Por favor siga las instrucciones que vienen dentro del mismo.";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *messageError = [error userInfo][@"error"];
            if (error.code == 205) {
                messageError = @"Parece ser que el correo electrónico ingresado no existe. Favor de verificar.";
            }else if (error.code == 125){
                messageError = @"El correo electrónico ingresado es invalido. Favor de verificar.";
            }
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageError
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            [self->_emailTextField becomeFirstResponder];
        }
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self resetButtonTapped:nil];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
