//
//  PasswordResetViewController.h
//  LiturGuia
//
//  Created by Krloz on 29/10/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordResetViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;

@end
