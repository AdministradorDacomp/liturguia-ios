//
//  LoginViewController.m
//  Liturguia
//
//  Created by Krloz on 07/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "LoginViewController.h"
//#import <Parse/PFFacebookUtils.h>
#import "DataSyncer.h"

@interface LoginViewController ()

@end

@implementation LoginViewController{
    UITextField *activeField;
}

@synthesize scrollView;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    [_emailTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation bar

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Server methods

-(void)login{
    if ([LiturGuia internetConnected]) {
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = NO;
        
        [KVNProgress showWithStatus:@"Iniciando sesión"];
        
        [PFUser logInWithUsernameInBackground:[_emailTextField.text lowercaseString] password:_passwordTextField.text
                                        block:^(PFUser *user, NSError *error) {
//                                            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                                            self.view.window.userInteractionEnabled = YES;
                                            if (user) {
                                                [KVNProgress dismiss];
                                                if ([user[@"emailVerified"] isEqual: @NO]) {
                                                    NSString *messageErr = [NSString stringWithFormat: @"El proceso de registro de la cuenta no se ha completado. Para completarlo, favor de dar clic en el enlace de confirmación del mensaje enviado a: %@", self->_emailTextField.text];
                                                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                                                 message:messageErr
                                                                                                delegate:nil
                                                                                       cancelButtonTitle:@"OK"
                                                                                       otherButtonTitles:nil];
                                                    [av show];
                                                }else{
                                                    PFQuery *query = [PFQuery queryWithClassName:@"_Session"];
                                                    int activeSessions = (int)[query countObjects];
                                                    
                                                    if (activeSessions  >= [kUserInstallationsLimit intValue] + 1) {
                                                        NSString *messageErr = [NSString stringWithFormat: @"Esta cuenta ya está siendo usada en %d dispositivos. Al iniciar sesión en este dispositivo se cerraran todas las sesiones. ¿Desea continuar?", activeSessions - 1];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                                                     message:messageErr
                                                                                                    delegate:self
                                                                                           cancelButtonTitle:@"No"
                                                                                           otherButtonTitles:@"Sí", nil];
                                                        [av show];
                                                        return;
                                                    }
                                                    
                                                    PFInstallation *installation = [PFInstallation currentInstallation];
                                                    installation[@"user"] = [PFUser currentUser];
                                                    [installation saveInBackground];
                                                    
                                                    [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                                    }];
                                                    
                                                    [DataSyncer syncMissals];
                                                    [DataSyncer syncPrayers];
//                                                    [DataSyncer syncLectioDivine];
                                                    
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginComplete" object:nil];
                                                }
                                                
                                            } else {
                                                [KVNProgress dismiss];
                                                NSString *errorString;
                                                if ([error code] == 101) {
                                                    errorString = @"Verifique que el usuario y contraseña estén correctos o asegúrese de estar registrado en LiturGuía";
                                                }else{
                                                    errorString = [error userInfo][@"error"];
                                                }
                                                NSLog(@"ERROR:  Loggin user failed - %@",errorString);
                                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                                             message:errorString
                                                                                            delegate:nil
                                                                                   cancelButtonTitle:@"OK"
                                                                                   otherButtonTitles:nil];
                                                [av show];
                                                
                                                [self->_passwordTextField becomeFirstResponder];
                                            }
                                        }];
        
    }else{
        
        NSString *messageErr = @"Conexión a internet no detectada. Verifique su conexión, encienda WiFi o datos móviles e intente de nuevo.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sin conexión a intenet", nil)
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"Aceptar"
                                           otherButtonTitles:nil];
        [av show];
    }
}

#pragma mark - Button actions

- (IBAction)loginButtonTapped:(id)sender {
    if (![self validateInfo]) {
        return;
    }
    
    [self login];
}

#pragma mark - Show keyboard on scroll view

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+50, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}


#pragma mark - UITextFieldDelegate


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    activeField = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    if (textField == _passwordTextField) {
        [self login];
    }


    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    activeField = nil;
}

#pragma mark - Helpers

-(BOOL)validateInfo{
    NSString *msgError;
    if ([_passwordTextField.text isEqual:@""]) {
        msgError = NSLocalizedString(@"falta_password", nil);
    }
    if ([_emailTextField.text isEqual:@""]) {
        msgError = NSLocalizedString(@"falta_email", nil);
    }
    
    if (msgError) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"informacion_necesaria", nil)
                                                     message:msgError
                                                    delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"aceptar", nil)
                                           otherButtonTitles:nil];
        [av show];
        return NO;
    }
    
    return YES;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.cancelButtonIndex != buttonIndex) {
        [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession *currentSession, NSError *error){
            if (!error) {
                PFQuery *query = [PFQuery queryWithClassName:@"_Session"];
                [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                    if (!error) {
                        for (PFSession *session in objects) {
                            if (![currentSession.objectId isEqual:session.objectId]) {
                                [session deleteInBackground];
                            }
                        }
                        
                        PFInstallation *installation = [PFInstallation currentInstallation];
                        installation[@"user"] = [PFUser currentUser];
                        [installation saveInBackground];
                        
                        [self.navigationController dismissViewControllerAnimated:YES completion:^{
                        }];
                        
                        [DataSyncer syncMissals];
                        [DataSyncer syncPrayers];
//                        [DataSyncer syncLectioDivine];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginComplete" object:nil];
                    }
                }];
            }
        }];
    }else{
        [PFUser logOut];
    }
}

@end
