//
//  ViewController.m
//  Liturguia
//
//  Created by Krloz on 07/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "NotLoggedInViewController.h"
//#import <Parse/PFFacebookUtils.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "DataSyncer.h"
#import <Parse/PFTwitterUtils.h>
#import <GoogleSignIn/GIDSignIn.h>
#import "NSDate+ServerDate.h"
#import "KLCPopup.h"
#import <Parse/PF_Twitter.h>
#import <AuthenticationServices/AuthenticationServices.h>

@interface NotLoggedInViewController (){
    KLCPopup *pupopView;
}

@end

@implementation NotLoggedInViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // TODO(developer) Configure the sign-in button look/feel
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate = self;

    pupopView = [KLCPopup popupWithContentView:_infoView
                                                showType:KLCPopupShowTypeGrowIn
                                             dismissType:KLCPopupDismissTypeGrowOut
                                                maskType:KLCPopupMaskTypeDimmed
                                dismissOnBackgroundTouch:YES dismissOnContentTouch:YES];
    
    
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
// UIViewController.

// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [self hideHUD];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    viewController.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    viewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
//    [self hideHUD];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginWithFacebookButtonTapped:(id)sender {
    LiturGuiaFacebookAuthDelegate *authDelegate = [LiturGuiaFacebookAuthDelegate shared];
    authDelegate.delegate = self;
    [authDelegate startSignInFlow];
}

- (IBAction)loginWithTwitterButtonTapped:(id)sender {
//    [self showHUD];
    [PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
        if (!user) {
            [self hideHUD];
            NSLog(@"The user cancelled the Twitter login.");
            return;
        } else if (user.isNew) {
            NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
            [[PFTwitterUtils twitter] signRequest:request];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:request queue:queue
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                       [self hideHUD];
                                       if (!error) {
                                           NSDictionary *json = [NSJSONSerialization
                                                                 JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                 error:&error];
                                           
                                           NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                           NSLog(@"%@",jsonString);
                                           
                                           
                                           if ([self checkNoNullValue:json[@"id_str"]]) {
                                               user[@"twitterId"] = json[@"id_str"];
                                           }
                                           
                                           if ([self checkNoNullValue:json[@"screen_name"]]) {
                                               user[@"username"] = json[@"screen_name"];
                                           }
                                           
                                           user[@"profilePhotoTwitter"] = json[@"profile_image_url_https"];
                                           
                                           
                                           NSString * names = json[@"name"];
                                           NSMutableArray * array = [NSMutableArray arrayWithArray:[names componentsSeparatedByString:@" "]];
                                           if ( array.count > 1){
                                               user[@"apellido"] = [array lastObject];
                                               
                                               [array removeLastObject];
                                               user[@"nombre"] = [array componentsJoinedByString:@" "];
                                           }
                                           
                                           
                                           [self signUpComplete];

                                       }
                                   }];

        } else {
            [self hideHUD];
            PFQuery *query = [PFQuery queryWithClassName:@"_Session"];
            int activeSessions = (int)[query countObjects];
            
            if (activeSessions  >= [kUserInstallationsLimit intValue] + 1) {
                [self hideHUD];
                NSString *messageErr = [NSString stringWithFormat: @"Esta cuenta ya está siendo usada en %d dispositivos. Al iniciar sesión en este dispositivo se cerraran todas las sesiones. ¿Desea continuar?", activeSessions - 1];
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:messageErr
                                                            delegate:self
                                                   cancelButtonTitle:@"No"
                                                   otherButtonTitles:@"Sí", nil];
                av.tag = 0;
                [av show];
                return;
            }
            
            [self loginComplete];
        }
        
    }];

}

- (IBAction)loginWithAppleButtonTapped:(id)sender {
    NSFetchRequest * allMissals = [[NSFetchRequest alloc] init];
    [allMissals setEntity:[NSEntityDescription entityForName:@"MissalEntity" inManagedObjectContext:[LiturGuia moc]]];
    [allMissals setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * missals = [[LiturGuia moc] executeFetchRequest:allMissals error:&error];
    //error handling goes here
    for (NSManagedObject * missal in missals) {
        [[LiturGuia moc] deleteObject:missal];
    }
    
    NSFetchRequest * allLectioDivine = [[NSFetchRequest alloc] init];
    [allLectioDivine setEntity:[NSEntityDescription entityForName:@"LectioDivineEntity" inManagedObjectContext:[LiturGuia moc]]];
    [allLectioDivine setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    

    NSArray * lectioDivines = [[LiturGuia moc] executeFetchRequest:allLectioDivine error:&error];
    //error handling goes here
    for (NSManagedObject * lectioDivine in lectioDivines) {
        [[LiturGuia moc] deleteObject:lectioDivine];
    }
    
    NSFetchRequest * allPrayers = [[NSFetchRequest alloc] init];
    [allPrayers setEntity:[NSEntityDescription entityForName:@"PrayerEntity" inManagedObjectContext:[LiturGuia moc]]];
    [allPrayers setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    
    NSArray * prayers = [[LiturGuia moc] executeFetchRequest:allPrayers error:&error];
    //error handling goes here
    for (NSManagedObject * prayer in prayers) {
        [[LiturGuia moc] deleteObject:prayer];
    }
    
    [LiturGuia saveContext];
    
    AppleAuthDelegate *authDelegate = [AppleAuthDelegate shared];
    authDelegate.delegate = self;
    [authDelegate startSignInFlow];
}

- (IBAction)infoButtonTapped:(id)sender {
    [pupopView showAtCenter:self.view.center inView:self.view];
}

-(void)signUpComplete{
    PFUser *user = [PFUser currentUser];
    NSDate *expirationDate = [NSDate serverDate];
    
    NSNumber *trialTime = @([[NSUserDefaults standardUserDefaults] integerForKey:@"trialTime"]);

    NSCalendar *calendar = [NSCalendar currentCalendar];
    expirationDate = [calendar dateByAddingUnit:NSCalendarUnitMonth value:[trialTime intValue] toDate:expirationDate options:0];
    
    user[@"fVenceSuscripcion"] = expirationDate;
//    NSNumber *freeCredits = @([[NSUserDefaults standardUserDefaults] integerForKey:@"freeCredits"]);
//    user[@"creditos"] = freeCredits;
//    user[@"misalesDescargados"] = @[];
//    user[@"lectioDivinaDescargadas"] = @[];
    [user saveInBackground];
    
    [self loginComplete];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loginComplete{
    [DataSyncer syncMissals];
    [DataSyncer syncPrayers];
//    [DataSyncer syncLectioDivine];
    
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"user"] = [PFUser currentUser];
    [installation saveInBackground];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginComplete" object:nil];
    
    [self hideHUD];
    [self dismissViewControllerAnimated:YES completion: nil];
}

- (IBAction)googleButtonTapped:(id)sender {
    NSFetchRequest * allMissals = [[NSFetchRequest alloc] init];
    [allMissals setEntity:[NSEntityDescription entityForName:@"MissalEntity" inManagedObjectContext:[LiturGuia moc]]];
    [allMissals setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * missals = [[LiturGuia moc] executeFetchRequest:allMissals error:&error];
    //error handling goes here
    for (NSManagedObject * missal in missals) {
        [[LiturGuia moc] deleteObject:missal];
    }
    
    NSFetchRequest * allLectioDivine = [[NSFetchRequest alloc] init];
    [allLectioDivine setEntity:[NSEntityDescription entityForName:@"LectioDivineEntity" inManagedObjectContext:[LiturGuia moc]]];
    [allLectioDivine setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    

    NSArray * lectioDivines = [[LiturGuia moc] executeFetchRequest:allLectioDivine error:&error];
    //error handling goes here
    for (NSManagedObject * lectioDivine in lectioDivines) {
        [[LiturGuia moc] deleteObject:lectioDivine];
    }
    
    NSFetchRequest * allPrayers = [[NSFetchRequest alloc] init];
    [allPrayers setEntity:[NSEntityDescription entityForName:@"PrayerEntity" inManagedObjectContext:[LiturGuia moc]]];
    [allPrayers setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    
    NSArray * prayers = [[LiturGuia moc] executeFetchRequest:allPrayers error:&error];
    //error handling goes here
    for (NSManagedObject * prayer in prayers) {
        [[LiturGuia moc] deleteObject:prayer];
    }
    
    [LiturGuia saveContext];
    
    [self showHUD];

    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    [signIn signIn];
}

-(void)showHUD{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.view.userInteractionEnabled = NO;
    [KVNProgress show];
}

-(void)hideHUD{
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    self.view.userInteractionEnabled = YES;
    [KVNProgress dismiss];
}

-(void)requestPublishPermissions{
//    [PFFacebookUtils linkUserInBackground:[PFUser currentUser] withPublishPermissions:@[@"publish_actions"] block:^(BOOL succeeded, NSError *error) {
//        if (succeeded) {
//            NSLog(@"User now has read and publish permissions!");
//        }
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)googleUser
     withError:(NSError *)error {
    if (error) {
        [self hideHUD];
        if (error.code == -5) {
            return;
        }
        NSString *messageErr = error.localizedDescription;
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
        
        return;
    }
    // Perform any operations on signed in user here.
    NSString *userId = googleUser.userID;                  // For client-side use only!
//    NSString *idToken = googleUser.authentication.idToken; // Safe to send to the server
    NSString *names = googleUser.profile.name;
    NSString *email = [googleUser.profile.email lowercaseString];
    NSURL *profileURL = [googleUser.profile imageURLWithDimension:200];
    
    PFUser *user = [PFUser user];
    user.username = email;
    user.password = @"GoogleAccount"; // It will use a common password for all google+ users.
    user.email = email;
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            user[@"googleId"] = userId;
            user[@"profilePhotoGoogle"] = [profileURL absoluteString];
            
            NSMutableArray * array = [NSMutableArray arrayWithArray:[names componentsSeparatedByString:@" "]];
            if ( array.count > 1){
                user[@"apellido"] = [array lastObject];
                
                [array removeLastObject];
                user[@"nombre"] = [array componentsJoinedByString:@" "];
            }
            
            
            
            [self hideHUD];
            [self signUpComplete];
        } else {
            if (error.code == 202) {
                [PFUser logInWithUsernameInBackground:email password:@"GoogleAccount"
                                                block:^(PFUser *user, NSError *error) {
                                                    [self hideHUD];
                                                    if (user) {
                                                        [self loginComplete];
                                                    }else{
                                                        NSString *messageErr = error.localizedDescription;
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                                                     message:messageErr
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK"
                                                                                           otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }
                                                }];

            }else{
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:error.localizedDescription
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
                [av show];
                [self hideHUD];
            }
        }
    }];
    
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}

- (IBAction)skipSignUpButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 0) {
        if (alertView.cancelButtonIndex != buttonIndex) {
            [self showHUD];
            [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession *currentSession, NSError *error){
                [self hideHUD];
                if (!error) {
                    PFQuery *query = [PFQuery queryWithClassName:@"_Session"];
                    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                        if (!error) {
                            for (PFSession *session in objects) {
                                if (![currentSession.objectId isEqual:session.objectId]) {
                                    [session deleteInBackground];
                                }
                            }
                            [self requestPublishPermissions];
                            
                            
                            [self loginComplete];
                        }
                    }];
                }
            }];
        }else{
            [PFUser logOut];
        }
    }else if(alertView.tag == 1){
        [self infoButtonTapped:nil];
    }
    
}

- (void)authenticationFlowComplete:(NSError *)error isNewUser:(BOOL)isNewUser {
    if (error) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message: [error localizedDescription]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];

        [av show];
    } else if (isNewUser){
        [self signUpComplete];
    } else {
        [self loginComplete];
    }
}

-(BOOL)checkNoNullValue:(id)value{
    if (value && value != [NSNull null]) {
        return YES;
    }
    return NO;
}

@end
