//
//  ViewController.h
//  Liturguia
//
//  Created by Krloz on 07/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GoogleSignIn/GoogleSignIn.h>

@interface NotLoggedInViewController : UIViewController<UIAlertViewDelegate, GIDSignInUIDelegate,GIDSignInDelegate, LiturguiaAuthProtocol>

@property (strong, nonatomic) IBOutlet UIView *infoView;

@end

