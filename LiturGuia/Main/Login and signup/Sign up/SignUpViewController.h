//
//  SignUpViewController.h
//  LiturGuia
//
//  Created by Krloz on 13/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;


@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;


@property (strong, nonatomic) IBOutlet UITextField *DOBTextField;
@property (strong, nonatomic) IBOutlet UITextField *countryTextField;
@property (strong, nonatomic) IBOutlet UIView *genderSwitchView;
@property (strong, nonatomic) IBOutlet UIImageView *genderSwitch;
@property (strong, nonatomic) IBOutlet UILabel *maleLabel;

@property (strong, nonatomic) IBOutlet UILabel *femaleLabel;

@end
