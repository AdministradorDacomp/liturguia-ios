//
//  SignUpViewController.m
//  LiturGuia
//
//  Created by Krloz on 13/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "SignUpViewController.h"
#import "NSDate+ServerDate.h"

#define maleSwitchColor [UIColor colorWithRed:0.1411 green:0.6823 blue:0.9294 alpha:1]
#define femaleSwitchColor [UIColor colorWithRed:1 green:0.6823 blue:0.9294 alpha:1]

@interface SignUpViewController (){
    BOOL male;
    BOOL genderSelected;
    UITextField *activeField;
    NSArray *countriesArray;
    PFObject *selectedCountry;
    NSDateFormatter *formatter;
    NSDate *DOB;
}

@end

@implementation SignUpViewController

@synthesize scrollView;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    male = NO;
    
    [self registerForKeyboardNotifications];
    
    _genderSwitchView.layer.cornerRadius = 5;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:[LiturGuia defaultDateFormat]];
    
    [self setupTextFieldToolbars];
    [self loadCountries];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void)setupTextFieldToolbars{
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateDOBTextField:) forControlEvents:UIControlEventValueChanged];
//    _DOBTextField.text = [formatter stringFromDate:[NSDate date]];
    DOB = [NSDate date];
    [_DOBTextField setInputView:datePicker];
    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 0, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           
                           [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(textFieldDoneButtonTapped:)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]
                            initWithTitle:NSLocalizedString(@"Anterior", nil)    style:UIBarButtonItemStyleDone target:self action:@selector(DOBPreviousButtonTapped:)],[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Siguiente", nil)  style:UIBarButtonItemStyleDone target:self action:@selector(DOBNextButtonTapped:)],
                           nil];
    [numberToolbar sizeToFit];
    
    numberToolbar.barTintColor = kGlobalColor;
    numberToolbar.tintColor = [UIColor whiteColor];
    
    _DOBTextField.inputAccessoryView = numberToolbar;
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 0, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Anterior", nil) style:UIBarButtonItemStyleDone target:self action:@selector(countryPreviousButtonTapped:)],
                           
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(textFieldDoneButtonTapped:)],
                           nil];
    [numberToolbar sizeToFit];
    
    numberToolbar.barTintColor = kGlobalColor;
    numberToolbar.tintColor = [UIColor whiteColor];
    
    _countryTextField.inputAccessoryView = numberToolbar;
}

-(void)setupCountrySelector{
    UIPickerView *countryPicker = [[UIPickerView alloc] init];
    countryPicker.dataSource = self;
    countryPicker.delegate = self;
    [_countryTextField setInputView:countryPicker];
    //    [self pickerView:countryPicker didSelectRow:0 inComponent:0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation methods

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Button actions

- (IBAction)createUser:(id)sender {
    if (![self validateInfo]) {
        return;
    }
    
    [self registerUser];
}


#pragma mark - Server methods

-(void)loadCountries{
    PFQuery * query = [PFQuery queryWithClassName:@"Pai"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            self->countriesArray = objects;
            [self setupCountrySelector];
        }
    }];
}

-(void)registerUser{
    if ([LiturGuia internetConnected]) {
        PFUser *user = [PFUser user];
        user.username = _emailTextField.text;
        user.password = _passwordTextField.text;
        user.email = [_emailTextField.text lowercaseString];
        
        if (genderSelected) {
            user[@"sexo"] = male?@"M":@"F";
        }
        
        user[@"nombre"] = _nameTextField.text;
        user[@"apellido"] = _lastNameTextField.text;
        if (selectedCountry) {
            user[@"pai"] = selectedCountry;
        }
        
        if (![_DOBTextField.text isEqual:@""]) {
            user[@"fechanac"] = DOB;
        }
        
        
        NSDate *expirationDate = [NSDate serverDate];
        
        NSNumber *trialTime = @([[NSUserDefaults standardUserDefaults] integerForKey:@"trialTime"]);
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        expirationDate = [calendar dateByAddingUnit:NSCalendarUnitMonth value:[trialTime intValue] toDate:expirationDate options:0];
        
        user[@"fVenceSuscripcion"] = expirationDate;
        
        
//        NSNumber *freeCredits = @([[NSUserDefaults standardUserDefaults] integerForKey:@"freeCredits"]);
//        user[@"creditos"] = freeCredits;
//        //    user[@"iddisp":[LiturGuia createUUID]]
//        user[@"misalesDescargados"] = @[];
//        user[@"lectioDivinaDescargadas"] = @[];
        
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = NO;
        [KVNProgress showWithStatus:@"Registrando usuario..."];

        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
            [KVNProgress dismiss];
            if (!error) {
                
                NSString *messageErr = [NSString stringWithFormat: @"¡Gracias por unirse a LiturGuía!\nPara completar el registro, favor de dar clic en el enlace de confirmación del mensaje enviado a: %@", [self->_emailTextField.text lowercaseString]];
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:messageErr
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
                [av show];
                [self.navigationController popViewControllerAnimated:YES];
                [PFUser logOut];
            } else {
                NSString *errorString = [error userInfo][@"error"];
                if (error.code == 202) {
                    errorString = @"El correo electrónico ingresado ya fue registrado anteriormente. Por favor, seleccione otro o vaya a la sección de inicio de sesión";
                }
                NSLog(@"ERROR: Sign up user failed - %@",[error userInfo][@"error"]);
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:errorString
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
                [av show];
            }
        }];

    }else{
        NSString *messageErr = @"Actualmente no estas conectado a internet. Por favor conectate a una red WiFi o enciende los datos móviles e intenta nuevamente.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Sin conexión a intenet", nil)
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"Aceptar"
                                           otherButtonTitles:nil];
        [av show];
    }
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return countriesArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return countriesArray[row][@"nombre"];
}


#pragma mark - UIPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    _countryTextField.text = countriesArray[row][@"nombre"];
    selectedCountry = countriesArray[row];
}


#pragma mark - Gender Switch methods


- (IBAction)genderValueChanged:(id)sender {
    genderSelected = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    if (male) {
        CGRect f = _genderSwitch.frame;
        f.origin.x = 2;
        _genderSwitch.frame = f;
        _genderSwitchView.backgroundColor = femaleSwitchColor;
        _femaleLabel.text = @"F";
    }else{
        CGRect f = _genderSwitch.frame;
        f.origin.x = 38;
        _genderSwitch.frame = f;
        _genderSwitchView.backgroundColor = maleSwitchColor;
        _maleLabel.text = @"M";
    }
    [UIView commitAnimations];
    
    male = !male;
}


#pragma mark - Show keyboard

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+50, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}



#pragma mark - UITextFieldDelegate

-(void)updateDOBTextField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)_DOBTextField.inputView;
    
    NSString *stringFromDate = [formatter stringFromDate:picker.date];
    
    _DOBTextField.text = stringFromDate;
    DOB = picker.date;
}


-(void)DOBPreviousButtonTapped:(id)sender{
    [_passwordTextField becomeFirstResponder];
}

-(void)DOBNextButtonTapped:(id)sender{
    [_countryTextField becomeFirstResponder];
}

-(void)countryPreviousButtonTapped:(id)sender{
    [_DOBTextField becomeFirstResponder];
}

-(void)textFieldDoneButtonTapped:(id)sender{
    [activeField resignFirstResponder];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _nameTextField) {
        [_lastNameTextField becomeFirstResponder];
    }
    if (textField == _lastNameTextField) {
        [_emailTextField becomeFirstResponder];
    }
    if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    if (textField == _passwordTextField) {
        [_DOBTextField becomeFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

#pragma mark - Helpers

-(BOOL)validateInfo{
    NSString *msgError;
    if ([_passwordTextField.text isEqual:@""]) {
        msgError = @"Por favor, ingrese una contraseña";
    }
    if ([_emailTextField.text isEqual:@""]) {
        msgError = @"Por favor, ingrese su correo electronico";
    }
    if ([_lastNameTextField.text isEqual:@""]) {
        msgError = @"Por favor, ingrese su apellido";
    }
    if ([_nameTextField.text isEqual:@""]) {
        msgError = @"Por favor, ingrese su nombre";
    }
    
    
    if (msgError) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información necesaria", nil)
                                                     message:msgError
                                                    delegate:nil
                                           cancelButtonTitle:@"Aceptar"
                                           otherButtonTitles:nil];
        [av show];
        return NO;
    }
    
    return YES;
}

@end
