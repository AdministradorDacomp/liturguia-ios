//
//  TrialInfoViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 18/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

import UIKit
import Parse
import MaryPopin

@objc protocol TrialInfoDelegate {
    func userDidCloseTrialInfo()
}

@objc class TrialInfoViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    @objc static var isShowingAlert = false
    
    @objc var delegate: TrialInfoDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        TrialInfoViewController.isShowingAlert = true
        let user = PFUser.current()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let createdAt = user?["createdAt"] as? Date ?? Date()
        let defaultExpiration = Date().addMonth(n: 2)
        let expiration = user?["fVenceSuscripcion"] as? Date ?? defaultExpiration
        textLabel.text = textLabel.text?.replacingOccurrences(of: "{{today}}", with: dateFormatter.string(from: createdAt))
        textLabel.text = textLabel.text?.replacingOccurrences(of: "{{trialExpiration}}", with: dateFormatter.string(from: expiration))
        
        user?["wasTrialInfoShowed"] = true
        user?.saveInBackground()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        delegate?.userDidCloseTrialInfo()
    }
}
