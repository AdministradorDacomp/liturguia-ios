//
//  MeditationsViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 10/19/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import KVNProgress
import Parse
import AFNetworking

class MeditationDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    var dataSourceArray = [Any]()
    
    var feedURL:URL? = nil
    var sourceTitle = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40.0
        
        reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MeditationLinkSegue",
            let vc = segue.destination as? MeditationLinkViewController,
            let sender = sender as? [String:Any]{
            vc.url = sender["url"] as? URL
            vc.title = sender["title"] as? String ?? "Reflexión"
            vc.sourceTitle = sourceTitle
        }
        
    }
    
    //MARK: - Helpers
    func reloadData() {
        KVNProgress.show()
        
        if let feedURL = feedURL{
            let parser = FeedParser(URL: feedURL)
            
            parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) {[weak self] (result) in
                guard let strongSelf = self else { return }
                DispatchQueue.main.async {
                    KVNProgress.dismiss()
                    switch result{
                    case .success(let feed):
                        switch feed {
                        case .atom(let atom):
                            strongSelf.title = atom.title ?? "Reflexión"
                            
                            strongSelf.dataSourceArray = atom.entries ?? []
                        case .rss(let rss):
                            
                            strongSelf.title = rss.title ?? "Reflexión"
                            
                            strongSelf.dataSourceArray = rss.items ?? []
                        case .json(let json): print(json)
                        }
                    case .failure(let error): print(error)
                    }
                    strongSelf.tableView.reloadData()

                }
            }
        }

    
    }
    

}

extension MeditationDetailViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "MeditationSourceCell", for: indexPath) as! MeditationSourceTableViewCell
        
        var description:String?
        var title:String?
        var author:String?
        var pubDate:Date?

        if let rssFeet = dataSourceArray[indexPath.row] as? RSSFeedItem{
            description = rssFeet.description?.trimmed
            title = rssFeet.title
            author = rssFeet.author
            pubDate = rssFeet.pubDate
            
            
        
//
//            let url = description?.replacingOccurrences(of: text, with: "")
//            url = [url stringByReplacingOccurrencesOfString:@"<img src=\"" withString:@""];
//            url = [url stringByReplacingOccurrencesOfString:@"\" align=\"left\"> " withString:@""];
            
//            rowDict[@"imageUrl"] = [NSURL URLWithString:url];
        }else if let atomFeet = dataSourceArray[indexPath.row] as? AtomFeedEntry{
            title = atomFeet.title
            
            description = "\(atomFeet.content?.value ?? "")"
            
            if let authors = atomFeet.authors,
                authors.count > 0{
                author = authors[0].name
            }
            pubDate = atomFeet.published ?? atomFeet.updated
        }
        
        
       let imagesInText = (description ?? "").components(separatedBy:"<img").count
        if  imagesInText == 2,
            !((description ?? "").contains("<i>")),
            !((description ?? "").contains("<div>")),
            !((description ?? "").contains("<br")),
            !((description ?? "").contains("<a")),
            let range = description?.range(of: "\">"){
            let urlText = String(description?[..<range.upperBound] ?? Substring())
            description = description?.replacingOccurrences(of: urlText, with: "")


            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: urlText, options: [], range: NSRange(location: 0, length: urlText.utf16.count))


            if matches.count > 0,
                let range = Range(matches[0].range, in: urlText) {
                let imageUrlString = urlText[range]
                let cell = tableView.dequeueReusableCell(withIdentifier: "MeditationImageCell", for: indexPath) as! MeditationTableViewCellSwift
                cell.titleLabel.text = title
                cell.descriptionLabel.text = description?.trimmed

                cell.authorLabel.text = author

                if let pubDate = pubDate{
                    cell.pubDateLabel.text = "\(DateFormatter.localizedString(from: pubDate, dateStyle: .short, timeStyle: .none))"
                }else{
                    cell.pubDateLabel.text = ""
                }

                if let imageURL = URL(string: String(imageUrlString)){
                    cell.descriptionImageView.setImageWith(imageURL, placeholderImage: nil)

                }
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MeditationTextCell", for: indexPath) as! MeditationTableViewCellSwift
            cell.titleLabel.text = title
            
            cell.descriptionLabel.text = description?.trimmed
            if cell.descriptionLabel.text!.contains("<i>")
                || cell.descriptionLabel.text!.contains("<a")
                || cell.descriptionLabel.text!.contains("<br")
                || cell.descriptionLabel.text!.contains("<div>")
                || cell.descriptionLabel.text!.contains("<img"){
                cell.descriptionLabel.attributedText = cell.descriptionLabel.text!.htmlToAttributedString
            }
            
            cell.authorLabel.text = author
            
            if let pubDate = pubDate{
                cell.pubDateLabel.text = "\(DateFormatter.localizedString(from: pubDate, dateStyle: .short, timeStyle: .none))"
            }else{
                cell.pubDateLabel.text = ""
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
            
        if let rssFeet = dataSourceArray[indexPath.row] as? RSSFeedItem,
            let link = rssFeet.link,
            let url = URL(string: link),
            let title = rssFeet.title
            {
                self.performSegue(withIdentifier: "MeditationLinkSegue", sender:
                    [
                        "url": url,
                        "title": title
                    ]
                )
        }else if let atomFeed = dataSourceArray[indexPath.row] as? AtomFeedEntry{
            let title = atomFeed.title ?? "Reflexión"
            for link in atomFeed.links ?? []{
                if link.attributes?.type == "text/html",
                    link.attributes?.rel == "alternate",
                    let href = link.attributes?.href,
                    let url = URL(string: href){
                    self.performSegue(withIdentifier: "MeditationLinkSegue", sender:
                        [
                            "url": url,
                            "title": title
                        ]
                    )
                    break
                }
                
            }
        }
    }
}


class MeditationTableViewCellSwift:UITableViewCell{
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var descriptionLabel:UILabel!
    @IBOutlet weak var authorLabel:UILabel!
    @IBOutlet weak var pubDateLabel:UILabel!
    @IBOutlet weak var descriptionImageView:UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        descriptionImageView.image = nil
    }
}
