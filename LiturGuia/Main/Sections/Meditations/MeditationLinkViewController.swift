//
//  MeditationLinkViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 10/20/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import KVNProgress
import Parse
import WebKit


class MeditationLinkViewController: UIViewController {
    @IBOutlet weak var webView:WKWebView!
    
    var isShowingAlert = false
    var sourceTitle:String?
    var url:URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        
        if let url = url{
            let urlRequest = URLRequest(url: url)
            webView.load(urlRequest)
        }
    }
    
    @IBAction func shareButtonTapped(sender: UIButton){
        KVNProgress.show()
        DynamicLinksManager.shareMeditation(withURL: url!, title: self.title) { (shareURL, error) in
            if let error = error{
                KVNProgress.dismiss()
                let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else if let shareURL = shareURL{
                
                let text = "Te comparto esta reflexión \(self.sourceTitle != nil ? "de " + self.sourceTitle! : "") a través de LiturGuía"
                let activityViewController = UIActivityViewController(activityItems: [shareURL, text], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: {
                    KVNProgress.dismiss()
                })
            }
        }
    }
}

extension MeditationLinkViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if !isShowingAlert{
            KVNProgress.show(withStatus: "", on: webView)
            isShowingAlert = true
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        isShowingAlert = false
        KVNProgress.dismiss()
    }
}
