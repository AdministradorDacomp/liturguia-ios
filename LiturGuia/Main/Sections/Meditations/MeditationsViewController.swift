//
//  MeditationsViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 10/19/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import KVNProgress
import Parse
import AFNetworking

class MeditationsViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    var dataSourceArray = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40.0
        
        reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailSegue",
            let vc = segue.destination as? MeditationDetailViewController,
            let sender = sender as? [String:Any],
            let url = sender["url"] as? URL,
            let sourceTitle = sender["title"] as? String
    {
            vc.feedURL = url
        vc.sourceTitle = sourceTitle
        }
    }
    
    //MARK: - Helpers
    func reloadData() {
        KVNProgress.show()
        
        MeditationsManager.shared.getMeditaions {[weak self] (sourcesArray, error) in
            guard let strongSelf = self else { return }
            KVNProgress.dismiss()
            if let error = error{
                let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                strongSelf.present(alert, animated: true, completion: nil)
            }else if let sourcesArray = sourcesArray{
                strongSelf.dataSourceArray = sourcesArray
                strongSelf.tableView.reloadData()
            }
        }
    }
    

}

extension MeditationsViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeditationSourceCell", for: indexPath) as! MeditationSourceTableViewCell
        
        let rowData = dataSourceArray[indexPath.row]
        
        if let title = rowData["title"] as? String{
            cell.titleLabel.text = title
        }
        if let views = rowData["views"] as? Int{
            cell.countLabel.text = "\(views)"
        }
        if let contentType = rowData["contentType"] as? String{
            switch contentType {
            case "imagen": cell.contentTypeImageView.image = #imageLiteral(resourceName: "meditationTypeImage")
            case "texto": cell.contentTypeImageView.image = #imageLiteral(resourceName: "meditationTypeText")
            case "audio": cell.contentTypeImageView.image = #imageLiteral(resourceName: "meditationTypeAudio")
            default: break
            }
        }
        
        if let sourceImageURL = rowData["imageURL"] as? URL{
            cell.sourceImageView.setImageWith(sourceImageURL, placeholderImage: #imageLiteral(resourceName: "pictures_menu_icon"))
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let rowData = dataSourceArray[indexPath.row]
        
        if let parseObject = rowData["parseObject"] as? PFObject{
            parseObject.incrementKey("vistas", byAmount: 1)
            parseObject.saveInBackground { (success, error) in
                if let error = error{
                    print(error.localizedDescription)
                }else{
                    MeditationsManager.shared.getMeditaions(forceUpdate: true, completion: {[weak self] (sourcesArray, error) in
                        guard let strongSelf = self else { return }
                        KVNProgress.dismiss()
                        if let error = error{
                            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            strongSelf.present(alert, animated: true, completion: nil)
                        }else if let sourcesArray = sourcesArray{
                            strongSelf.dataSourceArray = sourcesArray
                            strongSelf.tableView.reloadData()
                        }
                    })
                }
            }
        }


        if let url = rowData["sourceURL"] as? URL,
            let sourceTitle = rowData["title"] as? String{
            self.performSegue(
                withIdentifier: "DetailSegue",
                sender: [
                    "url":url,
                    "title": sourceTitle
                ]
            )
        }
    }
}

class MeditationSourceTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var countLabel:UILabel!
    @IBOutlet weak var contentTypeImageView:UIImageView!
    @IBOutlet weak var sourceImageView:UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentTypeImageView.image = nil
        sourceImageView.image = nil
    }
}
