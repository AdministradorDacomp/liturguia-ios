//
//  LectioDivineViewController.h
//  LiturGuia
//
//  Created by Krloz on 26/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LectioDivineEntity.h"
#import <WebKit/WebKit.h>
#import "StyleSelectorViewController.h"

@interface LectioDivineViewController: UIViewController<WKNavigationDelegate, StyleSelectorViewControllerDelegate, UIAlertViewDelegate>


@property (strong, nonatomic) IBOutlet WKWebView *webView;

@property (nonatomic, strong) LectioDivineEntity *lectioDivine;
@property (nonatomic, strong) NSDate *date;

@end
