//
//  LectioDivineViewController.m
//  LiturGuia
//
//  Created by Krloz on 26/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "LectioDivineViewController.h"
#import "WYPopoverController.h"
#import "WYStoryboardPopoverSegue.h"
#import "UIColor+Expanded.h"
#import "NSDate+ServerDate.h"
#import "NSDate+Utils.h"
#import "SuscriptionsViewController.h"
#import <Appirater/Appirater.h>

@interface LectioDivineViewController (){
    WYPopoverController* stylePopoverController;
}

@end

@implementation LectioDivineViewController;

@synthesize lectioDivine, date;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Appirater userDidSignificantEvent:YES];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    self.title = stringFromDate;
    
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [KVNProgress showWithStatus:@"Formando Lectio Divina..."];
    
    [self loadWebView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateStyleColor];
    
    PFUser *user = [PFUser currentUser];
    NSDate *serverDate = [NSDate serverDate];
    if (!serverDate || [serverDate isLaterThan:user[@"fVenceSuscripcion"]]) {
        if (![user[@"mensajeLectioDivina"] boolValue]) {
            NSString *messageErr = @"Si quieres ver las lecturas en la Lectio Divina es necesario que renueves tu suscripción.";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:self
                                               cancelButtonTitle:@"Ahora no"
                                               otherButtonTitles:@"Renovar", @"No volver a mostrar", nil];
            [av show];
        }
    }

        
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Lectio divina"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"StyleSelectorSegue"])
    {
        UINavigationController *navigationController = segue.destinationViewController;
        StyleSelectorViewController* contentVC = [[navigationController viewControllers] objectAtIndex:0];
        contentVC.preferredContentSize = CGSizeMake(280, 240);
        contentVC.delegate = self;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            SettingsPanelManager *controller = [SettingsPanelManager shared];
            [controller setUpSettingsPanelWithContentVC:(contentVC) toParent:(self)];
        }else{
            WYStoryboardPopoverSegue* popoverSegue = (WYStoryboardPopoverSegue*)segue;
            stylePopoverController = [popoverSegue popoverControllerWithSender:sender
                                                      permittedArrowDirections:WYPopoverArrowDirectionAny
                                                                      animated:YES];
            stylePopoverController.popoverLayoutMargins = UIEdgeInsetsMake(4, 4, 4, 4);
        }
    }
}

-(void)loadWebView{
    NSString *htmlString = lectioDivine.text;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *cssCode = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    
    NSString *textColor;
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        textColor = @"black";
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        textColor = [NSString stringWithFormat:@"#%@", [SEPIA_TEXT_COLOR hexStringValue]];
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        textColor = @"white";
    }
    
    NSString *bodyCode = [NSString stringWithFormat:@"body { margin: 15px; padding: 0px; text-align: justify; color: %@;}", textColor];
    cssCode = [NSString stringWithFormat:@"<style>%@ %@</style>", cssCode, bodyCode];
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<style></style>" withString:cssCode];
    
    [_webView loadHTMLString:htmlString baseURL:nil];
    
    _webView.navigationDelegate = self;
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [KVNProgress dismiss];
}

- (IBAction)shareButtonTapped:(id)sender {
    
    [_webView evaluateJavaScript:@"document. body.textContent" completionHandler:^(NSString *result, NSError * _Nullable error) {
        NSString *text = result;
        
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = text;
        
    //    self.view.window.userInteractionEnabled = NO;
    //    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
        [KVNProgress show];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            NSArray *objectsToShare = @[text];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            NSString *messageErr = @"Para compartir la Lectio Divina en Facebook es necesario pegar el contenido del portapapeles en la ventana que aparecerá al seleccionar esa opción.";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];

            
            [self presentViewController:activityVC animated:YES completion:^{
                [KVNProgress dismiss];
    //            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
    //            self.view.window.userInteractionEnabled = YES;
            }];
        });
    }];
    
}

-(void)updateStyleColor{
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        
        self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
        self.view.backgroundColor = [UIColor whiteColor];
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        
        self.navigationController.navigationBar.barTintColor = SEPIA_TEXT_COLOR;
        self.view.backgroundColor = SEPIA_BACKGROUND_COLOR;
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        self.view.backgroundColor = [UIColor blackColor];
    }
}

#pragma mark - Style's

- (void)fontSizeChanged{
    [self updateStyle];
}

-(void)fontChanged{
    [self updateStyle];
}

-(void)styleChanged{
    [self updateStyle];
    [self updateStyleColor];
}

-(void)updateStyle{
//    self.view.window.userInteractionEnabled = NO;
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.labelText = @"Actualizando...";
    
    [KVNProgress showWithStatus:@"Actualizando..."];
    
    [self loadWebView];
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
        if (buttonIndex == 1) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Left menu" bundle:nil];
            SuscriptionsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SuscriptionsViewController"];
            [vc setToModalView];
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
            nc.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:nc animated:YES completion:nil];
        }else if (buttonIndex == 2) {
            PFUser *user = [PFUser currentUser];
            user[@"mensajeLectioDivina"] = @YES;
            [user saveInBackground];
        }
    }
}

@end
