//
//  LectioDivineCalendarViewController.m
//  LiturGuia
//
//  Created by Krloz on 26/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "LectioDivineCalendarViewController.h"
#import "LectioDivineViewController.h"
#import "CalendarGenerator.h"
#import "NSDate+Utils.h"
#import "LiturgicalDate.h"
#import "NSDate+ServerDate.h"
#import "AppDelegate.h"

@interface LectioDivineCalendarViewController (){
    NSMutableArray *lectioAvailableArray;
    //    JTCalendar *calendar;
    NSManagedObjectContext *moc;
    NSMutableArray *selectedLectioDivine;
    JTCalendarDayView *dView;
    NSDate *selectedDate;
}

@end

@implementation LectioDivineCalendarViewController{
    NSArray *variableSpecialDates;
}

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *d = appDelegate;
    moc = d.managedObjectContext;
    
    [self initCalendar];
    
    variableSpecialDates = @[@"domingoDeRamos",@"ascension",@"santisimaTrinidad",@"bautismo",@"sagradaFamilia",@"cristoRey"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncFinished) name:@"LectioDivineSyncCompleted" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Calendario de Lectio divina"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)initCalendar{
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    _calendarManager.dateHelper.calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    
    [_calendarManager setDate:[NSDate date]];
    
    [self loadLectioDivineDownloaded];
    
    [_calendarManager reload];
}

-(void)syncFinished{
    [self loadLectioDivineDownloaded];
    
    [_calendarManager reload];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //    [calendar repositionViews];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"LectioDivineSegue"]) {
        LectioDivineViewController *vc = segue.destinationViewController;
        
        //        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        //        NSEntityDescription *entity =
        //        [NSEntityDescription entityForName:@"LectioDivineEntity"
        //                    inManagedObjectContext:moc];
        //        [request setEntity:entity];
        //
        //        NSTimeInterval length;
        //
        //        [[NSCalendar currentCalendar] rangeOfUnit:NSMinuteCalendarUnit
        //                                        startDate:&sender
        //                                         interval:&length
        //                                          forDate:sender];
        //
        //        NSDate *endDate = [sender dateByAddingTimeInterval:length];
        //
        //        CalendarGenerator *object = [CalendarGenerator sharedInstance];
        //        NSDictionary *info = [object liturgyInfoForDate:sender];
        //
        //        NSPredicate *predicate;
        //        if (info[@"especial"]) {
        //            predicate = [NSPredicate predicateWithFormat:@"specialDate == %@", info[@"especial"]];
        //        }else{
        //            predicate = [NSPredicate predicateWithFormat:
        //                         @"week == %@ AND day = %@ AND time == %@ AND cycle",
        //                         info[@"semana"],
        //                         @([sender dayOfTheWeek]),
        //                         info[@"tiempo"], @"C"];
        //        }
        
        //        [request setPredicate:predicate];
        
        //        NSError *error;
        //        NSArray *array = [moc executeFetchRequest:request error:&error];
        //        if (array != nil && array.count > 0) {
        //            vc.lectioDivine = array[0];
        //        }
        vc.lectioDivine = sender;
        vc.date = [NSDate dateWithLocalTime:selectedDate];
    }
}

#pragma mark - JTCalendarDataSource

- (BOOL)haveEventForDay:(NSDate *)date{
    date = [LiturGuia dateWithoutTime:date];
//    date = [NSDate removeTimeFromDate:date];
    
    
    LiturgicalDate *liturgicalDate = [[CalendarGenerator sharedInstance] liturgicalInfoForDate:[NSDate dateWithLocalTime:date]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    for (LectioDivineEntity *lectioDivine in lectioAvailableArray) {
        NSInteger dayOfWeek = [[NSDate dateWithLocalTime: liturgicalDate.date] dayOfTheWeek];
        BOOL sameTime = [liturgicalDate.time isEqualToString:lectioDivine.time];
        BOOL sameWeek = [liturgicalDate.week isEqual:lectioDivine.week];
        BOOL sameDay = [lectioDivine.day integerValue] == dayOfWeek;
        BOOL sunday = dayOfWeek == 1;
        BOOL sameCycle = [liturgicalDate.cycle isEqual:lectioDivine.cycle];
        BOOL sameEven = liturgicalDate.even == lectioDivine.even;
        BOOL isSpecialDate = (liturgicalDate.specialDate && ![liturgicalDate.specialDate isEqualToString:@""]) && ![variableSpecialDates containsObject:liturgicalDate.specialDate];
        BOOL sameSpecialDate = isSpecialDate && [lectioDivine.specialDate isEqualToString:liturgicalDate.specialDate];
        
        if (
            (!isSpecialDate
             && sameTime
             && sameWeek
             && sameDay
             && ((sunday && sameCycle) || (!sunday && sameEven))
             )
            ||
            (isSpecialDate
             && sameSpecialDate)
            )
        {
            
           return YES;
        }
    }
    
    
    return NO;
}
//
//- (void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date{
//    date = [self dateWithoutTime:date];
//    if ([missalsAvailableArray containsObject:date]) {
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self performSegueWithIdentifier:@"MissalSegue" sender:date];
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
//        });
//    }else{
//
//        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
//
//        int weekday = (int)[comps weekday];
//
//        NSDate *weekStartDate = [date dateByAddingTimeInterval:-weekday*24*60*60];
//        NSDate *weekEndDate = [weekStartDate dateByAddingTimeInterval:6*24*60*60];
//
//        PFQuery *query = [PFQuery queryWithClassName:@"Misal"];
//        [query whereKey:@"fecha" greaterThanOrEqualTo:weekStartDate];
//        [query whereKey:@"fecha" lessThanOrEqualTo:weekEndDate];
//
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//            if (!error) {
//                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                self.view.window.userInteractionEnabled = YES;
//                if (objects.count == 7) {
//                    selectedMissals = objects;
//
//                    PFUser *user = [PFUser currentUser];
//
//                    if ([user[@"creditos"] intValue] < [kWeekMissalPrice intValue]) {
//                        NSString *messageErr = @"No hay suficientes créditos para descargar este misal. Para poder descargarlo es necesario adquirir mas créditos.";
//                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                     message:messageErr
//                                                                    delegate:nil
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil];
//                        [av show];
//                        return;
//                    }
//
//                    NSString *messageErr = [NSString stringWithFormat:@"Aún no ha adquirido los misales de esa semana. El costo de los 7 misales es de %d créditos y usted cuenta con %d créditos. ¿Desea descargar estos misales?", [kWeekMissalPrice intValue], [user[@"creditos"] intValue]];
//                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                 message:messageErr
//                                                                delegate:self
//                                                       cancelButtonTitle:@"No"
//                                                       otherButtonTitles:@"Si",nil];
//                    [av show];
//
//                    selectedDate = date;
//
//
//                }else{
//                    NSString *messageErr = @"El misal seleccionado no está disponible";
//                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                 message:messageErr
//                                                                delegate:nil
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil];
//                    [av show];
//
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"kJTCalendarDaySelected" object:nil];
//                }
//            }
//        }];
//    }
//}
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    //here we have to get the time difference between GMT and the current users Date (its in seconds)
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:destinationGMTOffset];
    
    // Test if the dayView is from another month than the page
    // Use only in month mode for indicate the day of the previous or next month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:date isTheSameDayThan:dayView.date] && !(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date])){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [kNavigationBarColor colorWithAlphaComponent:0.5f];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = kNavigationBarColor;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = kNavigationBarColor;
        dayView.textLabel.textColor = [UIColor darkGrayColor];
    }
    

    // Your method to test if a date have an event for example
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}


#pragma mark - Views customization

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Heavy" size:18];
    label.backgroundColor = kNavigationBarColor;
    label.textColor = [UIColor whiteColor];
    
    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    menuItemView.text = [[dateFormatter stringFromDate:date] capitalizedString];
}

- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *view = [JTCalendarWeekDayView new];
    
    for(UILabel *label in view.dayViews){
        label.textColor = [UIColor blackColor];
        label.font = [UIFont fontWithName:@"Avenir-Black" size:12];
    }
    
    return view;
}

- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    
    view.textLabel.font = [UIFont fontWithName: @"Avenir-Medium" size:16];
    
    //    view.circleRatio = 1.5;
    //    view.dotRatio = 1. / .9;
    
    return view;
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    
    [KVNProgress showWithStatus:@"Cargando..."];
    // Use to indicate the selected date
    _dateSelected = dayView.date;
    dView = dayView;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [self->_calendarManager reload];
                    } completion:nil];
    
    // Load the previous or next page if touch a day from another month
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
//    selectedDate = [NSDate removeTimeFromDate:dayView.date];
    selectedDate = [LiturGuia dateWithoutTime:dayView.date];
    
    LiturgicalDate *liturgicalDate = [[CalendarGenerator sharedInstance] liturgicalInfoForDate:[NSDate dateWithLocalTime:selectedDate]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    selectedLectioDivine = [NSMutableArray array];
    for (LectioDivineEntity *lectioDivine in lectioAvailableArray) {
        NSInteger dayOfWeek = [[NSDate dateWithLocalTime: selectedDate] dayOfTheWeek];
        BOOL sameTime = [liturgicalDate.time isEqualToString:lectioDivine.time];
        BOOL sameWeek = [liturgicalDate.week isEqual:lectioDivine.week];
        BOOL sameDay = [lectioDivine.day integerValue] == dayOfWeek;
        BOOL sunday = dayOfWeek == 1;
        BOOL sameCycle = [liturgicalDate.cycle isEqual:lectioDivine.cycle];
        BOOL sameEven = liturgicalDate.even == lectioDivine.even;
        BOOL isSpecialDate = (liturgicalDate.specialDate && ![liturgicalDate.specialDate isEqualToString:@""]) && ![variableSpecialDates containsObject:liturgicalDate.specialDate];
        BOOL sameSpecialDate = isSpecialDate && [lectioDivine.specialDate isEqualToString:liturgicalDate.specialDate];
        
        if (
                (!isSpecialDate
                    && sameTime
                    && sameWeek
                    && sameDay
                    && ((sunday && sameCycle) || (!sunday && sameEven))
                 )
            ||
                (isSpecialDate
                    && sameSpecialDate)
                )
            {
        
            [selectedLectioDivine addObject:lectioDivine];
        }
    }
    
    if (selectedLectioDivine.count > 0) {
        [KVNProgress dismiss];
//        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = YES;
        if (selectedLectioDivine.count == 1) {
            [self showLectioDivine:selectedLectioDivine[0]];
        }else{
            [self showVersionsToDownload];
        }
    }else{
        //        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        //        [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        //        NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
        //
        //        int weekday = (int)[comps weekday];
        //
        //
        //        NSDate *weekStartDate = [date dateByAddingTimeInterval:-(weekday-1)*24*60*60];
        //        NSDate *weekEndDate = [weekStartDate dateByAddingTimeInterval:6*24*60*60];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        CalendarGenerator *object = [CalendarGenerator sharedInstance];
        LiturgicalDate *liturgicalDate = [object liturgicalInfoForDate:[NSDate dateWithLocalTime:selectedDate]];
        
        
        PFQuery *query = [PFQuery queryWithClassName:@"LectioDivina"];
        [query whereKey:@"idioma" equalTo:[userDefaults objectForKey:@"language"]];
        
        BOOL isSpecialDate = (liturgicalDate.specialDate && ![liturgicalDate.specialDate isEqualToString:@""]) && ![variableSpecialDates containsObject:liturgicalDate.specialDate];

        
        if (isSpecialDate) {
            [query whereKey:@"fechaEspecial" equalTo:liturgicalDate.specialDate];
        }else{
            [query whereKey:@"ciclo" equalTo:liturgicalDate.cycle];
            [query whereKey:@"semana" equalTo:liturgicalDate.week];
            [query whereKey:@"tiempo" equalTo:liturgicalDate.time];
            
            NSInteger dayOfWeek = [[NSDate dateWithLocalTime:selectedDate] dayOfTheWeek];
            if (dayOfWeek == 1) {
                [query whereKey:@"ciclo" equalTo:liturgicalDate.cycle];
            }else{
                [query whereKey:@"par" equalTo:liturgicalDate.even];
            }
            [query whereKey:@"dia" equalTo:@(dayOfWeek)];
        }
        
        //        }
        //        [query whereKey:@"version" equalTo:@"es"];
        

        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                
                if (objects && objects.count != 0) {
                    
                    for (PFObject *parseLectioDivine in objects) {
                        NSEntityDescription *entity =
                        [NSEntityDescription entityForName:@"LectioDivineEntity"
                                    inManagedObjectContext:[LiturGuia moc]];
                        
                        LectioDivineEntity *lectioDivine = [[LectioDivineEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[LiturGuia moc]];
                        
                        lectioDivine.week = parseLectioDivine[@"semana"];
                        lectioDivine.time = parseLectioDivine[@"tiempo"];
                        lectioDivine.day = parseLectioDivine[@"dia"];
                        lectioDivine.cycle = parseLectioDivine[@"ciclo"];
                        lectioDivine.language = parseLectioDivine[@"idioma"];
                        lectioDivine.text = parseLectioDivine[@"texto"];
                        lectioDivine.specialDate = parseLectioDivine[@"fechaEspecial"];
                        lectioDivine.objectId = parseLectioDivine.objectId;
                        lectioDivine.lectioDivineVersion = parseLectioDivine[@"version"];
                        [self->selectedLectioDivine addObject:lectioDivine];
                        //                        [array addObject:lectioDivine.objectId];
                    }
                    
                    
                    
                    PFUser *user = [PFUser currentUser];
                    NSDate *serverDate = [NSDate serverDate];
                    if (serverDate && [serverDate isEarlierThan:user[@"fVenceSuscripcion"]]) {
                        PFQuery *missalQuery = [PFQuery queryWithClassName:@"Misal"];
                        [missalQuery whereKey:@"fecha" equalTo:self->selectedDate];
                        [missalQuery whereKey:@"idioma" equalTo:[userDefaults objectForKey:@"language"]];
                        PFObject *object = [missalQuery getFirstObject];
                        if (object) {
                            NSMutableString *textToAdd = [NSMutableString string];
                            for (LectioDivineEntity *lectioDivine in self->selectedLectioDivine) {
                                NSData *data = [object[@"jsonMisal"] dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0
                                                                                       error:nil];
                                for (NSDictionary *section in json[@"secciones"]) {
                                    NSString *sectionName = section[@"seccion"];
                                    if ([sectionName isEqualToString:@"PL"] || [sectionName isEqualToString:@"SL"] || [sectionName isEqualToString:@"EVA"]) {
                                        [textToAdd appendString:@"<p align=\"center\"><b>"];
                                        [textToAdd appendString:section[@"titulo"]];
                                        [textToAdd appendString:@" "];
                                        [textToAdd appendString:section[@"versiculos"]];
                                        [textToAdd appendString:@"</b></p>"];
                                        [textToAdd appendString:@"<p>"];
                                        [textToAdd appendString:section[@"texto"]];
                                        [textToAdd appendString:@"</p>"];
                                    }else if ([sectionName isEqualToString:@"SR"]) {
                                        [textToAdd appendString:@"<p align=\"center\"><b>"];
                                        [textToAdd appendString:section[@"titulo"]];
                                        [textToAdd appendString:@"</b></p>"];
                                        
                                        
                                        NSString *text = section[@"texto"];
                                        text = [text stringByReplacingOccurrencesOfString:section[@"respuesta"] withString:[NSString stringWithFormat:@"</p><p align=\"right\"><i>%@</i></p><p>",section[@"respuesta"]]];
                                        
                                        [textToAdd appendString:@"<p>"];
                                        [textToAdd appendString:section[@"respuesta"]];
                                        [textToAdd appendString:@"</p>"];
                                        [textToAdd appendString:@"<p align=\"right\"><i>"];
                                        [textToAdd appendString:section[@"respuesta"]];
                                        [textToAdd appendString:@"</i></p>"];
                                        [textToAdd appendString:@"<p>"];
                                        [textToAdd appendString:text];
                                        [textToAdd appendString:@"</p>"];
                                        
                                    }
                                }
                                
                                lectioDivine.text = [lectioDivine.text stringByReplacingOccurrencesOfString:@"{lecturas}" withString:[textToAdd stringByReplacingOccurrencesOfString:@"\n" withString:@"</br>"]];
                            }
                        }
//                        else{
                        

                        }
                        
                        
//                    }else{
//                        for (LectioDivineEntity *lectioDivine in selectedLectioDivine) {
//                            lectioDivine.text = [lectioDivine.text stringByReplacingOccurrencesOfString:@"{lecturas}" withString:@"<p style=\"text-align:center; color:red\"><strong>(Para ver las lecturas correspondientes a éste día es necesario que renueve su suscripción)</strong> </p>"];
//                        }
//                    }
                    
                    for (LectioDivineEntity *lectioDivine in self->selectedLectioDivine) {
                        lectioDivine.text = [lectioDivine.text stringByReplacingOccurrencesOfString:@"{lecturas}" withString:@""];
                    }
                    
                    [LiturGuia saveContext];
                    [self syncFinished];
                    
                    if (self->selectedLectioDivine.count == 1) {
                        [self showLectioDivine:self->selectedLectioDivine[0]];
                    }else{
                        [self showVersionsToDownload];
                    }
                    [KVNProgress dismiss];
//                    [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                    self.view.window.userInteractionEnabled = YES;
                }else{
                    [KVNProgress dismiss];
//                    [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                    self.view.window.userInteractionEnabled = YES;
                    NSString *messageErr = @"La Lectio Divina seleccionada no está disponible";
                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                 message:messageErr
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                    [av show];
                    self->_dateSelected = nil;
                    [self->_calendarManager reload];
                }
            }else{
                [KVNProgress dismiss];
//                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                self.view.window.userInteractionEnabled = YES;
            }
        }];
    }
    
    
}

-(void)showVersionsToDownload{
    NSString *messageErr = @"Seleccione una versión:";
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:messageErr
                                                delegate:self
                                       cancelButtonTitle:nil
                                       otherButtonTitles:nil];
    for(LectioDivineEntity *lectioDivine in selectedLectioDivine) {
        [av addButtonWithTitle:lectioDivine.lectioDivineVersion];
    }
    [av show];
}

-(void)showLectioDivine:(LectioDivineEntity*)lectioDivine{
    //                        self.view.window.userInteractionEnabled = NO;
    //                        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"LectioDivineSegue" sender:lectioDivine];
        //                            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
        //                            self.view.window.userInteractionEnabled = YES;
    });
}

#pragma mark - Load data

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (void)loadLectioDivineDownloaded{
    lectioAvailableArray = [NSMutableArray new];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:@"LectioDivineEntity"
                inManagedObjectContext:moc];
    [request setEntity:entity];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array != nil) {
        for (LectioDivineEntity *lectioDivine in array) {
            [lectioAvailableArray addObject:lectioDivine];
        }
    }
}

#pragma  mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self showLectioDivine:selectedLectioDivine[buttonIndex]];
}

//-(void)downloadMissal{
//    PFUser *user = [PFUser currentUser];
//    int weekMissalPrice = [[[NSUserDefaults standardUserDefaults] stringForKey:@"weekMissalPrice"] intValue];
//    [user incrementKey:@"creditos" byAmount:@(-weekMissalPrice)];
//
//    NSMutableArray *array = [NSMutableArray arrayWithArray:user[@"misalesDescargados"]];
//    for (PFObject *parseMissal in selectedMissals) {
//        [array addObject:parseMissal.objectId];
//
//        LectioDivineEntity *missal = [NSEntityDescription
//                                insertNewObjectForEntityForName:@"MissalEntity"
//                                inManagedObjectContext:moc];
//        missal.title = parseMissal[@"titulo"];
//        missal.time = parseMissal[@"tiempo"];
//        missal.cycle = parseMissal[@"ciclo"];
//        missal.date = [LiturGuia dateWithoutTime:parseMissal[@"fecha"]];
//        missal.type = parseMissal[@"tipo"];
//        missal.language = parseMissal[@"idioma"];
//        missal.color = parseMissal[@"color"];
//        missal.jsonMissal = parseMissal[@"jsonMisal"];
//        missal.objectId = parseMissal.objectId;
//    }
//    user[@"misalesDescargados"] = array;
//
//    [user saveInBackground];
//    [LiturGuia saveContext];
//
//
//    [self syncFinished];
//
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        [self performSegueWithIdentifier:@"MissalSegue" sender:selectedDate];
//        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = YES;
//    });
//}


@end
