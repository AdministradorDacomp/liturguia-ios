//
//  StyleConscienceViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 6/5/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit

//MARK: - Protocols
protocol StyleConscienceViewControllerDelegate {
    func fontSizeChanged()
    func fontChanged()
}

//MARK: -
class StyleConscienceViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet var whiteButton:UIButton!
    @IBOutlet var sepiaButton:UIButton!
    @IBOutlet var nightButton:UIButton!
    @IBOutlet var brightnessSlider:UISlider!
    @IBOutlet var increaseFontSizeButton:UIButton!
    @IBOutlet var decreaseFontSizeButton:UIButton!
    @IBOutlet var fontNameLabel:UILabel!
    
    //MARK: - Vars
    var delegate:StyleConscienceViewControllerDelegate? = nil

    //MARK: - Life cycle
    override func viewDidLoad(){
        super.viewDidLoad()
        
        fontNameLabel.text = UserDefaults.standard.string(forKey:"fontDisplayNameConscience")
        brightnessSlider.value = Float(UIScreen.main.brightness)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let vc = segue.destination as? StyleConscienceFontViewController{
            vc.delegate = self
            vc.preferredContentSize = CGSize(width: 280, height: self.view.bounds.size.height)
        }
    }

    //MARK: - Button actions
    @IBAction func brightnessValueChanged(sender:UISlider){
        UIScreen.main.brightness = CGFloat(sender.value)
    }
    
    @IBAction func fontSizeButtonTapped(sender:UIButton){
        var fontSize = UserDefaults.standard.float(forKey:"fontSizeConscience")
        if sender == increaseFontSizeButton {
            fontSize += 2
        }else if sender == decreaseFontSizeButton{
            fontSize -= 2
        }
        UserDefaults.standard.set(fontSize, forKey: "fontSizeConscience")
        UserDefaults.standard.synchronize()
        
        
        
        if fontSize <= 10{
            decreaseFontSizeButton.isEnabled = false
        }else{
            decreaseFontSizeButton.isEnabled = true
        }
        if fontSize >= 40{
            increaseFontSizeButton.isEnabled = false
        }else{
            increaseFontSizeButton.isEnabled = true
        }
        delegate?.fontSizeChanged()
    }
}

//MARK: - StyleConscienceFontViewControllerDelegate
extension StyleConscienceViewController:StyleConscienceFontViewControllerDelegate{
    func fontChanged(){
        fontNameLabel.text = UserDefaults.standard.string(forKey:"fontDisplayNameConscience")
        delegate?.fontChanged()
    }
}

//MARK: -
protocol StyleConscienceFontViewControllerDelegate{
    func fontChanged()
}

class StyleConscienceFontViewController: UIViewController {
    var fontFamilyNameArray = ["Arial", "Helvetica Neue", "Georgia", "Palatino", "Times New Roman"]
    var fontNameArray = ["Arial", "HelveticaNeue", "Georgia", "Palatino", "TimesNewRomanPSMT"]
    
    var delegate:StyleConscienceFontViewControllerDelegate? = nil
    
    override func viewDidLoad(){
        super.viewDidLoad()
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension StyleConscienceFontViewController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fontNameArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FontCell", for: indexPath)
        
        let familyName = fontFamilyNameArray[indexPath.row]
        cell.textLabel?.text = familyName
        cell.textLabel?.font = UIFont(name: fontNameArray[indexPath.row], size: cell.textLabel?.font.pointSize ?? 0.0)
        
        cell.selectionStyle = .none
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.popToRootViewController(animated: true)
        
        UserDefaults.standard.set(fontNameArray[indexPath.row], forKey: "fontDisplayNameConscience")
        UserDefaults.standard.set(fontFamilyNameArray[indexPath.row], forKey: "fontNameConscience")
        UserDefaults.standard.synchronize()
        delegate?.fontChanged()
    }
}

