//
//  ConsciencePasswordViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/20/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit

//MARK: - Protocols
@objc protocol ConsciencePasswordViewControllerDelegate {
    func grantCancelled()
    func permissionGranted()
}

//MARK: -
@objc class ConsciencePasswordViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!

    @IBOutlet weak var acceptButtonTopConstraint: NSLayoutConstraint!
    //MARK: - Vars
    var currentPassword:String?
    @objc var delegate: ConsciencePasswordViewControllerDelegate?
    var isSecurityAlert = false

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordDidChanged()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        passwordTextField.becomeFirstResponder()
    }
    
    //MARK: - Button actions
    @IBAction func saveButtonTapped(_ sender: UIButton?){
        if let currentPassword = currentPassword{
            if currentPassword != passwordTextField.text{
                let alert = UIAlertController(title: nil, message: "La contraseña ingresada no coincide con la que fue registrada para el examen de conciencia.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar" , style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Crear un nuevo examen" , style: .destructive, handler: {[weak self](action) in
                    guard let strongSelf = self else { return }
                    let alert = UIAlertController(title: "Nuevo examen de conciencia", message: "¿Está seguro que quiere eliminar el examen de conciencia actual y crear uno nuevo?", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Sí", style: .destructive, handler: { (action) in
                        KeyChain.delete(key: "ConsciencePassword")
                        KeyChain.delete(key: "ExaminationOfConscience")
                        KeyChain.delete(key: "ExaminationOfConscienceInitialPrayers")
                        KeyChain.delete(key: "ManualExaminationOfConscience")
                        
                        strongSelf.passwordTextField.becomeFirstResponder()
                        strongSelf.currentPassword = nil
                        strongSelf.passwordDidChanged()
                    }))
                    alert.addAction(UIAlertAction(title: "No" , style: .default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                delegate?.permissionGranted()
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            var errorTitle:String? = nil
            var errorMessage:String? = nil
            
            if passwordTextField.text == ""{
                errorMessage = "Es necesario ingresar una contraseña válida."
                errorTitle = "Contraseña vacía"
            }else if passwordTextField.text != confirmPasswordTextField.text{
                errorMessage = "Las contraseñas ingresadas no coinciden."
                errorTitle = "Contraseñas diferentes"
            }
            
            if let errorMessage = errorMessage{
                let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                savePasswordAndClose()
            }
        }
       
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton){
        delegate?.grantCancelled()
        if isSecurityAlert{
            self.dismiss(animated: true, completion: {
                if let concienceVC = ConscienceManager.shared.conscienceNVC,
                    let nvc = concienceVC.navigationController{
                    nvc.popToRootViewController(animated: true)
                }
            })
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: - Helpers
    func passwordDidChanged() {
        if let consciencePassword = KeyChain.loadString(key: "ConsciencePassword"){
            currentPassword = consciencePassword
            confirmPasswordTextField.isHidden = true
            confirmPasswordLabel.isHidden = true
            messageLabel.text = "Por seguridad es necesario proporcionar la contraseña ingresada en la creación del examen de conciencia. Los pecados registrados en la aplicación son confidenciales y solo la persona que los ingresó debe verlos."
            acceptButtonTopConstraint.constant = 25
        }else{
            passwordTextField.text = ""
            confirmPasswordTextField.text = ""
            confirmPasswordTextField.isHidden = false
            confirmPasswordLabel.isHidden = false
            messageLabel.text = "Para asegurar la confidencialidad del examen de conciencia, es necesario ingresar una contraseña."
            acceptButtonTopConstraint.constant = 85.5
        }
    }
    
    func savePasswordAndClose()  {
        KeyChain.saveString(key: "ConsciencePassword" , value: passwordTextField.text ?? "")
        
        delegate?.permissionGranted()
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UITextFieldDelegate
extension ConsciencePasswordViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag: Int = textField.tag + 1
        let nextResponder: UIResponder? = textField.superview?.viewWithTag(nextTag)
        if nextResponder != nil {
            nextResponder?.becomeFirstResponder()
        }else {
            textField.resignFirstResponder()
        }
        
        if (textField == passwordTextField && confirmPasswordTextField.isHidden) || textField == confirmPasswordTextField{
            saveButtonTapped(nil)
        }
        
        return false
    }
}
