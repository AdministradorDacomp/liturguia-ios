//
//  ConfesionViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/20/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import Material
import Parse
import WYPopoverController
import MaryPopin

//MARK: -
class ConfessionViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var lastConfessionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var prayerButton: UIButton!
    
    //MARK: - Vars
    var dataSourceArray = [[String:Any]]()
    var selectedIndexPaths:Set<IndexPath> = []
    var desintegrate = false
    var stylePopoverController: WYPopoverController? = nil
    var totalQuestions = 0
    var fontName = UserDefaults.standard.string(forKey:"fontDisplayNameConscience") ?? "HelveticaNeue-Medium"
    var fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
    var popUpVC:ContritionContainerViewController?
    private lazy var useCaseController = SettingsPanelManager.shared
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        if let user = PFUser.current(){
            lastConfessionLabel.text = user["ultimaConfesion"] as? String
        }
        
        prayerButton.titleLabel?.lineBreakMode = .byWordWrapping
        prayerButton.titleLabel?.numberOfLines = 2
        prayerButton.titleLabel?.textAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadExam()
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StyleSelectorSegue"{
            let nvc = segue.destination as! UINavigationController
            if let contentVC = nvc.viewControllers[0] as? StyleConscienceViewController{
                contentVC.preferredContentSize = CGSize(width: 280, height: 160)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
    }
    
    //MARK: - Helpers
    func reloadExam(reloadTable:Bool = true) {
        ConscienceManager.shared.getQuestionsFromArray(KeyChain.loadArray(key: "ExaminationOfConscience") as? [String] ?? []) { ( exams) in
            var questionsArray = [[String:Any]]()
            var mutableExams = exams
            
            self.totalQuestions = KeyChain.loadArray(key: "ExaminationOfConscience")?.count ?? 0
            
            if let manualQuestions = KeyChain.loadArray(key: "ManualExaminationOfConscience"),
                manualQuestions.count > 0{
                self.totalQuestions += manualQuestions.count
                
                for question in KeyChain.loadArray(key: "ManualExaminationOfConscience") ?? []{
                    questionsArray.append(["text":question])
                }
                mutableExams["manualAdded"] = [
                    "title" : "Agregados manualmente",
                    "examOrder" : -1,
                    "questions": questionsArray
                ]
            }
            
            self.dataSourceArray = mutableExams.toSortedArrayByKey("examOrder", otherKey:"order")
            
            if reloadTable{
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Button actions
    @IBAction func endButtonTapped(_ sender:Any?){
        let alert = UIAlertController(title: nil, message: "¿Desea terminar la confesión? (el examen de conciencia será eliminado permanentemente)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:   "Sí", style: .default, handler: {[weak self] (action) in
            guard let strongSelf = self else { return }
            
            KeyChain.delete(key: "ConsciencePassword")
            KeyChain.delete(key: "ExaminationOfConscience")
            KeyChain.delete(key: "ExaminationOfConscienceInitialPrayers")
            KeyChain.delete(key: "ManualExaminationOfConscience")
            
            strongSelf.desintegrate = true
            strongSelf.tableView.reloadData()
            
            if let user = PFUser.current(){
                let dataString = DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .none)
                
                user["ultimaConfesion"] = dataString
                user.saveInBackground()
            }
            
            strongSelf.view.isUserInteractionEnabled = false
            strongSelf.popUpVC = strongSelf.storyboard?.instantiateViewController(withIdentifier: "ContritionContainerViewController") as? ContritionContainerViewController
            if strongSelf.popUpVC != nil {
                strongSelf.popUpVC?.view.frame.size = CGSize(width: 300, height: 360)
                strongSelf.popUpVC?.view.layer.cornerRadius = 5.0
                strongSelf.popUpVC?.view.clipsToBounds = true
                
                strongSelf.popUpVC?.setPopinTransitionStyle(.slide)
                strongSelf.popUpVC?.setPopinTransitionDirection(.top)
                strongSelf.popUpVC?.setPopinOptions(BKTPopinOption.disableAutoDismiss)
                strongSelf.popUpVC?.delegate = strongSelf
                
                strongSelf.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                

                
                let vc = strongSelf.navigationController != nil ? strongSelf.navigationController! : strongSelf

                
                vc.presentPopinController(strongSelf.popUpVC!, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func showInitialPrayerButtonTapped(_ sender:CheckButton){
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConsciencePrayersViewController"){
            let nvc = UINavigationController(rootViewController: vc)
            nvc.modalPresentationStyle = .fullScreen
            self.navigationController?.present(nvc, animated: true, completion: nil)
        }
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource

extension ConfessionViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < dataSourceArray.count {
            let sectionDictionary = dataSourceArray[section]
            if let questions = sectionDictionary["questions" ] as? [[String:Any]]{
                return questions.count + 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionDictionary = dataSourceArray[indexPath.section]
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupSinCell", for: indexPath) as! GroupSinCell
            
            cell.groupLabel.text = sectionDictionary["title"] as? String ?? ""
            cell.subtitleGroupLabel.text = sectionDictionary["subtitle"] as? String ?? ""
            cell.groupLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0) + 4.0)?.bold()
            cell.subtitleGroupLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0))?.italic()
            
            if desintegrate{
                let duration = Double.random(in: 2.0 ..< 6.0)
                
                cell.groupLabel.fadeOut(duration: duration)
                cell.subtitleGroupLabel.fadeOut(duration: duration)
            }
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SinCell", for: indexPath) as! SinCell
            let questions = sectionDictionary["questions" ] as? [[String:Any]] ?? []
            if questions.count > indexPath.row - 1{
                let question = questions[indexPath.row - 1]["text" ] as? String ?? ""
                cell.sinLabel.text = question
                
                cell.sinLabel.font = UIFont(name: fontName, size: fontSize >= 8 ? fontSize : 8.0)
                
            }
            cell.row = indexPath.row
            cell.section = indexPath.section
            cell.delegate = self
            
            cell.checkButton?.iconSize = 25.0
            cell.checkButton?.pulseColor = .red
            cell.checkButton?.setIconColor(#colorLiteral(red: 0.2470588235, green: 0.3254901961, blue: 0.6901960784, alpha: 1), for: .selected)
            
            cell.checkButton?.setSelected(selectedIndexPaths.contains(indexPath), animated: false)
            
            if desintegrate{
                let duration = Double.random(in: 2.0 ..< 6.0)
                cell.sinLabel.fadeOut(duration: duration)
                UIView.animate(withDuration: duration) {
                    cell.checkButton?.alpha = 0.0
                }
            }
            
            
            cell.sinLabel.font = UIFont(name: fontName, size: fontSize >= 8 ? fontSize : 8.0)
            
            return cell
        }
    }
}

//MARK: - StyleConscienceViewControllerDelegate
extension ConfessionViewController:StyleConscienceViewControllerDelegate{
    func fontSizeChanged() {
        fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
        tableView.reloadData()
    }
    
    func fontChanged() {
        fontName = UserDefaults.standard.string(forKey:"fontNameConscience") ?? "HelveticaNeue-Medium"
        tableView.reloadData()
    }
}

//MARK: - SinCellDelegate

extension ConfessionViewController:SinCellDelegate{
    func userDidTapCheckButton(button: CheckButton, inSection section: Int, atRow row: Int) {
        let indexPath = IndexPath(row: row, section: section)
        
        if selectedIndexPaths.contains(indexPath){
            selectedIndexPaths.remove(indexPath)
        }else{
            selectedIndexPaths.insert(indexPath)
        }
        

    }
    
    func userDidTapDeleteButton(button: UIButton, inSection section: Int, atRow row: Int) {
        
    }
}

extension ConfessionViewController:ContritionViewControllerDelegate{
    func dismissContrition() {
        
        let vc = self.navigationController != nil ? self.navigationController! : self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        

        vc.dismissCurrentPopinController(animated: true) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.popUpVC = nil
            strongSelf.navigationController?.popToRootViewController(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if let topVC = UIApplication.topViewController(),
                    let popUpVC = topVC.storyboard?.instantiateViewController(withIdentifier: "PurposeViewController") as? PurposeViewController{
                    popUpVC.view.frame.size = CGSize(width: 300, height: 200)
                    popUpVC.view.layer.cornerRadius = 5.0
                    popUpVC.view.clipsToBounds = true
                    
                    popUpVC.setPopinTransitionStyle(.slide)
                    popUpVC.setPopinTransitionDirection(.top)
                    let blurParameters = BKTBlurParameters()
                    blurParameters.alpha = 1.0
                    blurParameters.radius = 8
                    blurParameters.saturationDeltaFactor = 0.9
                    blurParameters.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
                    popUpVC.setBlurParameters(blurParameters)
                    popUpVC.setPopinOptions(BKTPopinOption(rawValue: BKTPopinOption.blurryDimmingView.rawValue))
                    
                    topVC.presentPopinController(popUpVC, animated: true, completion: nil)
                }
            }
        }
    }
}


