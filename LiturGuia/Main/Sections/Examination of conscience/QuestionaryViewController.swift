//
//  QuestionaryViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/20/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import DropDown
import Material
import WYPopoverController

//MARK: - UIViewController
class QuestionaryViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var typeButton: UIButton!
    
    //MARK: - Vars
    var dataSourceArray = [[String:Any]]()
    var selectedQuestions:NSMutableOrderedSet?
    var stylePopoverController: WYPopoverController? = nil
    var fontName = UserDefaults.standard.string(forKey:"fontDisplayNameConscience") ?? "HelveticaNeue-Medium"
    var fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
    var dropDown = DropDown()
    var conscienceExams = [[String:Any]]()
    private lazy var useCaseController = SettingsPanelManager.shared
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        
        var examsTitles = [String]()
        
        selectedQuestions = NSMutableOrderedSet(array: KeyChain.loadArray(key: "ExaminationOfConscience") ?? [])
        typeButton.setTitle("Cargando...", for: .normal )
        
        ConscienceManager.shared.getConscienceExams { [weak self](parseExams, error) in
            guard let strongSelf = self else { return }
            if let error = error{
                let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                strongSelf.present(alert, animated: true, completion: nil)
            }else{
                strongSelf.conscienceExams = parseExams ?? []
                
                for conscienceExam in strongSelf.conscienceExams{
                    if let title = conscienceExam["title"] as? String{
                        examsTitles.append(title)
                    }
                }
                strongSelf.dropDown.selectionAction = { (index, item) in
                    guard let strongSelf = self else { return }
                    strongSelf.typeButton.setTitle(item, for: .normal)
                    strongSelf.userDidSelectExam(strongSelf.conscienceExams[index])
                }
                strongSelf.typeButton.setTitle(examsTitles.first ?? "", for: .normal )
                strongSelf.dropDown.dataSource = examsTitles
                
                strongSelf.dropDown.anchorView = strongSelf.typeButton
                strongSelf.customizeDropDown()
                strongSelf.dropDown.selectRow(0)
                
                strongSelf.userDidSelectExam(strongSelf.conscienceExams[0])
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StyleSelectorSegue"{
            let nvc = segue.destination as! UINavigationController
             if let contentVC = nvc.viewControllers[0] as? StyleConscienceViewController{
                contentVC.preferredContentSize = CGSize(width: 280, height: 160)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
    }
    
    //MARK: - Button actions
    @IBAction func typeButtonTapped(_ sender:UIButton){
        dropDown.show()
    }
    
    @IBAction func confessionButtonTapped(_ sender: Any) {
        if selectedQuestions?.count == 0{
            let alert = UIAlertController(title: nil, message: "Es necesario agregar al menos un pecado a la lista.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.performSegue(withIdentifier: "ConfessionSegue", sender: nil)
        }
    }
    
    //MARK: - Helpers
    func customizeDropDown() {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        if #available(iOS 11.0, *) {
            appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }
    }

    func userDidSelectExam(_ exam:[String:Any]) {
        dataSourceArray = exam["groups"]  as? [[String:Any]] ?? []
        

        
        reloadAndScrollToTop()
    }
    
    func reloadAndScrollToTop() {
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.scroll(to: .top, animated: true)
    }
}

//MARK: - UITableViewDelegate,UITableViewDataSource
extension QuestionaryViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArray.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < dataSourceArray.count {
            let sectionDictionary = dataSourceArray[section]
            if let questions = sectionDictionary["questions" ] as? [[String:Any]]{
                return questions.count + 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionDictionary = dataSourceArray[indexPath.section]

        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupSinCell", for: indexPath) as! GroupSinCell

            cell.groupLabel.text = sectionDictionary["title"] as? String ?? ""
            cell.subtitleGroupLabel.text = sectionDictionary["subtitle"] as? String ?? ""
            
            cell.groupLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0) + 4.0)?.bold()
            cell.subtitleGroupLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0))?.italic()

            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SinCell", for: indexPath) as! SinCell
            let questions = sectionDictionary["questions" ] as? [[String:Any]] ?? []
            if questions.count > indexPath.row - 1{
                let question = questions[indexPath.row - 1]["text" ] as? String ?? ""
                let questionId = questions[indexPath.row - 1]["id"] as? String ?? ""
                cell.sinLabel.text = question
                cell.checkButton?.setSelected(selectedQuestions?.contains(questionId) ?? false, animated: false)
            }
            cell.row = indexPath.row
            cell.section = indexPath.section
            cell.delegate = self
            
            cell.checkButton?.iconSize = 25.0
            cell.checkButton?.pulseColor = .red
            cell.checkButton?.setIconColor(#colorLiteral(red: 0.2470588235, green: 0.3254901961, blue: 0.6901960784, alpha: 1), for: .selected)
            
            cell.sinLabel.font = UIFont(name: fontName, size: fontSize >= 8 ? fontSize : 8.0)
            
            return cell
        }
    }
}

//MARK: - SinCellDelegate
extension QuestionaryViewController:SinCellDelegate{
    func userDidTapCheckButton(button: CheckButton, inSection section: Int, atRow row: Int) {
        let sectionDictionary = dataSourceArray[section]

        let questions = sectionDictionary["questions" ] as? [[String:Any]] ?? []
        if questions.count > button.tag - 1{
            if button.isSelected{
                selectedQuestions?.add(questions[row - 1]["id"] ?? "")

            }else{
                selectedQuestions?.remove(questions[row - 1]["id"] ?? "")
            }
            KeyChain.saveArray(key: "ExaminationOfConscience", value: selectedQuestions?.array ?? [])
        }
        
    }
    
    func userDidTapDeleteButton(button: UIButton, inSection section: Int, atRow row: Int) {
        
    }
}

//MARK: - StyleConscienceViewControllerDelegate
extension QuestionaryViewController:StyleConscienceViewControllerDelegate{
    func fontSizeChanged() {
        fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
        tableView.reloadData()
    }
    
    func fontChanged() {
        fontName = UserDefaults.standard.string(forKey:"fontNameConscience") ?? "HelveticaNeue-Medium"
        tableView.reloadData()
    }
}

//MARK: -
class GroupSinCell: UITableViewCell {
    //MARK: - IBOutlets
    @IBOutlet weak var groupLabel: AnimatedLabel!
    @IBOutlet weak var subtitleGroupLabel: AnimatedLabel!
}

