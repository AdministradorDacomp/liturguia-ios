//
//  ConsciencePrayersViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/31/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import WYPopoverController

class ConsciencePrayersViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var closeButton: UIButton!
    
    var stylePopoverController: WYPopoverController? = nil
    var isNewExam = false
    lazy var originalAttributedText = textView.attributedText
    private lazy var useCaseController = SettingsPanelManager.shared
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
        fontSizeChanged()
        
        closeButton.setTitle(isNewExam ? "Realizar examen de conciencia" : "Cerrar", for: .normal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StyleSelectorSegue"{
            let nvc = segue.destination as! UINavigationController
            if let contentVC = nvc.viewControllers[0] as? StyleConscienceViewController{
                contentVC.preferredContentSize = CGSize(width: 280, height: 160)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        textView.setContentOffset(.zero, animated: false)

    }
    
    //MARK: - Button actions
    @IBAction func showConscienceButtonTapped(_ sender: Any) {
        KeyChain.saveString(key: "ExaminationOfConscienceInitialPrayers", value: "true")
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: - StyleConscienceViewControllerDelegate
extension ConsciencePrayersViewController:StyleConscienceViewControllerDelegate{
    func fontSizeChanged() {
        let fontName = UserDefaults.standard.string(forKey:"fontNameConscience") ?? "HelveticaNeue-Medium"
        let font = UIFont(name: fontName, size: 14.0)!
        
        var appSize =  CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
        appSize = appSize != 0 ? appSize : 14.0
        let sizeDifference = appSize - 16

        if  let attributedText = originalAttributedText?.mutableCopy() as? NSMutableAttributedString{
            attributedText.enumerateAttribute(NSAttributedString.Key.font, in: NSRange(location: 0, length: attributedText.length), options: [], using: { (value, range, stop2) in
                if let value = value as? UIFont {
                    let oldFont = value
                    let oldSize = value.pointSize

                    let newSize = oldSize + sizeDifference

                    var newFont = font.withSize(newSize)
                    if oldFont.isBold{
                        newFont = newFont.bold()
                    }
                    if oldFont.isItalic{
                        newFont = newFont.italic()
                    }
                    
                    attributedText.removeAttribute(NSAttributedString.Key.font, range: range)
                    attributedText.addAttribute(NSAttributedString.Key.font, value: newFont, range: range)
                }
            })
            textView.attributedText = attributedText

        }
        
    }
    
    func fontChanged() {
        
        fontSizeChanged()
    }
}
