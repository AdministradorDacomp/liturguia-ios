//
//  ConscienceViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/20/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import Material
import EmptyDataSet_Swift
import MaryPopin
import Parse
import WYPopoverController
import IQKeyboardManagerSwift

//MARK: -
class ConscienceViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sinTextField: UITextField!
    @IBOutlet weak var lastConfessionTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var sinTextFieldBottomContstraint: NSLayoutConstraint!
    
    //MARK: - Vars
    var dataSourceArray = [[String:Any]]()
    var stylePopoverController: WYPopoverController? = nil
    private lazy var useCaseController = SettingsPanelManager.shared
    var fontName = UserDefaults.standard.string(forKey:"fontDisplayNameConscience") ?? "HelveticaNeue-Medium"
    var fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
    
    var config = ConfigurationEmptyDataSet(
            text: "No hay entradas al examen de conciencia" ,
            detailText:"Se pueden agregar manualmente con el campo de texto o utilizando el cuestionario.",
            image: nil )
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.isEnabled = false
        
        tableView.isHidden = true
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension

        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.emptyDataSetView { [weak self] view in
            if let `self` = self {
                view.titleLabelString(self.config.titleString)
                    .detailLabelString(self.config.detailString)
                    .image(self.config.image)
                    .imageAnimation(self.config.imageAnimation)
                    .buttonTitle(self.config.buttonTitle(.normal), for: .normal)
                    .buttonTitle(self.config.buttonTitle(.highlighted), for: .highlighted)
                    .buttonBackgroundImage(self.config.buttonBackgroundImage(.normal), for: .normal)
                    .buttonBackgroundImage(self.config.buttonBackgroundImage(.highlighted), for: .highlighted)
                    .dataSetBackgroundColor(self.config.backgroundColor)
                    .verticalSpace(self.config.spaceHeight)
                    .shouldDisplay(true)
                    .shouldFadeIn(true)
                    .isTouchAllowed(true)
                    .isScrollAllowed(true)
                    .isImageViewAnimateAllowed(false)
                    .didTapDataButton {
                    }
                    .didTapContentView {
//                        self.emptyDataSetDidTapView()
                }
                
            }
        }
        
        if let user = PFUser.current(){
           lastConfessionTextField.text =  user["ultimaConfesion"] as? String
        }
        
        ConscienceManager.shared.conscienceNVC = self
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        reloadExam()
        if KeyChain.loadString(key: "ExaminationOfConscienceInitialPrayers") == nil,
            let vc = storyboard?.instantiateViewController(withIdentifier: "ConsciencePrayersViewController") as? ConsciencePrayersViewController{
            let nvc = UINavigationController(rootViewController: vc)
            vc.isNewExam = true
            nvc.modalPresentationStyle = .fullScreen
            self.navigationController?.present(nvc, animated: true, completion: nil)
        }
        
        fontSizeChanged()
        fontChanged()
        
        IQKeyboardManager.shared.enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(ConscienceViewController.keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ConscienceViewController.keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(sender: NSNotification?) {
        var keyboardHeight:CGFloat = 0.0 //297.0
        
        if let userInfo = sender?.userInfo{
            let value = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue)
            let rawFrame = value.cgRectValue
            let keyboardFrame = self.view.convert(rawFrame, from: nil)
             keyboardHeight = keyboardFrame.size.height

        }
        if  UIResponder.currentFirstResponder == sinTextField{
            sinTextFieldBottomContstraint.constant = keyboardHeight + 10.0
            if tableView.numberOfSections != 0{
//                tableView.scroll(to: .bottom, animated: true)

                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) { [weak self] in
                    guard let strongSelf = self else { return }
                    let numberOfRows = strongSelf.tableView.numberOfRows(inSection: 0)
                   
                    if numberOfRows > 0 {
                        let indexPath = IndexPath(row: numberOfRows - 1, section: 0)
                        strongSelf.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    }
                }
            }
        }else{
            keyboardWillHide(sender: sender)
        }
        
    }
    
    @objc func keyboardWillHide(sender: NSNotification?) {
        if  sinTextFieldBottomContstraint.constant != 175.0{
            sinTextFieldBottomContstraint.constant = 175.0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StyleSelectorSegue"{
            let nvc = segue.destination as! UINavigationController
            if let contentVC = nvc.viewControllers[0] as? StyleConscienceViewController{
                contentVC.preferredContentSize = CGSize(width: 280, height: 160)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
    }
    
    //MARK: - Helpers
    func reloadExam(reloadTable:Bool = true) {
        ConscienceManager.shared.getQuestionsFromArray(KeyChain.loadArray(key: "ExaminationOfConscience") as? [String] ?? []) { ( exams) in
            var questionsArray = [[String:Any]]()
            var mutableExams = exams

            if let manualQuestions = KeyChain.loadArray(key: "ManualExaminationOfConscience"),
                manualQuestions.count > 0{
                
                for question in KeyChain.loadArray(key: "ManualExaminationOfConscience") ?? []{
                    questionsArray.append(["text":question])
                }
                mutableExams["manualAdded"] = [
                    "title" : "Agregados manualmente",
                    "examOrder" : -1,
                    "questions": questionsArray
                ]
            }
            
            self.dataSourceArray = mutableExams.toSortedArrayByKey("examOrder", otherKey:"order")
            
            if reloadTable{
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Button actions
    @IBAction func addSinButtonTapped(_ sender:UIButton?){
        if sinTextField.text != ""{
//            dataSourceArray.insert(sinTextField.text ?? "", at: 0)
            var exam = KeyChain.loadArray(key: "ManualExaminationOfConscience") ?? []
            exam.append(sinTextField.text ?? "")
            
            sinTextField.text = ""
            addButton.isEnabled = false
            
            KeyChain.saveArray(key: "ManualExaminationOfConscience", value: NSMutableOrderedSet(array: exam).array)
            
            reloadExam()
            
//            tableView.scroll(to: .bottom, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) { [weak self] in
                guard let strongSelf = self else { return }
                let numberOfRows = strongSelf.tableView.numberOfRows(inSection: 0)
                
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows - 1, section: 0)
                    strongSelf.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    @IBAction func confessionButtonTapped(_ sender: Any) {
        if dataSourceArray.count == 0{
            let alert = UIAlertController(title: nil, message: "Es necesario agregar al menos un pecado a la lista.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.performSegue(withIdentifier: "ConfessionSegue", sender: nil)
        }
    }
    
    @IBAction func showInitialPrayerButtonTapped(_ sender:CheckButton){
       if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConsciencePrayersViewController"){
            let nvc = UINavigationController(rootViewController: vc)
        nvc.modalPresentationStyle = .fullScreen
            self.navigationController?.present(nvc, animated: true, completion: nil)
        }
    }
}

//MARK: - EmptyDataSetSource, EmptyDataSetDelegate
extension ConscienceViewController:EmptyDataSetSource, EmptyDataSetDelegate{}

//MARK: - ConsciencePasswordViewControllerDelegate
extension ConscienceViewController:ConsciencePasswordViewControllerDelegate{
    func grantCancelled() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func permissionGranted() {
//        KeyChain.delete(key: "NotShowConscienceAlert")

        if KeyChain.loadString(key: "NotShowConscienceAlert") == nil{
            let alert = UIAlertController(title: nil, message: "El examen de conciencia es guardado únicamente en el celular y en forma encriptada. Para tener acceso al examen es necesario la clave de entrada a esta sección.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {(action) in
                KeyChain.saveString(key: "NotShowConscienceAlert", value: "true")
            }))
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        tableView.isHidden = false
    }
}

//MARK: - UITableViewDelegate,UITableViewDataSource
extension ConscienceViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < dataSourceArray.count {
            let sectionDictionary = dataSourceArray[section]
            if let questions = sectionDictionary["questions" ] as? [[String:Any]]{
                return questions.count + 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionDictionary = dataSourceArray[indexPath.section]
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupSinCell", for: indexPath) as! GroupSinCell
            
            cell.groupLabel.text = sectionDictionary["title"] as? String ?? ""
            cell.subtitleGroupLabel.text = sectionDictionary["subtitle"] as? String ?? ""
            
            cell.groupLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0) + 4.0)?.bold()
            cell.subtitleGroupLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0))?.italic()
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SinCell", for: indexPath) as! SinCell
            let questions = sectionDictionary["questions" ] as? [[String:Any]] ?? []
            if questions.count > indexPath.row - 1{
                let question = questions[indexPath.row - 1]["text" ] as? String ?? ""
                cell.sinLabel.text = question
            }
            cell.row = indexPath.row
            cell.section = indexPath.section
            cell.delegate = self
            
            cell.checkButton?.iconSize = 25.0
            cell.checkButton?.pulseColor = .red
            cell.checkButton?.setIconColor(#colorLiteral(red: 0.2470588235, green: 0.3254901961, blue: 0.6901960784, alpha: 1), for: .selected)
            
            cell.sinLabel.font = UIFont(name: fontName, size: fontSize >= 8 ? fontSize : 8.0)
            
            return cell
        }
    }
}

//MARK: - SinCellDelegate
extension ConscienceViewController:SinCellDelegate{
    
    func userDidTapCheckButton(button: CheckButton, inSection section: Int, atRow row: Int) { }
    
    func userDidTapDeleteButton(button: UIButton, inSection section: Int, atRow row: Int) {
//        let alert = UIAlertController(title: nil, message: "¿Quieres borrar este pecado?", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Sí", style: .destructive, handler: {[weak self](action) in
//            guard let strongSelf = self else { return }
        
            let sectionDictionary = dataSourceArray[section]
            let questions = sectionDictionary["questions" ] as? [[String:Any]] ?? []
            let question = questions[row - 1]
            
            let questionaryExam = NSMutableOrderedSet(array: KeyChain.loadArray(key: "ExaminationOfConscience") ?? [])
            let manualAdded = NSMutableOrderedSet(array: KeyChain.loadArray(key: "ManualExaminationOfConscience") ?? [])
            if let questionId = question["id"] as? String{
                questionaryExam.remove(questionId)
                
                KeyChain.saveArray(key: "ExaminationOfConscience", value: questionaryExam.array)
            }else{
                let questionText = question["text"] as? String ?? ""
                manualAdded.remove(questionText)
                KeyChain.saveArray(key: "ManualExaminationOfConscience", value: manualAdded.array)
            }

            if tableView(tableView, numberOfRowsInSection: section) == 2{
                reloadExam(reloadTable: false)

                tableView.deleteSections([section], with: .automatic)
            }else{
                reloadExam(reloadTable: false)

                tableView.deleteRows(at: [IndexPath(row: row, section: section)], with: .automatic)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.reloadExam()
            }

            
//        }))
//
//        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
}

protocol SinCellDelegate {
    func userDidTapCheckButton(button:CheckButton, inSection section:Int, atRow row:Int)
    func userDidTapDeleteButton(button:UIButton, inSection section:Int, atRow row:Int)
}

//MARK: -

class SinCell: UITableViewCell {
    //MARK: - IBOutlets
    @IBOutlet weak var sinLabel: AnimatedLabel!
    @IBOutlet weak var deleteButton: UIButton?
    @IBOutlet weak var checkButton: CheckButton?

    //MARK: - Vars
    var section = 0
    var row = 0
    var delegate:SinCellDelegate?

    //MARK: - Button actions
    @IBAction func deleteButtonTapped(_ sender:CheckButton){
        delegate?.userDidTapDeleteButton(button: sender, inSection: section, atRow: row)
    }
    
    @IBAction func checkButtonTapped(_ sender:CheckButton){
        delegate?.userDidTapCheckButton(button: sender, inSection: section, atRow: row)
    }
    

    

}

//MARK: - StyleConscienceViewControllerDelegate
extension ConscienceViewController:StyleConscienceViewControllerDelegate{
    func fontSizeChanged() {
        fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
        tableView.reloadData()
    }
    
    func fontChanged() {
        fontName = UserDefaults.standard.string(forKey:"fontNameConscience") ?? "HelveticaNeue-Medium"
        tableView.reloadData()
    }
}

//MARK: - UITextFieldDelegate
extension ConscienceViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == lastConfessionTextField{
            sinTextField.becomeFirstResponder()
            keyboardWillShow(sender: nil)
        }else if textField == sinTextField{
            addSinButtonTapped(nil)
            
        }

        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == sinTextField{
            var text = sinTextField.text ?? ""
            if string == ""{
                text = String(text.dropLast())
            }else{
                text += string
            }
            
            addButton.isEnabled = text != ""
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == lastConfessionTextField{
            if let user = PFUser.current(){
                user["ultimaConfesion"] = textField.text!
                user.saveInBackground()
            }
        }
    }
}
