//
//  ContritionViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/21/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import MaryPopin
import WYPopoverController

//MARK: - Protocols
protocol ContritionViewControllerDelegate {
    func dismissContrition()
}

//MARK: - 
class ContritionContainerViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK: - Vars
    var delegate: ContritionViewControllerDelegate?
    
    
    let contentInfo = [
        ["title" : "ACTO DE CONTRICIÓN 1",
         "text" : """
            Jesús, mi Señor y Redentor, yo me arrepiento de todos los pecados que he cometido hasta hoy, y me pesa de todo corazón, porque con ellos ofendí a un Dios tan bueno.
            
            Propongo firmemente no volver a pecar y confío que por tu infinita misericordia me has de conceder el perdón de mis culpas y me has de llevar a la vida eterna.
            
            Amén
            """
        ],
        ["title" : "ACTO DE CONTRICIÓN 2",
         "text" : """
            Señor mío Jesucristo, me arrepiento de todo corazón de haberte ofendido porque eres infinitamente bueno, y has muerto por mí en la cruz.
            
            Propongo no volver a pecar, huir de las ocasiones de pecado, y cumplir la penitencia. Ayuda con tu gracia mi débil voluntad para que no vuelva a ofenderte.
            
            Amén
            """
        ],
        ["title" : "ACTO DE CONTRICIÓN 3",
         "text" : """
            Señor mío, Jesucristo, Dios y hombre verdadero, me pesa de todo corazón haberte ofendido; propongo firmemente nunca más pecar, apartarme de todas las ocasiones de pecado, confesarme y cumplir la penitencia.
            
            Te ofrezco, Señor, mi vida, obras y trabajos en satisfacción de todos mis pecados.
            
            Amén.
            """
        ]
    ]
    
    //MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let slides  = createConstritionViews()
        
        setupSlideScrollView(slides:slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        
//        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
//    @objc func appMovedToBackground() {
//        delegate?.dismissContrition()
//    }
    
    //MARK: - Helpers
    func createConstritionViews() -> [ContritionViewController] {
        var array = [ContritionViewController]()
        for element in contentInfo{
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ContritionViewController") as? ContritionViewController{
                vc.titleString = element["title"] ?? ""
                vc.textString = element["text"] ?? ""
                
                array.append(vc)
            }
        }
        
        return array
    }
    
    func setupSlideScrollView(slides : [ContritionViewController]) {
        scrollView.contentSize = CGSize(width: 300.0 * CGFloat(slides.count), height: 252.0)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].view.frame = CGRect(x: 300.0 * CGFloat(i), y: 0, width: 300.0, height: 252.0)
            scrollView.addSubview(slides[i].view)
        }
    }
    
    //MARK: - Button actions
    @IBAction func endButtonTapped(_ sender:UIButton?){
        delegate?.dismissContrition()
    }

}

//MARK: - UIScrollViewDelegate
extension ContritionContainerViewController:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}

//MARK: -
class PurposeViewController: UIViewController {
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: -
class ContritionViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    //MARK: - Vars
    var titleString = ""
    var textString = ""
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = titleString
        textLabel.text = textString
    }
}
