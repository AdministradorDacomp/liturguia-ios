//
//  MissalOptionsViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 25/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

import UIKit

protocol MissalOptionsDelegate {
    func userDidSelect(option: Missal.Section)
}

class MissalOptionsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    var options = [Missal.Section]()
    
    lazy var htmlsArray = [String]()
    
    lazy var cssCode: String = "" {
        didSet {
            updateTable()
        }
    }
    
    var delegate: MissalOptionsDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        reloadStyle()
    }

    func loadHTMLs(){
        var array = [String]()
        for var section in options {
            var htmlString = section.loadHTML()

            let style = "<style>\(cssCode)</style>"
            
            htmlString = htmlString.replacingOccurrences(of: "<style></style>", with: style)

            
            array.append(htmlString)
        }
        
        htmlsArray = array
    }
    
    func updateTable() {
        loadHTMLs()
        
        tableView.reloadData()
    }
    
    func reloadStyle(){
        updateStyleColor()
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let folderPath = documentsDirectory.appending("/HTML")
        let styleFile = folderPath.appending("/style.css")
        cssCode = (try? String(contentsOfFile:styleFile)) ?? ""
    }
    
    func updateStyleColor() {
        if UserDefaults.standard.string(forKey:"TextStyle") == "white" {
            self.view.backgroundColor = .white
            let navigationBarTintColor = UIColor(red:0.247, green:0.318, blue:0.710, alpha:1.0)
            
            self.navigationController?.navigationBar.barTintColor = navigationBarTintColor
        }else if UserDefaults.standard.string(forKey:"TextStyle") == "sepia" {
            self.view.backgroundColor = sepiaBGColor
            self.navigationController?.navigationBar.barTintColor = sepiaTextColor
        }else if UserDefaults.standard.string(forKey:"TextStyle") == "night" {
            self.view.backgroundColor = .black
            self.navigationController?.navigationBar.barTintColor = .black
        }
    }
}

extension MissalOptionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MissalElementCell", for: indexPath) as! MissalElementCell
        
        if indexPath.row < htmlsArray.count {
            let htmlString = htmlsArray[indexPath.row]
            cell.htmlLabel.attributedText = htmlString.htmlToAttributedString
        }
        
        cell.backgroundColor = .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var section = options[indexPath.row]
        
        section.isShortVersion = false
            
        delegate?.userDidSelect(option: section)
        
        self.navigationController?.popViewController(animated: true)
    }
}
