//
//  MissalDateViewController.h
//  LiturGuia
//
//  Created by Krloz on 19/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"

@interface MissalDateViewController : UIViewController<JTCalendarDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (strong, nonatomic) NSDate *dateSelected;

@end
