//
//  MissalModel.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 25/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

import UIKit

struct Missal: Codable {
    var secciones: [Section] = []
    
    struct Section: Codable {
        var seccion: ValidSection
        var titulo: String?
        var subtitulo: String?
        var texto: String?
        var forma: Int?
        var versiculos: String?
        var respuesta: String?
        var aclamacion1: String?
        var aclamacion2: String?
        var notas: String?
        var introduccion: String?
        var conclusion: String?
        var resumen: String?
        var tipo: String?
        lazy var isShortVersion = false
        
        mutating func loadHTML() -> String {
            let fileName = getFileName()
            
            var htmlString = FilePaths(fileName: fileName).loadHTML(withSection: self.seccion)
            var texto = ""
            var todos = ""
            if seccion == .PR{
                texto = isShortVersion ? self.resumen ?? "" : self.texto ?? ""
                todos = isShortVersion ? "" : "<p id=\"todos\">Santo, Santo, Santo es el Señor, Dios del Universo. Llenos están el cielo y la tierra de tu gloria. Hosanna en el cielo. Bendito el que viene en nombre del Señor. Hosanna en el cielo.</p>"
            }else{
                texto = isShortVersion ? self.resumen ?? "" : self.texto ?? ""
            }
            texto += todos
            texto = texto.replacingOccurrences(of: "\n\n", with: "</p><p id=\"padre\">")
            texto = texto.replacingOccurrences(of: "\n", with: "<br/>")
            let response = self.respuesta ?? ""
            texto = texto.replacingOccurrences(of: response, with: "</p><p id=\"respuesta\">\(response)</p><p id=\"padre\">")
   
            htmlString = htmlString.replacingOccurrences(of: "{texto}", with: texto)
            htmlString = htmlString.replacingOccurrences(of: "{todos}", with: todos)
            htmlString = htmlString.replacingOccurrences(of: "{titulo}", with: self.titulo ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{versiculos}", with: self.versiculos ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{notas}", with: self.notas ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{aclamacion1}", with: self.aclamacion1 ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{aclamacion2}", with: self.aclamacion2 ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{respuesta}", with: response)
            htmlString = htmlString.replacingOccurrences(of: "{introduccion}", with: self.introduccion ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{conclusion}", with: self.conclusion ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{subtitulo}", with: self.subtitulo ?? "")
            htmlString = htmlString.replacingOccurrences(of: "{tipo}", with: self.tipo ?? "")
            return htmlString
        }
        
        private mutating func getFileName() -> String {
            var fileName = ""
            switch self.seccion {
            case .RAMOS, .SAL, .AP, .KYR, .PF, .CON_ACL, .RD, .INV_PN, .PR:
                fileName = "\(self.seccion.rawValue)\(self.forma ?? 1)"
            case .CON_P1:
                fileName = "CON\(self.forma ?? 1)\(!isShortVersion ? "_P1" : "")"
            case .CON_P2:
                fileName = "CON\(self.forma ?? 1)\(!isShortVersion ? "_P2" : "")"
            default: break
            }
            
            if isShortVersion {
                fileName += "_CORTO"
            }
            return fileName
        }
    }
}

enum ValidSection: String, Codable {
    case TITULO
    case AE
    case SAL
    case AP
    case KYR
    case OC
    case MON
    case PL
    case SR
    case SL
    case SEC
    case ACL
    case EVA
    case PF
    case OF
    case OO
    case PE
    case PR
    case CON_ACL
    case AC
    case ODC
    case RD
    case INV_PN
    case CON_P1
    case CON_P2
    case RAMOS
    case RI
    case GL
    case GL_J_SANTO
    case HOM
    case PO
    case PN
    case COM
    case CON
    case RC
    case CENIZA
    case LAVATORIO
    case TRAS_SANTISIMO
    case V_SANTO
    case S_SANTO
    case R_PROMESAS
    case MISA_CRISMAL
}




enum PrefacesOptions: String, CaseIterable {
    case comun
    case santos
    case apostoles
    case santaMaria = "santa maria"
    case eucaristia
    case adviento
    case navidad
    case pasion
    case cuaresma
    case pascua
    case dominical
}

let lang: String = {
    return UserDefaults.standard.string(forKey: "language") ?? "es"
}()

struct FilePaths {
    var fileName = ""
    
    func loadHTML(withSection section: ValidSection) -> String {
        let path = getPath(forSection: section)
        let mainFile = Bundle.main.path(forResource: path, ofType:"html") ?? ""
        let htmlString = (try? String(contentsOfFile: mainFile, encoding: .utf8)) ?? ""
        return htmlString
    }
    
    func getPath(forSection section: ValidSection) -> String {
        switch section {
        case .TITULO: return "Missal HTML/\(lang)/TITULO"
        case .AE:  return "Missal HTML/\(lang)/AE"
        case .SAL: return "Missal HTML/\(lang)/SAL/\(fileName)"
        case .AP:  return "Missal HTML/\(lang)/AP/\(fileName)"
        case .KYR: return "Missal HTML/\(lang)/KYR/\(fileName)"
        case .OC:  return "Missal HTML/\(lang)/OC"
        case .MON: return "Missal HTML/\(lang)/MON"
        case .PL:  return "Missal HTML/\(lang)/PL"
        case .SR:  return "Missal HTML/\(lang)/SR"
        case .SL:  return "Missal HTML/\(lang)/SL"
        case .SEC: return "Missal HTML/\(lang)/SEC"
        case .ACL: return "Missal HTML/\(lang)/ACL"
        case .EVA: return "Missal HTML/\(lang)/EVA"
        case .PF:  return "Missal HTML/\(lang)/PF/\(fileName)"
        case .OF:  return "Missal HTML/\(lang)/OF"
        case .OO:  return "Missal HTML/\(lang)/OO"
        case .PE:  return "Missal HTML/\(lang)/PE"
        case .PR:  return "Missal HTML/\(lang)/PR/PR"
        case .CON_ACL: return "Missal HTML/\(lang)/CON_ACL/\(fileName)"
        case .AC:  return "Missal HTML/\(lang)/AC"
        case .ODC: return "Missal HTML/\(lang)/ODC"
        case .RD:  return "Missal HTML/\(lang)/RD/\(fileName)"
        case .INV_PN: return "Missal HTML/\(lang)/INV_PN/\(fileName)"
        case .CON_P1: return "Missal HTML/\(lang)/CON/\(fileName)"
        case .CON_P2: return "Missal HTML/\(lang)/CON/\(fileName)"
        case .RAMOS:  return "Missal HTML/\(lang)/RAMOS/\(fileName)"
        case .RI, .GL, .GL_J_SANTO, .HOM, .PO, .PN, .COM, .CON, .RC, .CENIZA, .LAVATORIO, .TRAS_SANTISIMO, .V_SANTO, .S_SANTO, .R_PROMESAS, .MISA_CRISMAL:
            return "Missal HTML/\(lang)/\(section.rawValue)"

        }
    }
}
