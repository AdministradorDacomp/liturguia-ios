//
//  MissalDateViewController.m
//  LiturGuia
//
//  Created by Krloz on 19/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "MissalDateViewController.h"
#import "MissalEntity.h"
//#import "MissalViewController.h"
#import "NSDate+ServerDate.h"
#import "NSDate+Utils.h"
#import "SuscriptionsViewController.h"
#import "AppDelegate.h"

@interface MissalDateViewController (){
    NSMutableArray *missalsAvailableArray;
//    JTCalendar *calendar;
    NSManagedObjectContext *moc;
    NSArray *selectedMissals;
    NSDate *selectedDate;
    JTCalendarDayView *dView;
    NSArray *versionsArray;
}

@end

@implementation MissalDateViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate *d = appDelegate;
    moc = d.managedObjectContext;
    
    [self initCalendar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncFinished) name:@"MissalSyncCompleted" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Calendario de misales"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)initCalendar{
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    _calendarManager.dateHelper.calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];

    

    
    
    

    [_calendarManager setDate:[NSDate date]];
    
//    {
//        calendar.calendarAppearance.calendar.firstWeekday = 1;
//        calendar.calendarAppearance.ratioContentMenu = 1.;
//        calendar.calendarAppearance.dayDotRatio = 1./9.;
//        calendar.calendarAppearance.dayDotColor = [UIColor blueColor];
//        calendar.calendarAppearance.dayDotColorOtherMonth = [UIColor blueColor];
//        calendar.calendarAppearance.dayDotColorSelected = [UIColor whiteColor];
//        calendar.calendarAppearance.dayDotColorSelectedOtherMonth = [UIColor whiteColor];
//        calendar.calendarAppearance.dayCircleColorSelected = kNavigationBarColor;
//        calendar.calendarAppearance.dayCircleColorSelectedOtherMonth = kNavigationBarColor;
//        calendar.calendarAppearance.dayFormat = @"d";
//        calendar.calendarAppearance.weekDayFormat = JTCalendarWeekDayFormatSingle;
//        _calendarMenuView.backgroundColor = kNavigationBarColor;
//        calendar.calendarAppearance.menuMonthTextColor = [UIColor whiteColor];
//        calendar.calendarAppearance.menuMonthTextFont = [UIFont fontWithName: @"HelveticaNeue-Bold" size:18];
//        calendar.calendarAppearance.weekDayTextFont = [UIFont fontWithName: @"HelveticaNeue-Bold" size:12];
//        calendar.calendarAppearance.dayTextFont = [UIFont fontWithName: @"HelveticaNeue" size:16];
//        
//        calendar.calendarAppearance.weekDayTextColor = [UIColor grayColor];
//        
//        // Customize the text for each month
//        calendar.calendarAppearance.monthBlock = ^NSString *(NSDate *date, JTCalendar *jtcalendar){
//            NSCalendar *cal = jtcalendar.calendarAppearance.calendar;
//            NSDateComponents *comps = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date];
//            NSInteger currentMonthIndex = comps.month;
//            
//            static NSDateFormatter *dateFormatter;
//            if(!dateFormatter){
//                dateFormatter = [NSDateFormatter new];
//                dateFormatter.timeZone = jtcalendar.calendarAppearance.calendar.timeZone;
//            }
//            
//            while(currentMonthIndex <= 0){
//                currentMonthIndex += 12;
//            }
//            
//            NSString *monthText = [[dateFormatter standaloneMonthSymbols][currentMonthIndex - 1] capitalizedString];
//            
//            return [NSString stringWithFormat:@"%@ %d", monthText, (int)comps.year];
//        };
//    }
    
//    [calendar setMenuMonthsView:_calendarMenuView];
//    [calendar setContentView:_calendarContentView];
//    [calendar setDataSource:self];
    
    [self loadMissalsDownloaded];
    
    [_calendarManager reload];
}

-(void)syncFinished{
    [self loadMissalsDownloaded];

    [_calendarManager reload];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
//    [calendar repositionViews];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"MissalSegue"]) {
        MissalViewController *vc = segue.destinationViewController;
        vc.missal = sender;
        [KVNProgress dismiss];
//        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = YES;
    }
}

#pragma mark - JTCalendarDataSource

- (BOOL)haveEventForDay:(NSDate *)date
{
    date = [LiturGuia dateWithoutTime:date];
//    date = [NSDate removeTimeFromDate:date];
    if([missalsAvailableArray containsObject:date]){
        return YES;
    }
    
    return NO;
}
//
//- (void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date{
//    date = [self dateWithoutTime:date];
//    if ([missalsAvailableArray containsObject:date]) {
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self performSegueWithIdentifier:@"MissalSegue" sender:date];
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
//        });
//    }else{
//        
//        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
//        
//        int weekday = (int)[comps weekday];
//
//        NSDate *weekStartDate = [date dateByAddingTimeInterval:-weekday*24*60*60];
//        NSDate *weekEndDate = [weekStartDate dateByAddingTimeInterval:6*24*60*60];
//        
//        PFQuery *query = [PFQuery queryWithClassName:@"Misal"];
//        [query whereKey:@"fecha" greaterThanOrEqualTo:weekStartDate];
//        [query whereKey:@"fecha" lessThanOrEqualTo:weekEndDate];
//        
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//            if (!error) {
//                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                self.view.window.userInteractionEnabled = YES;
//                if (objects.count == 7) {
//                    selectedMissals = objects;
//                    
//                    PFUser *user = [PFUser currentUser];
//                    
//                    if ([user[@"creditos"] intValue] < [kWeekMissalPrice intValue]) {
//                        NSString *messageErr = @"No hay suficientes créditos para descargar este misal. Para poder descargarlo es necesario adquirir mas créditos.";
//                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                     message:messageErr
//                                                                    delegate:nil
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil];
//                        [av show];
//                        return;
//                    }
//                    
//                    NSString *messageErr = [NSString stringWithFormat:@"Aún no ha adquirido los misales de esa semana. El costo de los 7 misales es de %d créditos y usted cuenta con %d créditos. ¿Desea descargar estos misales?", [kWeekMissalPrice intValue], [user[@"creditos"] intValue]];
//                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                 message:messageErr
//                                                                delegate:self
//                                                       cancelButtonTitle:@"No"
//                                                       otherButtonTitles:@"Si",nil];
//                    [av show];
//                    
//                    selectedDate = date;
//                    
//                    
//                }else{
//                    NSString *messageErr = @"El misal seleccionado no está disponible";
//                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                 message:messageErr
//                                                                delegate:nil
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil];
//                    [av show];
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"kJTCalendarDaySelected" object:nil];
//                }
//            }
//        }];
//    }
//}
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    //here we have to get the time difference between GMT and the current users Date (its in seconds)
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:destinationGMTOffset];
    
    // Test if the dayView is from another month than the page
    // Use only in month mode for indicate the day of the previous or next month
    if([dayView isFromAnotherMonth]){
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:date isTheSameDayThan:dayView.date] && !(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date])){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [kNavigationBarColor colorWithAlphaComponent:0.5f];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = kNavigationBarColor;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = kNavigationBarColor;
        dayView.textLabel.textColor = [UIColor darkGrayColor];
    }
    
    // Your method to test if a date have an event for example
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}


#pragma mark - Views customization

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Heavy" size:18];
    label.backgroundColor = kNavigationBarColor;
    label.textColor = [UIColor whiteColor];

    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    menuItemView.text = [[dateFormatter stringFromDate:date] capitalizedString];
}

- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *view = [JTCalendarWeekDayView new];
    
    for(UILabel *label in view.dayViews){
        label.textColor = [UIColor blackColor];
        label.font = [UIFont fontWithName:@"Avenir-Black" size:12];
    }
    
    return view;
}

- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    
    view.textLabel.font = [UIFont fontWithName: @"Avenir-Medium" size:16];
    
//    view.circleRatio = 1.5;
//    view.dotRatio = 1. / .9;
    
    return view;
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    [KVNProgress showWithStatus:@"Cargando misal..."];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        PFUser *user = [PFUser currentUser];
        NSDate *serverDate = [NSDate serverDate];
        if (!serverDate) {
            [KVNProgress dismiss];
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
            NSString *messageErr = @"Aparentemente la fecha del dispositivo es incorrecta. Por favor, verifique que sea correcta e intente nuevamente.";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            return;
        }
        if ([serverDate isLaterThan:user[@"fVenceSuscripcion"]]) {
            [KVNProgress dismiss];
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            
            NSString *stringFromDate = [formatter stringFromDate:user[@"fVenceSuscripcion"]];
            NSString *messageErr = [NSString stringWithFormat:@"Su suscripción caducó el día %@. Para poder acceder a esta sección es necesario renovar su suscripción.", stringFromDate];
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:self
                                               cancelButtonTitle:@"Ahora no"
                                               otherButtonTitles:@"Renovar", nil];
            [av show];
            av.tag = 0;
            return;
        }
        // Use to indicate the selected date
        self->_dateSelected = dayView.date;
        
        
        self->dView = dayView;
        
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        [UIView transitionWithView:dayView
                          duration:.3
                           options:0
                        animations:^{
                            dayView.circleView.transform = CGAffineTransformIdentity;
                            [self->_calendarManager reload];
                        } completion:nil];
        
        // Load the previous or next page if touch a day from another month
        if(![self->_calendarManager.dateHelper date:self->_calendarContentView.date isTheSameMonthThan:dayView.date]){
            if([self->_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
                [self->_calendarContentView loadNextPageWithAnimation];
            }
            else{
                [self->_calendarContentView loadPreviousPageWithAnimation];
            }
        }
        
        NSDate *date = [LiturGuia dateWithoutTime:dayView.date];
//        NSDate *date = [NSDate removeTimeFromDate: dayView.date];
        if ([self->missalsAvailableArray containsObject:date]) {
            [self showMissalOptionsForDate:date];
            
        }else{
            if (![LiturGuia internetConnected]) {
                [KVNProgress dismiss];
//                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                self.view.window.userInteractionEnabled = YES;
                NSString *messageErr = @"El misal del día seleccionado no ha sido descargado aún. Para descargarlo, por favor conectese a una red WiFi o encienda los datos moviles del dispositivo.";
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:messageErr
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
                [av show];
                return;
            }
            //            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            //            [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
            //            NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
            //
            //            int weekday = (int)[comps weekday];
            
            
            //            NSDate *weekStartDate = [date dateByAddingTimeInterval:-(weekday-1)*24*60*60];
            //            NSDate *weekEndDate = [weekStartDate dateByAddingTimeInterval:6*24*60*60];
            
            //            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            PFQuery *query = [PFQuery queryWithClassName:@"Misal"];
            //            [query whereKey:@"fecha" greaterThanOrEqualTo:weekStartDate];
            //            [query whereKey:@"fecha" lessThanOrEqualTo:weekEndDate];
            [query whereKey: @"fecha" equalTo:date];
            //            [query whereKey:@"idioma" equalTo:[userDefaults objectForKey:@"language"]];
            
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    
                    if (objects.count != 0) {
                        self->selectedMissals = objects;
                        
                        //                        PFUser *user = [PFUser currentUser];
                        //                        int weekMissalPrice = [[[NSUserDefaults standardUserDefaults] stringForKey:@"weekMissalPrice"] intValue];
                        
                        //                        if ([user[@"creditos"] intValue] < weekMissalPrice) {
                        //                            NSString *messageErr = @"Créditos insuficientes para descargar. Es necesario adquirir más créditos.";
                        //                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                        //                                                                         message:messageErr
                        //                                                                        delegate:nil
                        //                                                               cancelButtonTitle:@"OK"
                        //                                                               otherButtonTitles:nil];
                        //                            [av show];
                        //                            return;
                        //                        }
                        
                        //                        NSString *messageErr = [NSString stringWithFormat:@"El costo de los 7 misales es de %d créditos.  Usted cuenta con %d créditos. ¿Desea descargar estos misales?", weekMissalPrice, [user[@"creditos"] intValue]];
                        //                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                        //                                                                     message:messageErr
                        //                                                                    delegate:self
                        //                                                           cancelButtonTitle:@"No"
                        //                                                           otherButtonTitles:@"Si",nil];
                        //                        [av show];
                        
                        self->selectedDate = date;
                        [self downloadMissal];
                        
                    }else{
//                        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                        self.view.window.userInteractionEnabled = YES;
                        [KVNProgress dismiss];
                        NSString *messageErr = @"El misal seleccionado no está disponible";
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                     message:messageErr
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                        [av show];
                        self->_dateSelected = nil;
                        [self->_calendarManager reload];
                        
                    }
                }
            }];
        }
        
    });
    
}

#pragma mark - Load data

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (void)loadMissalsDownloaded{
    missalsAvailableArray = [NSMutableArray new];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:@"MissalEntity"
                inManagedObjectContext:moc];
    [request setEntity:entity];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"language == %@", [userDefaults objectForKey:@"language"]];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array != nil) {
        for (MissalEntity *missal in array) {
            [missalsAvailableArray addObject:missal.date];
        }
    }
}

#pragma  mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 0) {
        if (alertView.cancelButtonIndex != buttonIndex) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Left menu" bundle:nil];
            SuscriptionsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SuscriptionsViewController"];
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
            nc.modalPresentationStyle = UIModalPresentationFullScreen;
            [vc setToModalView];
            [self presentViewController:nc animated:YES completion:nil];
        }
    }else if(alertView.tag == 1){
        [self showMissal:versionsArray[buttonIndex]];
    }
    
}

-(void)downloadMissal{
//    PFUser *user = [PFUser currentUser];
//    int weekMissalPrice = [[[NSUserDefaults standardUserDefaults] stringForKey:@"weekMissalPrice"] intValue];
//    [user incrementKey:@"creditos" byAmount:@(-weekMissalPrice)];
    
//    NSMutableArray *array = [NSMutableArray arrayWithArray:user[@"misalesDescargados"]];
    for (PFObject *parseMissal in selectedMissals) {
//        [array addObject:parseMissal.objectId];
        
        MissalEntity *missal = [NSEntityDescription
                                insertNewObjectForEntityForName:@"MissalEntity"
                                inManagedObjectContext:moc];
        missal.title = parseMissal[@"titulo"];
        missal.time = parseMissal[@"tiempo"];
        missal.cycle = parseMissal[@"ciclo"];
        missal.date = [LiturGuia dateWithoutTime:parseMissal[@"fecha"]];
//        missal.date = [NSDate removeTimeFromDate:parseMissal[@"fecha"]];
        missal.type = parseMissal[@"tipo"];
        missal.language = parseMissal[@"idioma"];
        missal.color = parseMissal[@"color"];
        missal.jsonMissal = parseMissal[@"jsonMisal"];
        missal.objectId = parseMissal.objectId;
        if (parseMissal[@"version"] && parseMissal[@"version"] != [NSNull null]) {
            missal.missalVersion = parseMissal[@"version"];
        }
    }
//    user[@"misalesDescargados"] = array;
    
//    [user saveInBackground];
    [LiturGuia saveContext];

    
    [self syncFinished];
    
    [self showMissalOptionsForDate:selectedDate];
    
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        [self performSegueWithIdentifier:@"MissalSegue" sender:selectedDate];
//        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = YES;
//    });
}

-(void)showMissalOptionsForDate:(NSDate *)date{

        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity =
        [NSEntityDescription entityForName:@"MissalEntity"
                    inManagedObjectContext:moc];
        [request setEntity:entity];
        
        NSTimeInterval length;
        
        [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitMinute
                                        startDate:&date
                                         interval:&length
                                          forDate:date];
        
        NSDate *endDate = [date dateByAddingTimeInterval:length];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(date >= %@) AND (date < %@) AND language == %@", date, endDate, [userDefaults objectForKey:@"language"]];
        
        //        NSPredicate *predicate =
        //        [NSPredicate predicateWithFormat:@"date == %@", sender];
        [request setPredicate:predicate];
        
        NSError *error;
        versionsArray = [moc executeFetchRequest:request error:&error];
        if (versionsArray != nil && versionsArray.count > 0) {
            if (versionsArray.count > 1) {
                NSString *messageErr = @"Seleccione el misal correspondiente a la misa a la que desea asistir:";
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                             message:messageErr
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:nil];
                for(MissalEntity *missal in versionsArray) {
                    NSString *title = missal.missalVersion  == nil ? @" " : missal.missalVersion;
                    [av addButtonWithTitle:title];
                }
                av.tag = 1;
                [av show];
            }else{
                [self performSegueWithIdentifier:@"MissalSegue" sender:versionsArray[0]];
            }
        }

    
}

-(void)showMissal:(MissalEntity *)missal{

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"MissalSegue" sender:missal];
    });

}

@end
