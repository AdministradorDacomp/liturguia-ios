//
//  MissalViewController.m
//  LiturGuia
//
//  Created by Krloz on 19/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

//#import "MissalViewController.h"
//#import "PrefaceEntity.h"
//#import "WYStoryboardPopoverSegue.h"
//#import "NSString+Contains.h"
//#import <Appirater/Appirater.h>
import WYPopoverController
import Appirater
import KVNProgress
import FloatingPanel

let sepiaBGColor = UIColor(red: 253/255.0, green:243/255.0, blue:219/255.0, alpha: 1.0)
let sepiaTextColor = UIColor(red: 112.0/255.0, green:66.0/255.0, blue:20.0/255.0, alpha:1.0)
let navigationBarColor = UIColor(red: 0.247, green: 0.318, blue: 0.710, alpha: 1.0)


class MissalViewController : UIViewController {
    
    // MARK: - Vars
    private var stylePopoverController: WYPopoverController?
    private var isHolyThursday: Bool = false
    private var htmlsArray = [String]()
    private var sectionsArray = [Missal.Section]()
    private var missalObject = Missal()
    private var selectedIndexPath: IndexPath?
    private var estolaImage = UIImage()
    private var time = ""
    private var cycle = ""
    
    private lazy var useCaseController = SettingsPanelManager.shared
    private var fixedSections:[ValidSection] = [
        .TITULO,
        .RI,
        .GL,
        .GL_J_SANTO,
        .HOM,
        .PO,
        .PN,
        .PE,
        .COM,
        .CENIZA,
        .LAVATORIO,
        .TRAS_SANTISIMO,
        .V_SANTO,
        .S_SANTO,
        .R_PROMESAS,
        .MISA_CRISMAL
    ]
    private var selectableSections:[ValidSection] = [
        .SAL,
        .AP,
        .KYR,
        .PF,
        .CON_ACL,
        .RD,
        .INV_PN,
        .CON_P1,
        .CON_P2,
        .RAMOS,
    ]
    
    @objc var missal: MissalEntity?
    
    lazy var cssCode: String = "" {
        didSet {
            updateTable()
        }
    }
    
    let formatter: DateFormatter = {
        let df = DateFormatter()
        df.timeZone = TimeZone(abbreviation: "GMT")
        df.dateFormat = "dd/MM/yyyy"
        
        return df
    }()
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Appirater.userDidSignificantEvent(true)
        
        self.updateStyleColor()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if let missal = missal {
            load(missal: missal)
        } else {
            self.navigationController?.popViewController(animated: true)
            
            let alert = UIAlertController(title: nil, message: "Ocurrio un error al cargar el misal. Intente nuevamente.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            navigationController?.popViewController(animated: true)
        }
        reloadStyle()
    }
    
    override func viewWillDisappear(_ animated:Bool) {
        super.viewWillDisappear(animated)
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(tableView.contentOffset.y, forKey:missal?.objectId ?? "")
        userDefaults.synchronize()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SectionOptionsSegue") {
            if let vc = segue.destination as? MissalOptionsViewController,
               let sender = sender as? [String: Any],
               let title = sender["title"] as? String,
               let options = sender["options"] as? [Missal.Section] {
                vc.options = options
                vc.title = title
                vc.delegate = self
            }
        }
        if (segue.identifier == "StyleSelectorSegue") {
            if let nc = segue.destination as? UINavigationController,
               let contentVC = nc.viewControllers[0] as? StyleSelectorViewController {
                contentVC.preferredContentSize = CGSize(width: 280, height: 240)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
        let userDefaults = UserDefaults.standard
        userDefaults.set(tableView.contentOffset.y, forKey:missal?.objectId ?? "")
        userDefaults.synchronize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - actions
    @IBAction func pinchGestureAction(_ sender:UIPinchGestureRecognizer!) {
        if sender.state == .ended {
            var fontSize:Float = UserDefaults.standard.float(forKey: "fontSize")
            if sender.scale > 1 {
                fontSize += 2
            }else {
                fontSize -= 2
            }
            UserDefaults.standard.set(fontSize, forKey:"fontSize")
            UserDefaults.standard.synchronize()
            
            if fontSize <= 10 || fontSize >= 40 {
                return
            }
            
            let documentsDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
            let folderPath = documentsDirectory.appending("/HTML")
            let styleFile = folderPath.appending("/style.css")
            var content = (try? String(contentsOfFile:styleFile)) ?? ""
            
            let range = content.range(of: "font-size: ")!
            
            let end = content.index(range.upperBound, offsetBy: 2)
            let actualSizeRange = range.upperBound..<end
            
            
            content = content.replacingCharacters(in: actualSizeRange, with: String(format:"%02d", Int(fontSize)))
            
            try? content.write(toFile: styleFile,
                               atomically:false,
                               encoding:.nonLossyASCII)
            fontSizeChanged()
        }
    }
    
    // MARK: - Helpers
    
    func load(missal: MissalEntity) {
        let stringFromDate = formatter.string(from: missal.date ?? Date())
        self.title = stringFromDate
        
        if let jsonMisal = missal.jsonMissal,
           let objectData = jsonMisal.data(using: .utf8),
           let modelObject = try? JSONDecoder().decode(Missal.self, from: objectData) {
            missalObject = modelObject
        }
        
        estolaImage = UIImage(named: "estola_\(missal.color ?? "")") ?? UIImage()
        time = missal.time ?? "Tiempo ordinario"
        cycle = "Ciclo \(missal.cycle ?? "")"
        
        if let first = missalObject.secciones.first,
           first.seccion != .TITULO {
            sectionsArray.append(Missal.Section(seccion: .TITULO, titulo: missal.title, tipo: missal.type))
        }
        
        for section in missalObject.secciones {
            if (section.seccion == .LAVATORIO) {
                isHolyThursday = true
            }
            if (section.seccion == .GL) && isHolyThursday {
                sectionsArray.append(Missal.Section(seccion: .GL_J_SANTO))
            }else if (section.seccion == .CON) {
                if isHolyThursday {
                    sectionsArray.append(Missal.Section(seccion: .CON_P1, forma: 1))
                    sectionsArray.append(Missal.Section(seccion: .CON_ACL, forma: 1))
                    sectionsArray.append(Missal.Section(seccion: .CON_P2, forma: 1))
                }else{
                    sectionsArray.append(Missal.Section(seccion: .CON_P1, forma: 2))
                    sectionsArray.append(Missal.Section(seccion: .CON_ACL, forma: 1))
                    sectionsArray.append(Missal.Section(seccion: .CON_P2, forma: 2))
                }
                
            }else if (section.seccion == .RC) {
                sectionsArray.append(Missal.Section(seccion: .COM))
                sectionsArray.append(Missal.Section(seccion: .INV_PN, forma: 1))
                sectionsArray.append(Missal.Section(seccion: .PN))
            }else{
                sectionsArray.append(section)
                if (section.seccion == .PR) {
                    let title = (section.titulo ?? "").lowercased().simpleString
                    
                    if title.contains("comun")
                        || title.contains("dominical")
                        || title.contains("de los santos")
                        || title.contains("apostoles")
                        || title.contains("santa maria")
                        || title.contains("eucaristia")
                        || title.contains("adviento")
                        || title.contains("navidad")
                        || title.contains("pasion")
                        || title.contains("cuaresma")
                        || title.contains("pascua") {
                        selectableSections.append(.PR)
                    }
                }
            }
        }
        
        if let jsonData = try? JSONEncoder().encode(missalObject) {
            let jsonString = String(data:jsonData, encoding:.utf8)
            
            missal.jsonMissal = jsonString
            
            LiturGuia.saveContext()
        }
    }
    
    func optionsForRD(isShortVersion: Bool = false) -> [Missal.Section] {
        var options = [Missal.Section]()
        let missalTime = missal?.time?.lowercased() ?? ""
        options.append(Missal.Section(seccion: .RD, forma: 1, isShortVersion: isShortVersion))
        if missalTime.contains("adviento") {
            options.append(Missal.Section(seccion: .RD, forma: 2, isShortVersion: isShortVersion))
        }else if missalTime.contains("navidad") {
            options.append(Missal.Section(seccion: .RD, forma: 3, isShortVersion: isShortVersion))
        }else if missalTime.contains("ordinario") {
            options.append(Missal.Section(seccion: .RD, forma: 4, isShortVersion: isShortVersion))
            options.append(Missal.Section(seccion: .RD, forma: 5, isShortVersion: isShortVersion))
        }else if missalTime.contains("cuaresma") {
            options.append(Missal.Section(seccion: .RD, forma: 6, isShortVersion: isShortVersion))
        }else if missalTime.contains("pascua") {
            options.append(Missal.Section(seccion: .RD, forma: 7, isShortVersion: isShortVersion))
        }
        options.append(Missal.Section(seccion: .RD, forma: 8, isShortVersion: isShortVersion))
        options.append(Missal.Section(seccion: .RD, forma: 9, isShortVersion: isShortVersion))
        
        return options
    }
    
    func loadOptionalPrefaces(section: Missal.Section) -> [Missal.Section] {
        let title = (section.titulo ?? "").lowercased().simpleString
        
        var type = PrefacesOptions.comun
        
        for prefaceOption in PrefacesOptions.allCases {
            if title.contains("de los santos") {
                type = .santos
                break
            } else if title.contains(prefaceOption.rawValue) {
                type = prefaceOption
                break
            }
        }
        
        let request = NSFetchRequest<PrefaceEntity>(entityName: "PrefaceEntity")
        
        let predicate = NSPredicate(format: "type == %@", type.rawValue)
        request.predicate = predicate
        
        let array = (try? LiturGuia.moc().fetch(request))
        
        var options = [Missal.Section]()
        if let array = array {
            for preface in array {
                let option = Missal.Section(
                    seccion: .PR,
                    titulo: preface.title ?? "",
                    subtitulo: preface.subtitle ?? "",
                    texto: preface.text ?? "",
                    resumen: preface.shortText ?? "",
                    isShortVersion: true
                )
                options.append(option)
            }
        }
        
        return options
    }
    
    // MARK: - Load sections
    
    func loadHTMLs(atRow row: Int? = nil) {
        let embedHTML = { (_ section: Missal.Section) -> String in
            var mutableCopy = section
            let htmlString = mutableCopy.loadHTML()
            let style = "<style>\(self.cssCode)</style>"
            return htmlString.replacingOccurrences(of: "<style></style>", with: style)
        }
        
        if let row = row,
           row < htmlsArray.count {
            let section = sectionsArray[row]
            htmlsArray[row] = embedHTML(section)
        } else {
            htmlsArray = []
            for section in sectionsArray {
                htmlsArray.append(embedHTML(section))
            }
        }
    }
    
    // MARK: - MissalOptionsViewControllerDelegate
    
    func updateTable() {
        loadHTMLs()
        tableView.reloadData()
    }
    
    func getOptions(forSection seccion: ValidSection, forms: Int = 1) -> [Missal.Section]{
        var array = [Missal.Section]()
        
        let shortVersions:[ValidSection] = [.AP, .KYR, .SAL, .PF, .RD, .CON_P1, .CON_P2]
        
        for i in 1...forms {
            array.append(Missal.Section(seccion: seccion, forma: i, isShortVersion: shortVersions.contains(seccion)))
        }
        return array
    }
}

extension MissalViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionDic = sectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: indexPath.row == 0 ? "HeaderCell" : "MissalElementCell", for: indexPath) as! MissalElementCell
        
        
        cell.selectionStyle = selectableSections.contains(sectionDic.seccion) && indexPath.row != 0 ? .gray : .none
        let enableOptions = sectionDic.seccion == .CON_P2 ? false : true
        if !enableOptions{
            cell.optionsImageView?.isHidden =  !enableOptions
            cell.isUserInteractionEnabled = enableOptions
        }else{
            cell.optionsImageView?.isHidden =  !selectableSections.contains(sectionDic.seccion)
            cell.isUserInteractionEnabled = selectableSections.contains(sectionDic.seccion)
        }
        cell.estolaImageView?.image = estolaImage
        cell.cycleLabel?.text = cycle
        cell.timeLabel?.text = time
        
        if (indexPath.row) < htmlsArray.count {
            let htmlString = htmlsArray[indexPath.row]
            cell.htmlLabel.attributedText = htmlString.htmlToAttributedString
        }
        
        cell.backgroundColor = self.view.backgroundColor
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated:true)
        
        let sectionDic = sectionsArray[indexPath.row]
        
        selectedIndexPath = indexPath
        
        var options = [Missal.Section]()
        var viewTitle = ""
        if sectionDic.seccion == .RAMOS {
            viewTitle = "Domingo de ramos"
            options.append(contentsOf: getOptions(forSection: .RAMOS, forms: 3))
        }else if sectionDic.seccion == .SAL {
            viewTitle = "Saludo"
            options.append(contentsOf: getOptions(forSection: .SAL, forms: 10))
        }else if sectionDic.seccion == .AP {
            viewTitle = "Acto penitencial"
            options.append(contentsOf: getOptions(forSection: .AP, forms: 3))
        }else if sectionDic.seccion == .KYR {
            viewTitle = "Kyrie"
            options.append(contentsOf: getOptions(forSection: .KYR, forms: 5))
        }else if ((sectionDic.seccion) == .PF) {
            viewTitle = "Profesión de fe"
            options.append(contentsOf: getOptions(forSection: .PF, forms: 3))
        }else if sectionDic.seccion == .CON_ACL {
            viewTitle = "Aclamación"
            options.append(contentsOf: getOptions(forSection: .CON_ACL, forms: 4))
        }else if sectionDic.seccion == .PR {
            viewTitle = "Prefacios"
            options.append(contentsOf: loadOptionalPrefaces(section: sectionDic))
        }else if sectionDic.seccion == .RD {
            viewTitle = "Rito de despedida"
            options.append(contentsOf: optionsForRD(isShortVersion: true))
        }else if sectionDic.seccion == .INV_PN {
            viewTitle = "Invitación al Padre Nuestro"
            options.append(contentsOf: getOptions(forSection: .INV_PN, forms: 4))
        }else if sectionDic.seccion == .CON_P1 {
            viewTitle = "Plegaria eucarística"
            options.append(contentsOf: getOptions(forSection: .CON_P1, forms: 4))
        }
        
        self.performSegue(
            withIdentifier: "SectionOptionsSegue",
            sender: [
                "title": viewTitle,
                "options": options
            ]
        )
    }
}

extension MissalViewController: StyleSelectorViewControllerDelegate {
    func fontSizeChanged() {
        reloadStyle()
    }
    
    func fontChanged() {
        reloadStyle()
    }
    
    func styleChanged() {
        updateStyleColor()
        reloadStyle()
    }
    
    func reloadStyle(){
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let folderPath = documentsDirectory.appending("/HTML")
        let styleFile = folderPath.appending("/style.css")
        cssCode = (try? String(contentsOfFile:styleFile)) ?? ""
    }
    
    func updateStyleColor() {
        if UserDefaults.standard.string(forKey:"TextStyle") == "white" {
            self.view.backgroundColor = .white
            let navigationBarTintColor = UIColor(red:0.247, green:0.318, blue:0.710, alpha:1.0)
            self.navigationController?.navigationBar.barTintColor = navigationBarTintColor
        }else if UserDefaults.standard.string(forKey:"TextStyle") == "sepia" {
            self.view.backgroundColor = sepiaBGColor
            self.navigationController?.navigationBar.barTintColor = sepiaTextColor
        }else if UserDefaults.standard.string(forKey:"TextStyle") == "night" {
            self.view.backgroundColor = .black
            self.navigationController?.navigationBar.barTintColor = .black
        }
    }
}

extension MissalViewController: MissalOptionsDelegate {
    func userDidSelect(option: Missal.Section) {
        if let indexPath = selectedIndexPath {
            sectionsArray[indexPath.row] = option
            if option.seccion == .CON_P1{
                if sectionsArray[indexPath.row+2].seccion == .CON_P2{
                    sectionsArray[indexPath.row+2].forma = option.forma
                    missalObject.secciones = sectionsArray
                    loadHTMLs(atRow: indexPath.row+2)
                }
            }
            missalObject.secciones = sectionsArray
            loadHTMLs(atRow: indexPath.row)
            DispatchQueue.main.async {
                if option.seccion == .CON_P1{
                    self.tableView.reloadData()
                    self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }else{
                    self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            }
            
            if let jsonData = try? JSONEncoder().encode(missalObject) {
                let jsonString = String(data:jsonData, encoding:.utf8)
                
                missal?.jsonMissal = jsonString
                
                LiturGuia.saveContext()
            }
        }
    }
}
