//
//  MissalElementCell.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 24/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

import UIKit

class MissalElementCell: UITableViewCell {
    @IBOutlet weak var htmlLabel: UILabel!
    @IBOutlet weak var optionsImageView: UIImageView?
    @IBOutlet weak var estolaImageView: UIImageView?
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var cycleLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
