//
//  FontSelectorViewController.m
//  LiturGuia
//
//  Created by Krloz on 04/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "FontSelectorViewController.h"

@interface FontSelectorViewController (){
    NSArray *fontFamilyNameArray;
    NSArray *fontNameArray;
}

@end

@implementation FontSelectorViewController

@synthesize delegate;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    fontFamilyNameArray = @[@"Arial", @"Helvetica Neue", @"Georgia", @"Palatino", @"Times New Roman"];
    fontNameArray = @[@"Arial", @"HelveticaNeue",@"Georgia",@"Palatino",@"TimesNewRomanPSMT"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return fontNameArray.count;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FontTableViewCell" forIndexPath:indexPath];
    
    NSString *familyName = fontFamilyNameArray[indexPath.row];
    cell.textLabel.text = familyName;
    cell.textLabel.font = [UIFont fontWithName:fontNameArray[indexPath.row] size:cell.textLabel.font.pointSize];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:fontNameArray[indexPath.row] forKey:@"fontName"];
    [[NSUserDefaults standardUserDefaults] setObject:fontFamilyNameArray[indexPath.row] forKey:@"fontDisplayName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [delegate fontChanged];
    
}


@end
