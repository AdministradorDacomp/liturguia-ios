//
//  SelectedRowsActionsViewController.h
//  LiturGuia
//
//  Created by Krloz on 05/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"

@protocol SelectedRowsActionsViewControllerDelegate <NSObject>

@required

-(void)foregroundColorChanged:(UIColor*)color;
-(void)shareVerses:(BOOL)facebook;
-(void)removeSelection;

@end

@interface SelectedRowsActionsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *yellowButton;
@property (strong, nonatomic) IBOutlet UIButton *greenButton;
@property (strong, nonatomic) IBOutlet UIButton *whiteButton;
@property (strong, nonatomic) id delegate;

@end
