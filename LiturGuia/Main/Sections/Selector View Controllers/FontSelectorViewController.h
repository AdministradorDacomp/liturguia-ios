//
//  FontSelectorViewController.h
//  LiturGuia
//
//  Created by Krloz on 04/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FontSelectorViewControllerDelegate <NSObject>

@required

-(void)fontChanged;

@end

@interface FontSelectorViewController : UIViewController
@property (nonatomic, strong) id delegate;

@end
