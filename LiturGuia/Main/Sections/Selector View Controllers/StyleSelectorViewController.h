//
//  StyleSelectorViewController.h
//  LiturGuia
//
//  Created by Krloz on 04/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FontSelectorViewController.h"



@protocol StyleSelectorViewControllerDelegate <NSObject>

@required

- (void)fontSizeChanged;
- (void)fontChanged;

@optional
- (void)styleChanged;

@end

@interface StyleSelectorViewController : UIViewController<FontSelectorViewControllerDelegate>


@property (strong, nonatomic) IBOutlet UIButton *whiteButton;
@property (strong, nonatomic) IBOutlet UIButton *sepiaButton;
@property (strong, nonatomic) IBOutlet UIButton *nightButton;
@property (strong, nonatomic) IBOutlet UISlider *brightnessSlider;
@property (strong, nonatomic) IBOutlet UIButton *increaseFontSizeButton;
@property (strong, nonatomic) IBOutlet UIButton *decreaseFontSizeButton;
@property (strong, nonatomic) IBOutlet UILabel *fontNameLabel;



@property (strong, nonatomic) id delegate;

@end
