//
//  SelectedRowsActionsViewController.m
//  LiturGuia
//
//  Created by Krloz on 05/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "SelectedRowsActionsViewController.h"

@interface SelectedRowsActionsViewController ()

@end

@implementation SelectedRowsActionsViewController

@synthesize delegate;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _yellowButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _greenButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _whiteButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -  Button actions

- (IBAction)foregroundButtonTapped:(id)sender {
    [delegate foregroundColorChanged:(UIColor*)[sender backgroundColor]];
}

- (IBAction)shareButtonTapped:(id)sender {
    [delegate shareVerses:NO];
}

- (IBAction)facebookButtonTapped:(id)sender {
    [delegate shareVerses:YES];
}

- (IBAction)removeSelection:(id)sender {
    [delegate removeSelection];
}

@end
