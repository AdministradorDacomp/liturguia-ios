//
//  StyleSelectorViewController.m
//  LiturGuia
//
//  Created by Krloz on 04/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "StyleSelectorViewController.h"
#import "UIColor+Expanded.h"

@interface StyleSelectorViewController ()

@end

@implementation StyleSelectorViewController{
    NSString *sepiaBackgroudColorHex;
    NSString *sepiaColorHex;
}

@synthesize delegate;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sepiaBackgroudColorHex = [NSString stringWithFormat:@"background-color: #%@", [SEPIA_BACKGROUND_COLOR hexStringValue]];
    sepiaColorHex = [NSString stringWithFormat:@"color: #%@", [SEPIA_TEXT_COLOR hexStringValue]];
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        [self styleButtonTapped:_whiteButton];
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        [self styleButtonTapped:_sepiaButton];
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        [self styleButtonTapped:_nightButton];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"white" forKey:@"TextStyle"];
        [self styleButtonTapped:_whiteButton];
    }
    
    _fontNameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"fontDisplayName"];
    _brightnessSlider.value = [[UIScreen mainScreen] brightness];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    FontSelectorViewController *vc = segue.destinationViewController;
    vc.delegate = self;
    vc.preferredContentSize = CGSizeMake(280, self.view.bounds.size.height);
}


#pragma mark - Button actions

- (IBAction)styleButtonTapped:(id)sender {
    if (sender == _whiteButton) {
        _whiteButton.layer.borderWidth = 1.5;
        _whiteButton.layer.borderColor = [[UIColor blueColor] CGColor];
        
        _sepiaButton.layer.borderWidth = 0.5;
        _sepiaButton.layer.borderColor = [[UIColor grayColor] CGColor];
        
        _nightButton.layer.borderWidth = 0.5;
        _nightButton.layer.borderColor = [[UIColor grayColor] CGColor];
        [self setCSSWhite];
        [[NSUserDefaults standardUserDefaults] setObject:@"white" forKey:@"TextStyle"];
        
    }else if (sender == _sepiaButton){
        _whiteButton.layer.borderWidth = 0.5;
        _whiteButton.layer.borderColor = [[UIColor grayColor] CGColor];
        
        _sepiaButton.layer.borderWidth = 1.5;
        _sepiaButton.layer.borderColor = [[UIColor blueColor] CGColor];
        
        _nightButton.layer.borderWidth = 0.5;
        _nightButton.layer.borderColor = [[UIColor grayColor] CGColor];
        [self setCSSSepia];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"sepia" forKey:@"TextStyle"];
        
    }else if (sender == _nightButton){
        _whiteButton.layer.borderWidth = 0.5;
        _whiteButton.layer.borderColor = [[UIColor grayColor] CGColor];
        
        _sepiaButton.layer.borderWidth = 0.5;
        _sepiaButton.layer.borderColor = [[UIColor grayColor] CGColor];
        
        _nightButton.layer.borderWidth = 1.5;
        _nightButton.layer.borderColor = [[UIColor blueColor] CGColor];
        [self setCSSNight];
        [[NSUserDefaults standardUserDefaults] setObject:@"night" forKey:@"TextStyle"];
    }
    
    if ([delegate respondsToSelector:@selector(styleChanged)]) {
        [delegate styleChanged];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)brightnessValueChanged:(UISlider*)sender {
    [[UIScreen mainScreen] setBrightness:sender.value];
}


- (IBAction)fontSizeButtonTapped:(id)sender {
    float fontSize = [[NSUserDefaults standardUserDefaults] floatForKey:@"fontSize"];
    if (sender == _increaseFontSizeButton) {
        fontSize+=2;
    }else if(sender == _decreaseFontSizeButton){
        fontSize-=2;
    }
    [[NSUserDefaults standardUserDefaults] setFloat:fontSize forKey:@"fontSize"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    
    if (fontSize <= 10) {
        _decreaseFontSizeButton.enabled = NO;
    }else{
        _decreaseFontSizeButton.enabled = YES;
    }
    if (fontSize >= 40) {
        _increaseFontSizeButton.enabled = NO;
    }else{
        _increaseFontSizeButton.enabled = YES;
    }
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *content = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    
    NSRange range = [content rangeOfString:@"font-size: "];
    NSRange actualSizeRange = {range.location+11, 2};\

    content = [content stringByReplacingCharactersInRange:actualSizeRange withString:[NSString stringWithFormat:@"%02d", (int)fontSize]];
    
    [content writeToFile:styleFile
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
    
    if ([delegate respondsToSelector:@selector(fontSizeChanged)]) {
        [delegate fontSizeChanged];
    }
}


#pragma mark - FontSelectorViewControllerDelegate

-(void)fontChanged{
    _fontNameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"fontDisplayName"];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *content = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    
    NSRange range = [content rangeOfString:@"font-family: "];
    
    NSRange rangeCuted = {range.location+13, 40};
    
    NSString *cutedString = [content substringWithRange:rangeCuted];
    
    const char *c = [cutedString UTF8String];
    
    int commaPosition = 0;
    for (int i = 0; i <= 40; i++) {
        if (cutedString.length > i && c[i] == ';') {
            commaPosition = i;
            break;
        }
    }
    
    NSRange actualSizeRange = {range.location+13, commaPosition};
    
    content = [content stringByReplacingCharactersInRange:actualSizeRange withString:[[NSUserDefaults standardUserDefaults] objectForKey:@"fontDisplayName"]];
    
    [content writeToFile:styleFile
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
    
    if ([delegate respondsToSelector:@selector(fontChanged)]) {
        [delegate fontChanged];
    }
}

#pragma mark - CSS

-(void)setCSSWhite{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *content = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    

    
    content = [content stringByReplacingOccurrencesOfString:sepiaColorHex withString:@"color: black"];
    content = [content stringByReplacingOccurrencesOfString:@"color: white" withString:@"color: black"];
    
    content = [content stringByReplacingOccurrencesOfString:@"background-color: black" withString:@"background-color: white"];
    content = [content stringByReplacingOccurrencesOfString:sepiaBackgroudColorHex withString:@"background-color: white"];
    
    content = [content stringByReplacingOccurrencesOfString:@"color: yellow" withString:@"color: red"];
    
    [content writeToFile:styleFile
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
}

-(void)setCSSSepia{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *content = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    

    
    content = [content stringByReplacingOccurrencesOfString:@"color: black" withString:sepiaColorHex];
    content = [content stringByReplacingOccurrencesOfString:@"color: white" withString:sepiaColorHex];
    
    content = [content stringByReplacingOccurrencesOfString:@"background-color: black" withString:sepiaBackgroudColorHex];
    content = [content stringByReplacingOccurrencesOfString:@"background-color: white" withString:sepiaBackgroudColorHex];
    content = [content stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"background-color: #%@", [SEPIA_TEXT_COLOR hexStringValue]] withString:sepiaBackgroudColorHex];
    
        content = [content stringByReplacingOccurrencesOfString:@"color: yellow" withString:@"color: red"];
    
    [content writeToFile:styleFile
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
}

-(void)setCSSNight{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *content = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    

    
    content = [content stringByReplacingOccurrencesOfString:sepiaColorHex withString:@"color: white"];
    content = [content stringByReplacingOccurrencesOfString:@"color: black" withString:@"color: white"];
    
    content = [content stringByReplacingOccurrencesOfString:@"background-color: white" withString:@"background-color: black"];
    content = [content stringByReplacingOccurrencesOfString:sepiaBackgroudColorHex withString:@"background-color: black"];
    
    content = [content stringByReplacingOccurrencesOfString:@"color: red" withString:@"color: yellow"];
    
    [content writeToFile:styleFile
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
}

@end
