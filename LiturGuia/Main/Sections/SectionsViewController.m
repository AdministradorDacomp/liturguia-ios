//
//  SectionsViewController.m
//  LiturGuia
//
//  Created by Krloz on 14/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "SectionsViewController.h"
#import "RESideMenu.h"
#import "FMDatabase.h"
#import "SQLiteDBManager.h"
//#import <Parse/PFFacebookUtils.h>
#import "DataSyncer.h"
#import "MissalEntity.h"
//#import "MissalViewController.h"
#import "NSDate+ServerDate.h"
#import "LiturgicalDate.h"
#import "CalendarGenerator.h"
#import "NSDate+Utils.h"
#import <MaryPopin/UIViewController+MaryPopin.h>
#import "AppDelegate.h"

@interface SectionsViewController (){
}

@end

@implementation SectionsViewController

#pragma mark - Life cycle



- (void)viewDidLoad {
    [super viewDidLoad];

    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    
//    NSDate *initialDate = [formatter dateFromString:@"01/11/2015"];
//    NSDate *endDate = [formatter dateFromString:@"30/11/2016"];
//    
//    NSMutableArray *array = [NSMutableArray array];
//    CalendarGenerator *object = [CalendarGenerator sharedInstance];
//    while ([initialDate isEarlierThan:endDate]) {
//        [array addObject:[object liturgicalInfoForDate:initialDate]];
//        initialDate = [initialDate dateByAddingTimeInterval:(24*60*60)];
//    }
//    for (LiturgicalDate *liturgicalDate in array) {
//        NSDate *date = [NSDate dateWithLocalTime:liturgicalDate.date];
//        
//        
//        NSString *dayOfWeek;
//        switch ([date dayOfTheWeek]) {
//            case 1:
//                dayOfWeek = @"D";
//                break;
//            case 2:
//                dayOfWeek = @"L";
//                break;
//            case 3:
//                dayOfWeek = @"M";
//                break;
//            case 4:
//                dayOfWeek = @"M";
//                break;
//            case 5:
//                dayOfWeek = @"J";
//                break;
//            case 6:
//                dayOfWeek = @"V";
//                break;
//            case 7:
//                dayOfWeek = @"S";
//                break;
//                
//            default:
//                break;
//        }
//        
//        
//        if (liturgicalDate.specialDate) {
//            NSLog(@"%@ %@ %@ - %@ %dº %@ (%@)",liturgicalDate.even?@"Par":@"Impar", liturgicalDate.cycle, [formatter stringFromDate:date],dayOfWeek, [liturgicalDate.week intValue], liturgicalDate.time, liturgicalDate.specialDate);
//        }else{
//            NSLog(@"%@ %@ %@ - %@ %dº %@",liturgicalDate.even?@"Par":@"Impar", liturgicalDate.cycle, [formatter stringFromDate:date],dayOfWeek, [liturgicalDate.week intValue], liturgicalDate.time);
//        }
//    }

    
    AppDelegate *d = appDelegate;
    d.sectionsViewController = self;
    
    if (![PFUser currentUser]) {
        [self performSelector:@selector(showLoginView) withObject:nil afterDelay:0.0];
        [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    }else{
        [[PFUser currentUser] fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                if ([LiturGuia internetConnected]) {
                    [DataSyncer syncMissals];
//                    [DataSyncer syncLectioDivine];
                    [DataSyncer syncPrayers];
                }

                
            }else{
                switch (error.code) {
                    case kPFErrorInvalidSessionToken: {
                        NSString *messageErr = @"La sesión ha caducado o fue cerrada desde otro dispositivo. Por favor, inicie sesión nuevamente";
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                     message:messageErr
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                        [av show];
                        
                        [self performSelector:@selector(showLoginView) withObject:nil afterDelay:0.0];
                        break;
                    }
                }
            }
            
        }];
    }
    
    if ([LiturGuia internetConnected]) {
        [DataSyncer syncPrayers];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 32, 32);
    [button setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    [barButton setCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 32, 32);
    [button setImage:[UIImage imageNamed:@"calendar_icon"] forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    barButton = [[UIBarButtonItem alloc] init];
    [barButton setCustomView:button];
    
    barButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = barButton;
}

- (IBAction)missalButtonTapped:(id)sender {
    if (![PFUser currentUser]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *nc = [sb instantiateInitialViewController];
        nc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.navigationController presentViewController:nc animated:YES completion:^{
            
        }];
        
        NSString *messageErr = @"Para acceder a esta sección es necesario registrarse de forma gratuita.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:[nc viewControllers][0]
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"Aceptar", nil];
        av.tag = 1;
        [av show];
    }else{
        [self performSegueWithIdentifier:@"MissalDateSegue" sender:nil];
    }
}

- (IBAction)conscienceButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"ConsciencePasswordSegue" sender:nil];
}



- (IBAction)bibleButtonTapped:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"BibleOpened"]){
        [self performSegueWithIdentifier:@"BibleDescriptionSegue" sender:nil];
    }else{
        [self performSegueWithIdentifier:@"BibleSegue" sender:nil];
    }
}

- (IBAction)prayerButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"PrayersSegue" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"MissalSegue"]) {
        MissalViewController *vc = segue.destinationViewController;
        vc.missal = sender;
    }
    if ([segue.identifier isEqualToString:@"ConsciencePasswordSegue"]) {
        ConsciencePasswordViewController *vc = segue.destinationViewController;
        vc.modalTransitionStyle = UIModalPresentationFullScreen;
        ConscienceViewController *vc2 =  [self.storyboard instantiateViewControllerWithIdentifier:@"ConscienceViewController"];
        vc.delegate = vc2;
        [[self navigationController] pushViewController:vc2 animated:NO];

    }
}

- (IBAction)lectioDivineButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"LectioDivineSegue" sender:nil];
//    [self commingSoon:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (![PFUser currentUser]) {
//        [self.navigationItem setLeftBarButtonItem:nil];
    }else{
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, 32, 32);
        [button setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
        [barButton setCustomView:button];
        self.navigationItem.leftBarButtonItem = barButton;
        
        if ([PFUser currentUser][@"bloqueoAutomatico"] == [NSNull null]) {
            [PFUser currentUser][@"bloqueoAutomatico"] = @NO;
            [[PFUser currentUser] saveInBackground];
        }
        
        [[UIApplication sharedApplication] setIdleTimerDisabled: ![[PFUser currentUser][@"bloqueoAutomatico"] boolValue]];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated ];
    PFUser * user = [PFUser currentUser];
    NSDate *serverDate = [NSDate serverDate];

    if (user) {
        int rndValue = 1 + arc4random() % (5 - 1);
        
        if (!user[@"fVenceSuscripcion"]
            || [serverDate isLaterThan:user[@"fVenceSuscripcion"]] || rndValue == 1) {
            if (![AdViewController adWasShowed]) {
                AdViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AdViewController"];
                UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController: vc];
                nc.modalPresentationStyle = UIModalPresentationFullScreen;
                [nc setNavigationBarHidden: NO];

                [nc.view setTintColor:UIColor.whiteColor];

                [self presentViewController:nc animated:YES completion:nil];
            }
        } else if ([user[@"wasTrialInfoShowed"] boolValue] == NO && ![TrialInfoViewController isShowingAlert]){
            TrialInfoViewController *popUpVC = [self.storyboard instantiateViewControllerWithIdentifier: @"TrialInfoViewController"];

            popUpVC.view.frame = CGRectMake(0, 0,  UIScreen.mainScreen.bounds.size.width * 300, 400);
            popUpVC.view.layer.cornerRadius = 5.0;
            popUpVC.view.clipsToBounds = YES;

            [popUpVC setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
            [popUpVC setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
            BKTBlurParameters *blurParameters = [[BKTBlurParameters alloc] init];
            blurParameters.alpha = 1.0;
            blurParameters.radius = 8;
            blurParameters.saturationDeltaFactor = 0.9;
            blurParameters.tintColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha: 0.6];
            [popUpVC setBlurParameters: blurParameters];
                [popUpVC setPopinOptions: BKTPopinBlurryDimmingView|BKTPopinDisableAutoDismiss];

            popUpVC.delegate = self;
            [self.navigationController.interactivePopGestureRecognizer setEnabled:NO];
            [self.navigationController presentPopinController:popUpVC animated:YES completion:nil];
        }
    }
    
}

#pragma mark - Helpers

-(void)showLoginView{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:vc animated:YES completion:^{
        
    }];
}

#pragma mark - Button actions

- (IBAction)commingSoon:(id)sender {
    NSString *messageErr = @"¡Próximamente!";
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:messageErr
                                                delegate:nil
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
    [av show];
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
//        if (alertView.tag == 1) {
//            if (buttonIndex == 1) {
//                NSFetchRequest *request = [[NSFetchRequest alloc] init];
//                NSEntityDescription *entity =
//                [NSEntityDescription entityForName:@"MissalEntity"
//                            inManagedObjectContext:[LiturGuia moc]];
//                [request setEntity:entity];
//                
//                NSPredicate *predicate =
//                [NSPredicate predicateWithFormat:@"date == %@", [LiturGuia dateWithoutTime:[NSDate date]]];
//                [request setPredicate:predicate];
//                
//                NSError *error;
//                NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];
//
//                
//                if (array != nil) {
//                    if (array.count == 0) {
//                        PFUser *user = [PFUser currentUser];
//                        
//                        if ([user[@"creditos"] intValue] <= 0) {
//                            NSString *messageErr = @"No hay suficientes créditos para descargar este misal. Para poder descargarlo es necesario adquirir mas créditos.";
//                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                         message:messageErr
//                                                                        delegate:nil
//                                                               cancelButtonTitle:@"OK"
//                                                               otherButtonTitles:nil];
//                            [av show];
//                            return;
//                        }
//
//                        NSString *messageErr = [NSString stringWithFormat:@"El misal de hoy no ha sido descargado todavía. Aún puede descargar %d misales. \n ¿Desea descargar este misal?", [user[@"creditos"] intValue]];
//                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                     message:messageErr
//                                                                    delegate:self
//                                                           cancelButtonTitle:@"No"
//                                                           otherButtonTitles:@"Si",nil];
//                        av.tag = 2;
//                        [av show];
//                    }else{
//                        [self performSegueWithIdentifier:@"MissalSegue" sender:array[0]];
//                    }
//                }
//            }else if (buttonIndex == 2){
//                [self performSegueWithIdentifier:@"MissalDateSegue" sender:nil];
//            }
//        if (alertView.tag == 2){
//            PFQuery *query = [PFQuery queryWithClassName:@"Misal"];
//            [query whereKey:@"fecha" equalTo:[LiturGuia dateWithoutTime:[NSDate date]]];
//            
//            self.view.window.userInteractionEnabled = NO;
//            [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                if (!error) {
//                    [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                    self.view.window.userInteractionEnabled = YES;
//                    if (objects.count > 0) {
//                        PFObject *selectedMissal = objects[0];
//                        PFUser *user = [PFUser currentUser];
//                        [user incrementKey:@"creditos" byAmount:@(-1)];
//                        
//                        NSMutableArray *array = [NSMutableArray arrayWithArray:user[@"misalesDescargados"]];
//                        [array addObject:selectedMissal.objectId];
//                        user[@"misalesDescargados"] = array;
//                        
//                        [user saveInBackground];
//                        
//                        MissalEntity *missal = [NSEntityDescription
//                                                insertNewObjectForEntityForName:@"MissalEntity"
//                                                inManagedObjectContext:[LiturGuia moc]];
//                        missal.title = selectedMissal[@"titulo"];
//                        missal.time = selectedMissal[@"tiempo"];
//                        missal.cycle = selectedMissal[@"ciclo"];
//                        missal.date = selectedMissal[@"fecha"];
//                        missal.type = selectedMissal[@"tipo"];
//                        missal.language = selectedMissal[@"idioma"];
//                        missal.color = selectedMissal[@"color"];
//                        missal.jsonMissal = selectedMissal[@"jsonMisal"];
//                        missal.objectId = selectedMissal.objectId;
//                        
//                        
//                        [LiturGuia saveContext];
//                    
//                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
//                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                            [self performSegueWithIdentifier:@"MissalSegue" sender:missal];
//                        });
//
//                    }else{
//                        NSString *messageErr = @"El misal seleccionado no está disponible";
//                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                     message:messageErr
//                                                                    delegate:nil
//                                                           cancelButtonTitle:@"OK"
//                                                           otherButtonTitles:nil];
//                        [av show];
//                    }
//                }
//            }];
//        }
    }
}

- (void)userDidCloseTrialInfo {
    [self.navigationController dismissCurrentPopinControllerAnimated:true];
}


@end
