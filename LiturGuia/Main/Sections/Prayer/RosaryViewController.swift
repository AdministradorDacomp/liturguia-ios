//
//  RosaryViewController.m
//  LiturGuia
//
//  Created by Krloz on 08/07/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

//#import "RosaryViewController.h"
//#import "RosaryEntity.h"
//#import "UIColor+Expanded.h"
//#import <AudioToolbox/AudioToolbox.h>
//#import <Appirater/Appirater.h>
import WYPopoverController
import Appirater
import HMSegmentedControl
import KVNProgress
import AudioToolbox

class RosaryViewController: UIViewController {
    
    // MARK: - Life cycle
    private var prayersArray = [String]()
    private var rosaryImagesArray = [UIImage]()
    private var titlesArray = [String]()
    private var currentPrayer = 0
    private var loadedWebViews = 0
    private var selectedMysteries = "" {
        didSet {
            loadWebViews()
        }
    }
    
    private var stylePopoverController: WYPopoverController?
    private var mysteryTitle = ""
    private lazy var useCaseController = SettingsPanelManager.shared
    
    @IBOutlet weak var tabBar: HMSegmentedControl!
    @IBOutlet weak var rosaryImageView: UIImageView!
    @IBOutlet weak var prayerLabel: UILabel!
    
    lazy private var cssCode: String = "" {
        didSet {
            loadWebViews()
        }
    }
    
    private var initialPrayerHTML: String {
        return html(withPath: "Prayer HTML/\(lang)/oracion_inicial")
    }
    
    private var litanieHTML: String {
        return html(withPath: "Prayer HTML/\(lang)/letanias")
    }
    
    private var lastPrayersHTML: String {
        return html(withPath: "Prayer HTML/\(lang)/oracion_final")
    }
    
    private var announcementHTML: String {
        return html(withPath: "Prayer HTML/\(lang)/anunciacion")
    }
    
    private var aveMariaHTML: String {
        return html(withPath: "Prayer HTML/\(lang)/ave_maria")
    }
    
    private var lastAveMariaHTML: String {
        return html(withPath: "Prayer HTML/\(lang)/ultimo_ave_maria")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Appirater.userDidSignificantEvent(true)
        
        self.title = "Oraciones iniciales"
        currentPrayer = 0
        
        let gregorian = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
        let comps = gregorian?.components(NSCalendar.Unit.weekday, from: Date())
        
        tabBar.sectionTitles = ["GOZOSOS\nLunes/Sabado", "DOLOROSOS\nMartes/Viernes", "GLORIOSOS\nMiercoles/Domingo", "LUMINOSOS\nJueves"]
        
        tabBar.indexChangeBlock = {[weak self] (index:UInt) in
            guard let strongSelf = self else { return }
            switch index {
            case 0: strongSelf.selectedMysteries = "gozoso"
            case 1: strongSelf.selectedMysteries = "doloroso"
            case 2: strongSelf.selectedMysteries = "glorioso"
            case 3: strongSelf.selectedMysteries = "luminoso"
            default: break
            }
        }
        tabBar.selectionIndicatorHeight = 2.0
        tabBar.backgroundColor = navigationBarColor
        
        tabBar.selectionIndicatorColor = .white
        tabBar.selectionStyle = HMSegmentedControlSelectionStyle.fullWidthStripe
        tabBar.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.bottom
        tabBar.shouldAnimateUserSelection = true
        
        tabBar.titleFormatter = { (segmentedControl:HMSegmentedControl!,title,index:UInt,selected:Bool) in
            
            
            let font = UIFont(name: "HelveticaNeue", size: 8.0) ?? UIFont.systemFont(ofSize: 8.0)
            var color = UIColor.lightGray
            if selected {
                color = .white
            }
            var attString = NSMutableAttributedString(
                string: title,
                attributes:
                    [
                        NSAttributedString.Key.foregroundColor: color,
                        NSAttributedString.Key.font: font.withSize(11)
                    ]
            )
            
            var redTextRange: NSRange?
            switch index {
            case 0: redTextRange = title.nsRange(of: "Lunes/Sabado")
            case 1: redTextRange = title.nsRange(of: "Martes/Viernes")
            case 2: redTextRange = title.nsRange(of: "Miercoles/Domingo")
            case 3: redTextRange = title.nsRange(of: "Jueves")
            default: break
            }
            attString.addAttribute(NSAttributedString.Key.font, value: font, range: redTextRange ?? NSRange())
            
            return attString
        }
        
        let weekday = comps?.weekday ?? 1
        
        
        if weekday == 2 || weekday == 7 {
            selectedMysteries = "gozoso"
            tabBar.selectedSegmentIndex = 0
        }else if weekday == 3 || weekday == 6 {
            selectedMysteries = "doloroso"
            tabBar.selectedSegmentIndex = 1
        }else if weekday == 4 || weekday == 1 {
            selectedMysteries = "glorioso"
            tabBar.selectedSegmentIndex = 2
        }else if weekday == 5 {
            selectedMysteries = "luminoso"
            tabBar.selectedSegmentIndex = 3
        }
        
        let previousSelectedMysteries = UserDefaults.standard.string(forKey: "selectedMysteries")
        let previousSelectedSegmentIndex = UserDefaults.standard.integer(forKey: "rosarySelectedSegmentIndex")
        if let previousSelectedMysteries = previousSelectedMysteries {
            selectedMysteries = previousSelectedMysteries
        }
        
        
        let previousCurrenPrayer = UserDefaults.standard.integer(forKey: "rosaryCurrentPrayer")
        if previousCurrenPrayer != 0 {
            currentPrayer = (previousCurrenPrayer)
        }
        self.reloadStyle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadWebViews()
        self.loadRosaryImages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.barTintColor = navigationBarColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StyleSelectorSegue" {
            if let nc = segue.destination as? UINavigationController,
               let contentVC = nc.viewControllers[0] as? StyleSelectorViewController {
                contentVC.preferredContentSize = CGSize(width: 280, height: 240)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
    }
    
    // MARK: - Helpers
    
    func loadRosaryImages() {
        rosaryImagesArray = []
        rosaryImagesArray.append(#imageLiteral(resourceName: "rosary_initial"))
        
        titlesArray = []
        titlesArray.append("Oraciones inicales")
        
        for mystery in 1...5 {
            if mystery == 1 {
                rosaryImagesArray.append(#imageLiteral(resourceName: "rosary_mystery"))
            }else{
                rosaryImagesArray.append(#imageLiteral(resourceName: "rosary2_mystery"))
            }
            
            for aveMaria in 1...10 {
                var imageName = ""
                if mystery == 1 {
                    imageName = "rosary_\(aveMaria)"
                }else{
                    imageName = "rosary2_\(aveMaria)"
                }
                
                rosaryImagesArray.append(UIImage(named: imageName) ?? UIImage())
                titlesArray.append("\(mystery)º Misterio")
            }
            titlesArray.append("\(mystery)º Misterio")
        }
        titlesArray.append("Por las intenciones del Papa")
        titlesArray.append("Oraciones finales")
        rosaryImagesArray.append(UIImage(named: "litanies") ?? UIImage())
        self.title = titlesArray[currentPrayer]
        
    }
    
    func loadWebViews() {
        UserDefaults.standard.set(tabBar.selectedSegmentIndex, forKey:"rosarySelectedSegmentIndex")
        UserDefaults.standard.set(selectedMysteries, forKey:"selectedMysteries")
        DispatchQueue.main.asyncAfter(deadline: .now() +  0.01) {
            self.loadPrayers()
        }
    }
    
    func loadPrayers() {
        loadedWebViews = 0
        prayersArray = []
        
        let request = NSFetchRequest<RosaryEntity>()
        let entity = NSEntityDescription.entity(forEntityName: "RosaryEntity",
                                                in:LiturGuia.moc())
        request.entity = entity
        let predicate = NSPredicate(format: "type == %@", selectedMysteries)
        
        request.predicate = predicate
        
        let sd = NSSortDescriptor(key: "number", ascending:true)
        
        request.sortDescriptors = [sd]
        
        let array = (try? LiturGuia.moc().fetch(request))
        
        if let array = array,
           array.count > 0 {
            
            if array.count != 5 {
                let messageErr = "Error al cargar los misterios"
                
                let alert = UIAlertController(title: nil, message: messageErr, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            prayersArray.append(addStyle(toHTML: initialPrayerHTML))
            for mystery in 1...5 {
                prayersArray.append(addStyle(toHTML: generateMysteryHTML(rosary: array[mystery - 1])))
                let rosary = array[mystery - 1]
                let mysteryTitle = rosary.title ?? ""
                let aveMariaHTML = generateAveMaria(title: mysteryTitle, last: false)
                
                for _ in 1...9 {
                    prayersArray.append(addStyle(toHTML: aveMariaHTML))
                }
                let lastAveMariaHTML = generateAveMaria(title: mysteryTitle, last: true)
                prayersArray.append(addStyle(toHTML: lastAveMariaHTML))
            }
            prayersArray.append(addStyle(toHTML: litanieHTML))
            prayersArray.append(addStyle(toHTML: lastPrayersHTML))
        }
        
        self.updateWebView()
    }
    
    func generateAveMaria(title: String, last: Bool) -> String {
        var content = last ? lastAveMariaHTML : aveMariaHTML
        content = content.replacingOccurrences(of: "{titulo}", with: title)
        content = content.replacingOccurrences(of: "<style></style>", with: cssCode)
        
        return content
    }
    
    func generateMysteryHTML(rosary: RosaryEntity!) -> String {
        var content = announcementHTML
        content = content.replacingOccurrences(of: "{titulo}", with: rosary.title)
        content = content.replacingOccurrences(of: "{subtitulo}", with: rosary.subtitle)
        content = content.replacingOccurrences(of: "{tipo}", with: rosary.type.uppercased())
        content = content.replacingOccurrences(of: "{no_misterio}", with: rosary.number.stringValue)
        
        mysteryTitle = rosary.title
        
        return content
    }
    
    func updateWebView() {
        prayerLabel.attributedText = prayersArray[currentPrayer].htmlToAttributedString
        if currentPrayer < rosaryImagesArray.count {
            rosaryImageView.image = rosaryImagesArray[currentPrayer]
        }else{
            rosaryImageView.image = nil
        }
    }
    
    func html(withPath path: String) -> String {
        let file = Bundle.main.path(forResource: path, ofType:"html")
        return (try? String(contentsOfFile: file ?? "", encoding: .utf8)) ?? ""
    }
    
    func addStyle(toHTML html: String?) -> String {
        return html?.replacingOccurrences(of: "<style></style>", with: cssCode) ?? ""
    }
    
    // MARK: - Actions
    
    @IBAction func nextPrayer(_ sender: Any) {
        if currentPrayer + 1 < prayersArray.count {
            currentPrayer += 1
            self.updateWebView()
            if currentPrayer%11 == 0 {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            }
        }
        self.title = titlesArray[currentPrayer]
        UserDefaults.standard.set(currentPrayer, forKey:"rosaryCurrentPrayer")
        let previousCurrenPrayer = UserDefaults.standard.integer(forKey: "rosaryCurrentPrayer")
        if previousCurrenPrayer != 0 {
            currentPrayer = (previousCurrenPrayer)
        }
    }
    
    @IBAction func previousPrayer(_ sender: Any) {
        if currentPrayer - 1 >= 0 {
            currentPrayer -= 1
            self.updateWebView()
        }
        self.title = titlesArray[currentPrayer]
        UserDefaults.standard.set(currentPrayer, forKey:"rosaryCurrentPrayer")
    }
    
    @IBAction func styleSelectorButtonTapped(_ sender: Any) {
        if let nc = self.storyboard?.instantiateViewController(withIdentifier: "StyleSelector") as? UINavigationController{
            let segue:WYStoryboardPopoverSegue! = WYStoryboardPopoverSegue(identifier:"StyleSelectorSegue", source:self, destination:nc)
            self.prepare(for: segue, sender:sender)
            segue.perform()
            
        }
    }
    
    @IBAction func resetButtonTapped(_ sender: Any) {
        currentPrayer = 0
        self.title = "Oraciones iniciales"
        UserDefaults.standard.set(currentPrayer, forKey:"rosaryCurrentPrayer")
        self.updateWebView()
    }
    
}

// MARK: - Style's
extension RosaryViewController: StyleSelectorViewControllerDelegate {
    func styleChanged() {
        reloadStyle()
    }
    
    func fontChanged() {
        reloadStyle()
    }
    
    func fontSizeChanged() {
        reloadStyle()
    }
    
    func reloadStyle() {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let folderPath = documentsDirectory.appending("/HTML")
        let styleFile = folderPath.appending("/style.css")

        let style = (try? String(contentsOfFile:styleFile)) ?? ""
        
        var textColor = "black"
        if UserDefaults.standard.string(forKey: "TextStyle") == "white" {
            textColor = "black"
            self.view.backgroundColor = .white
            self.navigationController?.navigationBar.barTintColor = navigationBarColor
            tabBar.backgroundColor = navigationBarColor
        }else if UserDefaults.standard.string(forKey: "TextStyle") == "sepia" {
            textColor = "\(sepiaTextColor.toHexString())"
            self.view.backgroundColor = sepiaBGColor
            self.navigationController?.navigationBar.barTintColor = sepiaTextColor
            tabBar.backgroundColor = sepiaTextColor
        }else if UserDefaults.standard.string(forKey: "TextStyle") == "night" {
            textColor = "white"
            self.view.backgroundColor = .black
            self.navigationController?.navigationBar.barTintColor = .black
            tabBar.backgroundColor = .black
        }
        tabBar.setNeedsDisplay()
        
        let bodyCode = "body { margin: 15px; padding: 0px; text-align: justify; color: \(textColor);}"
        
        cssCode = "<style>\(style) \(bodyCode)</style>"
    }
    
}
