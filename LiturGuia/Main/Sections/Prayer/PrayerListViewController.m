//
//  PrayerListViewController.m
//  LiturGuia
//
//  Created by Krloz on 22/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "PrayerListViewController.h"
#import "PrayerEntity.h"
#import "DataSyncer.h"
#import "PrayerViewController.h"
#import "PrayerGroupTableViewCell.h"

@interface PrayerListViewController (){
    NSMutableDictionary *groupDictionary;
    NSMutableArray *groupNamesArray;
    NSArray *searchResult;
    NSMutableArray *expandedSections;
}

@end

@implementation PrayerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(prayerSyncFinished) name:@"PrayersSyncFinished" object:nil];
    
    [self prayerSyncFinished];
    
    
    
    [_tableView setContentOffset:CGPointMake(0.0, _tableView.tableHeaderView.frame.size.height) animated:YES];
    
        _searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
        //    _searchControllerAT.searchBar.scopeButtonTitles = [[NSArray alloc]initWithObjects:@"UserId", @"JobTitleName", nil];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = self;
        [_searchController.searchBar sizeToFit];
    //    _searchControllerNT.searchBar.barTintColor = self.navigationController.navigationBar.barTintColor;
        _searchController.searchBar.tintColor = self.navigationController.navigationBar.barTintColor;
        _searchController.dimsBackgroundDuringPresentation = NO;
        self.definesPresentationContext = YES;
    _tableView.tableHeaderView = _searchController.searchBar;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Lista de oraciones"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqual:@"PrayerSegue"]) {
        PrayerViewController *vc = segue.destinationViewController;
        vc.prayer = sender;
    }
}

-(void)prayerSyncFinished{    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:@"PrayerEntity"
                inManagedObjectContext:[LiturGuia moc]];
    [request setEntity:entity];
    
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor     sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"language == %@", [userDefaults objectForKey:@"language"]];
    
    [request setPredicate:predicate];

    
    NSError *error;
    NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];

    if (array != nil) {
        groupDictionary = [NSMutableDictionary dictionary];
        groupNamesArray = [NSMutableArray array];
        
        for (PrayerEntity *prayer in array) {
            if (!groupDictionary[prayer.group]) {
                groupDictionary[prayer.group] = [NSMutableArray array];
                [groupNamesArray addObject:prayer.group];
            }
            [groupDictionary[prayer.group] addObject:prayer];
        }
        
        expandedSections = [NSMutableArray array];
        
        for (int i = 0; i<groupDictionary.count; i++) {
            expandedSections[i] = @NO;
        }
        
        [_tableView reloadData];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    if ( _searchController.active) {
        return 1;
    } else {
        return [groupNamesArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_searchController.active) {
        return [searchResult count];
    } else {
        if ([expandedSections[section] isEqual:@YES]) {
            return [groupDictionary [groupNamesArray[section]] count]+1;
        } else {
            return 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    NSString *groupName = groupNamesArray [indexPath.section];
    NSArray *objectsForGroup = groupDictionary[groupName];

    
    if (_searchController.active) {
        cell = [_tableView dequeueReusableCellWithIdentifier:@"PrayerTableViewCell" ];
        PrayerEntity *prayer = searchResult[indexPath.row];
        cell.textLabel.text = prayer.name;
    }else{
        if (indexPath.row == 0) {
            PrayerGroupTableViewCell *groupCell;
            
            groupCell = [tableView dequeueReusableCellWithIdentifier:@"GroupTableViewCell" forIndexPath:indexPath];
            groupCell.groupNameLabel.text = groupName;
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if([(NSNumber*)expandedSections[indexPath.section]  isEqual: @YES]){
                groupCell.arrowImageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(0));
            }else{
                groupCell.arrowImageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(90));
            }
            return groupCell;
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"PrayerTableViewCell" forIndexPath:indexPath];
            PrayerEntity *prayer = objectsForGroup[indexPath.row - 1];
            cell.textLabel.text = prayer.name;
        }
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];


    
    if (_searchController.active) {
        PrayerEntity *prayer = searchResult[indexPath.row ];
        
        [self performSegueWithIdentifier:@"PrayerSegue" sender:prayer];
    } else {
        if (indexPath.row == 0) {
            if([(NSNumber*)expandedSections[indexPath.section]  isEqual: @YES]){
                expandedSections[indexPath.section] = @NO;
                
            }else{
                expandedSections[indexPath.section] = @YES;
            }
            [_tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
        }else{
            NSString *groupName = groupNamesArray[indexPath.section];
            NSArray *objectsForGroup = groupDictionary[groupName];
            
            PrayerEntity *prayer = objectsForGroup[indexPath.row - 1];
            
            [self performSegueWithIdentifier:@"PrayerSegue" sender:prayer];
        }
    }
}


#pragma mark - UISearchController

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity =
//    [NSEntityDescription entityForName:@"PrayerEntity"
//                inManagedObjectContext:[LiturGuia moc]];
//    [request setEntity:entity];
//
//    NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchText];
//
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.name matches[cd] %@", regexString];
//
//
////    NSPredicate *predicate =
////    [NSPredicate predicateWithFormat:@"text contains[cd] %@ OR name contains[cd] %@", [searchText lowercaseString], [searchText lowercaseString]];
//    [request setPredicate:predicate];
//
//    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor     sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
//
//    NSError *error;
//    NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];
//    if (array != nil) {
//        searchResult = array;
//    }
//}

//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString
//                               scope:[[controller.searchBar scopeButtonTitles]
//                                      objectAtIndex:[controller.searchBar
//                                                     selectedScopeButtonIndex]]];
//    return YES;
//}
//
//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [searchBar resignFirstResponder];
//}


-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *searchString = searchController.searchBar.text;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity =
        [NSEntityDescription entityForName:@"PrayerEntity"
                    inManagedObjectContext:[LiturGuia moc]];
        [request setEntity:entity];
        
        NSString *regexString  = [NSString stringWithFormat:@".*\\b%@.*", searchString];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.name matches[cd] %@", regexString];

        
    //    NSPredicate *predicate =
    //    [NSPredicate predicateWithFormat:@"text contains[cd] %@ OR name contains[cd] %@", [searchText lowercaseString], [searchText lowercaseString]];
        [request setPredicate:predicate];
        
        request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor     sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
        
        NSError *error;
        NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];
        if (array != nil) {
            searchResult = array;
        }
    
    
    
    [_tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    [self updateSearchResultsForSearchController:_searchController];
}

@end
