//
//  PrayerViewController.m
//  LiturGuia
//
//  Created by Krloz on 22/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "PrayerViewController.h"
#import "WYPopoverController.h"
#import "WYStoryboardPopoverSegue.h"
#import "UIColor+Expanded.h"
#import <Appirater/Appirater.h>
#import <LiturGuia-Swift.h>

@interface PrayerViewController (){
    WYPopoverController* stylePopoverController;
}

@end

@implementation PrayerViewController

@synthesize prayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Appirater userDidSignificantEvent:YES];
    
    self.title = prayer.name;
    
//    [KVNProgress showWithStatus:@"Cargando oración..."];
    
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [self loadHTML];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateStyleColor];

        [super viewWillAppear:animated];
        
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Oracion"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"StyleSelectorSegue"])
    {
        UINavigationController *navigationController = segue.destinationViewController;
        StyleSelectorViewController* contentVC = [[navigationController viewControllers] objectAtIndex:0];
        contentVC.preferredContentSize = CGSizeMake(280, 240);
        contentVC.delegate = self;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            SettingsPanelManager *controller = [SettingsPanelManager shared];
            [controller setUpSettingsPanelWithContentVC:(contentVC) toParent:(self)];
        }else{
            WYStoryboardPopoverSegue* popoverSegue = (WYStoryboardPopoverSegue*)segue;
            stylePopoverController = [popoverSegue popoverControllerWithSender:sender
                                                      permittedArrowDirections:WYPopoverArrowDirectionAny
                                                                      animated:YES];
            stylePopoverController.popoverLayoutMargins = UIEdgeInsetsMake(4, 4, 4, 4);
        }
    }
}

-(void)loadHTML{
    NSString *htmlString = prayer.text;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"HTML"];
    NSString *styleFile = [folderPath stringByAppendingPathComponent:@"style.css"];
    NSString *cssCode = [[NSString alloc] initWithContentsOfFile:styleFile
                                                    usedEncoding:nil
                                                           error:nil];
    
    NSString *textColor;
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        textColor = @"black";
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        textColor = [NSString stringWithFormat:@"#%@", [SEPIA_TEXT_COLOR hexStringValue]];
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        textColor = @"white";
    }
    
    NSString *bodyCode = [NSString stringWithFormat:@"body { margin: 15px; padding: 0px; text-align: justify; color: %@;}", textColor];
    
    htmlString = [htmlString cleanEmbbedCSS];
    cssCode = [NSString stringWithFormat:@"<style type=\"text/css\">%@ %@</style>", cssCode, bodyCode];
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<style></style>" withString:cssCode];
    
    [_testLabel setHtml:htmlString];
}

-(void)updateStyleColor{
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        
        self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
        self.view.backgroundColor = [UIColor whiteColor];
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        
        self.navigationController.navigationBar.barTintColor = SEPIA_TEXT_COLOR;
        self.view.backgroundColor = SEPIA_BACKGROUND_COLOR;
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        self.view.backgroundColor = [UIColor blackColor];
    }
}

- (IBAction)shareButtonTapped:(id)sender {
    NSString *text = _testLabel.text;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = text;
    
    //    self.view.window.userInteractionEnabled = NO;
    //    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    [KVNProgress showWithStatus:@"Espere..."];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSArray *objectsToShare = @[text];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        activityVC.excludedActivityTypes = excludeActivities;
        
        NSString *messageErr = @"Para compartir la oración en Facebook es necesario pegar el contenido del portapapeles en la ventana que aparecerá al seleccionar esa opción.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
        
        
        [self presentViewController:activityVC animated:YES completion:^{
            [KVNProgress dismiss];
            //            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
            //            self.view.window.userInteractionEnabled = YES;
        }];
    });
}

#pragma mark - Style's

- (void)fontSizeChanged{
    [self updateStyle];
}

-(void)fontChanged{
    [self updateStyle];
}

-(void)styleChanged{
    [self updateStyle];
    [self updateStyleColor];
}

-(void)updateStyle{
//    self.view.window.userInteractionEnabled = NO;
    
//    [KVNProgress showWithStatus:@"Actualizando..."];
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.labelText = @"Actualizando...";

    [self loadHTML];
}


@end



