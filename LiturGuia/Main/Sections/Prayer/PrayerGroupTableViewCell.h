//
//  PrayerGroupTableViewCell.h
//  LiturGuia
//
//  Created by Krloz on 06/07/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrayerGroupTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImageView;

@end
