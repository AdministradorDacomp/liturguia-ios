//
//  ThridPartyPrayersViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 6/21/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import Material
import MaryPopin
import Parse
import KVNProgress
import EmptyDataSet_Swift
import WYPopoverController

class ThridPartyPrayersViewController: UIViewController {
    
    //MARK: - @IBOutlets
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var howManyLabel:UILabel!
    @IBOutlet weak var shareButton:FABButton!
    @IBOutlet weak var addButton:FABButton!
    @IBOutlet weak var prayButton:FABButton!
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var footerView:UIView!
    
    var dataSourceArray = [PFObject]()
    var selectedIndexPaths:Set<IndexPath> = []
    var userDidPray = false
    var prayerCountObject:PFObject?
    
    var config = ConfigurationEmptyDataSet(
        text: "No hay intenciones agregadas para el día de hoy" ,
        detailText:"Puedes agregar tus intenciones presionando el botón \"+\" para que usuarios de LiturGuía pidan por ellas en todas partes del mundo.",
        image: nil )
    
    var fontName = UserDefaults.standard.string(forKey:"fontDisplayNameConscience") ?? "HelveticaNeue-Medium"
    var fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
    var stylePopoverController: WYPopoverController? = nil
    private lazy var useCaseController = SettingsPanelManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        
        reloadData()
        
        updateHowManyPrays()
        
        KVNProgress.show()
        
        headerView.isHidden = true
        tableView.isHidden = true
        footerView.isHidden = true
        shareButton.isEnabled = false
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.emptyDataSetView { [weak self] view in
            if let `self` = self {
                view.titleLabelString(self.config.titleString)
                    .detailLabelString(self.config.detailString)
                    .image(self.config.image)
                    .imageAnimation(self.config.imageAnimation)
                    .buttonTitle(self.config.buttonTitle(.normal), for: .normal)
                    .buttonTitle(self.config.buttonTitle(.highlighted), for: .highlighted)
                    .buttonBackgroundImage(self.config.buttonBackgroundImage(.normal), for: .normal)
                    .buttonBackgroundImage(self.config.buttonBackgroundImage(.highlighted), for: .highlighted)
                    .dataSetBackgroundColor(self.config.backgroundColor)
                    .verticalSpace(self.config.spaceHeight)
                    .shouldDisplay(true)
                    .shouldFadeIn(true)
                    .isTouchAllowed(true)
                    .isScrollAllowed(true)
                    .isImageViewAnimateAllowed(false)
                    .didTapDataButton {
                    }
                    .didTapContentView {
                        //                        self.emptyDataSetDidTapView()
                }
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StyleSelectorSegue"{
            let nvc = segue.destination as! UINavigationController
            if let contentVC = nvc.viewControllers[0] as? StyleConscienceViewController{
                contentVC.preferredContentSize = CGSize(width: 280, height: 160)
                contentVC.delegate = self
                if UIDevice.current.userInterfaceIdiom == .pad {
                    useCaseController.setUpSettingsPanel(contentVC: contentVC, toParent: self)
                }else{
                    if let popoverSegue = segue as? WYStoryboardPopoverSegue {
                        
                        stylePopoverController = popoverSegue.popoverControllerWithSender(
                            sender,
                            permittedArrowDirections:WYPopoverArrowDirection.any,
                            animated:true)
                        
                        stylePopoverController?.popoverLayoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
                    }
                }
            }
        }
    }
    
    //MARK: - Helpers
    
    func reloadData() {
        if let pai = PFUser.current()?["pai"] as? PFObject{
            print(pai)
        }
        let query = PFQuery(className: "IntencionesDeUsuarios")
            .whereKey("fecha", lessThanOrEqualTo: Date())
            .whereKey("vigencia", greaterThanOrEqualTo: Date())
        
        query.findObjectsInBackground {[weak self] (intentions, error) in
            guard let strongSelf = self else { return }
            KVNProgress.dismiss()

            let sortedIntentions = intentions?.sorted(by: { (one, two) -> Bool in
                return one.updatedAt ?? Date() > two.updatedAt ?? Date()
            })
            if let error = error{
                let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                strongSelf.present(alert, animated: true, completion: nil)
            }else if let intentions = sortedIntentions{
                strongSelf.headerView.isHidden = false
                strongSelf.tableView.isHidden = false
                strongSelf.footerView.isHidden = false
                strongSelf.dataSourceArray = intentions
                strongSelf.tableView.reloadData()
            }
        }
        
        if prayerCountObject == nil{
            let countQuery = PFQuery(className: "ConteoDeOraciones")
                .whereKey("fecha", equalTo: Date.removeTime(from: Date()))
            countQuery.getFirstObjectInBackground {[weak self] (object, error) in
                guard let strongSelf = self else { return }
                if let error = error as NSError?,
                    error.code != 101{
                    let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }else{
                    strongSelf.prayerCountObject = object

                    strongSelf.updateHowManyPrays()
                }
            }
            
        }
        
        
    }
    
    func updateHowManyPrays()  {
        if let prayerCountObject = prayerCountObject,
            let prayerCount = prayerCountObject["oraciones"] as? Int{
            
            if prayerCount == 1 && userDidPray{
                howManyLabel.text = "Usted ha orado por estas intenciones."
            }else{
                let personas = userDidPray ? prayerCount - 1: prayerCount
                howManyLabel.text = "\(userDidPray ? "Usted y " : "")\(personas) \(personas == 1 ? "persona" : "personas") \(userDidPray ? "más " : "")\(personas == 1 && !userDidPray ? "ha" : "han") orado por estas intenciones"
                if let countries = prayerCountObject["paises"] as? [String:Any]{
                    var countriesSet:Set<String> = []
                    let currentLocale = Locale.current as NSLocale
                    for (countryCode, _) in countries{
                        if let countryName = currentLocale.displayName(forKey: .countryCode, value: countryCode){
                            countriesSet.insert(countryName)
                        }
                    }
                    var countriesString = " en "
                    for (index, country) in countriesSet.sorted(by: { (one, two) -> Bool in
                        return one < two
                    }).enumerated(){
                        if index == countriesSet.count - 1 && countriesSet.count != 1{
                            countriesString += country.first?.lowercased() == "i" ? " e " : " y "
                        }else if index != 0{
                            countriesString += ", "
                        }
                        countriesString += country
                    }
                    
                    howManyLabel.text! += countriesString
                }
                howManyLabel.text! += "."
            }
            
        }else if userDidPray{
            howManyLabel.text = "Usted ha orado por estas intenciones."
        }else{
            howManyLabel.text = "Nadie ha orado por estas intenciones el día de hoy."
        }
    
    }
    
    func showIntentionAlert(withIntention intention:PFObject? = nil) {
        if let popUpVC = self.storyboard?.instantiateViewController(withIdentifier: "AddIntentionViewController") as? AddIntentionViewController{
            if let intention = intention{
                popUpVC.intention = intention
            }
            
            
            popUpVC.view.frame.size = CGSize(width: 310, height: 300)
            popUpVC.view.layer.cornerRadius = 5.0
            popUpVC.view.clipsToBounds = true
            
            popUpVC.setPopinTransitionStyle(.slide)
            popUpVC.setPopinTransitionDirection(.top)
            let blurParameters = BKTBlurParameters()
            blurParameters.alpha = 1.0
            blurParameters.radius = 8
            blurParameters.saturationDeltaFactor = 0.9
            blurParameters.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
            popUpVC.setBlurParameters(blurParameters)
            popUpVC.setPopinOptions(BKTPopinOption(rawValue: BKTPopinOption.blurryDimmingView.rawValue))
            popUpVC.delegate = self
            

            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            
            
            self.navigationController?.presentPopinController(popUpVC, animated: true, completion: nil)
        }
    }
    
    //MARK: - Button actions
    
    @IBAction func prayButtonTapped(_ sender:UIButton) {
        prayButton.isEnabled = false
        userDidPray = true
        
        let currentLocale = Locale.current as NSLocale
        let countryCode = currentLocale.object(forKey: .countryCode) as? String ?? ""
        if let prayerCountObject = prayerCountObject{
            prayerCountObject.incrementKey("oraciones", byAmount: 1)
            var paises = prayerCountObject["paises"] as? [String:Any] ?? [:]
            paises[countryCode] = true
            prayerCountObject["paises"] = paises
        }else{
            prayerCountObject = PFObject(className: "ConteoDeOraciones", dictionary:
                [
                    "oraciones": 1,
                    "fecha": Date.removeTime(from: Date()),
                    "paises": [countryCode:true]
                    
                ])
        }
        prayerCountObject?.saveInBackground()
        updateHowManyPrays()
    }
    
    @IBAction func shareButtonTapped(_ sender:UIButton) {
        var textToShare = """
http://www.liturguia.com

Oremos unos por otros.
Oraciones registradas en LiturGuía:\n\n
"""
        let sortedArray = selectedIndexPaths.sorted { (one, two) -> Bool in
            return one.row < two.row
        }
        for indexPath in sortedArray{
            let object = dataSourceArray[indexPath.row]
            textToShare += "Por \(object["persona"] as? String ?? "")\n\(object["peticion"] as? String ?? "")\n\n"
        }
        let activityViewController = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = []
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func addButtonTapped(_ sender:UIButton) {
        showIntentionAlert()
    }

}

extension ThridPartyPrayersViewController:StyleConscienceViewControllerDelegate{
    func fontSizeChanged() {
        fontSize = CGFloat(UserDefaults.standard.float(forKey:"fontSizeConscience"))
        tableView.reloadData()
    }
    
    func fontChanged() {
        fontName = UserDefaults.standard.string(forKey:"fontNameConscience") ?? "HelveticaNeue-Medium"
        tableView.reloadData()
    }
    
}

extension ThridPartyPrayersViewController:AddIntentionViewControllerDelegate{
    func userDidSelectAdd(person: String, request: String, expiration: Date, intention:PFObject?) {
        if let user = PFUser.current(){
            let object = intention ?? PFObject(className: "IntencionesDeUsuarios", dictionary: [
                "usuario": user,
                "fecha": Date()
                ])
            object["vigencia"] = expiration
            object["persona"] = person
            object["peticion"] = request
            
            KVNProgress.show()
            object.saveInBackground { [weak self] (success, error) in
                guard let strongSelf = self else { return }

                KVNProgress.dismiss()
                
                if let error = error{
                    let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }else{
                    strongSelf.reloadData()
                }
            }
        }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        self.navigationController?.dismissCurrentPopinController(animated: true)
    }
    
    func userDidSelectCancel() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        self.navigationController?.dismissCurrentPopinController(animated: true)
    }
    
    
}

extension ThridPartyPrayersViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IntentionCell", for: indexPath) as! IntentionCell
        
        let parseObject = dataSourceArray[indexPath.row]
        cell.personLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0) + 4.0)?.bold()
        cell.requestLabel.font = UIFont(name: fontName, size: (fontSize >= 8 ? fontSize : 8.0))
        
        cell.personLabel.text = "Por " + (parseObject["persona"] as? String ?? "")
        cell.requestLabel.text = parseObject["peticion"] as? String
        cell.contentView.backgroundColor =  selectedIndexPaths.contains(indexPath) ? #colorLiteral(red: 0.2470588235, green: 0.3254901961, blue: 0.6901960784, alpha: 0.1) : .white
        
        if let user = parseObject["usuario"] as? PFUser,
            user.objectId == PFUser.current()?.objectId,
            let vigencia = parseObject["vigencia"] as? Date,
            vigencia <= Date().addDay(n: 15){
            cell.editButton.isHidden = false
            cell.delegate = self
            cell.personLabelTrailingConstraint.isActive = true
            cell.requestLabelTrailingConstraint.isActive = true
            
            
            
            
        }else{
            cell.editButton.isHidden = true
            cell.personLabelTrailingConstraint.isActive = false
            cell.requestLabelTrailingConstraint.isActive = false
        }
        
        cell.editButton.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndexPaths.contains(indexPath){
            selectedIndexPaths.remove(indexPath)
        }else{
            selectedIndexPaths.insert(indexPath)
        }
        
        shareButton.isEnabled = selectedIndexPaths.count != 0
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
}

//MARK: - EmptyDataSetSource, EmptyDataSetDelegate
extension ThridPartyPrayersViewController:EmptyDataSetSource, EmptyDataSetDelegate{
    func emptyDataSetWillAppear(_ scrollView: UIScrollView) {
        headerView.isHidden = true
    }
    
    func emptyDataSetWillDisappear(_ scrollView: UIScrollView) {
        headerView.isHidden = false
    }
}

extension ThridPartyPrayersViewController:IntentionCellDelegate{
    func editButtonTapped(withRow row: Int) {
        let parseObject = dataSourceArray[row]
        showIntentionAlert(withIntention: parseObject)
    }
}

protocol IntentionCellDelegate {
    func editButtonTapped(withRow:Int)
}

class IntentionCell:UITableViewCell{
    var delegate: IntentionCellDelegate?
    @IBOutlet weak var personLabel:UILabel!
    @IBOutlet weak var requestLabel:UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet  var personLabelTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet  var requestLabelTrailingConstraint: NSLayoutConstraint!
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        delegate?.editButtonTapped(withRow: sender.tag)
    }
}

protocol AddIntentionViewControllerDelegate {
    func userDidSelectAdd(person:String, request:String, expiration:Date, intention:PFObject?)
    func userDidSelectCancel()
}

class AddIntentionViewController: UIViewController {
    @IBOutlet weak var personTextField:UITextField!
    @IBOutlet weak var requestTextView:UITextView!
    @IBOutlet weak var expirationTextField:UITextField!
    
    let datePicker = UIDatePicker()
    var delegate:AddIntentionViewControllerDelegate?
    var intention: PFObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date().addMonth(n: 3)
        datePicker.maximumDate = datePicker.maximumDate?.addDay(n: 1)
        datePicker.minimumDate = Date().addDay(n: 1)
        requestTextView.textContainer.maximumNumberOfLines = 10
        requestTextView.textContainer.lineBreakMode = .byTruncatingTail
        datePicker.addTarget(self, action: #selector(expirationDateDidChange(_:)), for: .valueChanged)
        expirationTextField.inputView = datePicker
        personTextField.delegate = self
        requestTextView.delegate = self
        
        if let intention = intention {
            personTextField.text = intention["persona"] as? String
            requestTextView.text = intention["peticion"] as? String
            datePicker.date = intention["vigencia"] as? Date ?? Date()
        }
        expirationDateDidChange(datePicker)
    }
    
    @objc func expirationDateDidChange(_ sender:UIDatePicker){
        expirationTextField.text = DateFormatter.localizedString(from: datePicker.date, dateStyle: .short, timeStyle: .none)
    }
    
    @IBAction func addButtonTapped(_ sender:UIButton) {
        var messageError = ""
        if personTextField.text! == "" {
            messageError = "Es necesario ingresar la persona(s) para la intención."
        }else if requestTextView.text! == "" {
            messageError = "Es necesario ingresar la petición que quiere realizar."
        }
        if messageError != ""{
            let alert = UIAlertController(title: nil, message: messageError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            delegate?.userDidSelectAdd(person: personTextField.text!, request: requestTextView.text!, expiration: datePicker.date, intention: intention)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender:UIButton) {
        delegate?.userDidSelectCancel()
    }
}

extension AddIntentionViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString text: String) -> Bool {
//        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//
////        if string.count > 1{
//            textField.text = String(newText.prefix(30))
//            return false
////        }else{
////            let maxLength = 30
////            let currentString: NSString = textField.text! as NSString
////            let newString: NSString =
////                currentString.replacingCharacters(in: range, with: string) as NSString
////            return newString.length <= maxLength
////        }
        
        if text == ""{
            return true
        }
        let str = (textField.text ?? "" + text)
        
        
        
        if str.count < 30 {
            return true
        }
    
        
        let endIndex = str.index(str.startIndex, offsetBy: 30)
        let range = str.startIndex..<endIndex
        textField.text = String(str[range])
        return false
    }
    
    
}

extension AddIntentionViewController:UITextViewDelegate{
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
//        let newLines = text.components(separatedBy: CharacterSet.newlines)
//        let linesAfterChange = existingLines.count + newLines.count - 1
//        if(text == "\n") {
//            return linesAfterChange <= textView.textContainer.maximumNumberOfLines
//        }
//
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.count
//        return text == "" ? true : numberOfChars <= 300
//    }
    
//    func textViewDidChange(_ textView: UITextView) {
//        textView.removeTextUntilSatisfying(maxNumberOfLines: 10)
//    }
    
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//            let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
//            let newLines = text.components(separatedBy: CharacterSet.newlines)
//            let linesAfterChange = existingLines.count + newLines.count - 1
//            if(text == "\n") {
//                return linesAfterChange <= textView.textContainer.maximumNumberOfLines
//            }
    //
//            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//
//            if string.count > 1{
//                textField.text = String(string.prefix(30))
//                return false
//            }else{
//                let numberOfChars = newText.count
//                return text == "" ? true : numberOfChars <= 300
//            }

            if text == ""{
                return true
            }
            
            let str = "\(textView.text ?? "" ) \(text)"
            
            
            
            
            if str.count < 300 {
                return true
            }
            
            
            let endIndex = str.index(str.startIndex, offsetBy: 300)
            let range = str.startIndex..<endIndex
            textView.text = String(str[range])
            return false
        }
}
