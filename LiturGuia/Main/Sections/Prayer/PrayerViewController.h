//
//  PrayerViewController.h
//  LiturGuia
//
//  Created by Krloz on 22/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrayerEntity.h"
#import "StyleSelectorViewController.h"
#import <WebKit/WebKit.h>
#import "UILabel+HTML.h"

@interface PrayerViewController : UIViewController<StyleSelectorViewControllerDelegate>

@property (nonatomic, strong) PrayerEntity *prayer;
@property (strong, nonatomic) IBOutlet UILabel *testLabel;

@end
