//
//  BookViewController.m
//  LiturGuia
//
//  Created by Krloz on 23/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "BookViewController.h"
#import "SQLiteDBManager.h"


@interface BookViewController ()

@end

@implementation BookViewController{
    NSMutableArray *atSortedArray;
    NSMutableDictionary *atDictionary;
    NSMutableArray *ntSortedArray;
    NSMutableDictionary *ntDictionary;
    NSString *preSelectedSection;
    NSArray *searchResults;
    NSMutableArray *fullResultArray;
}

@synthesize delegate;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tabBar setSectionTitles:@[@"Antiguo testamento", @"Nuevo testamento"]];
    
    if ([self checkModal]) {
        UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonTapped:)];
        btnCancel.tintColor = [UIColor whiteColor];
        [self.navigationItem setRightBarButtonItem:btnCancel];
    }
    
    __weak BookViewController* weakSelf = self;
    [_tabBar setIndexChangeBlock:^(NSUInteger index) {
        if (index == 0) {
            [weakSelf showAT];
        }else if(index == 1){
            [weakSelf showNT];
        }
    }];
    _tabBar.selectionIndicatorHeight = 2.0f;
    _tabBar.backgroundColor = kNavigationBarColor;
    _tabBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName:              [UIFont fontWithName:@"HelveticaNeue" size:14]};
    _tabBar.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    _tabBar.selectionIndicatorColor = [UIColor whiteColor];
    _tabBar.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    _tabBar.selectedSegmentIndex = 0;
    _tabBar.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _tabBar.shouldAnimateUserSelection = YES;
    
    fullResultArray = [NSMutableArray array];
    
    [self loadAT];
    [self loadNT];
    
    if (!preSelectedSection ||[preSelectedSection isEqual:@"AT"]) {
        [self showAT];
        [_tabBar setSelectedSegmentIndex:0];
    }else if ([preSelectedSection isEqual:@"NT"]){
        [self showNT];
        [_tabBar setSelectedSegmentIndex:1];
    }
    
//    [_atTableView setContentOffset:CGPointMake(0, 44)];
//    [_ntTableView setContentOffset:CGPointMake(0, 44)];
    
    
    _searchControllerAT = [[UISearchController alloc]initWithSearchResultsController:nil];
    //    _searchControllerAT.searchBar.scopeButtonTitles = [[NSArray alloc]initWithObjects:@"UserId", @"JobTitleName", nil];
    _searchControllerAT.searchBar.delegate = self;
    _searchControllerAT.searchResultsUpdater = self;
    [_searchControllerAT.searchBar sizeToFit];
//    _searchControllerAT.searchBar.barTintColor = self.navigationController.navigationBar.barTintColor;
//    _searchControllerAT.searchBar.barStyle = UISearchBarStyleProminent;
    _searchControllerAT.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    _atTableView.tableHeaderView = _searchControllerAT.searchBar;
    
    _searchControllerNT = [[UISearchController alloc]initWithSearchResultsController:nil];
    //    _searchControllerAT.searchBar.scopeButtonTitles = [[NSArray alloc]initWithObjects:@"UserId", @"JobTitleName", nil];
    _searchControllerNT.searchBar.delegate = self;
    _searchControllerNT.searchResultsUpdater = self;
    [_searchControllerNT.searchBar sizeToFit];
//    _searchControllerNT.searchBar.barTintColor = self.navigationController.navigationBar.barTintColor;
//    _searchControllerNT.searchBar.tintColor = self.navigationController.navigationBar.barTintColor;
    _searchControllerNT.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    _ntTableView.tableHeaderView = _searchControllerNT.searchBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

-(void)cancelButtonTapped:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Helpers

-(void)showAT{
    _atTableView.hidden = NO;
    _ntTableView.hidden = YES;
}

-(void)showNT{
    _atTableView.hidden = YES;
    _ntTableView.hidden = NO;
}

-(void)preSelectSection:(NSString*)section{
    preSelectedSection = section;
}

-(BOOL)checkModal{
    return [self.parentViewController.presentingViewController.presentedViewController isEqual:self.parentViewController];
}


#pragma mark - Database

-(void)loadAT{
    NSString *query = @"SELECT g.nombre AS grupo, l.id, l.nombre, l.id_grupo FROM grupo g, libro l WHERE l.id_grupo = g.id  AND g.tipo = ? ORDER BY g.orden, l.orden, l.id";
    NSMutableArray *atArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[@"A"]];
    
    atDictionary = [NSMutableDictionary dictionary];
    atSortedArray = [NSMutableArray array];
    for (NSDictionary *row in atArray) {
        if (!atDictionary[row[@"grupo"]]) {
            atDictionary[row[@"grupo"]] = [NSMutableArray array];
            [atSortedArray addObject:row[@"grupo"]];
        }
        NSMutableArray *array = atDictionary[row[@"grupo"]];
        [array addObject:row];
        [fullResultArray addObject:row];
    }
    
    
}

-(void)loadNT{
    NSString *query = @"SELECT g.nombre AS grupo, l.id, l.nombre, l.id_grupo FROM grupo g, libro l WHERE l.id_grupo = g.id  AND g.tipo = ? ORDER BY g.orden, l.orden, l.id";
    NSMutableArray *ntArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[@"N"]];
    
    ntDictionary = [NSMutableDictionary dictionary];
    ntSortedArray = [NSMutableArray array];
    for (NSDictionary *row in ntArray) {
        if (!ntDictionary[row[@"grupo"]]) {
            ntDictionary[row[@"grupo"]] = [NSMutableArray array];
            [ntSortedArray addObject:row[@"grupo"]];
        }
        NSMutableArray *array = ntDictionary[row[@"grupo"]];
        [array addObject:row];
        [fullResultArray addObject:row];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == _atTableView) {
        return [atSortedArray count];
    }else if(tableView == _ntTableView){
        return [ntSortedArray count];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_searchControllerAT.active || _searchControllerNT.active) {
        return [searchResults count];
    } else {
        if (tableView == _atTableView) {
            return [atDictionary [atSortedArray[section]] count]+1;
        }else if(tableView == _ntTableView){
            return [ntDictionary [ntSortedArray[section]] count]+1;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    NSString * groupName;
    NSArray * objectsForGroup;
    
    if (tableView == _atTableView) {
        groupName = atSortedArray [indexPath.section];
        objectsForGroup = atDictionary[groupName];
    }else if(tableView == _ntTableView){
        groupName = ntSortedArray [indexPath.section];
        objectsForGroup = ntDictionary[groupName];
    }
    
    if (_searchControllerAT.active || _searchControllerNT.active) {
        NSDictionary *dic = [searchResults objectAtIndex:indexPath.row];
        cell = [_ntTableView dequeueReusableCellWithIdentifier:@"BookTableViewCell"];
        cell.textLabel.text = dic[@"nombre"];
    }else{
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"GroupTableViewCell" forIndexPath:indexPath];
            cell.textLabel.text = groupName;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"BookTableViewCell" forIndexPath:indexPath];
            cell.textLabel.text = objectsForGroup[indexPath.row - 1][@"nombre"];
        }
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int groupId, bookId;
    
    if (_searchControllerAT.active || _searchControllerNT.active) {
        _searchControllerAT.delegate = nil;
        NSDictionary *dic = [searchResults objectAtIndex:indexPath.row];
        bookId = [dic[@"id"] intValue];
        groupId = [dic[@"id_grupo"] intValue];
    }else{
        if (indexPath.row == 0) {
            return;
        }
        
        NSString * groupName;
        NSArray * objectsForGroup;
    
        if (tableView == _atTableView) {
            groupName = atSortedArray [indexPath.section];
            objectsForGroup = atDictionary[groupName];
        }else if(tableView == _ntTableView){
            groupName = ntSortedArray [indexPath.section];
            objectsForGroup = ntDictionary[groupName];
        }
    
        bookId = [objectsForGroup[indexPath.row - 1][@"id"] intValue];
        groupId = [objectsForGroup[indexPath.row - 1][@"id_grupo"] intValue];
    }
    
    NSString *query  = @"SELECT c.id AS chapterId, v.id AS verseId FROM capitulo c, versiculo v WHERE c.id_libro = ? AND v.id_capitulo = c.id ORDER BY c.orden, v.orden LIMIT 1";
    NSMutableArray *versesArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[@(bookId)]];
    if (versesArray.count > 0) {
        NSDictionary *row = versesArray[0];
        [[NSUserDefaults standardUserDefaults] setInteger:[row[@"chapterId"] intValue] forKey:@"chapterId"];
        [[NSUserDefaults standardUserDefaults] setInteger:[row[@"verseId"] intValue] forKey:@"verseId"];
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:bookId forKey:@"bookId"];
    [[NSUserDefaults standardUserDefaults] setInteger:groupId forKey:@"groupId"];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"chapterOrder"];
    
    NSString *bookName = [[tableView cellForRowAtIndexPath:indexPath] textLabel].text;
    [[NSUserDefaults standardUserDefaults] setObject:bookName forKey:@"bookName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([self checkModal]) {
        [delegate bookChanged];

        if (_searchControllerNT.active){
            [_searchControllerNT dismissViewControllerAnimated:NO completion:nil];
        }else if (_searchControllerAT.active){
            [_searchControllerAT dismissViewControllerAnimated:NO completion:nil];
        }
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }else{
        [self performSegueWithIdentifier:@"VersePushSegue" sender:self];
    }
    
}

#pragma mark - UISearchController

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"nombre contains[c] %@ OR grupo contains[c] %@", searchText,searchText];
//    searchResults = [fullResultArray filteredArrayUsingPredicate:resultPredicate];
//}
//
//-(BOOL)searchController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString
//                               scope:[[controller.searchBar scopeButtonTitles]
//                                      objectAtIndex:[controller.searchBar
//                                                     selectedScopeButtonIndex]]];
//    return YES;
//}
//
//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [searchBar resignFirstResponder];
////    [_atTableView setContentOffset:CGPointMake(0, 44) animated:YES];
////    [_ntTableView setContentOffset:CGPointMake(0, 44) animated:YES];
//}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSString *searchString = searchController.searchBar.text;
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"nombre contains[c] %@ OR grupo contains[c] %@", searchString, searchString];
    searchResults = [fullResultArray filteredArrayUsingPredicate:resultPredicate];
    
    [_atTableView reloadData];
    [_ntTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    [self updateSearchResultsForSearchController:_searchControllerNT.searchBar ? _searchControllerNT : _searchControllerAT];
}

@end
