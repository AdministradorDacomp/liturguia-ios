//
//  SearchResultBibleViewController.h
//  LiturGuia
//
//  Created by Krloz on 08/12/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultBibleViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *chapterTitleLabel;
@property (strong, nonatomic) IBOutlet UITableView *versesTableView;
@property (strong, nonatomic) NSArray *rows;
@property  int chapterId;
@property (strong, nonatomic) NSString *bookName;
@property  int verseId;
@property  int chapterOrder;
@property  int bookId;
@property (strong, nonatomic) id delegate;
@property  int groupId;

@end
