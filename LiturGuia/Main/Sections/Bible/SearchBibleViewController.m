//
//  SearchBibleViewController.m
//  LiturGuia
//
//  Created by Krloz on 29/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "SearchBibleViewController.h"
#import "SQLiteDBManager.h"
#import <QuartzCore/QuartzCore.h>
#import "SearchResultBibleViewController.h"

@interface SearchBibleViewController ()

@end

@implementation SearchBibleViewController{
    NSMutableArray *searchResultsVerses;
    NSMutableArray *searchResultsThemes;
    NSMutableDictionary *groupDictionary;
    NSMutableArray *groupNamesArray;
    NSMutableArray *expandedSections;
}

@synthesize delegate;

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _searchButton.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    [_searchTextField becomeFirstResponder];
    

    for (UIView *view in [[self view] subviews]) {
        [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"SearchResultBibleSegue"]) {
            SearchResultBibleViewController *vc = segue.destinationViewController;
        vc.delegate = delegate;
            vc.bookName = sender[@"bookName"];
            vc.bookId = [sender[@"bookId"] intValue];
            vc.groupId = [sender[@"groupId"] intValue];
            vc.chapterOrder = [sender[@"chapterOrder"] intValue];
            vc.verseId = [sender[@"verseId"] intValue];
            vc.chapterId = [sender[@"chapterId"] intValue];
        
        if (sender[@"rows"]) {
            vc.rows = sender[@"rows"];
        }
    }
}
- (IBAction)segmentedControlChanged:(id)sender {
    [self searchButtonTapped:sender];
    [_tableView reloadData];
}

#pragma mark - Button actions

- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)searchButtonTapped:(id)sender {
    if (_segmentedControl.selectedSegmentIndex == 0) {
        NSString *query = @"SELECT * FROM versiculo WHERE replace(replace(replace(replace(replace(replace(replace(replace( replace(replace(replace( lower(texto), 'á','a'), 'ã','a'), 'â','a'), 'é','e'), 'ê','e'), 'í','i'), 'ó','o') ,'õ','o') ,'ô','o'),'ú','u'), 'ç','c') LIKE ? LIMIT 100";
        
        
        NSData *data = [_searchTextField.text dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *newStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"%@", newStr);
        
        NSString *searchFormat = [NSString stringWithFormat:@"%%%@%%",newStr];
        searchResultsVerses = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[searchFormat]];
        
    }else if(_segmentedControl.selectedSegmentIndex == 1){
//        NSString *query = @"SELECT l.abreviacion, c.orden, t.pasaje, t.tema, v.texto FROM libro l, capitulo c, tema t, versiculo v WHERE c.id = t.id_capitulo AND c.id_libro = l.id AND v.id_capitulo = c.id AND v.orden BETWEEN inicio and fin AND replace(replace(replace(replace(replace(replace(replace(replace( replace(replace(replace( lower(tema), 'á','a'), 'ã','a'), 'â','a'), 'é','e'), 'ê','e'), 'í','i'), 'ó','o') ,'õ','o') ,'ô','o'),'ú','u'), 'ç','c') LIKE ? ";
        NSString *query = @"SELECT * FROM tema t WHERE  replace(replace(replace(replace(replace(replace(replace(replace( replace(replace(replace( lower(tema), 'á','a'), 'ã','a'), 'â','a'), 'é','e'), 'ê','e'), 'í','i'), 'ó','o') ,'õ','o') ,'ô','o'),'ú','u'), 'ç','c') LIKE ? ORDER BY tema";
        
        
        
        NSData *data = [_searchTextField.text dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *newStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"%@", newStr);
        
        NSString *searchFormat = [NSString stringWithFormat:@"%%%@%%",newStr];
        NSArray *array = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[searchFormat]];
        
        groupDictionary = [NSMutableDictionary dictionary];
        groupNamesArray = [NSMutableArray array];
        
        for (NSDictionary *theme in array) {
            if (!groupDictionary[theme[@"tema"]]) {
                groupDictionary[theme[@"tema"]] = [NSMutableArray array];
                [groupNamesArray addObject:theme[@"tema"]];
            }
            [groupDictionary[theme[@"tema"]] addObject:theme];
        }
        
        expandedSections = [NSMutableArray array];
        
        for (int i = 0; i<groupDictionary.count; i++) {
            expandedSections[i] = @NO;
        }

    }
    [_searchTextField resignFirstResponder];
    [_tableView reloadData];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self searchButtonTapped:self];
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_segmentedControl.selectedSegmentIndex == 0) {
        return 1;
    }
    else if (_segmentedControl.selectedSegmentIndex == 1) {
        return groupNamesArray.count;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(0, 0);
    if (_segmentedControl.selectedSegmentIndex == 0) {
        NSString *text = searchResultsVerses[indexPath.row][@"texto"];
        
        CGRect textRect = [text boundingRectWithSize:CGSizeMake(290, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:14] }
                                             context:nil];
        
        size = textRect.size;

//        size = [text sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:14] constrainedToSize:CGSizeMake(290, 999) lineBreakMode:NSLineBreakByWordWrapping];
    }else if(_segmentedControl.selectedSegmentIndex == 1){
        if (indexPath.row == 0) {
            return 35;
        }
        NSDictionary *dic = groupDictionary[groupNamesArray[indexPath.section]][indexPath.row - 1];
        NSString *text = dic[@"pasaje"];
        
        CGRect textRect = [text boundingRectWithSize:CGSizeMake(290, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:14] }
                                             context:nil];
        
        size = textRect.size;
        return (float)size.height + 20.0;
    }
    
    
    return size.height + 10.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_segmentedControl.selectedSegmentIndex == 0) {
       return searchResultsVerses.count;
    }else if(_segmentedControl.selectedSegmentIndex == 1){
        if ([expandedSections[section] isEqual:@YES]) {
            return [groupDictionary [groupNamesArray[section]] count]+1;
        } else {
            return 1;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if (_segmentedControl.selectedSegmentIndex == 0) {
        cell = [_tableView dequeueReusableCellWithIdentifier:@"VerseTableViewCell"];
        cell.textLabel.text = searchResultsVerses[indexPath.row][@"texto"];
    }else if(_segmentedControl.selectedSegmentIndex == 1){
        
        
        if (indexPath.row == 0) {
            cell = [_tableView dequeueReusableCellWithIdentifier:@"GroupTableViewCell"];
            cell.textLabel.text = groupNamesArray[indexPath.section];
            if([(NSNumber*)expandedSections[indexPath.section]  isEqual: @YES]){
//                groupCell.arrowImageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(0));
            }else{
//                groupCell.arrowImageView.transform = CGAffineTransformMakeRotation(DegreesToRadians(90));
            }
            
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"ThemeTableViewCell" forIndexPath:indexPath];
            NSDictionary *dic = groupDictionary[groupNamesArray[indexPath.section]][indexPath.row - 1];
            cell.textLabel.text = dic[@"pasaje"];
        }

    }else{
        cell =  [UITableViewCell new];
    }
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_segmentedControl.selectedSegmentIndex == 0) {
        NSDictionary *verseDic = searchResultsVerses[indexPath.row];
        
        NSString *query = @"SELECT l.nombre AS libro, c.orden, id_libro, id_grupo FROM capitulo c, libro l WHERE c.id = ? AND c.id_libro = l.id";
        
        NSMutableArray *result = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[verseDic[@"id_capitulo"]]];
        
//        [[NSUserDefaults standardUserDefaults] setInteger:[verseDic[@"id_capitulo"] intValue] forKey:@"chapterId"];
//        [[NSUserDefaults standardUserDefaults] setInteger:[verseDic[@"id"] intValue] forKey:@"verseId"];
        
        if (result.count > 0) {
            NSDictionary *dic = result[0];
            
//            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"id_libro"] integerValue] forKey:@"bookId"];
//            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"id_grupo"] integerValue] forKey:@"groupId"];
//            [[NSUserDefaults standardUserDefaults] setInteger:[dic[@"orden"] integerValue] forKey:@"chapterOrder"];
//            [[NSUserDefaults standardUserDefaults] setObject:dic[@"libro"] forKey:@"bookName"];
//            [[NSUserDefaults standardUserDefaults] synchronize];

            [self performSegueWithIdentifier:@"SearchResultBibleSegue" sender:@{@"bookId":dic[@"id_libro"],
                        @"groupId":dic[@"id_grupo"],
                        @"chapterOrder":dic[@"orden"],
                                                                                @"bookName":dic[@"libro"],
                                                                                @"chapterId":verseDic[@"id_capitulo"],
                                                                                @"verseId":verseDic[@"id"]
                                                                            }];
            
//            [self dismissViewControllerAnimated:YES completion:^{
//                
//            }];
        }
    }else{

            if (indexPath.row == 0) {
                if([(NSNumber*)expandedSections[indexPath.section]  isEqual: @YES]){
                    expandedSections[indexPath.section] = @NO;
                    
                }else{
                    expandedSections[indexPath.section] = @YES;
                }
                [_tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }else{
                NSDictionary *dic = groupDictionary[groupNamesArray[indexPath.section]][indexPath.row - 1];
                
                NSString *query = @"SELECT g.id id_grupo, l.id  id_libro, c.id id_capitulo, v.id id_versiculo, l.nombre libro, c.orden orden, v.orden orden_v FROM grupo g, libro l, capitulo c, tema t, versiculo v WHERE t.id = ? AND c.id = ? AND v.id_capitulo = c.id AND v.orden BETWEEN ? and ? AND c.id_libro = l.id AND l.id_grupo = g.id ";
            
                NSArray *array = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[dic[@"id"], dic[@"id_capitulo"], dic[@"inicio"], dic[@"fin"]]];
                if (array.count > 0) {
                    NSDictionary *verse = array[0];
//                    
//                    [[NSUserDefaults standardUserDefaults] setInteger:[verse[@"id_capitulo"] intValue] forKey:@"chapterId"];
//                    [[NSUserDefaults standardUserDefaults] setInteger:[verse[@"id_versiculo"] intValue] forKey:@"verseId"];
//                    [[NSUserDefaults standardUserDefaults] setInteger:[verse[@"id_libro"] integerValue] forKey:@"bookId"];
//                    [[NSUserDefaults standardUserDefaults] setInteger:[verse[@"id_grupo"] integerValue] forKey:@"groupId"];
//                    [[NSUserDefaults standardUserDefaults] setInteger:[verse[@"orden"] integerValue] forKey:@"chapterOrder"];
//                    [[NSUserDefaults standardUserDefaults] setObject:verse[@"libro"] forKey:@"bookName"];
//                    [[NSUserDefaults standardUserDefaults] synchronize];
                    

                    NSMutableArray *rows = [NSMutableArray array];
                    for (NSDictionary *dic in array) {
                        [rows addObject:dic[@"orden_v"]];
                    }

                    [self performSegueWithIdentifier:@"SearchResultBibleSegue" sender:@{@"bookId":verse[@"id_libro"],
                                                                                        @"groupId":verse[@"id_grupo"],
                                                                                        @"chapterOrder":verse[@"orden"],
                                                                                        @"bookName":verse[@"libro"],
                                                                                        @"chapterId":verse[@"id_capitulo"],
                                                                                        @"verseId":verse[@"id_versiculo"],
                                                                                        @"rows":rows
                                                                                        }];
//                    [delegate searchVerseDidChanged:rows];
                    
//                    [self dismissViewControllerAnimated:YES completion:^{
//                        
//                    }];
                }
                
            }

    }
    
}



@end
