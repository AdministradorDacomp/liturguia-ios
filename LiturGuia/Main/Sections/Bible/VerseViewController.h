//
//  VerseViewController.h
//  LiturGuia
//
//  Created by Krloz on 23/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "BookViewController.h"
#import "SearchBibleViewController.h"
#import "WYPopoverController.h"
#import "WYStoryboardPopoverSegue.h"
#import "StyleSelectorViewController.h"
#import "SelectedRowsActionsViewController.h"


@interface VerseViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate, BookViewControllerDelegate, SearchBibleViewControllerDelegate, StyleSelectorViewControllerDelegate, SelectedRowsActionsViewControllerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet HMSegmentedControl *tabBar;
@property (strong, nonatomic) IBOutlet UITableView *chaptersTableView;
@property (strong, nonatomic) IBOutlet UITableView *versesTableView;
@property (strong, nonatomic) IBOutlet UILabel *chapterTitleLabel;

#pragma mark - Verse selector

@property (strong, nonatomic) IBOutlet UIScrollView *versesScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *versePageControl;
@property (strong, nonatomic) IBOutlet UIView *verseSelectorView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *previousChapterButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *nextChapterButton;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIButton *selectedRowsButton;


@end
