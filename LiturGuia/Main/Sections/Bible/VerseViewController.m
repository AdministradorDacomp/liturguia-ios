
//
//  VerseViewController.m
//  LiturGuia
//
//  Created by Krloz on 23/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "VerseViewController.h"
#import "SQLiteDBManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import <Parse/PFFacebookUtils.h>
#import <Appirater/Appirater.h>
//#import <SettingsPanelManager>

@import FloatingPanel;

@interface VerseViewController (){
    NSArray *versesArray;
    NSArray *chaptersArray;
    UIVisualEffectView *visualEffectView;
    NSNumber *preSelectedChapterId;
    NSNumber *preSelectedChapterOrder;
    NSMutableArray *selectedRows;
    WYPopoverController* stylePopoverController;
    WYPopoverController* selectedRowsActionsPopoverController;
    UIColor *textColor;
    UIColor *chapterTextColor;
    NSString *fontName;
    float fontSize;
    NSMutableDictionary *textBackgroundColorsDic;
}

@end

@implementation VerseViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Appirater userDidSignificantEvent:YES];
    
    fontName = [[NSUserDefaults standardUserDefaults] stringForKey:@"fontName"];
    fontSize = [[NSUserDefaults standardUserDefaults] floatForKey:@"fontSize"];
    
    if (!fontName) {
        fontName = @"HelveticaNeue";
        [[NSUserDefaults standardUserDefaults] setObject:fontName forKey:@"fontName"];
        [[NSUserDefaults standardUserDefaults] setObject:fontName forKey:@"fontDisplayName"];
        
    }
    if (fontSize == 0) {
        fontSize = 14.0;
        [[NSUserDefaults standardUserDefaults] setFloat:fontSize forKey:@"fontSize"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    selectedRows = [NSMutableArray array];

    _selectedRowsButton.hidden = YES;
    _selectedRowsButton.backgroundColor = kNavigationBarColor;

    
    NSString *bookName = [[NSUserDefaults standardUserDefaults] stringForKey:@"bookName"];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [btn setTitle:[NSString stringWithFormat:@"%@ ▾",bookName ] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(menuTitleTapped:) forControlEvents:UIControlEventTouchUpInside];
    btn.showsTouchWhenHighlighted = YES;
    btn.titleLabel.minimumScaleFactor = 0.5;
    btn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    self.navigationItem.titleView = btn;

    
    [_tabBar setSectionTitles:@[@"Libro", @"Capitulos"]];
    
    __weak typeof (self) weakSelf = self;
    [_tabBar setIndexChangeBlock:^(NSUInteger index) {
        if (index == 0) {
            [weakSelf showVerses];
        }else if(index == 1){
            [weakSelf showChapters];
        }
    }];
    
    _tabBar.selectionIndicatorHeight = 2.0f;
    _tabBar.backgroundColor = self.navigationController.navigationBar.barTintColor;
    _tabBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor lightGrayColor], NSFontAttributeName:              [UIFont fontWithName:@"HelveticaNeue" size:14]};
    _tabBar.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    _tabBar.selectionIndicatorColor = [UIColor whiteColor];
    _tabBar.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    _tabBar.selectedSegmentIndex = 0;
    _tabBar.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _tabBar.shouldAnimateUserSelection = YES;
    
    [self styleChanged];
    
    [self loadChapters];
    [self loadVerses];
    [self showVerses];
    
    _versesScrollView.pagingEnabled = YES;
    _versesScrollView.showsHorizontalScrollIndicator = NO;
    _versesScrollView.showsVerticalScrollIndicator = NO;
    _versesScrollView.scrollsToTop = NO;
    _versesScrollView.delegate = self;
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.alpha = 0.8;
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideVerseSelector:)];
    [visualEffectView addGestureRecognizer:tgr];
    
    visualEffectView.frame = self.view.bounds;
    
    
    [self hideVerseSelector:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        self.navigationController.navigationBar.barTintColor = SEPIA_TEXT_COLOR;
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    }

//        
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Biblia"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SearchTextSegue"]) {
        UINavigationController *nvc =  segue.destinationViewController;
        SearchBibleViewController *vc = [nvc viewControllers][0];

        vc.delegate = self;
    }
    if ([segue.identifier isEqualToString:@"StyleSelectorSegue"])
    {
        
        UINavigationController *navigationController = segue.destinationViewController;
        StyleSelectorViewController* contentVC = [[navigationController viewControllers] objectAtIndex:0];
        contentVC.preferredContentSize = CGSizeMake(280, 240);
        contentVC.delegate = self;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            SettingsPanelManager *controller = [SettingsPanelManager shared];
            [controller setUpSettingsPanelWithContentVC:(contentVC) toParent:(self)];
        }else{
            WYStoryboardPopoverSegue* popoverSegue = (WYStoryboardPopoverSegue*)segue;
            stylePopoverController = [popoverSegue popoverControllerWithSender:sender
                                                      permittedArrowDirections:WYPopoverArrowDirectionAny
                                                                      animated:YES];
            stylePopoverController.popoverLayoutMargins = UIEdgeInsetsMake(4, 4, 4, 4);
        }
    }
    if ([segue.identifier isEqualToString:@"SelectedRowsActionsSegue"])
    {
        SelectedRowsActionsViewController* vc = segue.destinationViewController;
        vc.preferredContentSize = CGSizeMake(280, 240);
        vc.delegate = self;
        
        WYStoryboardPopoverSegue* popoverSegue = (WYStoryboardPopoverSegue*)segue;
        
        selectedRowsActionsPopoverController = [popoverSegue popoverControllerWithSender:sender
                                                  permittedArrowDirections:WYPopoverArrowDirectionAny
                                                                  animated:YES];
        
        selectedRowsActionsPopoverController.popoverLayoutMargins = UIEdgeInsetsMake(4, 4, 4, 4);
    }
    
}

#pragma mark - Buttons actions

-(void)menuTitleTapped:(id)sender{
    UIStoryboard *sb = self.storyboard;
    BookViewController *vc =[sb instantiateViewControllerWithIdentifier:@"BookViewController"];
    
    vc.delegate = self;
    
    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:vc];
    nvc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:nvc animated:YES completion:^{
        
    }];
}

-(void)searchButtonTapped:(id)sender{
    [self performSegueWithIdentifier:@"SearchTextSegue" sender:self];
}

- (IBAction)previousChapterButtonTapped:(id)sender {
    int chapterOrder = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterOrder"] - 1;
    
    if (chaptersArray.count >= chapterOrder - 1) {
        int chapterId = [chaptersArray[chapterOrder - 1][@"id"] intValue];
        
        [[NSUserDefaults standardUserDefaults] setInteger:chapterId forKey:@"chapterId"];
        [[NSUserDefaults standardUserDefaults] setInteger:chapterOrder forKey:@"chapterOrder"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self loadVerses];
        [self showVerses];
        [_versesTableView reloadData];
        [_versesTableView scrollsToTop];
    }
    
    
}

- (IBAction)nextChapterButtonTapped:(id)sender {
    int chapterOrder = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterOrder"] + 1;
    
    int chapterId = [chaptersArray[chapterOrder - 1][@"id"] intValue];
    
    [[NSUserDefaults standardUserDefaults] setInteger:chapterId forKey:@"chapterId"];
    [[NSUserDefaults standardUserDefaults] setInteger:chapterOrder forKey:@"chapterOrder"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadVerses];
    [self showVerses];
    [_versesTableView reloadData];
    [_versesTableView scrollsToTop];
}

#pragma mark - Gesture actions

- (IBAction)rightSwipe:(id)sender {
    [self previousChapterButtonTapped:nil];
}

- (IBAction)leftSwipe:(id)sender {
    [self nextChapterButtonTapped:nil];
}

#pragma mark - Helpers

-(void)showVerses{
    _versesTableView.hidden = NO;
    _chaptersTableView.hidden = YES;
    [_tabBar setSelectedSegmentIndex:0 animated:YES];
    
    UIBarButtonItem *btnSearch = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonTapped:)];
    [self.navigationItem setRightBarButtonItems:@[btnSearch]];
    
    if (selectedRows.count > 0) {
        _selectedRowsButton.hidden = NO;
    }
}

-(void)showChapters{
    _selectedRowsButton.hidden = YES;
    _versesTableView.hidden = YES;
    _chaptersTableView.hidden = NO;
    [_tabBar setSelectedSegmentIndex:1 animated:YES];
    
    [self.navigationItem setRightBarButtonItems:nil];
}

-(void)selectedRowsChanged{
    if (selectedRows.count > 0) {
        
        [_selectedRowsButton setTitle:[NSString stringWithFormat:@"%ld",(long)selectedRows.count] forState:UIControlStateNormal];
        
        _selectedRowsButton.titleEdgeInsets = UIEdgeInsetsMake(0, -_selectedRowsButton.imageView.frame.size.width, 0, _selectedRowsButton.imageView.frame.size.width);
        _selectedRowsButton.imageEdgeInsets = UIEdgeInsetsMake(0, _selectedRowsButton.titleLabel.frame.size.width, 0, -_selectedRowsButton.titleLabel.frame.size.width);
        
        [UIView animateWithDuration:0.3 animations:^{
            self->_selectedRowsButton.hidden = NO;
            self->_selectedRowsButton.alpha = 1.0;
        }];
        
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self->_selectedRowsButton.alpha = 0.0;
        } completion:^(BOOL finished) {
            self->_selectedRowsButton.hidden = YES;
        }];
    }
}


-(NSString*)prepareTextToShare{
    NSString *bookName = [[NSUserDefaults standardUserDefaults] stringForKey:@"bookName"];
    int chapterOrder = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterOrder"];
    
    selectedRows = [NSMutableArray arrayWithArray:[selectedRows sortedArrayUsingSelector:@selector(compare:)]];
    
    int low = 0, high = 0;
    NSMutableArray *groups = [NSMutableArray array];
    for (int i = 0; i < selectedRows.count; i++) {
        if (i > 0) {
            if ([selectedRows[i] intValue] - 1 == [selectedRows[i - 1] intValue]) {
                high = [selectedRows[i] intValue];
                if (i == selectedRows.count - 1) {
                    [groups addObject:[NSString stringWithFormat:@"%d - %d", low + 1, high +1]];
                }
            }else{
                if (high != low) {
                    [groups addObject:[NSString stringWithFormat:@"%d - %d", low + 1, high +1]];
                }else{
                    [groups addObject:[NSString stringWithFormat:@"%d", high + 1]];
                }
                low = [selectedRows[i] intValue];
                high = low;
                if (i == selectedRows.count - 1) {
                    [groups addObject:[NSString stringWithFormat:@"%d", high + 1]];
                }
            }
        }else{
            low = [selectedRows[i] intValue];
        }
    }
    
    NSMutableString *titleString = [[NSMutableString alloc] init];
    [titleString appendFormat:@"%@ %d: ", bookName, chapterOrder];
    
    for (NSString *group in groups) {
        [titleString appendFormat:@" %@,", group];
    }
    
    [titleString deleteCharactersInRange:NSMakeRange([titleString length]-1, 1)];
    
    
    NSMutableString *textString = [[NSMutableString alloc] init];
    for (NSNumber *row in selectedRows) {
        int verseNumber = [versesArray[[row intValue]][@"orden"] intValue];
        NSString *text = versesArray[[row intValue]][@"versiculo"];
        [textString appendFormat:@"%d %@ ",verseNumber, text];
    }
    
    
    
    return [NSString stringWithFormat:@"\"%@\"\n\n%@",textString,titleString];
}

#pragma mark - Database

-(void)loadVerses{
    NSString *query = @"SELECT v.id, v.texto AS versiculo, v.orden FROM versiculo v WHERE v.id_capitulo = ? ORDER BY v.orden";
    int chapterId = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterId"];
    NSMutableArray *noSortedArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[@(chapterId)]];
    
    for (NSMutableDictionary *dic in versesArray) {
        dic[@"orden"] = [NSString stringWithFormat:@"%02d",[dic[@"orden"] intValue]];
    }
    
    int chapterOrder = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterOrder"];
    _chapterTitleLabel.text = [NSString stringWithFormat:@"Capitulo %d", chapterOrder];
    
    NSSortDescriptor *sortDescriptors = [[NSSortDescriptor alloc] initWithKey:@"orden" ascending:YES];
    versesArray = [noSortedArray sortedArrayUsingDescriptors:@[sortDescriptors]];
    textBackgroundColorsDic = [NSMutableDictionary dictionary];
    
    if (chapterOrder - 1 <= 0) {
        _previousChapterButton.enabled = NO;
        _rightSwipeGestureRecognizer.enabled = NO;
    }else{
        _previousChapterButton.enabled = YES;
        _rightSwipeGestureRecognizer.enabled = YES;
    }
    
    if (chapterOrder < chaptersArray.count) {
        _nextChapterButton.enabled = YES;
        _leftSwipeGestureRecognizer.enabled = YES;
    }else{
        _nextChapterButton.enabled = NO;
        _leftSwipeGestureRecognizer.enabled = NO;
    }
    selectedRows = [NSMutableArray array];
    [self selectedRowsChanged];
    
    
}

-(void)loadChapters{
    NSString *query = @"SELECT c.id, nombre AS capitulo, c.orden FROM capitulo c WHERE c.id_libro = ? ORDER BY c.orden";
    int bookId = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"bookId"];

    NSMutableArray *noSortedArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[@(bookId)]];
    
    for (NSMutableDictionary *dic in noSortedArray) {
        dic[@"orden"] = [NSString stringWithFormat:@"%02d",[dic[@"orden"] intValue]];
    }
    

    NSSortDescriptor *sortDescriptors = [[NSSortDescriptor alloc] initWithKey:@"orden" ascending:YES];
    chaptersArray = [noSortedArray sortedArrayUsingDescriptors:@[sortDescriptors]];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _versesTableView) {
        int verseNumber = [versesArray[indexPath.row][@"orden"] intValue];
        NSString *text = versesArray[indexPath.row][@"versiculo"];
        text = [NSString stringWithFormat:@"%d %@",verseNumber, text];
    
        CGRect textRect = [text boundingRectWithSize:CGSizeMake(290, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont fontWithName:fontName size:fontSize]}
                                             context:nil];
        
        CGSize size = textRect.size;
        
//        CGSize size = [text sizeWithFont:[UIFont fontWithName:fontName size:fontSize] constrainedToSize:CGSizeMake(290, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        return size.height + 10;
    }else{
        return UITableViewAutomaticDimension;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        if (tableView == _chaptersTableView) {
            return chaptersArray.count;
        }else if(tableView == _versesTableView){
            return versesArray.count;
        }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if (tableView == _chaptersTableView)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ChapterTableViewCell" forIndexPath:indexPath];
    
        cell.textLabel.text = [NSString stringWithFormat:@"CAPITULO %d",[chaptersArray[indexPath.row][@"orden"] intValue]];
        cell.detailTextLabel.text = chaptersArray[indexPath.row][@"capitulo"];
        cell.textLabel.textColor = chapterTextColor;
    }else if(tableView == _versesTableView){
        cell = [tableView dequeueReusableCellWithIdentifier:@"VerseTableViewCell" forIndexPath:indexPath];
        int verseNumber = [versesArray[indexPath.row][@"orden"] intValue];
        NSString *text = versesArray[indexPath.row][@"versiculo"];
        cell.textLabel.text = [NSString stringWithFormat:@"%d %@",verseNumber, text];
        
        cell.textLabel.font = [UIFont fontWithName:fontName size:fontSize];
        cell.textLabel.textColor = textColor;
        if ([selectedRows containsObject:@(indexPath.row)]) {
            cell.textLabel.textColor = [UIColor redColor];
        }
        
        NSRange numberTextRange = [cell.textLabel.text rangeOfString:[NSString stringWithFormat:@"%d",verseNumber]];
        

        UIFont *boldFont = [UIFont boldSystemFontOfSize:cell.textLabel.font.pointSize-3];

        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: cell.textLabel.textColor,
                                  NSFontAttributeName: cell.textLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:cell.textLabel.text
                                               attributes:attribs];
        
        NSString *rowString = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
        if (textBackgroundColorsDic[rowString]) {
            UIColor *color = textBackgroundColorsDic[rowString];
            if ([cell.textLabel.textColor isEqual:[UIColor whiteColor]] && ![LiturGuia isEqualColor:[UIColor clearColor] to:color]) {
                attribs = @{
                            NSForegroundColorAttributeName: [UIColor blackColor],
                            NSFontAttributeName: cell.textLabel.font
                            };
                attributedText =
                [[NSMutableAttributedString alloc] initWithString:cell.textLabel.text
                                                       attributes:attribs];
            }
            [attributedText addAttribute:NSBackgroundColorAttributeName
                                   value:color
                                   range:NSMakeRange(0, attributedText.length)];
        }
        
        [attributedText addAttributes:@{NSFontAttributeName:boldFont,
                                        NSForegroundColorAttributeName:[UIColor lightGrayColor]}
                                range:numberTextRange];

        cell.textLabel.attributedText = attributedText;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }else{
        cell = [UITableViewCell new];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == _chaptersTableView) {
        preSelectedChapterId = chaptersArray[indexPath.row][@"id"];
        NSString *query = @"SELECT count(*) AS versiculos, c.orden FROM capitulo c, versiculo v where c.id = ? AND id_capitulo = c.id GROUP by c.id LIMIT 1";
        NSMutableArray *resultArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[preSelectedChapterId]];
        
        int totalVerses;
        if (resultArray.count > 0) {
            totalVerses = [resultArray[0][@"versiculos"] intValue];
            preSelectedChapterOrder = resultArray[0][@"orden"];
            [self showVerseSelector:totalVerses];
        }else{
            return;
        }
    }else if (tableView == _versesTableView){
        
        if ([selectedRows containsObject:@(indexPath.row)]) {
            [selectedRows removeObject:@(indexPath.row)];
        }else{
            [selectedRows addObject:@(indexPath.row)];
        }
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self selectedRowsChanged];
    }
}

#pragma mark - Verse selector

-(void)verseNumberTapped:(id)sender{
    [[NSUserDefaults standardUserDefaults] setInteger:[preSelectedChapterId integerValue] forKey:@"chapterId"];
    [[NSUserDefaults standardUserDefaults] setInteger:[preSelectedChapterOrder integerValue] forKey:@"chapterOrder"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadVerses];
    [self showVerses];
    [_versesTableView reloadData];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag]-1
                                                    inSection:0];
        [self->_versesTableView scrollToRowAtIndexPath:indexPath
                                atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self->_versesTableView scrollToRowAtIndexPath:indexPath
                                    atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            
            UITableViewCell *cell = [self->_versesTableView cellForRowAtIndexPath:indexPath];
            [UIView animateWithDuration:0.2 animations:^{
                cell.contentView.backgroundColor = [UIColor lightGrayColor];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.9 animations:^{
                    cell.contentView.backgroundColor = [UIColor clearColor];
                }];
            }];
        });
    });
    
    _verseSelectorView.hidden = YES;
    visualEffectView.hidden = YES;
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = CGRectGetWidth(_versesScrollView.frame);
    NSUInteger page = floor((_versesScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _versePageControl.currentPage = page;
    
}

-(void)hideVerseSelector:(id)sender{
    [UIView animateWithDuration:0.25 animations:^{
        self->_verseSelectorView.alpha = 0.0f;
        self->visualEffectView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            self->_verseSelectorView.hidden = YES;
            self->visualEffectView.hidden = YES;
        }
    }];
}

-(void)showVerseSelector:(int)totalVerses{
    NSUInteger numberPages = totalVerses/30 + 1;
    
    _versesScrollView.contentSize = CGSizeMake(CGRectGetWidth(_versesScrollView.frame) * numberPages, _versesScrollView.frame.size.height);
    [[_versesScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGRect frame = _versesScrollView.frame;
    frame.origin.x = 0;
    [_versesScrollView scrollRectToVisible:frame animated:NO];
    
    _versePageControl.numberOfPages = numberPages;
    _versePageControl.currentPage = 0;
    
    float pageWidth = (0.875 * screenWidth);
    float buttonWidth = pageWidth/5;
    float buttonHeight = (0.3697 * 568)/6;
    
    
    int verseNumber = 1;
    for (int page = 0; page < numberPages; page++) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                if (verseNumber <= totalVerses) {
                    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(buttonWidth*j+(page*pageWidth), buttonHeight*i, buttonWidth, buttonHeight)];
                    [button setTitle:[NSString stringWithFormat:@"%d",verseNumber] forState:UIControlStateNormal] ;
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    button.tag = verseNumber;
                    
                    [button addTarget:self action:@selector(verseNumberTapped:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [_versesScrollView addSubview:button];
                }else{
                    break;
                }
                
                verseNumber++;
            }
        }
    }
    
    _verseSelectorView.hidden = NO;
    visualEffectView.hidden = NO;
    
    _verseSelectorView.alpha = 0.0f;
    _verseSelectorView.transform = CGAffineTransformMakeScale(0.001,0.001);
    [self.view addSubview:_verseSelectorView];
    _verseSelectorView.center = self.view.center;
    
    visualEffectView.alpha = 0.0;
    [self.view insertSubview:visualEffectView belowSubview:_verseSelectorView];
    
    
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.25];
    _verseSelectorView.transform = CGAffineTransformMakeScale(1,1);
    _verseSelectorView.alpha = 1.0f;
    
    visualEffectView.alpha = 0.8;
    
    [UIView commitAnimations];
}


#pragma mark - BookViewControllerDelegate

-(void)bookChanged{
    UIButton *btn = (UIButton*) self.navigationItem.titleView;
    NSString *bookName = [[NSUserDefaults standardUserDefaults] stringForKey:@"bookName"];

    [btn setTitle:[NSString stringWithFormat:@"%@ ▾",bookName ] forState:UIControlStateNormal];
    [btn sizeToFit];
    [self loadChapters];
    [self loadVerses];
    [self showVerses];
    
    [_versesTableView reloadData];
    [_chaptersTableView reloadData];
    
    
    [_versesTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [_chaptersTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];

    
}

#pragma mark - SearchBibleViewControllerDelegate

-(void)searchVerseDidChanged{
    UIButton *btn = (UIButton*) self.navigationItem.titleView;
    NSString *bookName = [[NSUserDefaults standardUserDefaults] stringForKey:@"bookName"];
    
    [btn setTitle:[NSString stringWithFormat:@"%@ ▾",bookName ] forState:UIControlStateNormal];
    
    [self loadChapters];
    [self loadVerses];
    [self showVerses];
    
    [_versesTableView reloadData];
    [_chaptersTableView reloadData];
    
    int verseId = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"verseId"];
    int verseRow = 0;
    for (NSDictionary *dic in versesArray) {
        if ([dic[@"id"] intValue] == verseId) {
            break;
        }
        verseRow++;
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:verseRow
                                                    inSection:0];
        [self->_versesTableView scrollToRowAtIndexPath:indexPath
                                atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self->_versesTableView scrollToRowAtIndexPath:indexPath
                                    atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            
            UITableViewCell *cell = [self->_versesTableView cellForRowAtIndexPath:indexPath];
            [UIView animateWithDuration:0.2 animations:^{
                cell.contentView.backgroundColor = [UIColor lightGrayColor];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.9 animations:^{
                    cell.contentView.backgroundColor = [UIColor clearColor];
                }];
            }];
        });
    });
    
    [_chaptersTableView scrollsToTop];
}

-(void)searchVerseDidChanged:(NSArray*)rows{
    UIButton *btn = (UIButton*) self.navigationItem.titleView;
    NSString *bookName = [[NSUserDefaults standardUserDefaults] stringForKey:@"bookName"];
    
    [btn setTitle:[NSString stringWithFormat:@"%@ ▾",bookName ] forState:UIControlStateNormal];
    
    [self loadChapters];
    [self loadVerses];
    [self showVerses];
    
    [_versesTableView reloadData];
    [_chaptersTableView reloadData];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSMutableArray *indexPaths = [NSMutableArray array];
        for (NSNumber *row in rows) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[row intValue] - 1
                                                        inSection:0];
            [indexPaths addObject:indexPath];
            

        }
        
        [self->_versesTableView scrollToRowAtIndexPath:indexPaths[0]
                                atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self->_versesTableView scrollToRowAtIndexPath:indexPaths[0]
                                    atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            
            self->selectedRows = [NSMutableArray array];
            
            for (NSIndexPath *indexPath in indexPaths) {
                [self->selectedRows addObject:@(indexPath.row)];
                
                [self->_versesTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }

            [self selectedRowsChanged];
        });
    });
    
    [_chaptersTableView scrollsToTop];
}

#pragma mark - SelectedRowsActionsViewControllerDelegate

-(void)foregroundColorChanged:(UIColor*)color{
    [selectedRowsActionsPopoverController dismissPopoverAnimated:YES];
    
    for (NSNumber *row in selectedRows) {
        textBackgroundColorsDic[[row stringValue]] = color;
    }
    selectedRows = [NSMutableArray array];
    [self selectedRowsChanged];
    [_versesTableView reloadData];
}

-(void)shareVerses:(BOOL)facebook{
    if (facebook) {
        NSString *messageErr = @"¿Desea compartir en Facebook los versículos seleccionados?";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:self
                                           cancelButtonTitle:@"No"
                                           otherButtonTitles:@"Sí", nil];
        [av show];
    }else{
        [selectedRowsActionsPopoverController dismissPopoverAnimated:YES];
        
        NSString *text = [self prepareTextToShare];
        
        [KVNProgress show];
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            NSArray *objectsToShare = @[text];
            
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = text;
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo];
            
            NSString *messageErr = @"Para compartir los versículos en Facebook es necesario pegar el contenido del portapapeles en la ventana que aparecerá al seleccionar esa opción.";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:^{
                [KVNProgress dismiss];
//                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//                self.view.window.userInteractionEnabled = YES;
            }];
        });
    }
}

-(void)removeSelection{
     [selectedRowsActionsPopoverController dismissPopoverAnimated:YES];
    selectedRows = [NSMutableArray array];
    [self selectedRowsChanged];
    [_versesTableView reloadData];
    
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
      
        /*if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
            NSString *messageErr = @"Esta cuenta no está ligada a una cuenta de Facebook. Si desea ligarla ir a configuración";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            return;
        }*/
        [selectedRowsActionsPopoverController dismissPopoverAnimated:YES];
        
        [KVNProgress showWithStatus:@"Compartiendo..."];
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];

        NSString *text = [self prepareTextToShare];

        NSDictionary *params = @{@"message": text};
        
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/feed" parameters:params HTTPMethod:@"POST"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
            
            if (!error) {
                [KVNProgress showSuccessWithStatus:@"Los versículos fueron compartidos exitosamente"];
//                NSString *messageErr = @"Los versículos fueron compartidos exitosamente";
//                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                             message:messageErr
//                                                            delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil];
//                [av show];
                self->selectedRows = [NSMutableArray array];
                [self selectedRowsChanged];
                [self->_versesTableView reloadData];
            }else{
                [KVNProgress showErrorWithStatus:error.localizedDescription];
            }
        }];
    }
}

#pragma mark - StyleViewControllerDelegate

- (void)fontSizeChanged{
    fontSize = [[NSUserDefaults standardUserDefaults] floatForKey:@"fontSize"];
    [_versesTableView reloadData];
}


- (void)fontChanged{
    fontName = [[NSUserDefaults standardUserDefaults] stringForKey:@"fontName"];
    [_versesTableView reloadData];
}

- (void)styleChanged{
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"white"]) {
        _versesTableView.backgroundColor = [UIColor whiteColor];
        _chaptersTableView.backgroundColor = [UIColor whiteColor];
        textColor = [UIColor blackColor];
        
        self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
        _tabBar.backgroundColor = kNavigationBarColor;
        _toolbar.barTintColor = kNavigationBarColor;
        
        _chapterTitleLabel.textColor = kNavigationBarColor;
        chapterTextColor = kNavigationBarColor;
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"sepia"]) {
        _versesTableView.backgroundColor = SEPIA_BACKGROUND_COLOR;
        _chaptersTableView.backgroundColor = SEPIA_BACKGROUND_COLOR;
        textColor = SEPIA_TEXT_COLOR;
        self.navigationController.navigationBar.barTintColor = SEPIA_TEXT_COLOR;
        _tabBar.backgroundColor = SEPIA_TEXT_COLOR;
        _toolbar.barTintColor = SEPIA_TEXT_COLOR;
        
        _chapterTitleLabel.textColor = SEPIA_TEXT_COLOR;
        chapterTextColor = SEPIA_TEXT_COLOR;
    }else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"TextStyle"] isEqual:@"night"]) {
        _versesTableView.backgroundColor = [UIColor blackColor];
        _chaptersTableView.backgroundColor = [UIColor blackColor];;
        textColor = [UIColor whiteColor];
        
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        _tabBar.backgroundColor = [UIColor blackColor];
        _toolbar.barTintColor = [UIColor blackColor];
        
        _chapterTitleLabel.textColor = [UIColor whiteColor];
        chapterTextColor = [UIColor whiteColor];
    }
    
    [_tabBar setNeedsDisplay];
    [_versesTableView reloadData];
    [_chaptersTableView reloadData];
}


@end
