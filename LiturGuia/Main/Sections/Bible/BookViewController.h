//
//  BookViewController.h
//  LiturGuia
//
//  Created by Krloz on 23/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
@class HMSegmentedControl;

@protocol BookViewControllerDelegate <NSObject>

-(void)bookChanged;

@end

@interface BookViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating>

@property (strong, nonatomic) IBOutlet HMSegmentedControl *tabBar;
@property (strong, nonatomic) IBOutlet UITableView *atTableView;
@property (strong, nonatomic) IBOutlet UITableView *ntTableView;
@property (strong, nonatomic) IBOutlet UISearchController *searchControllerNT;
@property (strong, nonatomic) IBOutlet UISearchController *searchControllerAT;
@property (strong, nonatomic) id delegate;


-(void)preSelectSection:(NSString*)section;


@end
