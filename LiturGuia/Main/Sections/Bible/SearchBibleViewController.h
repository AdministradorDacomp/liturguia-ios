//
//  SearchBibleViewController.h
//  LiturGuia
//
//  Created by Krloz on 29/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchBibleViewControllerDelegate <NSObject>

@required
-(void)searchVerseDidChanged;
-(void)searchVerseDidChanged:(NSArray*)rows;

@end

@interface SearchBibleViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) id delegate;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end
