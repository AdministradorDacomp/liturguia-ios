//
//  BibleDescriptionViewController.m
//  LiturGuia
//
//  Created by Krloz on 23/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "BibleDescriptionViewController.h"
#import "BookViewController.h"

@interface BibleDescriptionViewController ()

@end

@implementation BibleDescriptionViewController


#pragma mark - Life cycle

- (void)viewDidLoad {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BibleOpened"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

- (IBAction)ATButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"BookSegue" sender:@"AT"];
}

- (IBAction)NTButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"BookSegue" sender:@"NT"];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"BookSegue"]) {
        BookViewController *vc = [segue destinationViewController];
        [vc preSelectSection:sender];
    }
}

@end
