//
//  SearchResultBibleViewController.m
//  LiturGuia
//
//  Created by Krloz on 08/12/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//
#import "SearchBibleViewController.h"

#import "SearchResultBibleViewController.h"
#import "SQLiteDBManager.h"

@interface SearchResultBibleViewController ()

@end

@implementation SearchResultBibleViewController{
    NSArray *versesArray;
    NSString *fontName;
    NSMutableArray *selectedRows;
    float fontSize;
}

@synthesize rows, chapterId, chapterOrder, verseId, bookName, groupId, bookId, delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:bookName];
    
    fontName = [[NSUserDefaults standardUserDefaults] stringForKey:@"fontName"];
    fontSize = [[NSUserDefaults standardUserDefaults] floatForKey:@"fontSize"];
    
    if (!fontName) {
        fontName = @"HelveticaNeue";
        [[NSUserDefaults standardUserDefaults] setObject:fontName forKey:@"fontName"];
        [[NSUserDefaults standardUserDefaults] setObject:fontName forKey:@"fontDisplayName"];
        
    }
    if (fontSize == 0) {
        fontSize = 14.0;
        [[NSUserDefaults standardUserDefaults] setFloat:fontSize forKey:@"fontSize"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadVerses];
    
    selectedRows = [NSMutableArray array];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (rows) {
        [self searchVerseDidChangedWithRows];
    }else{
        [self searchVerseDidChanged];
    }
    
}

- (IBAction)doneButtonTapped:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:chapterId forKey:@"chapterId"];
    [[NSUserDefaults standardUserDefaults] setInteger:verseId forKey:@"verseId"];
    [[NSUserDefaults standardUserDefaults] setInteger:bookId forKey:@"bookId"];
    [[NSUserDefaults standardUserDefaults] setInteger:groupId forKey:@"groupId"];
    [[NSUserDefaults standardUserDefaults] setInteger:chapterOrder forKey:@"chapterOrder"];
    [[NSUserDefaults standardUserDefaults] setObject:bookName forKey:@"bookName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (rows) {
        [delegate searchVerseDidChanged:rows];
    }else{
        [delegate searchVerseDidChanged];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark - Database

-(void)loadVerses{
    NSString *query = @"SELECT v.id, v.texto AS versiculo, v.orden FROM versiculo v WHERE v.id_capitulo = ? ORDER BY v.orden";
//    int chapterId = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterId"];
    NSMutableArray *noSortedArray = [SQLiteDBManager executeQueryInBibleDB:query withArgumentsInArray:@[@(chapterId)]];
    
    for (NSMutableDictionary *dic in versesArray) {
        dic[@"orden"] = [NSString stringWithFormat:@"%02d",[dic[@"orden"] intValue]];
    }
    
//    int chapterOrder = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"chapterOrder"];
    _chapterTitleLabel.text = [NSString stringWithFormat:@"Capitulo %d", chapterOrder];
    
    NSSortDescriptor *sortDescriptors = [[NSSortDescriptor alloc] initWithKey:@"orden" ascending:YES];
    versesArray = [noSortedArray sortedArrayUsingDescriptors:@[sortDescriptors]];
//    textBackgroundColorsDic = [NSMutableDictionary dictionary];
//    
//    if (chapterOrder - 1 <= 0) {
//        _previousChapterButton.enabled = NO;
//        _rightSwipeGestureRecognizer.enabled = NO;
//    }else{
//        _previousChapterButton.enabled = YES;
//        _rightSwipeGestureRecognizer.enabled = YES;
//    }
    
//    if (chapterOrder < chaptersArray.count) {
//        _nextChapterButton.enabled = YES;
//        _leftSwipeGestureRecognizer.enabled = YES;
//    }else{
//        _nextChapterButton.enabled = NO;
//        _leftSwipeGestureRecognizer.enabled = NO;
//    }
//    selectedRows = [NSMutableArray array];
//    [self selectedRowsChanged];
    
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        int verseNumber = [versesArray[indexPath.row][@"orden"] intValue];
        NSString *text = versesArray[indexPath.row][@"versiculo"];
        text = [NSString stringWithFormat:@"%d %@",verseNumber, text];
        
        CGRect textRect = [text boundingRectWithSize:CGSizeMake(290, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont fontWithName:fontName size:fontSize]}
                                             context:nil];
        
        CGSize size = textRect.size;
        
        //        CGSize size = [text sizeWithFont:[UIFont fontWithName:fontName size:fontSize] constrainedToSize:CGSizeMake(290, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        return size.height + 10;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return versesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VerseTableViewCell" forIndexPath:indexPath];
        int verseNumber = [versesArray[indexPath.row][@"orden"] intValue];
        NSString *text = versesArray[indexPath.row][@"versiculo"];
        cell.textLabel.text = [NSString stringWithFormat:@"%d %@",verseNumber, text];
        
        cell.textLabel.font = [UIFont fontWithName:fontName size:fontSize];
        cell.textLabel.textColor = [UIColor blackColor];
        if ([selectedRows containsObject:@(indexPath.row)]) {
            cell.textLabel.textColor = [UIColor redColor];
        }
    
        NSRange numberTextRange = [cell.textLabel.text rangeOfString:[NSString stringWithFormat:@"%d",verseNumber]];
        
        
        UIFont *boldFont = [UIFont boldSystemFontOfSize:cell.textLabel.font.pointSize-3];
        
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: cell.textLabel.textColor,
                                  NSFontAttributeName: cell.textLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:cell.textLabel.text
                                               attributes:attribs];
        
//        NSString *rowString = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
//        if (textBackgroundColorsDic[rowString]) {
//            UIColor *color = textBackgroundColorsDic[rowString];
//            if ([cell.textLabel.textColor isEqual:[UIColor whiteColor]] && ![LiturGuia isEqualColor:[UIColor clearColor] to:color]) {
//                attribs = @{
//                            NSForegroundColorAttributeName: [UIColor blackColor],
//                            NSFontAttributeName: cell.textLabel.font
//                            };
//                attributedText =
//                [[NSMutableAttributedString alloc] initWithString:cell.textLabel.text
//                                                       attributes:attribs];
//            }
//            [attributedText addAttribute:NSBackgroundColorAttributeName
//                                   value:color
//                                   range:NSMakeRange(0, attributedText.length)];
//        }
    
        [attributedText addAttributes:@{NSFontAttributeName:boldFont,
                                        NSForegroundColorAttributeName:[UIColor lightGrayColor]}
                                range:numberTextRange];
        
        cell.textLabel.attributedText = attributedText;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(void)searchVerseDidChanged{
    [self loadVerses];
    
    [_versesTableView reloadData];
    
//    int verseId = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"verseId"];
    int verseRow = 0;
    for (NSDictionary *dic in versesArray) {
        if ([dic[@"id"] intValue] == verseId) {
            break;
        }
        verseRow++;
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:verseRow
                                                    inSection:0];
        [self->_versesTableView scrollToRowAtIndexPath:indexPath
                                atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self->_versesTableView scrollToRowAtIndexPath:indexPath
                                    atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            
            UITableViewCell *cell = [self->_versesTableView cellForRowAtIndexPath:indexPath];
            [UIView animateWithDuration:0.2 animations:^{
                cell.contentView.backgroundColor = [UIColor lightGrayColor];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.9 animations:^{
                    cell.contentView.backgroundColor = [UIColor clearColor];
                }];
            }];
        });
    });

}

-(void)searchVerseDidChangedWithRows{
//    NSString *bookName = [[NSUserDefaults standardUserDefaults] stringForKey:@"bookName"];
    
    
    
    [self loadVerses];
    
    [_versesTableView reloadData];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSMutableArray *indexPaths = [NSMutableArray array];
        for (NSNumber *row in self->rows) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[row intValue] - 1
                                                        inSection:0];
            [indexPaths addObject:indexPath];
            
            
        }
        
        [self->_versesTableView scrollToRowAtIndexPath:indexPaths[0]
                                atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self->_versesTableView scrollToRowAtIndexPath:indexPaths[0]
                                    atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            
            self->selectedRows = [NSMutableArray array];
            
            for (NSIndexPath *indexPath in indexPaths) {
                [self->selectedRows addObject:@(indexPath.row)];
                
                [self->_versesTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        });
    });

    
}

@end
