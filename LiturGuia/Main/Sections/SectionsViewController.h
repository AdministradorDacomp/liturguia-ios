//
//  SectionsViewController.h
//  LiturGuia
//
//  Created by Krloz on 14/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiturGuia-Swift.h"

@interface SectionsViewController : UIViewController<UIAlertViewDelegate, TrialInfoDelegate>


@end
