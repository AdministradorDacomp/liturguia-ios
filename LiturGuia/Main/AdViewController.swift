//
//  AdViewController.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 18/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

import UIKit

@objc class AdViewController: UIViewController {
    
    @objc static var adWasShowed = false
    
    @IBOutlet weak var imageView: UIImageView!
    
    let images = [
        #imageLiteral(resourceName: "iglesia"), #imageLiteral(resourceName: "iglesia2"),  #imageLiteral(resourceName: "desastres"),  #imageLiteral(resourceName: "desastres2"), #imageLiteral(resourceName: "educacion"),  #imageLiteral(resourceName: "educacion2"), #imageLiteral(resourceName: "mayores"),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        AdViewController.adWasShowed = true
        
        let randomInt = Int.random(in: 0..<images.count)
        imageView.image = images[randomInt]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func suscribeButtonTapped(_ sender: Any) {
        let sb = UIStoryboard(name: "Left menu", bundle: nil)
        if let vc = sb.instantiateViewController(withIdentifier: "SuscriptionsViewController") as? SuscriptionsViewController {
            vc.setToModalView()
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
