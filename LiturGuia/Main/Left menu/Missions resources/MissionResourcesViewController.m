
//
//  MissionResourcesViewController.m
//  LiturGuia
//
//  Created by Krloz on 08/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import "MissionResourcesViewController.h"
#import <CoreData/CoreData.h>
#import "MissionResourcesEntity.h"
#import "ReaderDocument.h"
#import "MissionResourceTableViewCell.h"

@interface MissionResourcesViewController ()

@end

@implementation MissionResourcesViewController{
    NSArray *downloadsArray;
    NSArray *downloadedArray;
    ReaderViewController *readerViewController;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.allowsMultipleSelectionDuringEditing = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
    
        [super viewWillAppear:animated];
        
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Recursos de misiones"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helpers

-(void)reloadData{
    NSManagedObjectContext *moc = [LiturGuia moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:@"MissionResourcesEntity"
                inManagedObjectContext:moc];
    [request setEntity:entity];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array != nil) {
        downloadedArray = array;
    }
    
    NSMutableArray *objectIdArray = [NSMutableArray array];
    for (MissionResourcesEntity *missionResources in downloadedArray) {
        [objectIdArray addObject:missionResources.parseId];
    }
    
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    [KVNProgress showWithStatus:@"Cargando información..."];
    
    PFQuery *query = [PFQuery queryWithClassName:@"RecursosMisiones"];
    [query whereKey:@"objectId" notContainedIn:objectIdArray];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [KVNProgress dismiss];
//        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = YES;
        if (!error) {
            self->downloadsArray = objects;
            [self->_tableView reloadData];
        }
    }];
}

- (IBAction)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITableViewDataSource

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MissionResourcesEntity *missionResource = downloadedArray[indexPath.row];
        
//        if ([bible.selected boolValue]) {
//            NSString *messageErr = @"Esta Biblia ya está descargada, para eliminarla, seleccione otra versión y después elimine ésta.";
//            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                         message:messageErr
//                                                        delegate:nil
//                                               cancelButtonTitle:@"OK"
//                                               otherButtonTitles:nil];
//            [av show];
//            return;
//        }
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"DB"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:missionResource.readerDocument.fileName];
        
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath: filePath error: &error];
        if (error) {
            NSLog(@"ERROR: %@",error.localizedDescription);
        }
        
        NSManagedObjectContext *moc = [LiturGuia moc];
        [moc deleteObject:missionResource];
        [LiturGuia saveContext];
        
        [self reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Descargados";
            break;
        case 1:
            sectionName = downloadsArray.count > 0?@"Disponibles para descargar": nil;
            break;
        default:
            sectionName = @"";
            break;
    }
    
    return sectionName;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return downloadedArray.count;
    }else{
        return downloadsArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MissionResourcesCell";
    
    MissionResourceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.section == 0) {
        MissionResourcesEntity *missionResources = downloadedArray[indexPath.row];
        
        
        cell.descriptionLabel.text = missionResources.title;
        cell.sizeLabel.text = missionResources.size;

    }else if (indexPath.section == 1){
        PFObject *missionResources = downloadsArray[indexPath.row];
        
        cell.descriptionLabel.text = missionResources[@"titulo"];
       
        cell.sizeLabel.text = missionResources[@"size"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.section == 0) {
        MissionResourcesEntity *missionResources = downloadedArray[indexPath.row];
        
        readerViewController = nil; // Release any old ReaderViewController first
        
        readerViewController = [[ReaderViewController alloc] initWithReaderDocument:missionResources.readerDocument];
        
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        
        readerViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:readerViewController animated:YES completion:NULL];
    }else{
        PFObject *missionResourceParse = downloadsArray[indexPath.row];
        
        [KVNProgress showProgress:0 status:@"Descargando..."];
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//        hud.labelText = @"Descargando...";
        
        PFFileObject *parseFile = missionResourceParse[@"file"];
      
        
        [parseFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            [hud hide:YES];
            [KVNProgress dismiss];
            
            if (data && !error) {
                NSEntityDescription *entity =
                [NSEntityDescription entityForName:@"MissionResourcesEntity"
                            inManagedObjectContext:[LiturGuia moc]];
                
                MissionResourcesEntity *missionResource = [[MissionResourcesEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[LiturGuia moc]];
                missionResource.parseId = missionResourceParse.objectId;
                missionResource.title = missionResourceParse[@"titulo"];
                missionResource.size = missionResourceParse[@"size"];
                
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"DB"];
                
                NSString *fileName = [NSString stringWithFormat:@"%@.%@",missionResource.title, [parseFile.name pathExtension]];
                
                [data writeToFile:[folderPath stringByAppendingPathComponent:fileName] options:NSDataWritingAtomic error:&error];
                if (error) {
                    NSLog(@"ERROR: %@", [error localizedDescription]);
                }
                
                
                
                
                ReaderDocument *document = [ReaderDocument insertInMOC:[LiturGuia moc] name:fileName path:folderPath];
                missionResource.readerDocument = document;
                
                [LiturGuia saveContext];
                
                [self reloadData];
                
                
                self->readerViewController = nil; // Release any old ReaderViewController first
                
                self->readerViewController = [[ReaderViewController alloc] initWithReaderDocument:missionResource.readerDocument];
                
                self->readerViewController.delegate = self; // Set the ReaderViewController delegate to self
                
                self->readerViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//                readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
                self.view.window.userInteractionEnabled = YES;
                [self presentViewController:self->readerViewController animated:YES completion:nil];
                
                
            }else{
                [KVNProgress showErrorWithStatus:error.localizedDescription];
            }
        } progressBlock:^(int percentDone) {
            [KVNProgress showProgress: percentDone/100.0 status:@"Descargando..."];
        }];

    }
}

- (void)dismissReaderViewController:(ReaderViewController *)viewController{
    [readerViewController dismissViewControllerAnimated:YES completion:nil];
}

@end

