//
//  MissionResourcesViewController.h
//  LiturGuia
//
//  Created by Krloz on 08/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"


@interface MissionResourcesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, ReaderViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
