//
//  MeditationTableViewCell.h
//  LiturGuia
//
//  Created by Krloz on 10/05/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeditationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) IBOutlet UILabel *meditationLabel;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
