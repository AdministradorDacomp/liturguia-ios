//
//  DailyMeditationViewController.h
//  LiturGuia
//
//  Created by Krloz on 10/05/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeditationViewController.h"
#import "MeditationTableViewCell.h"

@interface DailyMeditationViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end
