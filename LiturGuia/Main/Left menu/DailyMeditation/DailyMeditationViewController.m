//
//  DailyMeditationViewController.m
//  LiturGuia
//
//  Created by Krloz on 10/05/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import "DailyMeditationViewController.h"
#import "XMLReader.h"
#import "UIImageView+AFNetWorking.h"



@interface DailyMeditationViewController (){
    NSMutableArray *dataSourceArray;
    NSDateFormatter *rssDateformatter, *dateformatter;
    NSMutableDictionary *images;
}

@end

@implementation DailyMeditationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    images = [NSMutableDictionary new];
    
    rssDateformatter = [[NSDateFormatter alloc] init];
    [rssDateformatter setDateFormat:@"MMMM d yyyy HH:mm:ss zzz"];
    rssDateformatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    
    dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd/MM/yyyy"];
    
    _tableView.estimatedRowHeight = 300.0;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: @"http://es.catholic.net/rss/meditacion.xml"]];
    
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (error) {
                                   NSLog(@"ERROR:(%@)", error);
                               }else{
//                                   NSString *retVal = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                                   NSLog(@"%@",retVal);
                                   
                                   NSError *error = nil;
                                   NSDictionary *resultDict = [XMLReader dictionaryForXMLData:data error:&error];
                                  
                                   self->dataSourceArray = [NSMutableArray new];
                                   
                                   for (NSDictionary *dict in resultDict[@"rss"][@"channel"][@"item"]) {
                                       NSMutableDictionary *rowDict = [NSMutableDictionary new];
                                       rowDict[@"date"] = dict[@"pubDate"][@"text"];
                                       rowDict[@"title"] = dict[@"title"][@"text"];
                                       rowDict[@"author"] = dict[@"author"][@"text"];
                                       rowDict[@"link"] = dict[@"link"][@"text"];
                                       
                                       NSString *description = dict[@"description"][@"text"];
                                       NSRange range = [description rangeOfString:@"\"> "];
                                       NSString *text = [description substringWithRange:NSMakeRange(range.location + 3, description.length - range.location - 3)];
                                       ;
                                       rowDict[@"text"] = text;
                                       
                                       NSString *url = [description stringByReplacingOccurrencesOfString:text withString:@""];
                                       url = [url stringByReplacingOccurrencesOfString:@"<img src=\"" withString:@""];
                                       url = [url stringByReplacingOccurrencesOfString:@"\" align=\"left\"> " withString:@""];
                                       
                                       rowDict[@"imageUrl"] = [NSURL URLWithString:url];
                                       
                                       [self->dataSourceArray addObject:rowDict];
                                   }
                                   
                                   [self->_tableView reloadData];
                               }
                           }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Meditación del día"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"MeditationSegue"]) {
        MeditationViewController *vc = segue.destinationViewController;
        vc.data = sender;
    }
}

- (IBAction)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return dataSourceArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MeditationCell";
    MeditationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *rowDict = dataSourceArray[indexPath.row];
    
    NSString *pubDateString = rowDict[@"date"];
    
    NSDate *date = [rssDateformatter dateFromString:pubDateString];
    cell.dateLabel.text = [dateformatter stringFromDate:date];
    
    cell.titleLabel.text = rowDict[@"title"];
    cell.authorLabel.text = rowDict[@"author"];
    
    cell.meditationLabel.text = rowDict[@"text"];
    
    
    
    UIImage *image = images[[NSString stringWithFormat:@"%d", (int)indexPath.row]];
    if (image) { // if the dictionary of images has it just display it
        cell.photoImageView.image = image;
    }else{
        cell.photoImageView.image = nil;
        NSURLRequest *request = [NSURLRequest requestWithURL:rowDict[@"imageUrl"]];
        [cell.photoImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *  request, NSHTTPURLResponse *response, UIImage * remoteImage) {
            self->images[[NSString stringWithFormat:@"%d", (int)indexPath.row]] = remoteImage;
            MeditationTableViewCell *lookedUpCell = [tableView cellForRowAtIndexPath:indexPath];
            lookedUpCell.photoImageView.image = remoteImage;
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *  error) {
            
        }];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = dataSourceArray[indexPath.row];
    [self performSegueWithIdentifier:@"MeditationSegue" sender:dict];
}

@end
