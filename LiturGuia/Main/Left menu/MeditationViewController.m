//
//  MeditationViewController.m
//  LiturGuia
//
//  Created by Krloz on 10/05/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import "MeditationViewController.h"
#import "UIImageView+AFNetworking.h"

@interface MeditationViewController ()

@end

@implementation MeditationViewController

@synthesize data;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (data) {
        _titleLabel.text= data[@"title"];
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"MMM d yyyy HH:mm:ss zzz"];
        dateformatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        
        
        NSDate *date = [dateformatter dateFromString:data[@"date"]];
        [dateformatter setDateFormat:@"dd/MM/yyyy"];
        _dateLabel.text= [dateformatter stringFromDate:date];
        _authorLabel.text= data[@"author"];
        _descriptionLabel.text= data[@"text"];
        [_imageView setImageWithURL:data[@"imageUrl"]];
    }
    

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: data[@"link"]]];
    
    [request setHTTPMethod:@"GET"];
    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response, NSData *resultData, NSError *error) {
//                               if (error) {
//                                   NSLog(@"ERROR:(%@)", error);
//                               }else{
//                                   NSString *retVal = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding];
////                                   NSLog(@"%@",retVal);
////                                   NSRange range = [retVal rangeOfString:@"<div align='justify'  id='art_texto'>"];
////                                   
////                                   if (range.location != NSNotFound) {
////                                       retVal = [retVal substringWithRange:NSMakeRange(range.location, retVal.length - range.location)];
////                                   }
////                                   
////                                   
////                                   range = [retVal rangeOfString:@"</div>"];
////                                   
////                                   
////                                   
////                                   if (range.location != NSNotFound) {
////                                       retVal = [retVal substringWithRange:NSMakeRange(0, range.location+range.length)];
////                                   }
////                                   
//////                                   NSString *replace =@"<div align='center' class='mobile'><script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>\n<!-- CN 300 x 250 Articulos Body -->\n<ins class='adsbygoogle'\nstyle='display:inline-block;width:300px;height:250px'\ndata-ad-client='ca-pub-7448133621512387'\ndata-ad-slot='6378178351'></ins>\n<script>\n(adsbygoogle = window.adsbygoogle || []).push({});\n</script></div>";
//////                                   retVal = [retVal stringByReplacingOccurrencesOfString:replace withString:@""];
////                                   
////                                
////                                
////                                   
////                                   
////                                   
////                                  NSRange rangeStart = [retVal rangeOfString:@"<div align='center' class='mobile'><script async"];
////                                   
////                                  NSRange rangeEnd = [retVal rangeOfString:@"</script></div>"];
////                                   if(rangeStart.location != NSNotFound && rangeEnd.location!= NSNotFound){
////                                       NSString *replace = [retVal substringWithRange:NSMakeRange(rangeStart.location,-rangeStart.location + rangeEnd.location +rangeEnd.length)];
////                                       
////                                       retVal = [retVal stringByReplacingOccurrencesOfString:replace withString:@""];
////                                   }
////                                   
////                                   rangeStart = [retVal rangeOfString:@"<div><a"];
////                                   
////                                   rangeEnd = [retVal rangeOfString:@"</a></div>"];
////                                   if(rangeStart.location != NSNotFound && rangeEnd.location!= NSNotFound){
////                                       NSString *replace = [retVal substringWithRange:NSMakeRange(rangeStart.location,-rangeStart.location + rangeEnd.location +rangeEnd.length)];
////                                       
////                                       retVal = [retVal stringByReplacingOccurrencesOfString:replace withString:@""];
////                                   }
////                                   
////                                   rangeStart = [retVal rangeOfString:@"<a"];
////                                   
////                                   rangeEnd = [retVal rangeOfString:@"</a>"];
////                                   if(rangeStart.location != NSNotFound && rangeEnd.location!= NSNotFound){
////                                       NSString *replace = [retVal substringWithRange:NSMakeRange(rangeStart.location,-rangeStart.location + rangeEnd.location +rangeEnd.length)];
////                                       
////                                       retVal = [retVal stringByReplacingOccurrencesOfString:replace withString:@""];
////                                   }
////                                   
////                                   rangeStart = [retVal rangeOfString:@"<ul>"];
////                                   
////                                   rangeEnd = [retVal rangeOfString:@"</ul>"];
////                                   if(rangeStart.location != NSNotFound && rangeEnd.location!= NSNotFound){
////                                       NSString *replace = [retVal substringWithRange:NSMakeRange(rangeStart.location,-rangeStart.location + rangeEnd.location +rangeEnd.length)];
////                                       
////                                       retVal = [retVal stringByReplacingOccurrencesOfString:replace withString:@""];
////                                   }
////                                   
////                                   rangeStart = [retVal rangeOfString:@"_____"];
////                                   if(rangeStart.location != NSNotFound){
////                                       NSString *replace = [retVal substringWithRange:NSMakeRange(rangeStart.location,retVal.length - rangeStart.location)];
////                                       
////                                       retVal = [retVal stringByReplacingOccurrencesOfString:replace withString:@""];
////                                   }
////                                   
////                                   
////                                
////                                   retVal = [retVal stringByReplacingOccurrencesOfString:@"Puedes escuchar esta meditaci&oacute;n en audio entrando al Podcast de Catholic.net aqu&iacute;:" withString:@""];
////                                   
////                                   retVal = [retVal stringByReplacingOccurrencesOfString:@"<li style='font-size:16px;line-height: 1.3;   font-family: Roboto, Verdana, Arial, sans-serif;font-weight: normal;color: #36383d; '></li>" withString:@""];
////                                   
////
////                                   NSLog(@"%@",retVal);
//                                   
//                                   
//                                   [_webView loadHTMLString:retVal baseURL:nil];
//                               }
//                               
//                           }];
    
    [_webView loadRequest:request];
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";

    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];

    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    wkWebConfig.userContentController = wkUController;

    _webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:wkWebConfig];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Meditación"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)webView:(WKWebView *)aWebView didFinishNavigation:(WKNavigation *)navigation {
    float initialHeight = aWebView.frame.size.height;
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;

    float difference = fittingSize.height - initialHeight;
    CGSize scrollViewSize = _scrollView.contentSize;
    scrollViewSize.height += difference;
    _scrollView.contentSize = scrollViewSize;
    
    aWebView.frame = frame;
//
//    aWebView.scalesPageToFit = YES;
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";

    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];

    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    wkWebConfig.userContentController = wkUController;

    aWebView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:wkWebConfig];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
