//
//  DeleteAccountViewController.swift
//  LiturGuia
//
//  Created by Daniel Marrufo rivera on 13/09/23.
//  Copyright © 2023 DAComp SC. All rights reserved.
//

import Foundation
import UIKit
import Parse
import KVNProgress

@objc class DeleteAccountViewController: UIViewController,UIApplicationDelegate {
    
    @IBOutlet weak var btnDeleteAccount: UIButton!
    @IBOutlet weak var vwShowReasons: UIView!
    @IBOutlet weak var txtReasonSelected: UITextField!
    @IBOutlet weak var vwOtherReason: UIView!
    @IBOutlet weak var lblNumberCharacters: UILabel!
    @IBOutlet weak var txtOtherReason: UITextField!
    var reasonsList = ["No me resulta útil el contenido de esta app","Tengo otra cuenta","Uso otra app para llevar a cabo la liturgia","No recibí una buena atención","Quiero crear una nueva cuenta","Otro"]
    var reasonAdded = false
    var reasonName:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.validationFieldsComplete()
        self.loadInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    
    @IBAction func deleteAccountButtonTapped(_ sender: Any) {
        KVNProgress.show()
        DeleteManager.shared.deleteAllData(motivo: self.reasonName) { success, error in
            if let success = success,
               success{
                self.btnDeleteAccount.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.btnDeleteAccount.setTitleColor(.black, for: .normal)
                self.btnDeleteAccount.isEnabled = false
                print("success = \(success)")
                KVNProgress.dismiss()
                self.showSuccessAlert()
            }else{
                self.showAlertWithTitleAndMessage("Atención", "Ocurrio un error al tratar de eliminar tu cuenta, favor de intentarlo más tarde")
            }
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func loadInfo(){
        let reasonTapGeture = UITapGestureRecognizer(target: self, action: #selector(reasonTapped))
        vwShowReasons.addGestureRecognizer(reasonTapGeture)
        txtReasonSelected?.placeholder = "Elija un motivo"
        txtOtherReason.addTarget(self, action: #selector(textFieldDidChange(_:)),for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        textField.checkMaxLenght(maxLength: 30)
        if let text = textField.text,
           text.count > 0{
            self.lblNumberCharacters.text = "\(text.count)/30"
            self.reasonAdded = true
            self.reasonName = text
        }else{
            self.lblNumberCharacters.text = "\(0)/30"
            self.reasonAdded = false
        }
        self.validationFieldsComplete()
    }
    
    func showSuccessAlert() {
        let alert = UIAlertController(title: "Su cuenta de LiturGuía ha sido eliminada.", message:"", preferredStyle: .alert)
        present(alert, animated: true, completion: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                alert.dismiss(animated: true) {
                    KVNProgress.show()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        KVNProgress.dismiss()
                        self.presentingViewController?.presentingViewController?.dismiss(animated: true) {
                            let storyboard = UIStoryboard(name: "Login", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login")
                            let topVC = UIApplication.topViewController()
                            let nvc = UINavigationController(rootViewController: vc)
                            nvc.modalPresentationStyle = .fullScreen
                            topVC?.present(nvc, animated: true, completion: nil)
                        }
                    }
                }
            }
        })
    }
    
    
    func getReasonsActions(reasons: [String]) {
        var actions: [UIAlertAction] = []
        
        for reason in reasons {
            actions.append(UIAlertAction(title: reason, style: .default) { _ in
                self.updateReasonSelected(reason)
            })
        }
        actions.append(UIAlertAction(title: "Cancelar", style: .cancel))
        presentReasons(actions: actions)
    }
    @objc func reasonTapped() {
        self.getReasonsActions(reasons: reasonsList)
    }
    
    func presentReasons(actions: [UIAlertAction]) {
        showAlertWithActionsAndStyle(titulo: nil, mensaje: nil, actions: actions, style: .actionSheet)
    }
    
    func updateReasonSelected(_ selected: String) {
        reasonName = selected
        self.txtReasonSelected?.text = reasonName
        
        if reasonName == "Otro" {
            vwOtherReason.isHidden = false
            reasonAdded = false
            self.lblNumberCharacters.text = "\(0)/30"
        } else {
            self.txtOtherReason?.text = ""
            self.vwOtherReason.isHidden = true
            reasonAdded = true
            
        }
        
        validationFieldsComplete()
    }
    
    func validationFieldsComplete() {
        if self.reasonAdded {
            btnDeleteAccount.backgroundColor = #colorLiteral(red: 1, green: 0.2431372549, blue: 0.1333333333, alpha: 1)
            btnDeleteAccount.setTitleColor(.white, for: .normal)
            btnDeleteAccount.isEnabled = true
        } else {
            btnDeleteAccount.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            btnDeleteAccount.setTitleColor(.black, for: .normal)
            btnDeleteAccount.isEnabled = false
        }
    }
    
}
extension UITextField{
    /// Función que permite establecer la longitud del texto ingresado a un textFieldc sin validacion
    /// - Parameter maxLength: Int que define el tamaño de la cadena permitida
    public func checkMaxLenght(maxLength:Int) {
        if (self.text!.count > maxLength) {
            self.deleteBackward()
        }
    }
}

extension UIViewController {
    var top: UIViewController? {
        if let controller = self as? UINavigationController {
            return controller.topViewController?.top
        }
        if let controller = self as? UISplitViewController {
            return controller.viewControllers.last?.top
        }
        if let controller = self as? UITabBarController {
            return controller.selectedViewController?.top
        }
        if let controller = presentedViewController {
            return controller.top
        }
        return self
    }
    
    func showAlertWithActionsAndStyle(titulo: String?, mensaje: String? = nil, actions: [UIAlertAction], style: UIAlertController.Style, preferredAction: UIAlertAction? = nil,completion: ((UIAlertController) -> Void)? = nil) {
        let actionSheetVC = UIAlertController(title: titulo, message: mensaje, preferredStyle: style)
        for action in actions {
            actionSheetVC.addAction(action)
        }
        addActionSheetForiPad(actionSheet: actionSheetVC)
        actionSheetVC.preferredAction = preferredAction
        self.present(actionSheetVC, animated: true) {
            if let completion = completion {
                completion(actionSheetVC)
            }
        }
    }
}

extension UIViewController {
  public func addActionSheetForiPad(actionSheet: UIAlertController) {
    if let popoverPresentationController = actionSheet.popoverPresentationController {
      popoverPresentationController.sourceView = self.view
      popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
      popoverPresentationController.permittedArrowDirections = []
    }
  }
}
