//
//  PageViewController.h
//  EAIntroView
//
//  Created by Krloz on 09/02/16.
//  Copyright © 2016 SampleCorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface PageViewController : UIViewController

@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (strong, nonatomic) IBOutlet UIImageView *screenImageView;
@property (strong, nonatomic) NSString *videoName;
@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSString *descriptionString;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

@property BOOL isFirstController;

@end
