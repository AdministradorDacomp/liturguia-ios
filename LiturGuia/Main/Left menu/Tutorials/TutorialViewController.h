//
//  TutorialViewController.h
//  EAIntroView
//
//  Created by Krloz on 05/02/16.
//  Copyright © 2016 SampleCorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController<UIPageViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIPageControl *pageControlIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *screenImageView;
@property (weak, nonatomic) IBOutlet UIView *containerView;



@end
