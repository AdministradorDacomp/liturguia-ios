//
//  TutorialPageViewController.m
//  EAIntroView
//
//  Created by Krloz on 05/02/16.
//  Copyright © 2016 SampleCorp. All rights reserved.
//

#import "TutorialGeneralViewController.h"
#import "PageViewController.h"


@interface TutorialGeneralViewController ()

@end;



@implementation TutorialGeneralViewController{
    NSArray *myViewControllers;
}

@synthesize pageControlDelegate;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = pageControlDelegate;
    
    self.dataSource = self;
    
    PageViewController *p1 = [self.storyboard
                            instantiateViewControllerWithIdentifier:@"TutorialPage"];
    p1.titleString = @"Secciones";
    p1.descriptionString = @"Aquí encontrará las secciones de LiturGuía.\nMisales, Biblia, oraciones, lectio divina... ¡Y  muchos mas que estamos preparando!";
    p1.videoName = @"sections_general_es";
    p1.view.backgroundColor = [UIColor clearColor];
    p1.isFirstController = YES;

    PageViewController *p2 = [self.storyboard
                              instantiateViewControllerWithIdentifier:@"TutorialPage"];
    p2.titleString = @"Misales";
    p2.descriptionString = @"Aquí podrá usar a los misales de la Santa Misa de manera completa e interactiva.";
    p2.videoName = @"missal_general_es";
    p2.view.backgroundColor = [UIColor clearColor];
    
    PageViewController *p3 = [self.storyboard
                              instantiateViewControllerWithIdentifier:@"TutorialPage"];
    p3.titleString = @"Biblia";
    p3.descriptionString = @"Aquí podrá leer la Santa Biblia. Así como algunas funciones de gran utilidad: buscar versículos, compartir fragmentos con tus amigos o redes sociales, busqueda por temas, etc.";
    p3.videoName = @"bible_general_es";
    p3.view.backgroundColor = [UIColor clearColor];
    
    PageViewController *p4 = [self.storyboard
                              instantiateViewControllerWithIdentifier:@"TutorialPage"];
    p4.titleString = @"Oremos unos por otros";
    p4.descriptionString = @"Aquí podrá orar por intenciones de usuarios en todo el mundo, así como agregar sus peticiones para que otros usuarios oren por ellas.";
    p4.videoName = @"prayers_es";
    p4.view.backgroundColor = [UIColor clearColor];
    
    PageViewController *p5 = [self.storyboard
                              instantiateViewControllerWithIdentifier:@"TutorialPage"];
    p5.titleString = @"Rosario";
    p5.descriptionString = @"En ésta parte de la aplicación encontraras un rosario interactivo para que puedas rezarlo en cualquier momento si no tienes un rosario físico a la mano.";
    p5.videoName = @"rosary_general_es";
    p5.view.backgroundColor = [UIColor clearColor];
    
    PageViewController *p6 = [self.storyboard
                              instantiateViewControllerWithIdentifier:@"TutorialPage"];
    p6.titleString = @"Examen de conciencia";
    p6.descriptionString = @"Ésta parte de la aplicación sirve como guía para realizar un buen examen de conciencia y confesión, con la opción de agregar pecados manualmente o a partir de un cuestionario predefinido.";
    p6.videoName = @"exam_es";
    p6.view.backgroundColor = [UIColor clearColor];
    
    myViewControllers = @[p1, p2, p3, p4, p5, p6];
    
    [self setViewControllers:@[p1]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:NO completion:nil];
    
    NSLog(@"loaded!");
    
 
}

-(UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    return myViewControllers[index];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSArray *subviews = self.view.subviews;
    UIPageControl *thisControl;
    for (int i = 0; i < subviews.count; i++) {
        if ([subviews[i] isKindOfClass:[UIPageControl class]]) {
            thisControl = (UIPageControl *)subviews[i];
        }
    }
    
    thisControl.hidden = true;
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);
}
//-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
//     viewControllerBeforeViewController:(UIViewController *)viewController
//{
//    NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
//    
//    --currentIndex;
//    currentIndex = currentIndex % (myViewControllers.count);
//    return [myViewControllers objectAtIndex:currentIndex];
//}
//
//-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
//      viewControllerAfterViewController:(UIViewController *)viewController
//{
//    NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
//    
//    ++currentIndex;
//    currentIndex = currentIndex % (myViewControllers.count);
//    return [myViewControllers objectAtIndex:currentIndex];
//}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
    
    if ((currentIndex == 0) || (currentIndex == NSNotFound)) {
        return nil;
    }
    
    currentIndex--;
    return [self viewControllerAtIndex:currentIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
    
    if (currentIndex == NSNotFound || currentIndex >= myViewControllers.count - 1) {
        return nil;
    }
    
    currentIndex++;

    return [self viewControllerAtIndex:currentIndex];
}

-(NSInteger)presentationCountForPageViewController:
(UIPageViewController *)pageViewController
{
    return myViewControllers.count;
}

-(NSInteger)presentationIndexForPageViewController:
(UIPageViewController *)pageViewController
{
    return 0;
}

-(int)getIndexOfViewController:(UIViewController*)vc{
    int  currentIndex = (int)[myViewControllers indexOfObject:vc];
    return currentIndex;
}

-(void)setCurrentPage:(int)page{
    UIViewController *vc = myViewControllers[page];
    __weak UIPageViewController* pvcw = self;
    [self setViewControllers:@[vc]
                  direction:UIPageViewControllerNavigationDirectionForward
                   animated:YES completion:^(BOOL finished) {
                       UIPageViewController* pvcs = pvcw;
                       if (!pvcs) return;
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [pvcs setViewControllers:@[vc]
                                          direction:UIPageViewControllerNavigationDirectionForward
                                           animated:NO completion:nil];
                       });
                   }];
}


@end
