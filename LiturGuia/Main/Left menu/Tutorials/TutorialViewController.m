
//
//  TutorialViewController.m
//  EAIntroView
//
//  Created by Krloz on 05/02/16.
//  Copyright © 2016 SampleCorp. All rights reserved.
//

#import "TutorialViewController.h"
#import "TutorialGeneralViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TutorialViewController (){
    TutorialGeneralViewController *pageVC;
//    NSArray *screenImagesArray;
}

@end;

@implementation TutorialViewController{
    NSArray *myViewControllers;
    
    BOOL viewsAdded;
}

@synthesize containerView;

-(void)viewDidLoad
{
    [super viewDidLoad];
    viewsAdded = NO;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Tutorial"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    if (!viewsAdded) {
        viewsAdded = YES;
        float bottomNotch = 0.0;
        if (@available(iOS 11.0, *)) {
            bottomNotch = [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.bottom;
        }
        
        CGSize screenSize = [self view].frame.size;
        float padding = 42.0;
        
        if (screenSize.height <= 480) {
            padding = 66.0;
            
        }else if(screenSize.height <= 568){
            padding = 50.0;
        }
        float width = screenSize.width - padding * 2.0;
        float imageRatio = 750.0/1335.0;
        float height =  width/imageRatio;
        
        CGRect leftViewFrame = CGRectMake(0, screenSize.height - height - bottomNotch, padding + 5, height);
        UIView *leftView = [[UIView alloc] initWithFrame:leftViewFrame];
        leftView.backgroundColor = [UIColor colorWithRed:0.267 green:0.298 blue:0.537 alpha:1];
        [[self view] addSubview:leftView];
        
        CGRect rightViewFrame = CGRectMake(screenSize.width - padding - 5, screenSize.height - height - bottomNotch, padding + 5, height);
        UIView *rightView = [[UIView alloc] initWithFrame:rightViewFrame];
        rightView.backgroundColor = [UIColor colorWithRed:0.267 green:0.298 blue:0.537 alpha:1];
        [[self view] addSubview:rightView];
        
        CGRect mockupRect = CGRectMake(padding, screenSize.height - height - bottomNotch , width, height);
        
        UIImageView *mockupImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_mockup"]];
        [mockupImageView setFrame:mockupRect];
        [[self view] addSubview:mockupImageView];
        
        
        CGRect backViewFrame = CGRectMake(leftViewFrame.size.width, screenSize.height - height - bottomNotch +  30, screenWidth - leftViewFrame.size.width * 2, height - 30);
        UIView *backView = [[UIView alloc] initWithFrame:backViewFrame];
        backView.backgroundColor = [UIColor blackColor];
        [[self view] insertSubview:backView belowSubview:containerView];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqual:@"TutorialPageViewControllerSegue"]) {
        pageVC = [segue destinationViewController];
        pageVC.pageControlDelegate = self;
    }
}

- (IBAction)pageControlIndicatorValueChanged:(id)sender {
    UIPageControl *pager = sender;
    int page = (int)pager.currentPage;
    [pageVC setCurrentPage:page];
    _pageControlIndicator.currentPage = page;
}


- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    if (completed && finished) {
       int index = [pageVC getIndexOfViewController:[pageViewController.viewControllers objectAtIndex:0]];
        _pageControlIndicator.currentPage = index;
    }
}

- (IBAction)exitButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
