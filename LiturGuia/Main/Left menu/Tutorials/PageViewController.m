//
//  PageViewController.m
//  EAIntroView
//
//  Created by Krloz on 09/02/16.
//  Copyright © 2016 SampleCorp. All rights reserved.
//

#import "PageViewController.h"
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface PageViewController ()

@end

@implementation PageViewController

@synthesize videoName, titleString, descriptionString, imageName, isFirstController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(replayMovie:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    if (titleString) {
        _titleLabel.text = titleString;
    }
    if (descriptionString) {
        _descriptionLabel.text = descriptionString;
    }
    if (imageName) {
        [_screenImageView setImage:[UIImage imageNamed:imageName]];
    }
}


-(AVPlayerLayer*)playerLayer{
    if(!_playerLayer){
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:videoName ofType:@"m4v"];
        NSLog(@"videoName = %@", videoName);
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        NSLog(@"movieURL = %@", movieURL);
       
        _playerLayer = [AVPlayerLayer playerLayerWithPlayer:[[AVPlayer alloc]initWithURL:movieURL]];

//        float topNotch = 0.0;
//        float bottomNotch = 0.0;
//        if (@available(iOS 11.0, *)) {
//            topNotch = [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top - 20;
//            bottomNotch = [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.bottom;
//        }
        
        CGSize screenSize = [[self view] frame].size;
        float padding = 65.0;
        
        if (screenSize.height <= 480) {
            padding = 80.0;
            
        }else if(screenSize.height <= 568){
            padding = 66.0;
        }
        
        float width = screenSize.width - padding * 2.0;
        float videoRatio = 416.0/740.0;
        float height =  width/videoRatio;
        
        CGRect rect = CGRectMake(padding, screenSize.height - height - 5, width, height);
        
        
        _playerLayer.frame = rect;
        
        _playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [_playerLayer.player play];
        return _playerLayer;
    }
    return nil;
}


-(NSString *)documentsDirectory{
 NSArray *paths =
 NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 return documentsDirectory;
}

- (void) didFinishAutoLayout {
    if (videoName && !_playerLayer) {
        [self.view.layer addSublayer:self.playerLayer];
    }
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (!_playerLayer) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(didFinishAutoLayout)
                                                   object:nil];
        [self performSelector:@selector(didFinishAutoLayout) withObject:nil
                   afterDelay:0];
    }else{
//        float topNotch = 0.0;
//        float bottomNotch = 0.0;
//        if (@available(iOS 11.0, *)) {
//            topNotch = [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top - 20;
//            bottomNotch = [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.bottom;
//        }
        
        CGSize screenSize = [[self view] frame].size;
        float padding = 65.0;
        
        if (screenSize.height <= 480) {
            padding = 80.0;
            
        }else if(screenSize.height <= 568){
            padding = 66.0;
        }
        
        float width = screenSize.width - padding * 2.0;
        float videoRatio = 416.0/740.0;
        float height =  width/videoRatio;
        
        CGRect rect = CGRectMake(padding, screenSize.height - height - 5, width, height);
        
        
        _playerLayer.frame = rect;
    }
}


-(void)replayMovie:(NSNotification *)notification{
    AVPlayerItem *playerItem = [notification object];

//    NSURL *url = [playerItem ];
    if (playerItem == _playerLayer.player.currentItem) {
    
        [playerItem seekToTime:kCMTimeZero];
        
        [_playerLayer.player play];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
