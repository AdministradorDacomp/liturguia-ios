//
//  TutorialPageViewController.h
//  EAIntroView
//
//  Created by Krloz on 05/02/16.
//  Copyright © 2016 SampleCorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialViewController.h"

@interface TutorialGeneralViewController : UIPageViewController<UIPageViewControllerDataSource>
@property (strong, nonatomic) id pageControlDelegate;

-(int)getIndexOfViewController:(UIViewController*)vc;
-(void)setCurrentPage:(int)page;

@end
