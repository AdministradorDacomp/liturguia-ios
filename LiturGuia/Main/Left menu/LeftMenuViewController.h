//
//  LeftMenuViewController.h
//  LiturGuia
//
//  Created by Krloz on 14/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userPictureImageView;
@property (strong, nonatomic) IBOutlet UIImageView *coverPhotoImageView;

@end
