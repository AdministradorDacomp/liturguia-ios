//
//  ContactViewController.h
//  LiturGuia
//
//  Created by Krloz on 01/10/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITextView *commentsTextView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end
