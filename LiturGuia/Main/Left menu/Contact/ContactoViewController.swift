//
//  ContactoViewController.swift
//  LiturGuia
//
//  Created by Daniel Marrufo rivera on 14/12/21.
//  Copyright © 2021 DAComp SC. All rights reserved.
//

import Foundation
import UIKit
import Parse
import MaryPopin
import Parse
import WYPopoverController
import IQKeyboardManagerSwift

//MARK: -
class ContactoViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var emailTextField: UITextField!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setModalView()
        self.emailTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    
    
    
    //MARK: - Helpers
    func setModalView()  {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Salir", style: .done, target: self, action: #selector(ContactoViewController.closeModal))
        
    }
    
    @objc func closeModal() {
        let alert = UIAlertController(title: "Aún no se han enviado sus comentarios", message: "¿Está seguro que desea salir?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: { (action) in
            self.dismiss(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - Button actions
    @IBAction func sendCommentsButtonTapped(_ sender:UIButton?){
        
        if emailTextField.text == "" {
            self.showAlertWithMessage("Favor de ingresar una dirección de correo  electrónico para ponernos en contacto.")
            emailTextField.becomeFirstResponder()
            return
        }
        if !(emailTextField.text?.isValidEmail())! {
            self.showAlertWithMessage("Favor de ingresar una dirección de correo electrónico válida para ponernos en contacto.")
            emailTextField.becomeFirstResponder()
            return
        }
        
        if commentsTextView.text.isEmpty{
            self.showAlertWithMessage("Favor de ingresar comentarios.")
            commentsTextView.becomeFirstResponder()
            return
        }
        
        let comments:PFObject = .init(className: "Comentarios")
        if let user = PFUser.current(){
            comments["user"] = user
        }
        comments["email"] = emailTextField.text;
        comments["texto"] = commentsTextView.text;
        comments["texto"] = commentsTextView.text;
        comments["plataforma"] = "iOS"
        
        comments.saveInBackground()
        let alert = UIAlertController(title: nil, message: "¡Gracias por sus comentarios!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            self.emailTextField.text = ""
            self.commentsTextView.text = "";
            self.dismiss(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}




