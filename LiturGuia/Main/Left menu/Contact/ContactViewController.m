//
//  ContactViewController.m
//  LiturGuia
//
//  Created by Krloz on 01/10/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "ContactViewController.h"

@interface NSString (emailValidation)
  - (BOOL)isValidEmail;
@end
@interface ContactViewController ()

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 0, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(textViewDoneButtonTapped:)]
                           ];
    [numberToolbar sizeToFit];
    
    numberToolbar.barTintColor = kNavigationBarColor;
    numberToolbar.tintColor = [UIColor whiteColor];
    
    _commentsTextView.inputAccessoryView = numberToolbar;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)doneButtonTapped:(id)sender {
    if (![_commentsTextView.text isEqualToString:@""]) {
        NSString *messageErr = @"Aún no se han enviado los comentarios. ¿Desea enviar los comentarios?";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:self
                                           cancelButtonTitle:@"No"
                                           otherButtonTitles:@"Sí",nil];
        [av show];
        return;
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)sendCommentsButtonTapped:(id)sender {
    
    if ([_emailTextField.text isEqualToString:@""]) {
        NSString *messageErr = @"Favor de ingresar una dirección de correo  electrónico para ponernos en contacto.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
        [_emailTextField becomeFirstResponder];
        return;
    }
    if (![_emailTextField.text isValidEmail]) {
        NSString *messageErr = @"Favor de ingresar una dirección de correo electrónico válida para ponernos en contacto.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
        [_emailTextField becomeFirstResponder];
        return;
    }
    
    if ([_commentsTextView.text isEqualToString:@""]) {
        NSString *messageErr = @"Favor de ingresar comentarios.";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:messageErr
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
        [_commentsTextView becomeFirstResponder];
        return;
    }
    
    PFObject *comments = [PFObject objectWithClassName:@"Comentarios"];
    comments[@"user"] = [PFUser currentUser];
    comments[@"email"] = _emailTextField.text;
    comments[@"texto"] = _commentsTextView.text;
    
    [comments saveInBackground];

    NSString *messageErr = @"¡Gracias por sus comentarios!";
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:messageErr
                                                delegate:nil
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
    [av show];
    
    _commentsTextView.text = @"";
    [self doneButtonTapped:nil];
}

#pragma mark - UITextViewDelegate

-(void)textViewDoneButtonTapped:(id)sender{
    [_commentsTextView resignFirstResponder];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.cancelButtonIndex == buttonIndex) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
        }];
    }else{
        [self sendCommentsButtonTapped:nil];
    }
}

@end

@implementation NSString (emailValidation)
-(BOOL)isValidEmail
{
  BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
  NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
  NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
  NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
  NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
  return [emailTest evaluateWithObject:self];
}
@end
