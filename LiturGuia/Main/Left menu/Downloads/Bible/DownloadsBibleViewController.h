//
//  DownloadsBibleViewController.h
//  LiturGuia
//
//  Created by Krloz on 11/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadsBibleViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
