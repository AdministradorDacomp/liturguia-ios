//
//  DownloadsBibleTableViewCell.h
//  LiturGuia
//
//  Created by Krloz on 11/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadsBibleTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *purchaseButton;

@end
