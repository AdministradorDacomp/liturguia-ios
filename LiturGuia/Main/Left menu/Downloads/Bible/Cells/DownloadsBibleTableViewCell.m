//
//  DownloadsBibleTableViewCell.m
//  LiturGuia
//
//  Created by Krloz on 11/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "DownloadsBibleTableViewCell.h"

@implementation DownloadsBibleTableViewCell

#pragma mark - Helpers

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
