//
//  DownloadsBibleViewController.m
//  LiturGuia
//
//  Created by Krloz on 11/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "DownloadsBibleViewController.h"
#import "DownloadsBibleTableViewCell.h"
#import "BibleEntity.h"
#import <CoreData/CoreData.h>
#import "BibleEntity.h"


@interface DownloadsBibleViewController (){
    NSArray *downloadsArray;
    NSArray *downloadedArray;
}

@end

@implementation DownloadsBibleViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.allowsMultipleSelectionDuringEditing = NO;
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
    button.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = button;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Helpers

- (void)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)reloadData{
    NSManagedObjectContext *moc = [LiturGuia moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:@"BibleEntity"
                inManagedObjectContext:moc];
    [request setEntity:entity];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array != nil) {
        downloadedArray = array;
    }
    
    NSMutableArray *fileNameArray = [NSMutableArray array];
    for (BibleEntity *bible in downloadedArray) {
        [fileNameArray addObject:bible.fileName];
    }
    [KVNProgress showWithStatus:@"Cargando biblias disponibles..."];
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Biblia"];
    [query whereKey:@"nombreArchivo" notContainedIn:fileNameArray];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//        self.view.window.userInteractionEnabled = YES;
        if (!error) {
            [KVNProgress dismiss];
            self->downloadsArray = objects;
            [self->_tableView reloadData];
        }else{
            [KVNProgress showErrorWithStatus:error.localizedDescription];
        }
    }];
}


#pragma mark - Button actions

- (IBAction)purchaseButtonTapped:(id)sender {
    PFObject *bibleParse = downloadsArray[[sender tag]];
    
    [KVNProgress showProgress:0 status:@"Descargando..."];
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//    hud.labelText = @"Descargando...";
    
    PFFileObject *bibleFile = bibleParse[@"archivo"];
    
    [bibleFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//        [hud hide:YES];
        
        if (data && !error) {
            [KVNProgress showSuccess];
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"DB"];
            
            [data writeToFile:[folderPath stringByAppendingPathComponent:bibleParse[@"nombreArchivo"]] options:NSDataWritingAtomic error:&error];
            if (error) {
                NSLog(@"ERROR: %@", [error localizedDescription]);
            }
            
            
            NSEntityDescription *entity =
            [NSEntityDescription entityForName:@"BibleEntity"
                        inManagedObjectContext:[LiturGuia moc]];
            
            BibleEntity *bible = [[BibleEntity alloc] initWithEntity:entity insertIntoManagedObjectContext:[LiturGuia moc]];
            
            bible.bibleDescription = bibleParse[@"descripcion"];
            bible.selected = @NO;
            bible.language = bibleParse[@"idioma"];
            bible.fileName = bibleParse[@"nombreArchivo"];
            
            [LiturGuia saveContext];
            
            [self reloadData];
        }else{
            [KVNProgress showErrorWithStatus:error.localizedDescription];
        }
    } progressBlock:^(int percentDone) {
        [KVNProgress showProgress:percentDone/100.0 status:@"Descargando..."];
    }];
}

#pragma mark - UITableViewDataSource

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        BibleEntity *bible = downloadedArray[indexPath.row];
        
        if ([bible.selected boolValue]) {
            NSString *messageErr = @"Esta Biblia ya está descargada, para eliminarla, seleccione otra versión y después elimine ésta.";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            return;
        }
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"DB"];
        NSString *filePath = [folderPath stringByAppendingPathComponent:bible.fileName];
        
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath: filePath error: &error];
        if (error) {
            NSLog(@"ERROR: %@",error.localizedDescription);
        }
        
        NSManagedObjectContext *moc = [LiturGuia moc];
        [moc deleteObject:bible];
        [LiturGuia saveContext];
        
        [self reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Descargadas";
            break;
        case 1:
            sectionName = downloadsArray.count > 0?@"Disponibles para descarga": nil;
            break;
        default:
            sectionName = @"";
            break;
    }

    return sectionName;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return downloadedArray.count;
    }else{
        return downloadsArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifierDownload = @"DownloadCell";
    static NSString *CellIdentifierDownloaded = @"DownloadedCell";
    
    DownloadsBibleTableViewCell *cell;
    if (indexPath.section == 0) {
        BibleEntity *bible = downloadedArray[indexPath.row];
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierDownloaded];
        cell.descriptionLabel.text = bible.bibleDescription;
        cell.iconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@", bible.language]];
        if ([bible.selected boolValue]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else if (indexPath.section == 1){
        PFObject *bible = downloadsArray[indexPath.row];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierDownload];
        cell.descriptionLabel.text = bible[@"descripcion"];
        cell.iconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@", bible[@"idioma"]]];
        NSString *priceString;
        if ([bible[@"precio"] floatValue] == 0) {
            priceString = @"Gratis";
        }else{
            priceString = [NSString stringWithFormat:@"$%.2f",[bible[@"precio"] floatValue]];
        }
        cell.purchaseButton.tag = indexPath.row;
        [cell.purchaseButton setTitle:NSLocalizedString(priceString, nil) forState:UIControlStateNormal];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }else{
        cell = [DownloadsBibleTableViewCell new];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        for (BibleEntity *bible in downloadedArray) {
            if ([bible.selected boolValue]) {
                bible.selected = @NO;
            }
        }
        BibleEntity *bible = downloadedArray[indexPath.row];
        bible.selected = @YES;
        [LiturGuia saveContext];
        [_tableView reloadData];
    }
}

@end
