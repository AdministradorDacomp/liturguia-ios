//
//  DownloadsViewController.m
//  LiturGuia
//
//  Created by Krloz on 11/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "DownloadsViewController.h"

@interface DownloadsViewController ()

@end

@implementation DownloadsViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

- (IBAction)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)bibleButtonTapped:(id)sender {
}

- (IBAction)missalButtonTapped:(id)sender {
    NSString *messageErr = @"¡Próximamente!";
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:messageErr
                                                delegate:nil
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil];
    [av show];
}

@end
