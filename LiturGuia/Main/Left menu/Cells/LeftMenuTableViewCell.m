//
//  LeftMenuTableViewCell.m
//  LiturGuia
//
//  Created by Krloz on 14/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "LeftMenuTableViewCell.h"

@implementation LeftMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
