//
//  CreditsToSuscriptionViewController.m
//  LiturGuia
//
//  Created by Krloz on 04/02/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import "CreditsToSuscriptionViewController.h"
#import "NSDate+ServerDate.h"

@interface CreditsToSuscriptionViewController (){
    CGRect originalLogoFrame;
    int viewCount;
}

@end

@implementation CreditsToSuscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PFUser *user = [PFUser currentUser];
    NSDate *expirationDate = [NSDate serverDate];
    NSNumber *credits = user[@"creditos"];
    int weeks = [credits intValue] / 2;
    expirationDate = [expirationDate dateByAddingTimeInterval:24*60*60*weeks*7];
    user[@"fVenceSuscripcion"] = expirationDate;
    [user removeObjectForKey:@"creditos"];
    [user saveInBackground];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:expirationDate];
    
    _text3TextArea.text = [_text3TextArea.text stringByReplacingOccurrencesOfString:@"{semanas}" withString:[NSString stringWithFormat:@"%d", weeks]];

    _text3TextArea.text = [_text3TextArea.text stringByReplacingOccurrencesOfString:@"{creditos}" withString:[credits stringValue]];
    _text3TextArea.text = [_text3TextArea.text stringByReplacingOccurrencesOfString:@"{fecha}" withString:stringFromDate];
    

    
    NSRange rangeBoldWeeks = [_text3TextArea.text rangeOfString:[NSString stringWithFormat:@"%d semanas", weeks]];
    NSRange rangeBoldCredits = [_text3TextArea.text rangeOfString:[NSString stringWithFormat:@"%@ créditos", [credits stringValue]]];
    NSRange rangeBoldDate = [_text3TextArea.text rangeOfString:stringFromDate];
    
    UIFont *fontTextBold = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    UIFont *fontText = [UIFont fontWithName:@"HelveticaNeue" size:14];
    NSDictionary *dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontTextBold, NSFontAttributeName, nil];
    NSDictionary *dictRegularText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    
    NSMutableAttributedString *mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:_text3TextArea.text];
    [mutAttrTextViewString setAttributes:dictRegularText range:NSMakeRange(0, _text3TextArea.text.length - 1)];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBoldWeeks];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBoldCredits];
    [mutAttrTextViewString setAttributes:dictBoldText range:rangeBoldDate];
    
    [_text3TextArea setAttributedText:mutAttrTextViewString];
    _text3TextArea.textAlignment =  NSTextAlignmentJustified;
    
    _text1Label.alpha = 0;
    _text2TextArea.alpha = 0;
    _text3TextArea.alpha = 0;
    _okButton.alpha = 0;
    _logoImageView.hidden = YES;

}


- (void) didFinishAutoLayout {
    if (viewCount != 1) {
        viewCount++;
        return;
    }
    originalLogoFrame = _logoImageView.frame;
    
    _logoImageView.center = self.view.center;
   
    _logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    _logoImageView.hidden = NO;
    
        [UIView animateWithDuration:0.5/1.5 animations:^{
            self->_logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            
        } completion:^(BOOL finished) {
            if (finished) {
                [UIView animateWithDuration:0.5/2 animations:^{
                    self->_logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    if (finished) {
                        [UIView animateWithDuration:0.5/2 animations:^{
                            self->_logoImageView.transform = CGAffineTransformIdentity;
                        } completion:^(BOOL finished) {
                            if (finished) {
                                [UIView animateWithDuration:1.0 animations:^{
                                    self->_logoImageView.frame = self->originalLogoFrame;
                                    [UIView animateWithDuration:1.0 delay:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                                        self->_text1Label.alpha = 1.0;
                                        self->_text2TextArea.alpha = 1.0;
                                        self->_text3TextArea.alpha = 1.0;
                                        self->_okButton.alpha = 1.0;
                                    } completion:nil];
                                    
                                } completion:^(BOOL finished) {
                                }];
                            }
//
                            
                        }];
                    }
                    
                }];
            }
            
        }];
    

//    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        
//    } completion:nil];
//    [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        
//    } completion:nil];
//    [UIView animateWithDuration:1.0 delay:1.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        _text3TextArea.alpha = 1.0;
//    } completion:nil];
}


- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(didFinishAutoLayout)
                                               object:nil];
    [self performSelector:@selector(didFinishAutoLayout) withObject:nil
               afterDelay:0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)okButtonTapped:(id)sender {
    viewCount = 100;
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
