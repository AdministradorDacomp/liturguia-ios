//
//  CreditsToSuscriptionViewController.h
//  LiturGuia
//
//  Created by Krloz on 04/02/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditsToSuscriptionViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *text1Label;
@property (strong, nonatomic) IBOutlet UITextView *text2TextArea;
@property (strong, nonatomic) IBOutlet UITextView *text3TextArea;
@property (strong, nonatomic) IBOutlet UIButton *okButton;

@end
