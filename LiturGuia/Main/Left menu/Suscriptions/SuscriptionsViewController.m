//
//  SuscriptionsViewController.m
//  LiturGuia
//
//  Created by Krloz on 03/02/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import "SuscriptionsViewController.h"
#import "DownloadsBibleTableViewCell.h"
#import "IAPSubscriptions.h"
#import "NSString+Contains.h"
#import "NSDate+ServerDate.h"
#import "NSDate+Utils.h"
#import <FirebaseCore/FirebaseCore.h>
#import <MaryPopin/UIViewController+MaryPopin.h>

@interface SuscriptionsViewController (){
    NSArray *datasourceArray;
}

@end

@implementation SuscriptionsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *productsArray = @[[kMonthSuscription lowercaseString],
                               [k6MonthSuscription lowercaseString],
                               [k12MonthSuscription lowercaseString]];
    
    //    [KVNProgress showWithStatus:@"Cargando suscripciones..."];
    //    self.view.window.userInteractionEnabled = NO;
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[IAPSubscriptions sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            //            [KVNProgress dismiss];
            NSMutableArray *array = [NSMutableArray array];
            for (SKProduct *product in products) {
                if ([productsArray containsObject:[product.productIdentifier lowercaseString]] ) {
                    [array addObject:product];
                }
            }
            [array sortUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES], nil]];
            self->datasourceArray = array;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
            
        }else{
            //            [KVNProgress showErrorWithStatus:@"No fue posible cargar las suscripciones."];
        }
    }];
    
    [self updateCreditsAmount];
    [self setToModalView];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 60.0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(packPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchaseFailed:) name:IAPHelperProductPurchaseFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fakePurchase) name:IAPHelperProductPurchaseFakeNotification object:nil];
    
    
    //        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    //        [tracker set:kGAIScreenName value:@"Suscripciones"];
    //        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self.view setUserInteractionEnabled:YES];
}

-(void)setToModalView{
    //    if (self.navigationController != nil) {
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
    button.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = button;
    //    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)termsButtonTapped:(id)sender {
    UIViewController *popUpVC = [self.storyboard instantiateViewControllerWithIdentifier: @"SuscriptionsTermsViewController"];
    
    popUpVC.view.frame = CGRectMake(0, 0,  UIScreen.mainScreen.bounds.size.width * 310, 450);
    [popUpVC.view sizeToFit];
    popUpVC.view.layer.cornerRadius = 5.0;
    popUpVC.view.clipsToBounds = YES;
    
    [popUpVC setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
    [popUpVC setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    BKTBlurParameters *blurParameters = [[BKTBlurParameters alloc] init];
    blurParameters.alpha = 1.0;
    blurParameters.radius = 8;
    blurParameters.saturationDeltaFactor = 0.9;
    blurParameters.tintColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha: 0.6];
    [popUpVC setBlurParameters: blurParameters];
    [popUpVC setPopinOptions: BKTPopinBlurryDimmingView];
    [self.navigationController.interactivePopGestureRecognizer setEnabled:NO];
    [self.navigationController presentPopinController:popUpVC animated:YES completion:nil];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifierDownload = @"DownloadCell";
    
    DownloadsBibleTableViewCell *cell;
    
    SKProduct *product = datasourceArray[indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierDownload];
    
    //    NSLocale *locale = product.priceLocale;
    //    NSString *currency = [locale objectForKey:NSLocaleCurrencyCode];
    
    if (product.localizedTitle){
        cell.descriptionLabel.text = product.localizedTitle;
    }
    
    NSString *priceString = [NSString stringWithFormat:@"$%.2f",[product.price floatValue]];
    cell.purchaseButton.tag = indexPath.row;
    [cell.purchaseButton setTitle:priceString forState:UIControlStateNormal];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (IBAction)purchaseButtonTapped:(id)sender {
    [KVNProgress showWithStatus:@"Procesando pago..."];
    
    SKProduct *product = datasourceArray[[sender tag]];
    [[IAPSubscriptions sharedInstance] buyProduct:product];
}

- (void)packPurchased:(NSNotification *)notification {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSString * productIdentifier = notification.object;
        
        [self->datasourceArray enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
            NSString *productId = product.productIdentifier;
            if ([productId isEqualToString:productIdentifier]) {
                NSLocale *locale = product.priceLocale;
                
                PFUser *user = [PFUser currentUser];
                
                NSNumber *months;
                if ([productIdentifier isEqualToString:kMonthSuscription]) {
                    months = @1;
                }else if ([productIdentifier isEqualToString:k6MonthSuscription]) {
                    months = @6;
                }else if ([productIdentifier isEqualToString:k12MonthSuscription]) {
                    months = @12;
                }
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *startDate;
                if ([user[@"fVenceSuscripcion"] isLaterThan:[NSDate serverDate]]) {
                    startDate = user[@"fVenceSuscripcion"];
                }else{
                    startDate = [NSDate serverDate];
                }
                
                NSDate *expirationDate = [calendar dateByAddingUnit:NSCalendarUnitMonth value:[months intValue] toDate:startDate options:0];
                
                user[@"fVenceSuscripcion"] = expirationDate;
                
                
                PFObject *paymentObject = [PFObject objectWithClassName:@"Pago"];
                paymentObject[@"moneda"] = [locale objectForKey:NSLocaleCurrencyCode];
                paymentObject[@"monto"] = product.price;
                paymentObject[@"usuario"] = user;
                if (product.localizedTitle) {
                    paymentObject[@"concepto"] = product.localizedTitle;
                }
                
                paymentObject[@"productIdApple"] = product.productIdentifier;
                
                [user saveInBackground];
                [paymentObject saveInBackground];
                
                
                
                [self updateCreditsAmount];
                
                *stop = YES;
                
                [KVNProgress showSuccess];
            }
        }];
    });
    
    
}

- (void)productPurchaseFailed:(NSNotification *)notification {
    
    
    if (notification.object) {
        NSString *messageErr = notification.object;
        [KVNProgress showErrorWithStatus:messageErr];
        //        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
        //                                                     message:messageErr
        //                                                    delegate:nil
        //                                           cancelButtonTitle:@"OK"
        //                                           otherButtonTitles:nil];
        //        [av show];
    }else{
        [KVNProgress showError];
    }
}

-(void)updateCreditsAmount{
    PFUser *user = [PFUser currentUser];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:user[@"fVenceSuscripcion"]];
    _expirationDateLabel.text = [NSString stringWithFormat:@"Suscripción vigente hasta: %@", stringFromDate];
}

-(void)fakePurchase{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSString *messageErr = @"Ha ocurrido un error al verificar el pago.";
        //        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
        //                                                     message:messageErr
        //                                                    delegate:nil
        //                                           cancelButtonTitle:@"OK"
        //                                           otherButtonTitles:nil];
        //        [av show];
        [KVNProgress showErrorWithStatus:messageErr];
        
    });
    
    
}

@end
