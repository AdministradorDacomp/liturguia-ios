//
//  SuscriptionsViewController.h
//  LiturGuia
//
//  Created by Krloz on 03/02/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuscriptionsViewController : UIViewController<UITableViewDataSource, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *expirationDateLabel;

- (void)setToModalView;
- (IBAction)termsButtonTapped:(id)sender;

@end
