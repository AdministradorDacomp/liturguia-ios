//
//  LeftMenuViewController.m
//  LiturGuia
//
//  Created by Krloz on 14/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "LeftMenuTableViewCell.h"
#import "UIImageView+AFNetworking.h"
//#import <Parse/PFFacebookUtils.h>
#import "NSDate+ServerDate.h"
#import "NSDate+Utils.h"
#import "UIViewController+RESideMenu.h"

@interface LeftMenuViewController (){
    NSMutableArray *titles;
    NSMutableArray *images;
}

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titles = [NSMutableArray arrayWithArray:
              @[
                @"Suscripciones",
                @"Tutoriales de ayuda",
                @"Misiones",
                @"Imagen del día",
                @"Descargas",
                @"Contacto"
                ]
              ];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    

    
    images = [NSMutableArray arrayWithArray:
              @[
                [UIImage imageNamed:@"suscribe_icon"],
                [UIImage imageNamed:@"help_menu_icon"],
                [UIImage imageNamed:@"missions_icon"],
                [UIImage imageNamed:@"pictures_menu_icon"],
                [UIImage imageNamed:@"download_icon"],
                [UIImage imageNamed:@"contact_icon"]
                ]
              ];
   
//    if ([[NSDate date] isEarlierThan:[formatter dateFromString:@"21/03/2016"]]) {

//    }
    
    _userPictureImageView.layer.cornerRadius = _userPictureImageView.frame.size.height/2;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUser) name:@"LoginComplete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUser) name:@"UserInfoUpdated" object:nil];

    [self updateUser];
}

-(void)updateUser{
    PFUser *user = [PFUser currentUser];
    if (user) {
        [user fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (user[@"apellido"] && user[@"apellido"]  != [NSNull null]) {
                self->_nameLabel.text = [NSString stringWithFormat:@"%@ %@",user[@"nombre"], user[@"apellido"]];
            }else {
                self->_nameLabel.text = user[@"nombre"];
            }
            
        }];
        
        NSURL *url = [LiturGuia profilePhotoURL];
        [_userPictureImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_photo_placeholder"]];
        
        PFFileObject *parseFile = user[@"coverPhoto"];
        if (![parseFile isEqual:[NSNull null]]) {
            url = [NSURL URLWithString:parseFile.url];
        }
        
        
        [_coverPhotoImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cover_photo_placeholder"]];
        
        if (user[@"creditos"] && user[@"creditos"] != [NSNull null]) {
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreditsToSuscriptionViewController"];
                vc.modalTransitionStyle   = UIModalTransitionStyleFlipHorizontal;
                vc.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            });
            
        }
    }else{
        [_userPictureImageView setImage:[UIImage imageNamed:@"logo_liturguia"]];
        _nameLabel.text = NSLocalizedString(@"LiturGuía", nil);
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)settingsButtonTapped:(id)sender {
    if ([PFUser currentUser]) {
        [self performSegueWithIdentifier:@"SettingsSegue" sender:nil];
    }else{
        [self restrictedAccess];
    }
}

#pragma mark - UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 47;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"LeftMenuCell";
    LeftMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.titleLabel.text = titles [indexPath.row];
    cell.iconImageView.image = images [indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PFUser *user = [PFUser currentUser];
    
    if (indexPath.row == 0) {
        if (user) {
            [self performSegueWithIdentifier:@"SuscriptionsSegue" sender:nil];
        }else{
            [self restrictedAccess];
        }
        
    }else if (indexPath.row == 1) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Tutorial" bundle:nil];
        
        
        UIViewController *vc = [sb instantiateInitialViewController];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:YES completion:nil];
        

    }else if (indexPath.row == 2) {
        [self performSegueWithIdentifier:@"MissionResourcesSegue" sender:nil];
    }else if (indexPath.row == 3) {
        
        UIViewController *presentationVC = [UIApplication sharedApplication].keyWindow.rootViewController;
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DailyImageViewController"];
        presentationVC.definesPresentationContext = YES;
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [presentationVC presentViewController:vc animated:YES completion:nil];
    }else if (indexPath.row == 4) {
         [self performSegueWithIdentifier:@"DownloadsSegue" sender:nil];
    }else if (indexPath.row == 5) {
        if (user) {
            [self performSegueWithIdentifier:@"ContactSegue" sender:nil];
        }else{
            [self restrictedAccess];
        }
       
    }
}

-(void)restrictedAccess{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    nc.modalPresentationStyle = UIModalPresentationFullScreen;
    [[self topViewController] presentViewController:nc animated:YES completion:^{
        
    }];
    
    NSString *messageErr = @"Para acceder a esta sección es necesario registrarse de forma gratuita.";
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:messageErr
                                                delegate:[nc viewControllers][0]
                                       cancelButtonTitle:nil
                                       otherButtonTitles:@"Aceptar", nil];
    av.tag = 1;
    [av show];
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

@end
