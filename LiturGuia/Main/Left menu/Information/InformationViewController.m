//
//  InformationViewController.m
//  LiturGuia
//
//  Created by Krloz on 07/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "InformationViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];

    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];

    NSString * versionBuildString = [NSString stringWithFormat:@"%@ (%@)", appVersionString, appBuildString];

    _versionLabel.text = [NSString stringWithFormat:_versionLabel.text, versionBuildString];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Informacion"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions
- (IBAction)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)catholicNetButtonTapped:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.es.catholic.net/"];
    [[UIApplication sharedApplication] openURL:url];
}
- (IBAction)missionsButtonTapped:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.demisiones.org/"];
    [[UIApplication sharedApplication] openURL:url];
}

@end
