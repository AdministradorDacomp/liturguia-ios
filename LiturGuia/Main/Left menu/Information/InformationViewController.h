//
//  InformationViewController.h
//  LiturGuia
//
//  Created by Krloz on 07/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;

@end
