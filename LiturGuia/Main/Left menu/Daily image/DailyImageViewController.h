//
//  DailyImageViewController.h
//  LiturGuia
//
//  Created by Krloz on 11/02/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DailyImageViewController : UIViewController<UIDocumentInteractionControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) IBOutlet UILabel *noResultsLabel;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIImageView *shareImageView;

@property (retain) UIDocumentInteractionController * documentInteractionController;

@end
