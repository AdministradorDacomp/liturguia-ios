//
//  DailyImageViewController.m
//  LiturGuia
//
//  Created by Krloz on 11/02/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import "DailyImageViewController.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+Utils.h"

@interface DailyImageViewController ()

@end

@implementation DailyImageViewController

@synthesize objectId;

- (void)viewDidLoad {
    [super viewDidLoad];
    _noResultsLabel.hidden = YES;
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.view.bounds;
    [self.view insertSubview:visualEffectView belowSubview:_imageView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Imagen diaria"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Imagen"];
    NSDate *date = [NSDate removeTimeFromDate:[NSDate date]];
    if (objectId) {
        [query whereKey:@"objectId" equalTo:objectId];
    }else{
        [query whereKey:@"fecha" equalTo:date];
    }
    
    
    [KVNProgress showWithStatus:@"Descargando imagen..."];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            self.title = object[@"name"];
            PFFileObject *file = object[@"image"];
            
            NSURL *url = [NSURL URLWithString:file.url];
            __weak UIImageView *weakImageView = self.imageView;
            __weak typeof(self) weakSelf = self;
            NSURLRequest *urlReq = [[NSURLRequest alloc] initWithURL:url];
            [self->_imageView setImageWithURLRequest:urlReq placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                weakImageView.image = image;
                CATransition *transition = [CATransition animation];
                transition.duration = 0.7f;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionFade;
                
                [weakImageView.layer addAnimation:transition forKey:nil];
                
                [KVNProgress dismiss];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                [KVNProgress dismiss];
                weakSelf.noResultsLabel.hidden = NO;
                weakSelf.shareButton.hidden = YES;
                weakSelf.shareImageView.hidden = YES;
            }];
        }else{
            [KVNProgress dismiss];
            self->_noResultsLabel.hidden = NO;
            self->_shareButton.hidden = YES;
            self->_shareImageView.hidden = YES;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)doneButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareButtonTapped:(id)sender {
    [KVNProgress show];
//    self.view.window.userInteractionEnabled = NO;
//    [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:@[self->_imageView.image] applicationActivities:nil];
        
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        activityVC.excludedActivityTypes = excludeActivities;
        
        
        [self presentViewController:activityVC animated:YES completion:^{
            [KVNProgress dismiss];
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
//            self.view.window.userInteractionEnabled = YES;
        }];
    });
}

@end
