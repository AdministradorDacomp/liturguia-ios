//
//  LanguagesViewController.h
//  LiturGuia
//
//  Created by Krloz on 14/12/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguagesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
