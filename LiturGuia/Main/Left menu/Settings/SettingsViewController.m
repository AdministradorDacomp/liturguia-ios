//
//  SettingsViewController.m
//  LiturGuia
//
//  Created by Krloz on 04/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "SettingsViewController.h"
#import "SectionsViewController.h"
//#import <Parse/PFFacebookUtils.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"

@interface SettingsViewController (){
    UITextField *activeField;
    NSString *actualText;
    UIImagePickerController *profilePhotoImagePickerController;
    UIImagePickerController *coverPhotoImagePickerController;
}

@end

@implementation SettingsViewController

@synthesize scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    
    PFUser *user = [PFUser currentUser];
    /*if ([PFFacebookUtils isLinkedWithUser:user]) {
        _facebookStatusLabel.text = NSLocalizedString(@"Conectado", nil);
    }else{
        _facebookStatusLabel.text = NSLocalizedString(@"No conectado", nil);
    }
    */
    
    NSURL *url = [LiturGuia profilePhotoURL];
    [_profilePictureImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_photo_placeholder"]];
    
    PFFileObject *parseFile = user[@"coverPhoto"];
    if (![parseFile isEqual:[NSNull null]]) {
        url = [NSURL URLWithString:parseFile.url];
    }

    
    [_coverPhotoImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"cover_photo_placeholder"]];
    
    _nameTextField.text = user[@"nombre"];
    _lastNameTextField.text = user[@"apellido"];
    _emailTextField.text = user[@"email"];

    
    [_dailyImageSwitch setOn:!user[@"notificacionImagen"] ||[user[@"notificacionImagen"] boolValue] animated:NO] ;
    
    if (user[@"bloqueoAutomatico"] == [NSNull null]) {
        [PFUser currentUser][@"bloqueoAutomatico"] = @NO;
        [[PFUser currentUser] saveInBackground];
        
    }
    
    [_screenLockSwitch setOn:[user[@"bloqueoAutomatico"] boolValue] animated:NO] ;
    [[UIApplication sharedApplication] setIdleTimerDisabled: ![user[@"bloqueoAutomatico"] boolValue]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *languageCode = [userDefault objectForKey:@"language"];
    
    if ([languageCode isEqualToString:@"es"]) {
        _languageLabel.text = NSLocalizedString(@"Español", nil);
    }else if ([languageCode isEqualToString:@"en"]) {
        _languageLabel.text = NSLocalizedString(@"English", nil);
    }else if ([languageCode isEqualToString:@"pt"]) {
        _languageLabel.text = NSLocalizedString(@"Português", nil);
    }else if ([languageCode isEqualToString:@"it"]) {
        _languageLabel.text = NSLocalizedString(@"Italiano", nil);
    }

        [super viewWillAppear:animated];
        
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Configuracion"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

- (IBAction)doneButtonTapped:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)logoutButtonTapped:(id)sender {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.view.userInteractionEnabled = NO;
    [KVNProgress showWithStatus:@"Cerrando sesión..."];
    [PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
        PFInstallation *installation = [PFInstallation currentInstallation];
        [installation removeObjectForKey:@"user"];
        [installation saveInBackground];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
            UIViewController *vc = [sb instantiateInitialViewController];
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
            AppDelegate *d = appDelegate;
            [d.sectionsViewController.navigationController presentViewController:vc animated:YES completion:^{
                
            }];
            NSFetchRequest * allMissals = [[NSFetchRequest alloc] init];
            [allMissals setEntity:[NSEntityDescription entityForName:@"MissalEntity" inManagedObjectContext:[LiturGuia moc]]];
            [allMissals setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            
            NSError * error = nil;
            NSArray * missals = [[LiturGuia moc] executeFetchRequest:allMissals error:&error];
            //error handling goes here
            for (NSManagedObject * missal in missals) {
                [[LiturGuia moc] deleteObject:missal];
            }
            
            NSFetchRequest * allLectioDivine = [[NSFetchRequest alloc] init];
            [allLectioDivine setEntity:[NSEntityDescription entityForName:@"LectioDivineEntity" inManagedObjectContext:[LiturGuia moc]]];
            [allLectioDivine setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            

            NSArray * lectioDivines = [[LiturGuia moc] executeFetchRequest:allLectioDivine error:&error];
            //error handling goes here
            for (NSManagedObject * lectioDivine in lectioDivines) {
                [[LiturGuia moc] deleteObject:lectioDivine];
            }
            
            NSFetchRequest * allPrayers = [[NSFetchRequest alloc] init];
            [allPrayers setEntity:[NSEntityDescription entityForName:@"PrayerEntity" inManagedObjectContext:[LiturGuia moc]]];
            [allPrayers setIncludesPropertyValues:NO]; //only fetch the managedObjectID
            
            
            NSArray * prayers = [[LiturGuia moc] executeFetchRequest:allPrayers error:&error];
            //error handling goes here
            for (NSManagedObject * prayer in prayers) {
                [[LiturGuia moc] deleteObject:prayer];
            }
            
            [LiturGuia saveContext];
            [KVNProgress dismiss];
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            self.view.userInteractionEnabled = YES;
        }];

    }];
}

- (IBAction)facebookButtonTapped:(id)sender {
    PFUser *user = [PFUser currentUser];
    /*
    if ([PFFacebookUtils isLinkedWithUser:user]) {
//        NSString *messageErr = @"¿Desea desconectarse de Facebook?";
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                     message:messageErr
//                                                    delegate:self
//                                           cancelButtonTitle:@"No"
//                                           otherButtonTitles:@"Si", nil];
//        UIAlertController *ac =
//        av.tag = 1000;
//
//
//        [av show];
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:NSLocalizedString(@"¿Desea desconectarse de Facebook?", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        UIAlertAction* yesAction = [UIAlertAction actionWithTitle:
                                                           NSLocalizedString(@"Sí", nil) style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action) {
              [KVNProgress showWithStatus:@"Espere..."];
              
              PFUser *user = [PFUser currentUser];
              
              [PFFacebookUtils unlinkUserInBackground:user block:^(BOOL succeeded, NSError *error) {
                  //                self.view.window.userInteractionEnabled = YES;
                  //                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
                  
                  if (!error) {
                      [KVNProgress dismiss];
                      self->_facebookStatusLabel.text = NSLocalizedString(@"No conectado", nil);
                      [user removeObjectForKey:@"facebookId"];
                      [user saveInBackground];
                      
                      PFFileObject *parseFile = user[@"profilePhoto"];
                      NSURL *profilePhotoUrl = [NSURL URLWithString:parseFile.url];
                      [self->_profilePictureImageView setImageWithURL:profilePhotoUrl placeholderImage:[UIImage imageNamed:@"user_photo_placeholder"]];
                      
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
                  }else{
                      NSString *errorString = [error userInfo][@"error"];
                      //                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                      //                                                                 message:errorString
                      //                                                                delegate:nil
                      //                                                       cancelButtonTitle:@"OK"
                      //                                                       otherButtonTitles:nil];
                      //                    [av show];
                      [KVNProgress showErrorWithStatus:errorString];
                  }
              }];
          }];
        
        [alert addAction:defaultAction];
        [alert addAction:yesAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
//        self.view.window.userInteractionEnabled = NO;
//        [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
        
        [KVNProgress showWithStatus:@"Conectando con Facebook..."];
        
        [PFFacebookUtils linkUserInBackground:user withPublishPermissions:@[@"publish_actions"] block:^(BOOL succeeded, NSError *error) {
//            self.view.window.userInteractionEnabled = YES;
//            [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
            if (!error) {
                [KVNProgress showSuccess];
                self->_facebookStatusLabel.text = NSLocalizedString(@"Conectado", nil);
                
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (!error) {
                        NSDictionary *userData = (NSDictionary *)result;
                        
                        NSString *facebookID = userData[@"id"];
                        user[@"facebookId"] = facebookID;
                        
                        NSURL *url = [LiturGuia profilePhotoURL];
                        [self->_profilePictureImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_photo_placeholder"]];
                        [user saveInBackground];
                    }
                }];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
            }else{
                [KVNProgress showErrorWithStatus:[error userInfo][@"error"]];
//                NSString *errorString = [error userInfo][@"error"];
//                UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                             message:errorString
//                                                            delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil];
//                [av show];
            }
        }];
    }
     */
}

- (IBAction)coverPhotoButtonTapped:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Tomar desde camara", @"Seleccionar de libreria",@"Quitar fotografía", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
    actionSheet.destructiveButtonIndex = 2;
    [actionSheet showInView:self.view];
    actionSheet.tag = 1000;
}

- (IBAction)profilePhotoButtonTapped:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Tomar desde camara", @"Seleccionar de libreria",@"Quitar fotografía", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
    actionSheet.destructiveButtonIndex = 2;
    [actionSheet showInView:self.view];
    actionSheet.tag = 1001;
}

- (IBAction)dailySwitchChanged:(id)sender {
    PFUser *user = [PFUser currentUser];
    user[@"notificacionImagen"] = @(_dailyImageSwitch.isOn);

    [user saveInBackground];
}


- (IBAction)screenLockSwitchChanged:(id)sender {
    PFUser *user = [PFUser currentUser];
    user[@"bloqueoAutomatico"] = @(_screenLockSwitch.isOn);
    
    [user saveInBackground];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    actualText = textField.text;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self textFieldDidEndEditing:textField];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (actualText != textField.text) {
        if ([textField.text isEqual:@""]) {
            textField.text = actualText;
            return;
        }
        PFUser *user = [PFUser currentUser];
        if (textField == _nameTextField) {
            user[@"nombre"] = textField.text;
        }else if (textField == _lastNameTextField) {
            user[@"apellido"] = textField.text;
        }else if (textField == _passwordTextField) {
            NSString *messageErr = @"Confirme el cambio de contraseña";
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                         message:messageErr
                                                        delegate:self
                                               cancelButtonTitle:@"Cancelar"
                                               otherButtonTitles:@"Cambiar",nil];
            av.tag = 1001;
            [av show];
        }
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
        }];
    }
    [textField resignFirstResponder];
    activeField = nil;
}

#pragma mark - Helpers



#pragma mark - Show keyboard

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+50, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.cancelButtonIndex != buttonIndex) {
        if (alertView.tag == 1000) {
            /*
//            self.view.window.userInteractionEnabled = NO;
//            [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
            [KVNProgress showWithStatus:@"Espere..."];
            
            PFUser *user = [PFUser currentUser];
            
            [PFFacebookUtils unlinkUserInBackground:user block:^(BOOL succeeded, NSError *error) {
//                self.view.window.userInteractionEnabled = YES;
//                [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
                
                if (!error) {
                    [KVNProgress dismiss];
                    self->_facebookStatusLabel.text = NSLocalizedString(@"No conectado", nil);
                    [user removeObjectForKey:@"facebookId"];
                    [user saveInBackground];
                    
                    PFFileObject *parseFile = user[@"profilePhoto"];
                    NSURL *profilePhotoUrl = [NSURL URLWithString:parseFile.url];
                    [self->_profilePictureImageView setImageWithURL:profilePhotoUrl placeholderImage:[UIImage imageNamed:@"user_photo_placeholder"]];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
                }else{
                    NSString *errorString = [error userInfo][@"error"];
//                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                                 message:errorString
//                                                                delegate:nil
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil];
//                    [av show];
                    [KVNProgress showErrorWithStatus:errorString];
                }
            }];
             */
        }else if(alertView.tag == 1001) {
            PFUser *user = [PFUser currentUser];
            user[@"password"] = _passwordTextField.text;
            [user saveInBackground];
        }
        
    }
}

#pragma mark - UIImagePickerControllerDelegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    NSData *imageData = UIImagePNGRepresentation(chosenImage);
    PFFileObject *imageFile = [PFFileObject fileObjectWithName:@"image.png" data:imageData];
    
    
    [KVNProgress showProgress:0.0 status:@"Subiendo imagen..."];
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//    hud.labelText = @"Subiendo imagen...";
    
    
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        [hud hide:YES];
        if (!error) {
            PFUser *user = [PFUser currentUser];
            if (picker == self->profilePhotoImagePickerController) {
                user[@"profilePhoto"] = imageFile;
                self->_profilePictureImageView.image = chosenImage;
            }else if (picker == self->coverPhotoImagePickerController){
                user[@"coverPhoto"] = imageFile;
                self->_coverPhotoImageView.image = chosenImage;
            }
            
            [user saveInBackground];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
        }else{
            [KVNProgress showErrorWithStatus:error.localizedDescription];
        }
    } progressBlock:^(int percentDone) {
        [KVNProgress showProgress:percentDone/100.0 status:@"Subiendo imagen..."];
//        hud.progress = ;
    }];

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        if (actionSheet.destructiveButtonIndex == buttonIndex) {
            PFUser *user = [PFUser currentUser];
            
            if (actionSheet.tag == 1000) {
                [user removeObjectForKey: @"coverPhoto"];
                _coverPhotoImageView.image = [UIImage imageNamed:@"cover_photo_placeholder"];
            }else if(actionSheet.tag == 1001){
                [user removeObjectForKey: @"profilePhoto"];
                NSURL *url = [LiturGuia profilePhotoURL];
                [_profilePictureImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_photo_placeholder"]];
            }
            [user saveInBackground];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UserInfoUpdated" object:nil];
        }else{
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            if (actionSheet.tag == 1000) {
                coverPhotoImagePickerController = picker;
            }else if(actionSheet.tag == 1001){
                profilePhotoImagePickerController = picker;
            }
            picker.delegate = self;
            picker.allowsEditing = YES;
            
            if (buttonIndex == 0){
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    
                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                          message:
                                                           NSLocalizedString(@"Este dispositivo no cuenta con cámara", nil)              delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles: nil];
                    
                    [myAlertView show];
                    return;
                }
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            }else if (buttonIndex == 1){
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
            
            [KVNProgress show];
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self presentViewController:picker animated:YES completion:^{
                    [KVNProgress dismiss];
//                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                }];
            });
        }
    }
}



@end
