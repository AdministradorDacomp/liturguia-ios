//
//  LanguagesViewController.m
//  LiturGuia
//
//  Created by Krloz on 14/12/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "LanguagesViewController.h"

@interface LanguagesViewController (){
    NSMutableArray *languagesArray;
    NSString *currentLanguage;
    NSUserDefaults *userDefaults;
}

@end

@implementation LanguagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    languagesArray = [NSMutableArray array];
    [languagesArray addObject:@{@"name":@"Español",
                                @"code":@"es"}];
    [languagesArray addObject:@{@"name":@"English",
                                @"code":@"en"}];
    [languagesArray addObject:@{@"name":@"Português",
                                @"code":@"pt"}];
    [languagesArray addObject:@{@"name":@"Italiano",
                                @"code":@"it"}];
    
     userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return languagesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LanguageCell" forIndexPath:indexPath];
    NSDictionary *dic = languagesArray[indexPath.row];
    
    cell.textLabel.text = dic[@"name"];
//    cell.detailTextLabel.text = dic[@"code"];
    
    if ([[userDefaults objectForKey:@"language"] isEqualToString:dic[@"code"]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = languagesArray[indexPath.row];
    NSString *languageCode = dic[@"code"];
//    if ([languageCode isEqualToString:@"en"]) {
//        NSString *messageErr = @"Lo sentimos, el contenido en inglés sigue en desarrollo, pero estará disponible en futuras versiones.";
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"¡Proximamente!"
//                                                     message:messageErr
//                                                    delegate:nil
//                                           cancelButtonTitle:@"OK"
//                                           otherButtonTitles:nil];
//        [av show];
//    }else if ([languageCode isEqualToString:@"pt"]) {
//        NSString *messageErr = @"Lo sentimos, el contenido en portugués sigue en desarrollo, pero estará disponible en futuras versiones.";
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"¡Proximamente!"
//                                                     message:messageErr
//                                                    delegate:nil
//                                           cancelButtonTitle:@"OK"
//                                           otherButtonTitles:nil];
//        [av show];
//    }else if ([languageCode isEqualToString:@"ir"]) {
//        NSString *messageErr = @"Lo sentimos, el contenido en italiano sigue en desarrollo, pero estará disponible en futuras versiones.";
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"¡Proximamente!"
//                                                     message:messageErr
//                                                    delegate:nil
//                                           cancelButtonTitle:@"OK"
//                                           otherButtonTitles:nil];
//        [av show];
//    }else{
        [userDefaults setObject:languageCode forKey:@"language"];
        [userDefaults synchronize];
        
        [tableView reloadData];
//    }
}

@end
