//
//  LiturGuia.m
//  LiturGuia
//
//  Created by Krloz on 13/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

//#import <Parse/PFFacebookUtils.h>
#import <CoreData/CoreData.h>
#import <Parse/PFTwitterUtils.h>
#import "AppDelegate.h"


@implementation LiturGuia

//+ (NSString*) createUUID{
//    NSString *thisDeviceID;
////    NSString *systemVersion = [[UIDevice currentDevice]systemVersion];
////    NSString *model = [[UIDevice currentDevice]model];
////    NSString *retinaTag = @"Retina";
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id uuid = [defaults objectForKey:@"deviceID"];
//    if (uuid)
//        thisDeviceID = (NSString *)uuid;
//    else {
//        if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
//            thisDeviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//        }
//        else
//        {
//            CFStringRef cfUuid = CFUUIDCreateString(NULL, CFUUIDCreate(NULL));
//            thisDeviceID = (__bridge NSString *)cfUuid;
//            CFRelease(cfUuid);
//        }
//        [defaults setObject:thisDeviceID forKey:@"deviceID"];
//    }
//    NSLog(@"UUID = %@", thisDeviceID);
//    return thisDeviceID;
////    globalDeviceID = [NSString stringWithFormat:@"%@-%@-%@-%@",thisDeviceID,systemVersion,retinaTag,model];
////    NSLog(@"UUID with info = %@", globalDeviceID);
//}

+(NSString*) defaultDateFormat{
    return @"dd/MM/yyyy";
}

+ (NSString *) append:(NSString*) first, ...{
    NSMutableString *newContentString = [NSMutableString string];
    va_list args;
    va_start(args, first);
    for (NSString *arg = first; arg != nil; arg = va_arg(args, NSString*))
    {
        [newContentString appendString:arg];
    }
    va_end(args);
    
    return newContentString;
}

+ (BOOL)internetConnected{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

+ (NSDictionary*)currentUser{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   return (NSDictionary*)[defaults valueForKey:@"user"];
}

+ (BOOL)isEqualColor:(UIColor *)color to: (UIColor*)otherColor{
    CGColorSpaceRef colorSpaceRGB = CGColorSpaceCreateDeviceRGB();
    
    UIColor *(^convertColorToRGBSpace)(UIColor*) = ^(UIColor *color) {
        if(CGColorSpaceGetModel(CGColorGetColorSpace(color.CGColor)) == kCGColorSpaceModelMonochrome) {
            const CGFloat *oldComponents = CGColorGetComponents(color.CGColor);
            CGFloat components[4] = {oldComponents[0], oldComponents[0], oldComponents[0], oldComponents[1]};
            CGColorRef colorRef = CGColorCreate( colorSpaceRGB, components );
            
            UIColor *color = [UIColor colorWithCGColor:colorRef];
            CGColorRelease(colorRef);
            return color;
        } else
            return color;
    };
    
    UIColor *selfColor = convertColorToRGBSpace(color);
    otherColor = convertColorToRGBSpace(otherColor);
    CGColorSpaceRelease(colorSpaceRGB);
    
    return [selfColor isEqual:otherColor];
}

+(NSURL*)profilePhotoURL{
    PFUser *user = [PFUser currentUser];
    NSURL *profilePhotoUrl;
    if (user[@"profilePhoto"] && user[@"profilePhoto"] != [NSNull null]) {
        PFFileObject *parseFile = user[@"profilePhoto"];
        profilePhotoUrl = [NSURL URLWithString:parseFile.url];
        /*}else if ([PFFacebookUtils isLinkedWithUser:user]) {
        NSString *userImageURLString = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", user[@"facebookId"]];
        profilePhotoUrl = [NSURL URLWithString:userImageURLString];
         */
    }else if ([PFTwitterUtils isLinkedWithUser:user]) {
        NSString *userImageURLString = user[@"profilePhotoTwitter"];
        profilePhotoUrl = [NSURL URLWithString:userImageURLString];
    }else if (user[@"profilePhotoGoogle"] && user[@"profilePhotoGoogle"] != [NSNull null]) {
        NSString *userImageURLString = user[@"profilePhotoGoogle"];
        profilePhotoUrl = [NSURL URLWithString:userImageURLString];
    }
    
    return profilePhotoUrl;
}

+(NSManagedObjectContext*)moc{
    AppDelegate *d = appDelegate;
    return d.managedObjectContext;
}

+(void)saveContext{
    AppDelegate *d = appDelegate;
    return [d saveContext];
}

+(NSDate *)dateWithoutTime:(NSDate *)date
{
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:1]]; // I'm in Paris (+1)
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    
    comps.hour = 0;
    comps.minute = 0;
    comps.second = 0;
    
    return [cal dateFromComponents:comps];
}

+(NSDate*)dateByAddingMonths:(int)months toDate:(NSDate*)date{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:months];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:date options:0];
    return newDate;
}

+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}



@end
