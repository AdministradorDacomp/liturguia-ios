//
//  PrayerEntity.m
//  LiturGuia
//
//  Created by Krloz on 22/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "PrayerEntity.h"


@implementation PrayerEntity

@dynamic name;
@dynamic text;
@dynamic language;
@dynamic group;
@dynamic objectId;

@end
