//
//  RosaryEntity.m
//  LiturGuia
//
//  Created by Krloz on 08/07/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "RosaryEntity.h"


@implementation RosaryEntity

@dynamic title;
@dynamic subtitle;
@dynamic number;
@dynamic language;
@dynamic type;

@end
