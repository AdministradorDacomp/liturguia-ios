//
//  MissalEntity.h
//  LiturGuia
//
//  Created by Krloz on 07/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MissalEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MissalEntity+CoreDataProperties.h"
