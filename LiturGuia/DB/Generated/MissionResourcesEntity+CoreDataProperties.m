//
//  MissionResourcesEntity+CoreDataProperties.m
//  LiturGuia
//
//  Created by Krloz on 08/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MissionResourcesEntity+CoreDataProperties.h"

@implementation MissionResourcesEntity (CoreDataProperties)

@dynamic title;
@dynamic parseId;
@dynamic readerDocument;
@dynamic size;

@end
