//
//  CalendarEntity+CoreDataProperties.m
//  LiturGuia
//
//  Created by Krloz on 26/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CalendarEntity+CoreDataProperties.h"

@implementation CalendarEntity (CoreDataProperties)

@dynamic date;
@dynamic time;
@dynamic cicle;
@dynamic day;
@dynamic week;
@dynamic specialDate;

@end
