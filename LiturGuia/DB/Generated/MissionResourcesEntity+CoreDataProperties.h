//
//  MissionResourcesEntity+CoreDataProperties.h
//  LiturGuia
//
//  Created by Krloz on 08/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MissionResourcesEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MissionResourcesEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *size;
@property (nullable, nonatomic, retain) NSString *parseId;
@property (nullable, nonatomic, retain) ReaderDocument *readerDocument;

@end

NS_ASSUME_NONNULL_END
