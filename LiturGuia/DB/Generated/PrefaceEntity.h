//
//  PrefaceEntity.h
//  LiturGuia
//
//  Created by Krloz on 09/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PrefaceEntity : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * subtitle;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * shortText;

@end
