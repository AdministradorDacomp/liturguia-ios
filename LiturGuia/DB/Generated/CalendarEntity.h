//
//  CalendarEntity.h
//  LiturGuia
//
//  Created by Krloz on 26/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CalendarEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CalendarEntity+CoreDataProperties.h"
