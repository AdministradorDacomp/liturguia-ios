//
//  PrayerEntity.h
//  LiturGuia
//
//  Created by Krloz on 22/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PrayerEntity : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * group;
@property (nonatomic, retain) NSString * objectId;

@end
