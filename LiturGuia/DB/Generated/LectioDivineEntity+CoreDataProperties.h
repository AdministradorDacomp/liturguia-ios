//
//  LectioDivineEntity+CoreDataProperties.h
//  LiturGuia
//
//  Created by Krloz on 14/12/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LectioDivineEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface LectioDivineEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *cycle;
@property (nullable, nonatomic, retain) NSNumber *day;
@property (nullable, nonatomic, retain) NSString *language;
@property (nullable, nonatomic, retain) NSString *lectioDivineVersion;
@property (nullable, nonatomic, retain) NSString *objectId;
@property (nullable, nonatomic, retain) NSString *specialDate;
@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSString *time;
@property (nullable, nonatomic, retain) NSNumber *week;
@property (nullable, nonatomic, retain) NSNumber *even;

@end

NS_ASSUME_NONNULL_END
