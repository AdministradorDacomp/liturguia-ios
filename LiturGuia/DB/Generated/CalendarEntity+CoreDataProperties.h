//
//  CalendarEntity+CoreDataProperties.h
//  LiturGuia
//
//  Created by Krloz on 26/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CalendarEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CalendarEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *time;
@property (nullable, nonatomic, retain) NSString *cicle;
@property (nullable, nonatomic, retain) NSNumber *day;
@property (nullable, nonatomic, retain) NSNumber *week;
@property (nullable, nonatomic, retain) NSString *specialDate;

@end

NS_ASSUME_NONNULL_END
