//
//  MissalEntity+CoreDataProperties.m
//  LiturGuia
//
//  Created by Krloz on 07/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MissalEntity+CoreDataProperties.h"

@implementation MissalEntity (CoreDataProperties)

@dynamic color;
@dynamic cycle;
@dynamic date;
@dynamic jsonMissal;
@dynamic language;
@dynamic objectId;
@dynamic time;
@dynamic title;
@dynamic type;
@dynamic missalVersion;

@end
