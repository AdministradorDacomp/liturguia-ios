//
//  MissalEntity+CoreDataProperties.h
//  LiturGuia
//
//  Created by Krloz on 07/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MissalEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MissalEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *color;
@property (nullable, nonatomic, retain) NSString *cycle;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *jsonMissal;
@property (nullable, nonatomic, retain) NSString *language;
@property (nullable, nonatomic, retain) NSString *objectId;
@property (nullable, nonatomic, retain) NSString *time;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSString *missalVersion;

@end

NS_ASSUME_NONNULL_END
