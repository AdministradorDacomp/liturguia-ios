//
//  MissionResourcesEntity.h
//  LiturGuia
//
//  Created by Krloz on 08/03/16.
//  Copyright © 2016 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ReaderDocument;

NS_ASSUME_NONNULL_BEGIN

@interface MissionResourcesEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MissionResourcesEntity+CoreDataProperties.h"
