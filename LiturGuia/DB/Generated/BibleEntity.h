//
//  BibleEntity.h
//  LiturGuia
//
//  Created by Krloz on 11/05/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BibleEntity : NSManagedObject

@property (nonatomic, retain) NSString * bibleDescription;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSNumber * selected;

@end
