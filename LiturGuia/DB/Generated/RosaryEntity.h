//
//  RosaryEntity.h
//  LiturGuia
//
//  Created by Krloz on 08/07/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RosaryEntity : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * subtitle;
@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * type;

@end
