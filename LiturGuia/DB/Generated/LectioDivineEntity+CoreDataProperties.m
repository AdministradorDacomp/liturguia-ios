//
//  LectioDivineEntity+CoreDataProperties.m
//  LiturGuia
//
//  Created by Krloz on 14/12/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LectioDivineEntity+CoreDataProperties.h"

@implementation LectioDivineEntity (CoreDataProperties)

@dynamic cycle;
@dynamic day;
@dynamic language;
@dynamic lectioDivineVersion;
@dynamic objectId;
@dynamic specialDate;
@dynamic text;
@dynamic time;
@dynamic week;
@dynamic even;

@end
