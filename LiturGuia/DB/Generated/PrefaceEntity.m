//
//  PrefaceEntity.m
//  LiturGuia
//
//  Created by Krloz on 09/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "PrefaceEntity.h"


@implementation PrefaceEntity

@dynamic title;
@dynamic subtitle;
@dynamic text;
@dynamic language;
@dynamic type;
@dynamic shortText;

@end
