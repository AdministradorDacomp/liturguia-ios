//
//  SQLiteDBManager.m
//  LiturGuia
//
//  Created by Krloz on 22/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "SQLiteDBManager.h"
#import <CoreData/CoreData.h>
#import "BibleEntity.h"

@implementation SQLiteDBManager

+(NSMutableArray*)executeQueryInBibleDB:(NSString *)query withArgumentsInArray:(NSArray *)arguments{
    FMDatabase *db = [FMDatabase databaseWithPath:[SQLiteDBManager selectedBibleVersionFileName]];
    if (![db open]) {
        return nil;
    }
    
    FMResultSet *rs = [db executeQuery:query  withArgumentsInArray:arguments];
    NSMutableArray * array = [NSMutableArray array];
    
        while ([rs next]) {
            [array addObject:[rs resultDictionary]];
        }
    
    [db close];
    
    return array;
}

+(NSString*)selectedBibleVersionFileName{
    NSManagedObjectContext *moc = [LiturGuia moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:@"BibleEntity"
                inManagedObjectContext:moc];
    [request setEntity:entity];
    
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"selected == YES"];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array.count > 0) {
        BibleEntity *bible = array[0];
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"DB"];
        return [folderPath stringByAppendingPathComponent:bible.fileName];
    }else{
        return nil;
    }
}

@end
