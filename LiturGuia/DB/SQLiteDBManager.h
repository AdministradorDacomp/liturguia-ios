//
//  SQLiteDBManager.h
//  LiturGuia
//
//  Created by Krloz on 22/04/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "FMDatabase.h"
#import <Foundation/Foundation.h>

@interface SQLiteDBManager : NSObject

+(NSMutableArray*)executeQueryInBibleDB:(NSString *)query withArgumentsInArray:(NSArray *)arguments;

@end
