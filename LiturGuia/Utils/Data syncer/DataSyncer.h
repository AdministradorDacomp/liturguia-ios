//
//  DataSyncer.h
//  LiturGuia
//
//  Created by Krloz on 04/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataSyncer : NSObject

+(void)syncMissals;
+(void)syncPrayers;
//+(void)syncLectioDivine;


@end
