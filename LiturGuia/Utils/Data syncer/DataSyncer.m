//
//  DataSyncer.m
//  LiturGuia
//
//  Created by Krloz on 04/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "DataSyncer.h"
#import "MissalEntity.h"
#import "PrayerEntity.h"
#import "LectioDivineEntity.h"
#import "NSDate+Utils.h"

@implementation DataSyncer

+(void)syncMissals{
//    NSMutableArray *IdsArray = [NSMutableArray new];
//    
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity =
//    [NSEntityDescription entityForName:@"MissalEntity"
//                inManagedObjectContext:[LiturGuia moc]];
//    [request setEntity:entity];
//    
//    NSError *error;
//    NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];
//    if (array != nil) {
//        for (MissalEntity *missal in array) {
//            [IdsArray addObject:missal.objectId];
//        }
//    }
//    PFUser *user = [PFUser currentUser];
//    
//    
//    if (!user[@"misalesDescargados"] || user[@"misalesDescargados"] == [NSNull null]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MissalSyncCompleted" object:nil];
//        return;
//    }
//    
//    PFQuery *query = [PFQuery queryWithClassName:@"Misal"];
//    [query whereKey:@"objectId" containedIn:user[@"misalesDescargados"]];
//    [query whereKey:@"objectId" notContainedIn:IdsArray];
    
//    NSDate *now = [NSDate networkDate];
//    now = [now dateByAddingTimeInterval:-24*60*60];
    
//    PFQuery *query;
//    if (user[@"fVencimientoMisales"]) {
//        PFQuery *downloadedQuery = [PFQuery queryWithClassName:@"Misal"];
//        [downloadedQuery whereKey:@"fecha" lessThanOrEqualTo:user[@"fVencimientoMisales"]];
//        [downloadedQuery whereKey:@"fecha" greaterThanOrEqualTo:now];
//        [downloadedQuery whereKey:@"objectId" notContainedIn:IdsArray];
//        
//        query = [PFQuery orQueryWithSubqueries:@[freeDownloadedQuery,downloadedQuery]];
//    }else{
//        query = freeDownloadedQuery;
//    }
//    
//    
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            NSMutableArray *array = [NSMutableArray array];
//            for (PFObject *parseObject in objects) {
//                [array addObject:parseObject[@"fecha"]];
//                
//            }
//            PFQuery *query = [PFQuery queryWithClassName:@"Misal"];
//            [query whereKey:@"fecha" containedIn:array];
//            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
//                if (!error) {
//                    for (PFObject *parseObject in objects) {
//                        MissalEntity *missal = [NSEntityDescription
//                                                insertNewObjectForEntityForName:@"MissalEntity"
//                                                inManagedObjectContext:[LiturGuia moc]];
//                        missal.title = parseObject[@"titulo"];
//                        missal.time = parseObject[@"tiempo"];
//                        missal.cycle = parseObject[@"ciclo"];
//                        missal.date = parseObject[@"fecha"];
//                        missal.type = parseObject[@"tipo"];
//                        missal.language = parseObject[@"idioma"];
//                        missal.color = parseObject[@"color"];
//                        missal.jsonMissal = parseObject[@"jsonMisal"];
//                        missal.objectId = parseObject.objectId;
//                    }
//                    
//                [LiturGuia saveContext];
//                
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MissalSyncCompleted" object:nil];
//                }
//            }];
//        }
//    }];
}

+(void)syncPrayers{
    if ([LiturGuia internetConnected]) {
        NSMutableArray *IdsArray = [NSMutableArray new];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity =
        [NSEntityDescription entityForName:@"PrayerEntity"
                    inManagedObjectContext:[LiturGuia moc]];
        [request setEntity:entity];
        
        NSError *error;
        NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];
        if (array != nil) {
            for (PrayerEntity *prayer in array) {
                [IdsArray addObject:prayer.objectId];
            }
        }
        
        PFQuery *query = [PFQuery queryWithClassName:@"Oracion"];
        [query whereKey:@"objectId" notContainedIn:IdsArray];
        query.limit = 1000;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                if (objects.count == 0) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PrayersSyncFinished" object:nil];
                }
                for (PFObject *parseObject in objects) {
                    PrayerEntity *prayer = [NSEntityDescription
                                            insertNewObjectForEntityForName:@"PrayerEntity"
                                            inManagedObjectContext:[LiturGuia moc]];
                    prayer.name = parseObject[@"nombre"];
                    prayer.language = parseObject[@"idioma"];
                    prayer.group = parseObject[@"grupo"];
                    prayer.text = parseObject[@"texto"];
                    prayer.objectId = parseObject.objectId;
                    
                    
                }
                [LiturGuia saveContext];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PrayersSyncFinished" object:nil];
            }
        }];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PrayersSyncFinished" object:nil];
    }
}

//+(void)syncLectioDivine{
//    NSMutableArray *IdsArray = [NSMutableArray new];
//    
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity =
//    [NSEntityDescription entityForName:@"LectioDivineEntity"
//                inManagedObjectContext:[LiturGuia moc]];
//    [request setEntity:entity];
//    
//    NSError *error;
//    NSArray *array = [[LiturGuia moc] executeFetchRequest:request error:&error];
//    if (array != nil) {
//        for (LectioDivineEntity *lectioDivine in array) {
//            [IdsArray addObject:lectioDivine.objectId];
//        }
//    }
//    PFUser *user = [PFUser currentUser];
//    
//    
//    if (!user[@"lectioDivinaDescargadas"] || user[@"lectioDivinaDescargadas"] == [NSNull null]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"LectioDivineSyncCompleted" object:nil];
//        return;
//    }
//    
//    PFQuery *liturgicalDateQuery = [PFQuery queryWithClassName:@"FechaLiturgica"];
//    
////    [liturgicalDateQuery whereKey:@"" containedIn:];
//    
//    PFQuery *query = [PFQuery queryWithClassName:@"LectioDivina"];
//    [query whereKey:@"objectId" containedIn:user[@"lectioDivinaDescargadas"]];
//    [query whereKey:@"objectId" notContainedIn:IdsArray];
//    
//    //    NSDate *now = [NSDate networkDate];
//    //    now = [now dateByAddingTimeInterval:-24*60*60];
//    
//    //    PFQuery *query;
//    //    if (user[@"fVencimientoMisales"]) {
//    //        PFQuery *downloadedQuery = [PFQuery queryWithClassName:@"Misal"];
//    //        [downloadedQuery whereKey:@"fecha" lessThanOrEqualTo:user[@"fVencimientoMisales"]];
//    //        [downloadedQuery whereKey:@"fecha" greaterThanOrEqualTo:now];
//    //        [downloadedQuery whereKey:@"objectId" notContainedIn:IdsArray];
//    //
//    //        query = [PFQuery orQueryWithSubqueries:@[freeDownloadedQuery,downloadedQuery]];
//    //    }else{
//    //        query = freeDownloadedQuery;
//    //    }
//    
//    
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            for (PFObject *parseObject in objects) {
//                LectioDivineEntity *lectioDivine = [NSEntityDescription
//                                                    insertNewObjectForEntityForName:@"LectioDivineEntity"
//                                                    inManagedObjectContext:[LiturGuia moc]];
//                
//                lectioDivine.week = parseObject[@"semana"];
//                lectioDivine.time = parseObject[@"tiempo"];
//                lectioDivine.day = parseObject[@"dia"];
//                lectioDivine.cycle = parseObject[@"ciclo"];
//                lectioDivine.language = parseObject[@"idioma"];
//                lectioDivine.text = parseObject[@"texto"];
//                lectioDivine.specialDate = parseObject[@"fechaEspecial"];
//                lectioDivine.objectId = parseObject.objectId;
//                lectioDivine.lectioDivineVersion = parseObject[@"version"];
//
//            }
//            [LiturGuia saveContext];
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"LectioDivineSyncCompleted" object:nil];
//        }
//    }];
//}

@end
