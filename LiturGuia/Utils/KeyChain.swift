//
//  KeyChain.swift
//  OKTA Login
//
//  Created by Carlos Ruiz on 5/14/19.
//  Copyright © 2019 Carlos Ruiz. All rights reserved.
//

import UIKit

class KeyChain {
    class func saveString(key: String, value: String) {
        let data = value.data(using: String.Encoding.utf8) ?? Data()
        save(key: key, data: data, secClass: kSecClassGenericPassword as String)
    }
    
    class func loadString(key: String) -> String? {
        if let data = load(key: key, secClass: kSecClassGenericPassword as String){
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    class func saveArray(key: String, value: Array<Any>) {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        
        save(key: key, data: data, secClass: kSecClassGenericPassword as String)
    }
    
    class func loadDictionary(key: String) -> [String:Any]? {
        if let data = load(key: key, secClass: kSecClassGenericPassword as String),
        let dictionary = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String:Any]{
            return dictionary
        }
        return [:]
    }
    
    
    class func saveDictionary(key: String, value: [String:Any]) {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        
        save(key: key, data: data, secClass: kSecClassGenericPassword as String)
    }
    
    class func loadArray(key: String) -> Array<Any>? {
        if let data = load(key: key, secClass: kSecClassGenericPassword as String),
            let array = NSKeyedUnarchiver.unarchiveObject(with: data) as? Array<Any>{
            return array
        }
        return []
    }
    
    class func save(key: String, data: Data, secClass: String) {
        let query = [
            kSecClass as String       : secClass,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]
        
        SecItemDelete(query as CFDictionary)
        
        SecItemAdd(query as CFDictionary, nil)
    }
    
    class func load(key: String,  secClass: String) -> Data? {
        let query = [
            kSecClass as String       : secClass,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue ?? false,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]
        
        var dataTypeRef: AnyObject? = nil
        
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        
        if status == noErr,
            let data = dataTypeRef as? Data{
            return data
        } else {
            return nil
        }
    }
    
    class func delete(key: String) {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key] as [String : Any]
        
        SecItemDelete(query as CFDictionary)
    }
    
    
    
    class func createUniqueID() -> String {
        let uuid: CFUUID = CFUUIDCreate(nil)
        let cfStr: CFString = CFUUIDCreateString(nil, uuid)
        
        let swiftString: String = cfStr as String
        return swiftString
    }
}

extension Data {

    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }

    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.load(as: T.self) }
    }
}
