//
//  SettingsPanelManager.swift
//  LiturGuia
//
//  Created by Daniel Marrufo Rivera on 13/07/21.
//  Copyright © 2021 DAComp SC. All rights reserved.
//

import UIKit
import FloatingPanel

@objc class SettingsPanelManager: NSObject {
    @objc static let shared : SettingsPanelManager = {
        let instance = SettingsPanelManager()
        return instance
    }()
    private var settingsPanelVC: FloatingPanelController!
    
    @objc func setUpSettingsPanel(contentVC: UIViewController, toParent: UIViewController) {
        let oldMainPanelVC = settingsPanelVC
        settingsPanelVC = FloatingPanelController()
        let appearance = SurfaceAppearance()
        appearance.cornerRadius = 6.0
        appearance.backgroundColor = .clear
        if #available(iOS 13.0, *) {
            appearance.cornerCurve = .continuous
        }
        settingsPanelVC.surfaceView.appearance = appearance
        settingsPanelVC.isRemovalInteractionEnabled = true
        settingsPanelVC.backdropView.dismissalTapGestureRecognizer.isEnabled = true
        settingsPanelVC.set(contentViewController: contentVC)
        settingsPanelVC.surfaceView.containerMargins = .init(top: 20.0, left: 200.0, bottom: 16.0, right: 200.0)
        if let oldMainPanelVC = oldMainPanelVC {
            oldMainPanelVC.removePanelFromParent(animated: true, completion: {
                self.settingsPanelVC.addPanel(toParent: toParent, animated: true)
            })
        } else {
            settingsPanelVC.addPanel(toParent: toParent, animated: true)
        }
    }
}
