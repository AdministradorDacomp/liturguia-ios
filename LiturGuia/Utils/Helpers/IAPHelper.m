//
//  IAPHelper.m
//  LiturGuia
//
//  Created by Krloz on 25/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "IAPHelper.h"
#import <StoreKit/StoreKit.h>


NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";

NSString *const IAPHelperProductPurchaseFailedNotification = @"IAPHelperProductPurchaseFailedNotification";

NSString *const IAPHelperProductPurchaseFakeNotification = @"IAPHelperProductPurchaseFakeNotification";


@implementation IAPHelper {
    SKProductsRequest * _productsRequest;

    RequestProductsCompletionHandler _completionHandler;
    NSSet * _productIdentifiers;
//    NSMutableSet * _purchasedProductIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    if ((self = [super init])) {
        _productIdentifiers = productIdentifiers;
        
//        _purchasedProductIdentifiers = [NSMutableSet set];
//        for (NSString * productIdentifier in _productIdentifiers) {
//            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
//            if (productPurchased) {
//                [_purchasedProductIdentifiers addObject:productIdentifier];
//                NSLog(@"Previously purchased: %@", productIdentifier);
//            } else {
//                NSLog(@"Not purchased: %@", productIdentifier);
//            }
//        }
        
    }
    return self;
}


- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {

    _completionHandler = [completionHandler copy];

    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    _productsRequest.delegate = self;
    [_productsRequest start];
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Loaded list of products...");

    for (NSString *invalidIIdentifier in response.invalidProductIdentifiers) {
        NSLog(@"The following ID is invalid: %@", invalidIIdentifier);
    }
    
    
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

//- (BOOL)subscriptionPurchased:(NSString *)productIdentifier {
//    return [_purchasedProductIdentifiers containsObject:productIdentifier];
//}

- (void)buySubscription:(SKProduct *)product {
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)buyProduct:(SKProduct *)product {
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    [self verifyTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    
    [self registerSubscriptionForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    NSString *error;
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        error = transaction.error.localizedDescription;
        NSLog(@"Transaction error: %@", error);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchaseFailedNotification object:error userInfo:nil];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)registerSubscriptionForProductIdentifier:(NSString *)productIdentifier {
    
//    [_purchasedProductIdentifiers addObject:productIdentifier];

    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
}

-(void)verifyTransaction:(SKPaymentTransaction *)transaction{
    
    
    // Load the receipt from the app bundle.
//    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
//    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
//    if (receipt) {
//        //        NSData *receipt; // Sent to the server by the device
//        
//        // Create the JSON object that describes the request
//        NSError *error;
//        NSDictionary *requestContents = @{
//                                          @"receipt-data": [receipt base64EncodedStringWithOptions:0]
//                                          };
//        NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
//                                                              options:0
//                                                                error:&error];
//        
//        if (!requestData) { /* ... Handle error ... */ }
//        
//        NSString *url;
//        if (development) {
//            url = kReceiptURLDevelopment;
//        }else{
//            url = kReceiptURLProduction;
//        }
//        
//        // Create a POST request with the receipt data.
//        NSURL *storeURL = [NSURL URLWithString:url];
//        NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
//        [storeRequest setHTTPMethod:@"POST"];
//        [storeRequest setHTTPBody:requestData];
//        
//        // Make a connection to the iTunes Store on a background queue.
//        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//        [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
//                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//                                   if (connectionError) {
//                                       /* ... Handle error ... */
//                                   } else {
//                                       NSError *error;
//                                       NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//                                       if (!jsonResponse) {}
//                                       
//                                       for (NSDictionary *purchase in jsonResponse[@"receipt"][@"in_app"]) {
//                                           if ([purchase[@"transaction_id"] isEqualToString:transaction.transactionIdentifier]) {
//                                               NSLog(@"completeTransaction...");
                                               [self registerSubscriptionForProductIdentifier:transaction.payment.productIdentifier];
//                                               return;
//                                           }
//                                       }
//                                       NSLog(@"Fake purchase...");
//                                      [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchaseFakeNotification object:error userInfo:nil];
//                                   }
//                               }];
    
        
//    }
}


@end