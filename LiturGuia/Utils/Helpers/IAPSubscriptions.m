//
//  IAPSubscriptions.m
//  LiturGuia
//
//  Created by Krloz on 25/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "IAPSubscriptions.h"


@implementation IAPSubscriptions

+ (IAPSubscriptions *)sharedInstance {
    static dispatch_once_t once;
    static IAPSubscriptions * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      kMonthSuscription,
                                      k6MonthSuscription,
                                      k12MonthSuscription,
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
        
    });
    return sharedInstance;
}

@end
