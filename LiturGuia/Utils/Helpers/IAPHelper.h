//
//  IAPHelper.h
//  LiturGuia
//
//  Created by Krloz on 25/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h> 

UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString *const IAPHelperProductPurchaseFailedNotification;
UIKIT_EXTERN NSString *const IAPHelperProductPurchaseFakeNotification;

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver>

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;


- (void)buyProduct:(SKProduct *)product;
//- (BOOL)subscriptionPurchased:(NSString *)productIdentifier;

@end