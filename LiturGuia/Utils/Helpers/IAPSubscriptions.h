//
//  IAPSubscriptions.h
//  LiturGuia
//
//  Created by Krloz on 25/06/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "IAPHelper.h"

//NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";

@interface IAPSubscriptions : IAPHelper

//UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;

+ (IAPSubscriptions *)sharedInstance;

@end
