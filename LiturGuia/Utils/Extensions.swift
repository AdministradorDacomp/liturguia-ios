//
//  Extensions.swift
//  LiturGuia
//
//  Created by Carlos Ruiz on 5/21/19.
//  Copyright © 2019 DAComp SC. All rights reserved.
//

import UIKit
import Material
import CryptoKit

extension Dictionary{
    func toSortedArrayByKey(_ key: String, otherKey: String = "", descendent:Bool = false) -> [[String:Any]] {
        
        var unsortedArray:[[String:Any]]  = []
        for (_, value) in self{
            unsortedArray.append(value as! [String : Any])
        }
        
        let sortedArray = unsortedArray.sorted { (one, two) -> Bool in
            if one[key] is Int{
                if descendent{
                    if one[key] as? Int ?? 0 == two[key] as? Int ?? 0{
                        return one[otherKey] as? Int ?? 0 > two[otherKey] as? Int ?? 0
                    }else {
                        return one[key] as? Int ?? 0 > two[key] as? Int ?? 0
                    }
                }else{
                    if one[key] as? Int ?? 0 == two[key] as? Int ?? 0{
                        return one[otherKey] as? Int ?? 0 < two[otherKey] as? Int ?? 0
                    }else {
                        return one[key] as? Int ?? 0 < two[key] as? Int ?? 0
                    }
                }
                
            }
            return false
        }
        return sortedArray
    }

}

public extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
            layer.borderWidth = 1.0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.cornerRadius = newValue
            // layer.masksToBounds = borderWidth > 0
        }
    }
    
    
    //    @IBInspectable var borderWidth: CGFloat {
    //        get {
    //            return layer.borderWidth
    //        }
    //        set {
    //            layer.borderWidth = borderWidth
    //            //layer.masksToBounds = borderWidth > 0
    //        }
    //    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            //            layer.masksToBounds = newValue > 0
        }
    }
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }
    
    @IBInspectable var depthValue: NSInteger {
        get {
            if self.depthPreset == DepthPreset.depth1{
                return 1
            }else if self.depthPreset == DepthPreset.depth2{
                return 2
            }else if self.depthPreset == DepthPreset.depth3{
                return 3
            }else if self.depthPreset == DepthPreset.depth4{
                return 4
            }else if self.depthPreset == DepthPreset.depth5{
                return 5
            }
            return 0
        }
        set {
            if newValue == 1{
                self.depthPreset = DepthPreset.depth1
            }else if newValue == 2{
                self.depthPreset = DepthPreset.depth2
            }else if newValue == 3{
                self.depthPreset = DepthPreset.depth3
            }else if newValue == 4{
                self.depthPreset = DepthPreset.depth4
            }else if newValue == 5{
                self.depthPreset = DepthPreset.depth5
            }
        }
    }
    
  
    
}

//MARK: - UIApplication

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

class AnimatedLabel: UILabel {
    var beginAnimationTime:CFTimeInterval = 0
    var timer: Timer? = nil
    var completion:(() -> Void)? = nil
    var duration = 3.0
    
    func fadeOut(duration:Double = 3.0, _ completion:(() -> Void)? = nil) {
        self.duration = duration
        self.completion = completion
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.createDisplayLink()

        }
    }
    
    func createDisplayLink() {
        beginAnimationTime = CACurrentMediaTime()
         timer = Timer.scheduledTimer(timeInterval: 1.0/1000.0, target: self, selector: #selector(step), userInfo: nil, repeats: true)
    }
    
    @objc func step() {
        
        
        //        let beginTime = CADisplayLink
        
//                DispatchQueue.main.async {
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(attributedString: self.attributedText!)
        for i in 0..<self.attributedText!.length{
            
            
            let range = NSRange(location: i, length: 1)
            
            
            let elapsedTime = CACurrentMediaTime() - beginAnimationTime

            let percentage:Float = Float(elapsedTime / (duration))
            
            
            let previousColor = attributedString.attribute(NSAttributedString.Key.foregroundColor, at: i, longestEffectiveRange: nil, in: range) as? UIColor
            
            let newAlpha = CGFloat(Float.random(in: 0.1 ..< 1) * (1 - percentage ))

            
            let color = UIColor(white:0.0, alpha: min(previousColor?.cgColor.alpha ?? 0.0, newAlpha) )

            if range.location != NSNotFound {
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:color, range: range)
            }
            
            
            
            if elapsedTime > duration{
                timer?.invalidate()
                completion?()
            }
        }
        self.attributedText = attributedString
        //        }
    }
    
    
}

extension UIColor {
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}

extension UIFont {
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
    
    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}


//MARK: Dates

extension Date {
    static func removeTime(from date: Date) -> Date {
        var cal = Calendar(identifier: .gregorian)
        cal.timeZone = TimeZone(abbreviation: "GMT")!
        // I'm in Paris (+1)
        let timeZoneOffset = NSTimeZone.default.secondsFromGMT()
        // You could also use the systemTimeZone method
        let gmtTimeInterval: TimeInterval = date.timeIntervalSinceReferenceDate + TimeInterval(timeZoneOffset)
        let gmtDate = Date(timeIntervalSinceReferenceDate: gmtTimeInterval)
        var comps: DateComponents? = cal.dateComponents([.year, .month, .day], from: gmtDate)
        comps?.hour = 0
        comps?.minute = 0
        comps?.second = 0
        return cal.date(from: comps!)!
    }
    
    init(localTime date: Date) {
        
        let timeZoneSeconds: TimeInterval = TimeInterval(NSTimeZone.local.secondsFromGMT())
        
        let dateInLocalTimezone = date.addingTimeInterval(-timeZoneSeconds)
        
        self = dateInLocalTimezone
    }
    
    func isLaterThan(_ date: Date) -> Bool {
        if compare(date) == .orderedDescending {
            return true
        }
        return false
    }
    
    func isEarlierThan(_ date: Date) -> Bool {
        if compare(date) == .orderedAscending {
            return true
        }
        return false
    }
    
    func dayOfTheWeek() -> Int {
        let gregorian = Calendar(identifier: .gregorian)
        let weekdayComponents: DateComponents? = gregorian.dateComponents(([.day, .weekday]), from: self)
        let weekday: Int? = weekdayComponents?.weekday
        return weekday!
    }
    
    func daysElapsed(since date: Date) -> Int {
        let calendar = Calendar.current
        let difference = calendar.dateComponents([.day], from: date, to: self)
        return difference.day!
    }
    
    func weeksElapsed(since date: Date) -> Int {
        let calendar = Calendar.current
        let difference = calendar.dateComponents([.day], from: date, to: self)
        return Int(difference.day!) / 7
    }
    
    func year() -> Int {
        let components = Calendar.current.dateComponents([.day, .month, .year], from: self)
        return components.year!
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    func toMilliseconds() -> TimeInterval{
        return self.timeIntervalSince1970 * 1000.0
    }
    
    
    func addMonth(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .month, value: n, to: self)!
    }
    
    func addDay(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .day, value: n, to: self)!
    }
    
    func addSec(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .second, value: n, to: self)!
    }
    
    func addHour(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .hour, value: n, to: self)!
    }
    
    func addYear(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .year, value: n, to: self)!
    }
}


extension UITextView {
    var numberOfCurrentlyDisplayedLines: Int {
        let size = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        //for Swift <=4.0, replace with next line:
        //let size = systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        return Int(((size.height - layoutMargins.top - layoutMargins.bottom) / font!.lineHeight))
    }
    
    /// Removes last characters until the given max. number of lines is reached
    func removeTextUntilSatisfying(maxNumberOfLines: Int) {
        while numberOfCurrentlyDisplayedLines > (maxNumberOfLines) {
            text = String(text.dropLast())
            layoutIfNeeded()
        }
    }
}



extension UIResponder {
    
    private static weak var _currentFirstResponder: UIResponder?
    
    static var currentFirstResponder: UIResponder? {
        _currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
        
        return _currentFirstResponder
    }
    
    @objc func findFirstResponder(_ sender: Any) {
        UIResponder._currentFirstResponder = self
    }
}


extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            if numberOfSections == 0{
                return
            }
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
    }
    
    enum scrollsTo {
        case top, bottom
    }
}

extension String {
    subscript(value: NSRange) -> Substring {
        return self[value.lowerBound..<value.upperBound]
    }
    

}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    static func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    @available(iOS 13, *)
    func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    
    subscript(value: CountableClosedRange<Int>) -> Substring {
        get {
            return self[index(at: value.lowerBound)...index(at: value.upperBound)]
        }
    }
    
    subscript(value: CountableRange<Int>) -> Substring {
        get {
            return self[index(at: value.lowerBound)..<index(at: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        get {
            return self[..<index(at: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        get {
            return self[...index(at: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        get {
            return self[index(at: value.lowerBound)...]
        }
    }
    
    func index(at offset: Int) -> String.Index {
        return index(startIndex, offsetBy: offset)
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var simpleString: String {
        let userInput: String = self
        return userInput.folding(options: .diacriticInsensitive, locale: .current)
    }
    
    func nsRange(of string: String) -> NSRange? {
        if let range = range(of: string) {
            return NSRange(range, in: self)
        }
        return nil
    }
    
    func replacingFirstOccurrenceOfString(_ target: String, with replaceString: String) -> String {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }

}

@objc extension NSString{
    var cleanEmbbedCSS: NSString {
        var cleanedString = self as String
        cleanedString = cleanedString.removeCSSAttribute("font-size:")
        cleanedString = cleanedString.removeCSSAttribute("font-family:")
//        cleanedString = cleanedString.removeCSSAttribute("margin-top:")
//        cleanedString = cleanedString.removeCSSAttribute("margin-bottom:")
//        cleanedString = cleanedString.removeCSSAttribute("margin-left:")
//        cleanedString = cleanedString.removeCSSAttribute("margin-right:")
//        cleanedString = cleanedString.removeCSSAttribute("line-height:")
       
        
        return cleanedString as NSString
    }
    
    func removeCSSAttribute(_ attribute: String) -> String {
        var text = self as String
        while (text.contains(attribute)) {
            if let index = text.range(of: attribute) {
                let range = index.upperBound..<text.endIndex
                if let indexEnd = text.range(of: ";", options: .caseInsensitive, range: range){
                    let removableText = text[index.lowerBound...indexEnd.upperBound]
                    text = text.replacingOccurrences(of: removableText, with: "")
                }
            }
        }
        return text
    }
}

extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
}

extension UIBarButtonItem {
    private struct AssociatedObject {
        static var key = "action_closure_key"
    }
    
    var actionClosure: (()->Void)? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObject.key) as? ()->Void
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObject.key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            target = self
            action = #selector(didTapButton(sender:))
        }
    }
    
    @objc func didTapButton(sender: Any) {
        actionClosure?()
    }
}

extension UITableView {

    func scrollToBottom(animated: Bool = true){

        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections-1) - 1,
                section: self.numberOfSections - 1)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
        }
    }

    func scrollToTop(animated: Bool = true) {

        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .top, animated: animated)
           }
        }
    }

    func hasRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
    }
}

extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!
    }
    
    func showAlertWithError(_ error:Error!) {
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithMessage(_ message:String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithTitleAndMessage(_ title:String,_ message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func cleanViewControllers(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        if viewControllers.count > 0{
            for viewController in viewControllers {
                self.navigationController?.popToViewController(viewController, animated: true)
            }
        }
    }
}
