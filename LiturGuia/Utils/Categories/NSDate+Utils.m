//
//  NSDate+Compare.m
//  Ejemplo1
//
//  Created by Krloz on 13/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)

-(BOOL)isLaterThan:(NSDate*)date{
    if ([self compare:date] == NSOrderedDescending) {
        return YES;
    }
    return NO;
}

-(BOOL)isEarlierThan:(NSDate*)date{
    if ([self compare:date] == NSOrderedAscending) {
        return YES;
    }
    return NO;
}

-(NSInteger)dayOfTheWeek{
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:self];
    NSInteger weekday = [weekdayComponents weekday];
    
    return weekday;
}

- (NSInteger)daysElapsedSince:(NSDate *)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:date toDate:self options:0];
    
    return [difference day];
}

- (NSInteger)weeksElapsedSince:(NSDate *)date{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:date toDate:self options:0];
    
    return (int)[difference day]/7;
}


-(NSInteger)year{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self];
    
    return components.year;
}

+(NSDate *)removeTimeFromDate:(NSDate *)date
{
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [cal setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]]; // I'm in Paris (+1)
    
    NSTimeInterval timeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval gmtTimeInterval = [date timeIntervalSinceReferenceDate] + timeZoneOffset;
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSinceReferenceDate:gmtTimeInterval];
    
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:gmtDate];
    
    comps.hour = 0;
    comps.minute = 0;
    comps.second = 0;
    
    return [cal dateFromComponents:comps];
}


+(NSDate *)dateWithLocalTime:(NSDate *)date
{
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimezone = [date dateByAddingTimeInterval:-timeZoneSeconds];
    return dateInLocalTimezone;
}

@end
