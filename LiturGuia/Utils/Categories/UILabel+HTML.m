//
//  NSString+HTML.m
//  LiturGuia
//
//  Created by Carlos Ruiz on 24/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

#import "UILabel+HTML.h"

@implementation UILabel (HTML)

- (void) setHtml: (NSString*) html
    {
    NSError *err = nil;
    self.attributedText =
        [[NSAttributedString alloc]
                  initWithData: [html dataUsingEncoding:NSUTF8StringEncoding]
                       options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
            documentAttributes: nil
                         error: &err];
    if(err)
        NSLog(@"Unable to parse label text: %@", err);
    }

@end
