//
//  NSDate+ServerDate.m
//
//  Created by Shai Mishali on 10/2/13.
//  Copyright (c) 2013 Shai Mishali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSDate+ServerDate.h"
#import "NSDate+Utils.h"

static NSTimeInterval   _SDInterval;

@interface NSDate (ServerDateExtras)
+(void)_SDresetServerTime;
@end

@implementation NSDate (ServerDate)

+(NSDate *)serverDate{
    if(!_SDInterval){
        [self _SDresetServerTime];
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (_SDInterval == 0) {
        
        NSDate *lastServerDate = [userDefaults objectForKey:@"lastServerDate"];
        if ([lastServerDate isLaterThan:[NSDate date]]) {
            return nil;
        }
    }else{
        [userDefaults setObject:[[NSDate date] dateByAddingTimeInterval: _SDInterval] forKey:@"lastServerDate"];
        [userDefaults synchronize];
    }

     return [[NSDate date] dateByAddingTimeInterval: _SDInterval];
}

#pragma mark - Private Helpers
+(void)_SDresetServerTime{
    NSHTTPURLResponse *resp;
    NSError *err;
    
    NSMutableURLRequest *req        = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:_SD_SERVER]];
    req.HTTPMethod                  = @"HEAD";
    
    [NSURLConnection sendSynchronousRequest:req returningResponse:&resp error:&err];
    NSString *httpDate              = [resp allHeaderFields][@"Date"];
    
    NSDateFormatter *df             = [[NSDateFormatter alloc] init];
    df.dateFormat                   = _SD_FORMAT;
    df.locale                       = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    NSDate *serverDate             = [df dateFromString: httpDate];
    _SDInterval                    = [serverDate timeIntervalSinceNow];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:[self class] name:UIApplicationWillEnterForegroundNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:[self class] selector:@selector(_SDresetServerTime) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    #ifdef DEBUG
    NSLog(@"Server Clock resetted to: %@", serverDate);
    #endif
}

@end