//
//  NSDate+Compare.h
//  Ejemplo1
//
//  Created by Krloz on 13/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Compare)

-(BOOL)isLaterThan:(NSDate*)date;
-(BOOL)isEarlierThan:(NSDate*)date;
-(NSInteger)dayOfTheWeek;
- (NSInteger)daysElapsedToDate:(NSDate *)date;
- (NSInteger)weeksElapsedSince:(NSDate *)date;
-(NSInteger)year;

+(NSDate *)removeTimeFromDate:(NSDate *)date;
+(NSDate *)dateWithLocalTime:(NSDate *)date;




@end
