//
//  NSObject+Contains.m
//  LiturGuia
//
//  Created by Krloz on 14/09/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import "NSString+Contains.h"

@implementation NSString (Contains)

- (BOOL)myContainsString:(NSString*)other {
    NSRange range = [self rangeOfString:other];
    return range.length != 0;
}

@end
