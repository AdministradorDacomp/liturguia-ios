//
//  NSObject+Contains.h
//  LiturGuia
//
//  Created by Krloz on 14/09/15.
//  Copyright (c) 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Contains)
- (BOOL)myContainsString:(NSString*)other;

@end
