//
//  NSString+HTML.h
//  LiturGuia
//
//  Created by Carlos Ruiz on 24/10/20.
//  Copyright © 2020 DAComp SC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HTML)

- (void) setHtml: (NSString*) html;

@end
