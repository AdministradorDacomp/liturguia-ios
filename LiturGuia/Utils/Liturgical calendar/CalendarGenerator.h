//
//  LGCalendarGenerator.h
//  Ejemplo1
//
//  Created by Krloz on 12/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LiturgicalDate.h"


@interface CalendarGenerator : NSObject

@property (nonatomic, strong) NSMutableDictionary *yearInfo;


+ (id)sharedInstance;
-(LiturgicalDate*)liturgicalInfoForDate:(NSDate*)date;

@end
