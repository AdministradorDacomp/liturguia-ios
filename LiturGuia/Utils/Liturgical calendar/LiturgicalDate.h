//
//  LiturgyDate.h
//  Ejemplo1
//
//  Created by Krloz on 27/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiturgicalDate : NSObject

@property (nonatomic, strong) NSString *cycle;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *specialDate;
@property (nonatomic, strong) NSNumber* week;
@property (nonatomic, strong) NSNumber* even;

@end
