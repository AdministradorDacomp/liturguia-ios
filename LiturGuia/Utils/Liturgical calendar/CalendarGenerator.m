//
//  LGCalendarGenerator.m
//  Ejemplo1
//
//  Created by Krloz on 12/11/15.
//  Copyright © 2015 DAComp SC. All rights reserved.
//

#import "CalendarGenerator.h"
#import "NSDate+Utils.h"

@implementation CalendarGenerator

@synthesize yearInfo;

+ (id)sharedInstance {
    static CalendarGenerator *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        yearInfo = [NSMutableDictionary dictionary];
    }
    return self;
}


-(NSDictionary*)generateCalendarForYear:(NSInteger)year{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *christmas = [formatter dateFromString:[NSString stringWithFormat:@"25/12/%d",(int)year-1]];
    christmas = [NSDate removeTimeFromDate:christmas];
    NSDate *advent = [christmas dateByAddingTimeInterval:-(24*60*60)];
    
    while ([[NSDate dateWithLocalTime:advent] dayOfTheWeek] != 1) {
        advent = [advent dateByAddingTimeInterval:-(24*60*60)];
    }
    advent = [advent dateByAddingTimeInterval:-(3*7)*(24*60*60)];
    
    NSDate *holyFamily = [christmas dateByAddingTimeInterval:(24*60*60)];
    while ([[NSDate dateWithLocalTime:holyFamily] dayOfTheWeek] != 1) {
        holyFamily = [holyFamily dateByAddingTimeInterval:(24*60*60)];
    }
    if([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"01/01/%ld",(long)year]]] isEqualToDate:holyFamily]){
        holyFamily = [formatter dateFromString:[NSString stringWithFormat:@"30/12/%d",(int)year - 1]];
        
    }
//    holyFamily = [NSDate removeTimeFromDate:holyFamily];
    
    NSDate *baptism = [formatter dateFromString:[NSString stringWithFormat:@"07/01/%ld",(long)year]];
    baptism = [NSDate removeTimeFromDate:baptism];
    while ([[NSDate dateWithLocalTime:baptism] dayOfTheWeek] != 1) {
        
        baptism = [baptism dateByAddingTimeInterval:(24*60*60)];
    }
    NSDate *epiphany = [baptism dateByAddingTimeInterval:-7*(24*60*60)];
    
    NSDate *resurrection = [self centerDate:year];
    NSDate *holySaturday = [resurrection dateByAddingTimeInterval:-1*(24*60*60)];
    NSDate *holyFriday = [resurrection dateByAddingTimeInterval:-2*(24*60*60)];
    NSDate *holyThursday = [resurrection dateByAddingTimeInterval:-3*(24*60*60)];
    
    NSDate *palmSunday = [resurrection dateByAddingTimeInterval:-7*(24*60*60)];
    NSDate *ashWednesday = [resurrection dateByAddingTimeInterval:-46*(24*60*60)];
    NSDate *ascension = [resurrection dateByAddingTimeInterval:39*(24*60*60)];
    NSDate *pentecost = [resurrection dateByAddingTimeInterval:49*(24*60*60)];
    NSDate *trinity = [pentecost dateByAddingTimeInterval:7*(24*60*60)];
    NSDate *corpusChristi = [trinity dateByAddingTimeInterval:4*(24*60*60)];
    
    NSDate *suguienteNavidad = [formatter dateFromString:[NSString stringWithFormat:@"25/12/%ld",(long)year]];
    suguienteNavidad = [NSDate removeTimeFromDate:suguienteNavidad];
    
    NSDate *christTheKing = suguienteNavidad;
    if ([[NSDate dateWithLocalTime:christTheKing] dayOfTheWeek] == 1) {
        christTheKing = [christTheKing dateByAddingTimeInterval:-(24*60*60)];
    }
    while ([[NSDate dateWithLocalTime:christTheKing] dayOfTheWeek] != 1) {
        christTheKing = [christTheKing dateByAddingTimeInterval:-(24*60*60)];
    }
    christTheKing = [christTheKing dateByAddingTimeInterval:-(4*7)*(24*60*60)];
    
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    
    info[@"advent"] = advent;
    info[@"christmas"] = christmas;
    info[@"epiphany"] = epiphany;
    info[@"baptism"] = baptism;
    info[@"holyThursday"] = holyThursday;
    info[@"holyFriday"] = holyFriday;
    info[@"holySaturday"] = holySaturday;
    info[@"resurrection"] = resurrection;
    info[@"palmSunday"] = palmSunday;
    info[@"ashWednesday"] = ashWednesday;
    info[@"ascension"] = ascension;
    info[@"pentecost"] = pentecost;
    info[@"trinity"] = trinity;
    info[@"corpusChristi"] = corpusChristi;
    info[@"christTheKing"] = christTheKing;
    info[@"holyFamily"] = holyFamily;
    info[@"cycle"] = [self cycleForDate:advent];
    info[@"even"] = [resurrection year]%2 == 0? @YES:@NO;
    
    return info;
}

-(NSDate*)centerDate:(NSInteger)year{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    int a = year % 19;
    int b = floor(year / 100);
    int c = year % 100;
    int d = floor(b / 4);
    int e = b % 4;
    int f = floor((b + 8) / 25);
    int g = floor((b - f + 1) / 3);
    int h = (19 * a + b - d - g + 15) % 30;
    int i = floor(c / 4);
    int k = c % 4;
    int l = (32 + 2 * e + 2 * i - h - k) % 7;
    int m = floor((a + 11 * h + 22 * l) / 451);
    int p = (h + l - 7 * m + 114);
    
    int day = (p % 31) + 1;
    int month = floor(p / 31);
    
    NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%d/%d/%ld", day, month, (long)year]];
    
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    
    comps.hour = 0;
    comps.minute = 0;
    comps.second = 0;
    
    return [cal dateFromComponents:comps];
}

-(LiturgicalDate*)liturgicalInfoForDate:(NSDate*)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    date = [NSDate removeTimeFromDate:date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    NSInteger year = [components year];
    
    if (!yearInfo[@(year)]) {
        yearInfo[@(year)] = [self generateCalendarForYear:year];
    }
    NSDictionary *info = yearInfo[@(year)];
    NSDate *christTheKing = info[@"christTheKing"];
    NSDate *endDate = [christTheKing dateByAddingTimeInterval:6*24*60*60];
    if ([date isLaterThan:endDate]) {
        if (!yearInfo[@(year+1)]) {
            yearInfo[@(year+1)] = [self generateCalendarForYear:year+1];
        }
        info = yearInfo[@(year+1)];
        christTheKing = info[@"christTheKing"];
        endDate = [christTheKing dateByAddingTimeInterval:6*24*60*60];
    }
    LiturgicalDate *liturgicalDate = [[LiturgicalDate alloc] init];
    liturgicalDate.date = date;
    
    NSDate *advent = info[@"advent"];
    NSDate *christmas = info[@"christmas"];
    NSDate *epiphany = info[@"epiphany"];
    NSDate *baptism = info[@"baptism"];
    NSDate *holyThursday = info[@"holyThursday"];
    NSDate *holyFriday = info[@"holyFriday"];
    NSDate *holySaturday = info[@"holySaturday"];
    NSDate *resurrection = info[@"resurrection"];
    NSDate *palmSunday = info[@"palmSunday"];
    NSDate *ashWednesday = info[@"ashWednesday"];
    NSDate *firstLentSunday = [ashWednesday dateByAddingTimeInterval:4*24*60*60];
    NSDate *ascension = info[@"ascension"];
    NSDate *pentecost = info[@"pentecost"];
    NSDate *trinity = info[@"trinity"];
    NSDate *corpusChristi = info[@"corpusChristi"];
    NSDate *holyFamily = info[@"holyFamily"];
    
    
    if (([date isLaterThan:advent] && [date isEarlierThan:christmas]) || [date isEqualToDate:advent]) {
        liturgicalDate.time = @"adviento";
        liturgicalDate.week = @([date weeksElapsedSince:advent] + 1);
    }else if ([date isLaterThan:christmas] && [date isEarlierThan:baptism]) {
        liturgicalDate.time = @"navidad";
        if ([date isLaterThan:christmas] && [date isEarlierThan:epiphany]) {
            
            liturgicalDate.week = @0;
        }else{
            liturgicalDate.week = @([date weeksElapsedSince:epiphany] + 1);
        }
    }else if ([date isLaterThan:baptism] && [date isEarlierThan:ashWednesday]) {
        liturgicalDate.time = @"tiempo ordinario";
        liturgicalDate.week = @([date weeksElapsedSince:baptism] + 1);
        
        
    }else if ([date isLaterThan:ashWednesday] && [date isEarlierThan:resurrection]) {
        liturgicalDate.time = @"cuaresma";
        if ([date isEarlierThan:firstLentSunday]) {
            liturgicalDate.week = @(0);
        }else{
            liturgicalDate.week = @([date weeksElapsedSince:firstLentSunday] + 1);
        }
    }else if ([date isLaterThan:resurrection] && [date isEarlierThan:pentecost]) {
        liturgicalDate.time = @"pascua";
        liturgicalDate.week = @([date weeksElapsedSince:resurrection] + 1);
    }else if (([date isLaterThan:pentecost] && [date isEarlierThan:endDate]) || [date isEqualToDate:endDate]) {
        liturgicalDate.time = @"tiempo ordinario";
        long weeks = [endDate weeksElapsedSince:date];
        
        liturgicalDate.week = @(34-weeks);
    }
    
    if ([date isEqualToDate:christmas]) {
        liturgicalDate.specialDate = @"navidad";
        liturgicalDate.time = @"navidad";
        liturgicalDate.week = @0;
    }else if ([date isEqualToDate:holyFamily]) {
        liturgicalDate.specialDate = @"sagradaFamilia";
    }else if ([date isEqualToDate:epiphany]) {
        liturgicalDate.specialDate = @"epifania";
    }else if ([date isEqualToDate:baptism]) {
        liturgicalDate.specialDate = @"bautismo";
        liturgicalDate.time = @"navidad";
        liturgicalDate.week = @1;
    }else if ([date isEqualToDate:holyThursday]) {
        liturgicalDate.specialDate = @"juevesSanto";
    }else if ([date isEqualToDate:holyFriday]) {
        liturgicalDate.specialDate = @"viernesSanto";
    }else if ([date isEqualToDate:holySaturday]) {
        liturgicalDate.specialDate = @"sabadoSanto";
    }else if ([date isEqualToDate:resurrection]) {
        liturgicalDate.specialDate = @"resurreccion";
        liturgicalDate.time = @"pascua";
        liturgicalDate.week = @1;
    }else if ([date isEqualToDate:palmSunday]) {
        liturgicalDate.specialDate = @"domingoDeRamos";
    }else if ([date isEqualToDate:ashWednesday]) {
        liturgicalDate.specialDate = @"miercolesDeCeniza";
        liturgicalDate.time = @"cuaresma";
        liturgicalDate.week = @0;
    }else if ([date isEqualToDate:ascension]) {
        liturgicalDate.specialDate = @"ascension";
    }else if ([date isEqualToDate:pentecost]) {
        liturgicalDate.specialDate = @"pentecostes";
        liturgicalDate.time = @"pascua";
        liturgicalDate.week = @7;
    }else if ([date isEqualToDate:trinity]) {
        liturgicalDate.specialDate = @"santisimaTrinidad";
    }else if ([date isEqualToDate:corpusChristi]) {
        liturgicalDate.specialDate = @"corpusChristi";
    }else if ([date isEqualToDate:christTheKing]) {
        liturgicalDate.specialDate = @"cristoRey";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"26/12/%ld",(long)year]]] isEqualToDate:date]) {
        liturgicalDate.specialDate = @"IINavidad";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"27/12/%ld",(long)year]]] isEqualToDate:date]) {
        liturgicalDate.specialDate = @"IIINavidad";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"28/12/%ld",(long)year]]]isEqualToDate:date]) {
        liturgicalDate.specialDate = @"IVNavidad";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"29/12/%ld",(long)year]]] isEqualToDate:date]) {
        liturgicalDate.specialDate = @"VNavidad";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"30/12/%ld",(long)year]]] isEqualToDate:date]) {
        liturgicalDate.specialDate = @"VINavidad";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"31/12/%ld",(long)year]]] isEqualToDate:date]) {
        liturgicalDate.specialDate = @"VIINavidad";
    }else if ([[NSDate removeTimeFromDate:[formatter dateFromString:[NSString stringWithFormat:@"01/01/%d",(int)year+1]]] isEqualToDate:date]) {
        liturgicalDate.specialDate = @"VIIINavidad";
    }
    
    liturgicalDate.cycle = info[@"cycle"];
    liturgicalDate.even = info[@"even"];
    
    return liturgicalDate;
}

-(NSString*)cycleForDate:(NSDate*)date{
    NSString *cycle = @"B";
    NSInteger year = [date year];
    if (year > 2000) {
        for (int i = 2000; i <= year; i++) {
            if ([cycle isEqualToString:@"A"]) {
                cycle = @"B";
            }else if ([cycle isEqualToString:@"B"]) {
                cycle = @"C";
            }else{
                cycle =@"A";
            }
        }
    }else{
        for (int i = 2000; i >= year; i--) {
            if ([cycle isEqualToString:@"A"]) {
                cycle = @"C";
            }else if ([cycle isEqualToString:@"C"]) {
                cycle = @"B";
            }else{
                cycle =@"A";
            }
        }
    }
    
    return cycle;
}

@end

