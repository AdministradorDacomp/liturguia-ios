
import Foundation
import UIKit

struct ConfigurationEmptyDataSet {
    
    var isLoading = false
    
    var text = ""
    var detailText:String?
    var image:UIImage?
    
    init(text:String, detailText:String?, image:UIImage?) {
        self.text = text
        self.detailText = detailText
        self.image = image
    }
    
    var titleString: NSAttributedString? {
        let stringText = text
        let font = UIFont.init(name: "HelveticaNeue-Medium", size: 26) ?? UIFont.systemFont(ofSize: 26)
        let textColor =  UIColor.lightGray
 
        
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor : textColor
        ]
    
        return NSAttributedString.init(string: stringText, attributes: attributes)
    }
    
    
    var detailString: NSAttributedString? {
        guard let stringText = detailText else{ return nil}
        let font = UIFont.init(name: "HelveticaNeue-Medium", size: 16) ?? UIFont.systemFont(ofSize: 26)
        let textColor = UIColor.lightGray
        let attributes:[NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor : textColor
        ]

        return NSAttributedString.init(string: stringText, attributes: attributes)
    }
    
//    var image: UIImage? {
//        if isLoading {
//            return UIImage.init(named: "loading_imgBlue_78x78")
//        } else {
//            let imageNamed = ("placeholder_" + app.display_name!).lowercased().replacingOccurrences(of: " ", with: "_")
//            return #imageLiteral(resourceName: "logo")
//        }
//    }
    
    var imageAnimation: CAAnimation? {
        let animation = CABasicAnimation.init(keyPath: "transform")
        animation.fromValue = NSValue.init(caTransform3D: CATransform3DIdentity)
        animation.toValue = NSValue.init(caTransform3D: CATransform3DMakeRotation(.pi/2, 0.0, 0.0, 1.0))
        animation.duration = 0.25
        animation.isCumulative = true
        animation.repeatCount = MAXFLOAT
        
        return animation;
    }
    
    
    func buttonTitle(_ state: UIControl.State) -> NSAttributedString? {
//        var text: String?
//        var font: UIFont?
//        var textColor: UIColor?
//
//        switch app {
//        case .Airbnb:
//            text = "Start Browsing";
//            font = UIFont.boldSystemFont(ofSize: 16)
//            textColor = UIColor(hexColor: state == .normal ? "05adff" : "6bceff" )
//
//        case .Camera:
//            text = "Continue";
//            font = UIFont.boldSystemFont(ofSize: 17)
//            textColor = UIColor(hexColor: state == .normal ? "007ee5" : "48a1ea")
//
//        case .Dropbox:
//            text = "Learn more";
//            font = UIFont.systemFont(ofSize: 15)
//            textColor = UIColor(hexColor: state == .normal ? "007ee5" : "48a1ea")
//
//        case .Foursquare:
//            text = "Add friends to get started!";
//            font = UIFont.boldSystemFont(ofSize: 14)
//            textColor = UIColor(hexColor: state == .normal ? "00aeef" : "ffffff")
//
//        case .iCloud:
//            text = "Create New Stream";
//            font = UIFont.systemFont(ofSize: 14)
//            textColor = UIColor(hexColor: state == .normal ? "999999" : "ebebeb")
//
//        case .Kickstarter:
//            text = "Discover projects";
//            font = UIFont.boldSystemFont(ofSize: 14)
//            textColor = UIColor.white
//
//        case .WWDC:
//            text = "Sign In";
//            font = UIFont.systemFont(ofSize: 16)
//            textColor = UIColor(hexColor: state == .normal ? "fc6246" : "fdbbb2")
//
//        default:
//            break
//        }
//        if text == nil {
//            return nil
//        }
//        var attributes: [NSAttributedStringKey: Any] = [:]
//        if font != nil {
//            attributes[NSAttributedStringKey.font] = font!
//        }
//        if textColor != nil {
//            attributes[NSAttributedStringKey.foregroundColor] = textColor
//        }
//        return NSAttributedString.init(string: text!, attributes: attributes)
        return nil
    }
    
    func buttonBackgroundImage(_ state: UIControl.State) -> UIImage? {
//        var imageName = "button_background_\(app.display_name!)".lowercased()
//
//        if state == .normal {
//            imageName = imageName + "_normal"
//        }
//        if state == .highlighted {
//            imageName = imageName + "_highlight"
//        }
//
//        var capInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//        var rectInsets = UIEdgeInsets.zero
//
//        switch app {
//        case .Foursquare:
//            capInsets = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25)
//            rectInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//        case .iCloud:
//            rectInsets = UIEdgeInsets(top: -19, left: -61, bottom: -19, right: -61)
//        case .Kickstarter:
//            capInsets = UIEdgeInsets(top: 22, left: 22, bottom: 22, right: 22)
//            rectInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: -20)
//        default:
//            break;
//        }
//
//        let image = UIImage.init(named: imageName)
//
//        return image?.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
        return nil
    }
    
    var backgroundColor: UIColor? {

        return .clear
        
    }
    
//    var verticalOffset: CGFloat {
//        switch app  {
//        case .Kickstarter:
//            var offset = UIApplication.shared.statusBarFrame.height
//            offset += (controller.navigationController?.navigationBar.frame.height)!
//            return -offset
//        case .Twitter:
//            return -(CGFloat)(roundf(Float(controller.tableView.frame.height/2.5)))
//        default:
//            return 0
//        }
//    }
    
    var spaceHeight: CGFloat {
        return 24.0
        
    }
}
